/*
 * This file is part of SIMCOLEP, a simulation model developed by the Department of Public Health,
 * Erasmus MC, University Medical Center Rotterdam, Rotterdam, The Netherlands.
 *
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/
 * or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA
 *
 * You are free to:
 * Share � copy and redistribute the material in any medium or format
 * Under the following terms:
 * Attribution � You must give appropriate credit, provide a link to the license, and indicate if changes were
 * made. You may do so in any reasonable manner, but not in any way that suggests the licensor endorses
 * you or your use.
 * NonCommercial � You may not use the material for commercial purposes.
 * NoDerivatives � If you remix, transform, or build upon the material, you may not distribute the modified material.
 */

package simcolep.demo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;

import nl.mgz.simulation.discreteevent.Schedulable;
import simcolep.asgard.Odin;
import simcolep.exceptions.SimulationException;
import simcolep.fun2.Archimedes;
import simcolep.fun2.Function;
import simcolep.tools.PN;
import simcolep.tools.io.OutputElement;
import simcolep.tools.io.OutputList;
import simcolep.tools.io.Table;
import simcolep.tools.message.Message;
import simcolep.tools.message.Notifyable;
import simcolep.tools.message.Ratatosk;

/**
 * Cadastre is a register for levels
 *
 * @author ir. E.A.J. Fischer
 * @version 1.0
 */
public final class Cadastre implements ILevelRegister<Level>, Notifyable, Serializable {
	private static final String MESSAGE_EMPTYLEVEL = "empty level"; //$NON-NLS-1$

	/** serialVersionUID = 5232478146030274397L computed 15-11-2005 */
	private static final long serialVersionUID = 5232478146030274397L;

	/** number of categories */
	public static int catsize = 25;

	/** the message to which a normal Cadastre should listen */
	private static final String[] listensTo = { Message.ADDINHABITANT, Message.REMOVEINHABITANT };

	/***/
	private static final String LEVELSTRING = "Level ";

	/** store data for output */
	private boolean storeData = false;

	/** List of categories of Levels (that are stored in Lists) */
	protected ArrayList<Set<Level>> register = null;

	/** The upmost level */
	private Level toplevel = null;

	/** for output */
	private transient List<OutputList<Double>> out = null;

	/** Function that is used for weighting the levels */
	protected Function f = null;

	/** the number of levels of a certain size */
	private double[] sizes = new double[0];

	/** Contains the weight of each level-category */
	private double[] w = new double[0];

	/**
	 * contains the relative weight of a whole size-class (= w[x]*sizes[x]/sum
	 * of all w[z]*sizes[z])
	 */
	public double[] rW = new double[0];

	/**
	 * @param toprand
	 *            rank of highest level
	 */
	public Cadastre(int toprank) {

		// main register
		this.register = new ArrayList<Set<Level>>();

		// sizes
		this.sizes = new double[catsize];

		// output
		this.out = new ArrayList<OutputList<Double>>();

		// weights
		this.w = new double[catsize];

		this.toplevel = new Level(toprank);

		this.toplevel.setID(Odin.getOdin().getLevelId(toprank));

		// extra
		for (int x = 0; x < catsize; x++) {
			this.register.add(new LinkedHashSet<Level>());
			this.out.add(new OutputList<Double>(Integer.toString(x)));
		}

		this.setWeights(Archimedes.ask(PN.LEVELWEIGHT));

		// register to listen to those messages that changes Level sizes
		for (String s : listensTo) {
			Ratatosk.register(Message.formulate(s), this);
		}

	}

	/**
	 * Add a level to the register
	 *
	 * @param l
	 */
	@Override
	public void addLevel(Level l) {

		// get the size of the level
		int size = l.getHabitationSize();

		// size smaller than zero are not allowed
		if (size < 0)
			return;

		// if this size was not anticipated
		// add the elements that we need
		if (size + 1 > this.sizes.length) {
			double[] newS = new double[size + 1];
			double[] newW = new double[size + 1];
			for (int x = 0; x < this.sizes.length; x++) {
				newS[x] = this.sizes[x];

				if (this.f != null)
					newW[x] = this.f.getY(x);

			}

			this.sizes = newS;

			this.w = newW;
			// recalculate the relative weights
			setWeights(this.f);
			// add list to add this level size
			while (this.register.size() < size + 1) {
				// to the register
				this.register.add(new LinkedHashSet<Level>());

			}

		}
		// add to the right list
		this.register.get(size).add(l);

		// put in the right neighbourhood
		this.addToNeighbourhood(l);

		// update the sizes in simulation
		this.sizes[size] += 1;

	}

	/**
	 * @see simcolep.demo.Register#collectData(double)
	 */
	@Override
	@SuppressWarnings("unchecked")
	public void collectData(double time) {
		// if output has not been turned on don't bother
		if (!this.storeData)
			return;

		// enhance the list if needed
		int x = this.out.size() - 1;

		while (this.register.size() > this.out.size()) {
			x++;
			// determine the length of the outputlists
			int length = this.out.get(0).size();
			OutputList<Double> newout = new OutputList<Double>(LEVELSTRING + x);
			for (int j = 0; j < length; j++) {
				newout.add(new OutputElement<Double>(this.out.get(0).get(j).getTime(), new Double(0)));
			}
			this.out.add(newout);
		}

		for (int y = 0; y < this.register.size(); y++) {
			// create an element
			OutputElement<Double> e = new OutputElement<Double>(time, new Double(this.register.get(y).size()));
			out.get(y).add(e);
		}

	}

	/**
	 * Sets all value to the error code
	 *
	 * @param time
	 *            time interval to 'collect'
	 * @param as
	 *            number representing an error
	 * @param times
	 *            to collect
	 *
	 */
	@Override
	public void collectDataAtTimeAs(double time, double as) {
		// if output has not been turned on don't bother
		if (!this.storeData)
			return;

		collectData(time);

		for (int y = 0; y < this.register.size(); y++) {

			// create an element
			OutputElement<Double> e = new OutputElement<Double>(time, new Double(as));

			System.out.println(y + " " + e.getTime() + " " + e.getValue());
			out.get(y).add(e);

		}
	}

	/**
	 *
	 * @param level
	 * @return true if this level is in the register
	 */
	@Override
	public boolean contains(Level level) {
		// get the number of inhabitants of this level
		int x = level.getHabitationSize();
		// return whether the class contains this
		return register.get(x).contains(level);
	}

	/**
	 * Get a level randomly selected from this Cadastre. The weighting of levels
	 * (by size) is predefined by the function f of this cadastre<br>
	 *
	 * @param rand
	 *            - random number generator
	 * @return a
	 *
	 *
	 */
	@Override
	public Level getLevel(Random rand) {

		if (this.f == null)
			throw new SimulationException(
					"setSizesAndWeights has not yet been called for this Cadastre. Weighting function f == null");
		// determine which level size
		double cum = 0;
		int x = 0;
		double random = rand.nextDouble();

		for (; x < this.register.size(); x++) {
			// get the cumulative weighted fraction
			cum += this.rW[x];
			// break if the cumulative weighting is higher than the random
			// number
			if (cum >= random)
				break;
		}
		// if none of the class got a value
		if (cum == 0.0)
			return null;

		// determine the random index of the size x
		List<Level> temp = new ArrayList<Level>();
		Set<Level> cat1 = this.register.get(x);

		temp.addAll(cat1);
		// return the selected level
		Level selected = temp.get(rand.nextInt(cat1.size()));
		// don't allow empty level
		if (selected.getInhabitants().isEmpty())
			throw new SimulationException(MESSAGE_EMPTYLEVEL);

		return selected;

	}

	/**
	 * Set f to a new function
	 *
	 * @param rand
	 * @param f
	 * @see simcolep.demo.ILevelRegister#getLevel(Random, Function)
	 */
	@Override
	public Level getLevel(Random rand, Function f) {
		this.setWeights(f);
		return this.getLevel(rand);
	}

	/**
	 * @return register
	 * @see simcolep.demo.ILevelRegister#getLevels()
	 */
	@Override
	public List<Set<Level>> getLevels() {

		return this.register;
	}

	/**
	 *
	 * @param size
	 * @return set with levels of size
	 */
	@Override
	public Set<Level> getLevels(int size) {

		if (size >= this.register.size())
			return new LinkedHashSet<Level>();
		return this.register.get(size);
	}

	/**
	 * @return this.register A linkedlist with linkedhashsets of each level size
	 *         (size is index in list)
	 * @see simcolep.demo.Register#getRegister()
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Iterable getRegister() {
		return this.register;
	}

	/**
	 * @see simcolep.demo.ILevelRegister#getSize()
	 */
	@Override
	public int getSize() {
		int size = 0;
		for (double d : this.sizes) {
			size += d;
		}
		return size;
	}

	/**
	 *
	 * @see simcolep.demo.ILevelRegister#getToplevel()
	 */
	@Override
	public Level getToplevel() {

		return this.toplevel;
	}

	/**
	 * Notification of changes in the register. listensTo = {"ai","ri"}
	 *
	 * @param message
	 * @param subject
	 * @param source
	 * @throws IllegalArgumentException
	 *             if this is a database cadastre
	 */
	@Override
	public void notify(Message message, Object subject, Object source) {

		// add inhabitant
		if (message.equalsIgnoreCase(Message.ADDINHABITANT)) {
			// change of the size of a level is called by a level
			Level l = (Level) source;

			if (l.rank != Level.DWELLING_RANK)
				throw new SimulationException("not a dwelling!");

			int size = l.getHabitationSize();
			// remove from previous sizes list in this case an inhabitant was
			// added
			if (!this.register.get(size - 1).remove(l))
				throw new SimulationException(
						"Register of size class " + (l.getHabitationSize() - 1) + " does not contain " + source);
			// update the sizes
			this.sizes[size - 1]--;
			// if neccesary increase the number of size categories
			if (this.sizes.length <= size) {
				double[] newS = new double[size + 1];
				double[] newW = new double[size + 1];
				for (int x = 0; x < this.sizes.length; x++) {
					newS[x] = this.sizes[x];

					if (this.w[x] == 0 && this.f != null)
						newW[x] = this.f.getY(x);
					else
						newW[x] = this.w[x];
				}

				this.sizes = newS;

				this.w = newW;
				// recalculate the relative weights
				setWeights(this.f);
				// add list to add this level size
				while (this.register.size() < size + 1) {
					this.register.add(new LinkedHashSet<Level>());
				}
			}
			this.sizes[size]++;
			// add to the right sizes list (if not present create)
			while (this.register.size() < size + 1)
				this.register.add(new LinkedHashSet<Level>());

			this.register.get(size).add(l);

		}
		if (message.equalsIgnoreCase(Message.REMOVEINHABITANT)) {
			// change of the size of a level is called by a level
			Level l = (Level) source;

			if (l.rank != Level.DWELLING_RANK)
				throw new SimulationException("not a dwelling!");

			// size of the level
			int size = l.getHabitationSize();

			// remove from previous sizes list in this case an inhabitant was
			// removed
			if (!this.register.get(size + 1).remove(l))
				throw new SimulationException(
						"Register of size class " + (l.getHabitationSize() + 1) + " does not contain " + source);

			// update the sizes
			this.sizes[size + 1]--;

			// there are no empty levels
			this.sizes[size]++;

			// if the level became empty delete
			// don't add it any more
			if (size == 0) {
				// remove from its superlevels
				if (l.getSuperlevel() != null)
					l.getSuperlevel().removeMember(l);

				// remove from register
				this.removeLevel(l);
				// make null
				l = null;

				return;
			}
			// add to the right sizes list
			else
				this.register.get(size).add(l);
		}

	}

	/**
	 * @see simcolep.demo.ILevelRegister#output()
	 */
	@Override
	public List<OutputList<Double>> output() {
		return this.out;
	}

	/**
	 * remove a level from register
	 *
	 * @param l
	 * @return true if the level was removed or not added
	 */
	@Override
	public boolean removeLevel(Level l) {

		// remove from the right list
		boolean b = this.register.get(l.getHabitationSize()).remove(l);

		// remove events
		List<Schedulable> temp = new ArrayList<Schedulable>(l.getTargetedEvents());
		for (Schedulable s : temp) {
			Odin.getEventManager().cancel(s);
		}

		// update sizes in simulation
		this.sizes[l.getHabitationSize()] -= 1;

		// return b
		return b;
	}

	/**
	 * Checks every level whether the method getInhabitants returns an empty
	 * list. If so it removes this level.
	 *
	 *
	 */
	public void scrutinize() {
		List<Level> scrutinized = new ArrayList<Level>();
		// get empty levels
		for (Set<Level> s : this.register) {
			for (Level l : s) {
				// empty levels can be added to the scrutinizing list (removing
				// it)
				if (l.getInhabitants().isEmpty())
					scrutinized.add(l);
			}
		}
		// remove them
		for (Level l : scrutinized) {
			if (!this.removeLevel(l))
				throw new SimulationException("empty level not removed");
		}
	}

	/**
	 * Calculates and stores the (relative) weight of this whole group of levels
	 * and the relative weight of a whole size class.
	 *
	 * @param f
	 */
	public void setWeights(Function f) {
		// a function that is null does not help a lot
		if (f == null)
			return;

		// set the function
		this.f = f;

		// product sum of all weights*fraction/number of a certain size
		double prodsum = 0;
		this.rW = new double[this.sizes.length];

		for (int x = 0; x < this.sizes.length; x++) {
			// calculate weight
			this.w[x] = this.f.getY(x);

			// calculate the product of the sizes times the weights
			rW[x] = this.sizes[x] * this.w[x];

			// add the product to the product sum;
			prodsum += rW[x];
		}

		// divided the weights by the product sum
		// if the product sum equals 0 each level will have the same probability
		// = 1/size of cadastre
		if (prodsum > 0) {
			int index = 0;
			for (double d : rW) {
				rW[index] = d / prodsum;
				index++;
			}
		}

	}

	/**
	 * @see simcolep.demo.Register#storeData(boolean)
	 */
	@Override
	public void storeData(boolean b) {
		this.storeData = b;
	}

	/**
	 *
	 * @return s
	 */
	public String toFullString() {
		StringBuffer s = new StringBuffer();
		for (Set<Level> set : this.register) {
			for (Level l : set) {
				s.append(l.toString() + PN.LINESEPARATOR);
			}
		}
		return s.toString();
	}

	/**
	 *
	 * @return a part of the full string
	 */
	public String toPartString(int part) {
		StringBuffer s = new StringBuffer();
		int x = 0;
		for (Set<Level> set : this.register) {
			Iterator<Level> it = set.iterator();
			while (it.hasNext() && x < part) {
				s.append(it.next().toString() + PN.LINESEPARATOR);
				x++;
			}
			if (it.hasNext())
				s.append(PN.THREEDOTS + PN.LINESEPARATOR);
			x = 0;
		}
		return s.toString();
	}

	/**
	 *
	 * @see simcolep.demo.ILevelRegister#toTable()
	 */
	@Override
	public Table toTable() {

		// if no lists return
		if (out.size() <= 0)
			return null;

		// get size
		int rows = out.get(0).size();

		// set table
		Table table = new Table(rows, out.size());

		// set row names
		for (int index = 0; index < rows; index++) {
			table.setRowName(index, Double.toString(out.get(0).get(index).getTime()));
		}

		// add data
		for (int index = 0; index < out.size(); index++) {
			// list
			OutputList<Double> ol = out.get(index);
			// set column name
			table.setColumnName(index, ol.getName());

			// set values
			table.setColumn(index, ol);
		}
		// return table
		return table;
	}

	/**
	 * Sets this Cadastre to be used or not used as data base. When it is used a
	 * database adding and removing levels is not allowed. The Cadastre will not
	 * listen to Ratatosk. This Cadastre implementation considers that:
	 * <li>It is used in a full run <i>or</i>;
	 * <li>It is used as a database from which new levels can be drawn<br>
	 *
	 * In both cases, it would just return levels from the lists as requested.
	 * However when it is used as a data base, the Object that instantiates the
	 * cadastre should make sure that:
	 * <li>No events are targeted at levels <i>and/or</i> inhabitants of the
	 * levels
	 * <li>The database-cadastre is <b>NOT</b> registered at Ratatosk!
	 *
	 * @param b
	 */
	public void useAsDataBase(boolean b) {
		// if used as database
		if (b) {
			// check consistency of the cadastre
			this.scrutinize();

			// unregister to listen to anything
			for (String s : listensTo) {
				Ratatosk.unregister(Message.formulate(s), this);
			}
		}

	}

	/**
	 * now the level is just added to the toplevel
	 *
	 * @param l
	 */
	private void addToNeighbourhood(Level l) {
		this.toplevel.addMember(l);

		l.setSuperlevel(this.toplevel);
	}

}
