/*
 * This file is part of SIMCOLEP, a simulation model developed by the Department of Public Health,
 * Erasmus MC, University Medical Center Rotterdam, Rotterdam, The Netherlands.
 *
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/
 * or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA
 *
 * You are free to:
 * Share � copy and redistribute the material in any medium or format
 * Under the following terms:
 * Attribution � You must give appropriate credit, provide a link to the license, and indicate if changes were
 * made. You may do so in any reasonable manner, but not in any way that suggests the licensor endorses
 * you or your use.
 * NonCommercial � You may not use the material for commercial purposes.
 * NoDerivatives � If you remix, transform, or build upon the material, you may not distribute the modified material.
 */

/*
 * Created on 19-Oct-2005
 *
 */
package simcolep.demo;

import nl.mgz.simulation.discreteevent.Target;

/**
 *
 * Implementing this interface makes it possible to become a in habitant of a
 * class implementing Levelable
 *
 *
 * @author ir E.A.J. Fischer
 * @version 0.1.2 -- Last Modified 19-Oct-2005
 *
 */
public abstract class Inhabitant extends Target {

	/**
	 * 
	 * @return id
	 */
	public abstract int getID();

	/**
	 * @return the super-level of this member (null when highest ranking
	 *         levelable = super level not set)
	 */
	public abstract ILevel getLevel();

	/**
	 * Remove the level from this inhabitant
	 * 
	 * @param level
	 * 
	 */
	public abstract void removeLevel(ILevel level);

	/**
	 * 
	 * @param id
	 */
	public abstract void setID(int id);

	/**
	 * Set's this members superlevel.
	 * 
	 * @param level
	 */
	public abstract void setLevel(ILevel level);

}
