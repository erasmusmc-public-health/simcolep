/*
 * This file is part of SIMCOLEP, a simulation model developed by the Department of Public Health,
 * Erasmus MC, University Medical Center Rotterdam, Rotterdam, The Netherlands.
 *
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/
 * or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA
 *
 * You are free to:
 * Share � copy and redistribute the material in any medium or format
 * Under the following terms:
 * Attribution � You must give appropriate credit, provide a link to the license, and indicate if changes were
 * made. You may do so in any reasonable manner, but not in any way that suggests the licensor endorses
 * you or your use.
 * NonCommercial � You may not use the material for commercial purposes.
 * NoDerivatives � If you remix, transform, or build upon the material, you may not distribute the modified material.
 */

/*
 * Created on 04-Nov-2005
 *
 */
package simcolep.demo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import nl.mgz.simulation.discreteevent.Schedulable;
import simcolep.asgard.Idunn;
import simcolep.asgard.Odin;
import simcolep.demo.event.Age;
import simcolep.exceptions.SimulationException;
import simcolep.fun2.Archimedes;
import simcolep.lepra.Morbus;
import simcolep.tools.Historizable;
import simcolep.tools.History;
import simcolep.tools.Identifyable;
import simcolep.tools.PN;
import simcolep.tools.message.Message;
import simcolep.tools.message.Ratatosk;

/**
 *
 * Person is the implementation of a human that has a level to which it belongs,
 * has a birthdate and contains a list of disease objects.<br>
 * Person can be parameterized <code>&ltM&&gt </code>with the type of disease
 * object.
 *
 * *
 *
 * @author ir E.A.J. Fischer
 * @version 0.1.2 -- Last Modified 04-Nov-2005
 */
public abstract class Person extends Inhabitant implements Serializable, Identifyable, Historizable {

	/** serial number */
	private static final long serialVersionUID = 6869321238942826115L;

	public static final int UNRELATED = 0;
	public static final int SPOUSE = 1;
	public static final int CHILD = 2;
	public static final int PARENT = 3;
	public static final int SIBLING = 4;
	public static final int RELATIONS = 5;

	public static final String DATABASEHEADING = "ID*sep*BDATE*sep*AGECLASS*sep*SEX*sep*LEVELID*sep*LEVELSIZE*sep*MOVES*sep*MOVEAGE*sep*MARRIED*sep*EVERMARRIED*sep*CHILDREN*sep*SPOUSE*sep*SPOUSEBDATE*sep*";

	/** saving history for debugging purposes */
	public static boolean saveHistory = false;

	@SuppressWarnings("unchecked")
	public static Person strip(Person p) {
		/** identification number */
		p.id = Integer.MIN_VALUE;
		/** level */
		p.level = null;
		/** Container containing disease object */
		p.morbus = new Morbus[Archimedes.askInteger(PN.L)];
		/** counter for the number of levels that were set for this person */
		p.life_time_level = 0;
		/** birth date */
		p.bdate = Double.POSITIVE_INFINITY;
		/** age of death */
		p.deathAge = Double.POSITIVE_INFINITY;
		/** current age class */
		p.ageclass = 0;
		/** age of movement within the population */
		p.moveage = Double.POSITIVE_INFINITY;
		/** age of outmigration */
		p.migrationage = Double.POSITIVE_INFINITY;
		/** marrital state true if married */
		p.married = false;
		/** true if ever married */
		p.evermarried = false;
		/** engaged means a wedding event is targeted at this person */
		p.engaged = false;
		/** mother of this person */
		p.mother = null;

		p.father = null;
		/** children of this person */
		p.children = null;

		/** moves when becoming a widow(er) */
		p.movesaswidow = false;
		/** time that the spouse dies */
		p.spouseDeathTime = Double.POSITIVE_INFINITY;

		/** survivor of a marital couple */
		p.survivor = false;
		p.siblings = null;
		/** the chosen child */
		p.theChosen = null;

		/** the surviving Parent */
		p.survivingParent = null;

		/** chosen by a parent to move to when the spouse dies */
		p.chosen = false;

		/** time of deactivation */
		p.deactivated = Double.NEGATIVE_INFINITY;

		/** removal and adding boolean for debugging */
		p.removed_single = false;
		//
		p.add_single = false;

		/** mark for debugging purposes */
		p.mark = null;

		/**
		 * template ID is the identification number of a person from which this
		 * person was created Used when activating a morbus to know what
		 * transmission time is directed at what person
		 */
		p.templateID = Integer.MIN_VALUE;

		/** History of this person */
		p.history = null;

		Female.stripFemale(p);
		Male.stripMale(p);

		return p;
	}

	/** identification number */
	protected int id = Integer.MIN_VALUE;

	/** level */
	protected Level level = null;

	/** Container containing disease object */
	protected Morbus[] morbus = null;

	/** counter for the number of levels that were set for this person */
	public int life_time_level = 0;

	/** birth date */
	protected double bdate = Double.POSITIVE_INFINITY;

	/** age of death */
	protected double deathAge = Double.POSITIVE_INFINITY;

	/** current age class */
	protected int ageclass = 0;

	/** age of movement within the population */
	protected double moveage = Double.POSITIVE_INFINITY;

	/** age of outmigration */
	protected double migrationage = Double.POSITIVE_INFINITY;

	/** marrital state true if married */
	protected boolean married = false;

	/** true if ever married */
	protected boolean evermarried = false;

	/** engaged means a wedding event is targeted at this person */
	public boolean engaged = false;

	/** mother of this person */
	private transient Female mother = null;

	/** father of this person */
	private transient Male father = null;

	/** children of this person */
	protected List<Person> children = null;

	/** moves when becoming a widow(er) */
	protected boolean movesaswidow = false;

	/** time that the spouse dies */
	public double spouseDeathTime = Double.POSITIVE_INFINITY;

	/** survivor of a marital couple */
	public boolean survivor = false;

	/** the chosen child */
	private Person theChosen = null;

	/** the surviving Parent */
	private Person survivingParent = null;

	/** chosen by a parent to move to when the spouse dies */
	public boolean chosen = false;

	/** time of deactivation */
	public double deactivated = Double.NEGATIVE_INFINITY;

	/** removal and adding boolean for debugging */
	public boolean removed_single = false;

	//
	public boolean add_single = false;

	/** mark for debugging purposes */
	@SuppressWarnings("unused")
	private String mark = null;

	/**
	 * template ID is the identification number of a person from which this
	 * person was created Used when activating a morbus to know what
	 * transmission time is directed at what person
	 */
	public int templateID = Integer.MIN_VALUE;

	/** History of this person */
	private History history = null;

	private Set<Person> siblings = null;

	/**
	 * empty constructor
	 *
	 */
	@SuppressWarnings("unchecked")
	protected Person() {
		super();

		this.morbus = new Morbus[Archimedes.askInteger(PN.L)];
	}

	/**
	 * This person will be activated with the birth date and all ages and times
	 * as currently set instance variables Method should be used to initiate a
	 * virgin population from a set of distributions for age, sex etc.
	 *
	 */
	public void activate() {

		// regulate movement at Loki
		Odin.getOdin().getLoki().schedule(this);

		Idunn.getIdunn().addedToLevel(this);
		// check if this Person contains unscheduled events
		// make a list that can be iterate without
		// java.util.ConcurrentModificationException
		List<Schedulable> temp = new ArrayList<Schedulable>(this.getTargetedEvents());
		// schedule them

		for (Schedulable s : temp) {
			Odin.getEventManager().cancel(s);

			Odin.getEventManager().schedule(s);
		}

		// check if the aging events are present (if not schedule)
		if (!this.isTargetOf(Age.class.getName())) {
			// calculate the time at which this person is going to the next age
			// class
			double ageingtime = Age.calcAgeTime(AbstractPopulationRegister.ageclasswidth,
					this.getAge(Odin.getCurrentTime()), Odin.getCurrentTime());
			// schedule aging event
			Age age = new Age(this, this, ageingtime);
			if (age.getTime() < this.getDeathTime()) {
				Odin.getEventManager().schedule(age);
			}
		}
		//
		if (Person.saveHistory)
			this.addtoHistory(Odin.getCurrentTime(), "activation");

		// give a call that this person is 'born'
		Ratatosk.notify(Message.formulate(Message.BIRTH), this, this);

	}

	/**
	 * Add a child to this person
	 *
	 * @param p
	 * @return true if the list of children has changed due to the adding of
	 *         this person
	 */
	public boolean addChild(Person p) {
		// new list if not created before
		if (this.children == null)
			this.children = new ArrayList<Person>();

		//
		this.addtoHistory(Odin.getCurrentTime(), "child added: ", p);

		// add this person as child
		return this.children.add(p);
	}

	/**
	 *
	 * @param p
	 */
	public void addSibling(Person p) {
		if (this.siblings == null)
			this.siblings = new LinkedHashSet<Person>();

		this.siblings.add(p);
	}

	/**
	 * Add a person as a spouse to this person
	 *
	 * @param person
	 */
	public abstract void addSpouse(Person person);

	/**
	 * @see simcolep.tools.Historizable#addtoHistory(double, java.lang.String)
	 */
	@Override
	public void addtoHistory(double time, String s) {

		if (!Person.saveHistory)
			return;

		if (this.history == null)
			this.history = new History();

		this.history.add(time, s);

	}

	/**
	 * Add to the history of this person
	 *
	 * @param s
	 */
	@Override
	public void addtoHistory(double time, String s, Object subject) {

		if (!saveHistory)
			return;

		if (subject == null) {
			this.addtoHistory(time, s);
			return;
		}
		if (this.history == null)
			this.history = new History();

		this.history.add(time, s + " " + subject.toString());

	}

	/**
	 * Sets the disease object to null and calls the method braindead of the
	 * disease object
	 *
	 */
	public void braindead() {

		// make the morbus absolutely dead
		for (Morbus m : this.morbus)
			m.braindead();

		// set them to null
		this.morbus = null;

		this.addtoHistory(Odin.getCurrentTime(), "braindeath");

		Odin.getOdin().getHel().enterHel(this);
	}

	/**
	 * "ID BDATE AGECLASS SEX LEVELID LEVELSIZE MOVES MOVEAGE MARRIED
	 * EVERMARRIED CHILDREN ";
	 *
	 * @return person
	 */
	public String databaseEntry(String sepsign) {
		StringBuffer p = new StringBuffer();
		p.append(this.getID() + sepsign);
		p.append(this.getBdate() + sepsign);
		p.append(this.ageclass + sepsign);
		p.append(this.getSex() + sepsign);
		if (this.level != null) {
			p.append(this.getLevel().id + sepsign);
			p.append(this.getLevel().getHabitationSize() + sepsign);
		} else {
			p.append("NA" + sepsign);
			p.append("NA" + sepsign);
		}
		p.append(Math.max(0, this.life_time_level - 1) + sepsign);
		p.append(this.moveage + sepsign);
		p.append(this.married + sepsign);
		p.append(this.evermarried + sepsign);
		p.append(this.getChildren().size() + sepsign);
		if (this.getSpouse() != null) {
			p.append(this.getSpouse().getID() + sepsign);
			p.append(this.getSpouse().getBdate() + sepsign);
		} else {
			p.append("NA" + sepsign);
			p.append("NA" + sepsign);
		}
		String person = p.toString();
		return person;
	}

	/**
	 * Let this person die
	 * <li>cancel all events
	 * <li>remove links to : <br>
	 * spouse <br>
	 * mother <br>
	 * child(ren) <br>
	 * the chosen child <br>
	 * the surviving parent
	 *
	 * <li>remove from level
	 * <li>Let the disease object die
	 * <li>Notify the simulation through Ratatosk
	 */
	@SuppressWarnings("unchecked")
	public void die() {

		// cancel all events targeted at this person
		while (!this.getTargetedEvents().isEmpty()) {
			Schedulable s = this.getTargetedEvents().get(0);
			if (s.isScheduled())
				Odin.getEventManager().cancel(s);
		}

		// notify the rest of the simulation program
		Ratatosk.notify(Message.formulate(Message.DEATH), this, this);

		// remove from parents
		if (this.mother != null) {
			this.mother.removeChild(this);
		}
		if (this.father != null)
			this.father.removeChild(this);
		if (this.siblings != null)
			for (Person p : this.siblings) {
				p.removeSibling(this);
			}
		// and remove mother
		this.removeMother();

		// remove when still part of this linking
		// if married
		Person spouse = null;
		if (this.getSpouse() != null) {
			spouse = this.getSpouse();
			spouse.removeSpouse();
			this.removeSpouse();
		}

		// remove the link between this person and the child chosen for move
		// after becoming widow(er)
		if (this.theChosen != null)
			this.theChosen.setsurvivingParent(null);
		this.settheChosen(null);

		// remove the link between the surviving parent that will move to this
		// person
		if (this.survivingParent != null)
			this.survivingParent.settheChosen(null);

		this.setsurvivingParent(null);

		// cancel all events of the morbus ("let the disease die")
		for (Morbus m : this.morbus)
			m.die();

		// remove from level
		this.level.removeInhabitant(this);

		this.addtoHistory(Odin.getCurrentTime(), "death");
		// check
		if (Odin.inSafeMode()) {

			if (Odin.getOdin().getPopulationRegister().contains(this))
				throw new SimulationException("This Person should be DEAD and removed!!!");
		}
		//
		if (spouse != null) {
			// set unmarried
			spouse.setMarried(false);

			// notify
			Ratatosk.notify(Message.formulate(Message.SPOUSEDEATH), spouse, this);

		}

	}

	/**
	 *
	 * @param time
	 * @return time - this.bdate
	 */
	public double getAge(double time) {
		return time - this.bdate;
	}

	/**
	 *
	 *
	 * @return the age class of this person
	 */
	public int getAgeclass() {
		return this.ageclass;
	}

	/**
	 *
	 * @return bdate
	 */
	public double getBdate() {
		return this.bdate;
	}

	/**
	 * Get the list with the children of this person
	 * <li>in safe mode: after removing dead children if present
	 *
	 *
	 * @return children
	 */
	public List<Person> getChildren() {

		// update this list
		if (this.children == null)
			this.children = new ArrayList<Person>();

		// in safe mode check for dead children
		if (Odin.inSafeMode()) {
			Collection<Person> dead = new LinkedHashSet<Person>();
			for (Person p : this.children) {
				if (p.getBdate() + p.getDeathAge() < Odin.getCurrentTime())
					dead.add(p);
			}

			this.children.removeAll(dead);
		}

		// return the children
		return this.children;
	}

	/**
	 *
	 * @return age of death
	 */
	public double getDeathAge() {
		return this.deathAge;
	}

	/**
	 *
	 * @return birthdate + death age
	 */
	public double getDeathTime() {

		return this.bdate + this.deathAge;
	}

	/**
	 * @see simcolep.demo.Inhabitantable#getID()
	 */
	@Override
	public int getID() {
		return this.id;
	}

	/**
	 * @see simcolep.demo.Inhabitantable#getLevel()
	 */
	@Override
	public Level getLevel() {
		return this.level;
	}

	/**
	 * At what age will this person migrate
	 *
	 * @return migrationage
	 */
	public double getMigrationAge() {

		return this.migrationage;
	}

	/**
	 *
	 * @return
	 * @return
	 */
	public Morbus[] getMorbi() {

		return this.morbus;
	}

	/**
	 * Disease object getter
	 *
	 * @return the disease object
	 */
	public Morbus getMorbus(int index) {

		return this.morbus[index];
	}

	/**
	 *
	 * @return mother
	 */
	public Female getMother() {

		return this.mother;
	}

	/**
	 * The age of random movement
	 *
	 * @return moveage age of 'random' movement (i.e. not related to marriage,
	 *         death of spouse or any other mechanism)
	 */
	public double getMoveAge() {
		return this.moveage;
	}

	/**
	 *
	 * @param owner
	 * @return relation
	 */
	public int getRelationship(Person person) {
		// child
		if (this.children != null) {
			if (this.children.contains(person)) {
				return Person.CHILD;
			}
		}
		// mother
		if (this.mother != null) {
			if (this.mother.equals(person)) {
				return Person.PARENT;
			}
		} // father
		else if (this.father != null) {
			if (this.father.equals(person)) {
				return Person.PARENT;
			}
		}
		// sibling
		if (this.siblings != null) {
			if (this.siblings.contains(person))
				return SIBLING;
		}

		// spouse
		if (this.getSpouse() != null)
			if (this.getSpouse().equals(person))
				return Person.SPOUSE;

		return Person.UNRELATED;
	}

	/**
	 * Get the sex of this person (integer value coded in static variables MALE
	 * and FEMALE).
	 *
	 * @return Person.MALE or Person.FEMALE
	 */
	public abstract Sex getSex();

	/**
	 * Get the spouse of this person
	 *
	 * @return spouse
	 */
	public abstract Person getSpouse();

	/**
	 * The time that the spouse of this person will die
	 *
	 * @return spouseDeathTime
	 */
	public double getSpouseDeathTime() {

		return this.spouseDeathTime;
	}

	/**
	 * Get the parent that will survive his/her spouse and move to this person
	 *
	 * @return the surviving Parent
	 */
	public Person getsurvivingParent() {
		return this.survivingParent;
	}

	/**
	 *
	 * @return new Integer(this.templateID)
	 */
	public Integer getTemplateId() {
		return new Integer(this.templateID);
	}

	/**
	 * Get the child that this person will move to when the spouse dies
	 *
	 * @return the chosen child
	 */
	public Person gettheChosen() {
		return this.theChosen;
	}

	/**
	 * The parent that becomes widow(er) and will move has 'chosen' this child
	 *
	 * @return true if this is the chosen child
	 */
	public boolean isChosenByParent() {

		return this.chosen;
	}

	/**
	 * check if this person is ever been married
	 *
	 * @return this.evermarried
	 */
	public boolean isevermarried() {

		return this.evermarried;
	}

	/**
	 * Is this person married or engaged?
	 *
	 * @return true if married and spouse is alive (i.e. this person is target
	 *         of spouse death event) or engaged
	 */
	public boolean ismarried() {
		// engaged is married...
		if (this.engaged)
			return true;

		else // and married is married
			return this.married;

	}

	/**
	 * This person is set or not to move level when her/his spouse dies.
	 *
	 * @return true if she/he will move
	 */
	public boolean movesAsWidow() {
		return this.movesaswidow;
	}

	/**
	 * Set the movesaswidow boolean to b
	 *
	 * @param b
	 */
	public void movesAsWidow(boolean b) {
		this.movesaswidow = b;
	}

	/**
	 * Remove a child from this person
	 *
	 * @param p
	 * @return true if the list of children has changed by removal of this
	 *         person
	 */
	public boolean removeChild(Person p) {
		if (children != null) {
			this.addtoHistory(Odin.getCurrentTime(), "child removed", p);

			return this.children.remove(p);
		}
		return false;
	}

	/**
	 * this.mother = null
	 *
	 */
	public void removeFather() {
		this.father = null;

		this.addtoHistory(Odin.getCurrentTime(), "mother removed  ");
	}

	/**
	 * @see simcolep.demo.Inhabitantable#removeLevel(simcolep.demo.Level)
	 */
	@Override
	public void removeLevel(ILevel level) {
		if (this.level.equals(level))
			this.level = null;
		else
			throw new SimulationException("Person is not an inhabitant of this level");

		this.addtoHistory(Odin.getCurrentTime(), "level removed: ", level);
	}

	/**
	 * this.mother = null
	 *
	 */
	public void removeMother() {
		this.mother = null;

		this.addtoHistory(Odin.getCurrentTime(), "mother removed  ");
	}

	/**
	 *
	 * @param p
	 */
	public void removeSibling(Person p) {
		this.siblings.remove(p);
	}

	/**
	 * remove spouse from this person
	 *
	 */
	public abstract void removeSpouse();

	/**
	 *
	 * @param ac
	 */
	public void setAgeclass(int ac) {
		this.ageclass = ac;
	}

	/**
	 * Sets the birth date of this person
	 *
	 * @param bdate
	 */
	public void setBdate(double bdate) {
		this.bdate = bdate;
	}

	/**
	 * The parent that becomes widow(er) and will move has 'chosen' this child
	 *
	 * @param b
	 *            - true if chosen / false if not chosen anymore
	 */
	public void setChosenByParent(boolean b) {
		this.addtoHistory(Odin.getCurrentTime(), "set to be chosen = " + b);
		this.chosen = b;
	}

	/**
	 *
	 * @param age
	 */
	public void setDeathAge(double age) {
		this.deathAge = age;
	}

	/**
	 * @see simcolep.demo.Inhabitantable#setID(int)
	 */
	@Override
	public void setID(int id) {
		this.id = id;

		if (this.templateID < 0)
			this.templateID = id;
	}

	/**
	 *
	 * @see simcolep.demo.Inhabitant#setLevel(simcolep.demo.Level)
	 */
	@Override
	public void setLevel(ILevel level) {
		if (level == null) {
			this.level = null;
			return;
		}

		if (this.deactivated > 0)
			throw new SimulationException("you don't want to add" + " a deactivated person to anything");

		if (this.level != null)
			throw new SimulationException("already member of a Level");

		// each time the level is set add one
		this.life_time_level++;

		if (Person.saveHistory)
			this.addtoHistory("level set to ", (Level) level);
		// set the level
		this.level = (Level) level;

	}

	/**
	 *
	 * @param mark
	 */
	public void setMark(String mark) {
		this.mark = mark;
	}

	/**
	 * Set married boolean true or false
	 *
	 * @param m
	 */
	public void setMarried(boolean m) {
		this.married = m;

		this.addtoHistory(Odin.getCurrentTime(), "set married = ", m);
	}

	/**
	 * Set the age at which this person will migrate
	 *
	 * @param age
	 */
	public void setMigrationAge(double age) {
		this.migrationage = age;

	}

	/**
	 * Add a disease object to this Person at the specified index
	 *
	 * @param m
	 * @param index
	 */
	public void setMorbus(Morbus m, int index) {
		// set the disease object to null
		if (m == null) {
			this.morbus[index] = null;
			return;
		}
		// if a disease object was already attached to this person throw
		// IllegalArgumentException!
		if (this.morbus[index] != null)
			throw new SimulationException("Person already has a disease attribute!");
		// add the disease object to this person
		this.morbus[index] = m;

		// set this person to be the owner of the disease object
		this.morbus[index].setOwner(this);

		if (m.getLayer() != index)
			throw new IllegalArgumentException("hey this is wrong");
		// historise
		if (Person.saveHistory)
			this.addtoHistory(Odin.getCurrentTime(), "morbus set for layer " + index);
	}

	/**
	 * Set the age at which this person is going to change level within the
	 * population
	 *
	 * @param d
	 */
	public void setMoveAge(double d) {
		this.moveage = d;
		this.addtoHistory(Odin.getCurrentTime(), "move age set to " + d);
	}

	/**
	 *
	 * @param f
	 */
	public void setParents(Male m, Female f) {
		this.mother = f;
		this.father = m;

		this.addtoHistory(Odin.getCurrentTime(), "mother =  ", f);
	}

	/**
	 * Set the time that the spouse of this person dies
	 *
	 * @param t
	 */
	public void setSpouseDeathTime(double t) {
		this.spouseDeathTime = t;
		// check if an child was already chosen for a previous marriage or so
		if (theChosen != null) {
			if (theChosen.getDeathTime() < t) {
				theChosen.setChosenByParent(false);
				theChosen = null;
			}
		}
		this.survivor = true;

	}

	/**
	 * Set the parent that will survive his/her spouse and will move to the
	 * level of this person
	 *
	 * @param parent
	 */
	public void setsurvivingParent(Person parent) {
		// if parent is null remove and set false
		if (parent == null) {
			// set to null
			this.survivingParent = null;
			// set chosen = false
			setChosenByParent(false);
			// return
			return;
		}

		// if this parent does not move return
		if (!parent.movesaswidow)
			return;

		// else set the surviving parent
		this.survivingParent = parent;

		// add to personal history that a surviving parent is set
		this.addtoHistory(Odin.getCurrentTime(), "surviving parent set");
	}

	/**
	 * Set the child to whom this person will move when the spouse dies
	 *
	 * @param child
	 */
	public void settheChosen(Person child) {

		// when this person does not move as a widow(er) return without setting
		// a child
		if (!this.movesaswidow) {
			this.theChosen = null;
			return;
		}

		// if the current chosen != null but child is
		if (child == null)
			if (this.theChosen != null)
				this.theChosen.setsurvivingParent(null);

		// set the Chosen to be this child
		this.theChosen = child;

		// add to the history
		this.addtoHistory(Odin.getCurrentTime(), "Chosen child set: ", this.theChosen);
	}

	/**
	 *

	 */
	public String toExtendedString() {
		// creat the string
		StringBuffer p = new StringBuffer(this.id + PN.TAB + this.getSex() + PN.TAB + "birth: " + PN.TAB + this.bdate
				+ PN.TAB + "death:" + PN.TAB + this.deathAge + PN.TAB + "ever married:" + PN.TAB + this.married
				+ PN.LINESEPARATOR + this.level.getID() + PN.TAB + this.level.getHabitationSize());

		// if it contains a morbus object add to the string
		if (this.morbus != null)
			p.append(this.morbus.toString());
		// return the string
		return p.toString();
	}

	/**
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer();
		sb.append(this.id + "\t" + this.getSex() + ":\t" + this.bdate);

		if (this.level != null)
			sb.append(PN.TAB + this.level.getID());
		else
			sb.append("no level (yet)");

		return sb.toString();
	}

	/**
	 *
	 * @param string
	 * @param level
	 */
	private void addtoHistory(String string, Level level) {
		if (!Person.saveHistory)
			return;

		if (this.history == null)
			this.history = new History();

		this.history.add(Odin.getCurrentTime(), string + " " + level.toString());

	}

}
