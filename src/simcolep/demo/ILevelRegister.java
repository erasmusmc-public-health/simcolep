/*
 * This file is part of SIMCOLEP, a simulation model developed by the Department of Public Health,
 * Erasmus MC, University Medical Center Rotterdam, Rotterdam, The Netherlands.
 *
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/
 * or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA
 *
 * You are free to:
 * Share � copy and redistribute the material in any medium or format
 * Under the following terms:
 * Attribution � You must give appropriate credit, provide a link to the license, and indicate if changes were
 * made. You may do so in any reasonable manner, but not in any way that suggests the licensor endorses
 * you or your use.
 * NonCommercial � You may not use the material for commercial purposes.
 * NoDerivatives � If you remix, transform, or build upon the material, you may not distribute the modified material.
 */

/*
 * Created on 04-Nov-2005
 *
 */
package simcolep.demo;

import java.util.List;
import java.util.Random;
import java.util.Set;

import simcolep.fun2.Function;
import simcolep.tools.io.OutputList;
import simcolep.tools.io.Table;

/**
 * Register of levels
 *
 *
 * @author ir E.A.J. Fischer
 * @version 0.1.2 -- Last Modified 04-Nov-2005
 * @param <L>
 *
 */
@SuppressWarnings("unchecked")
public interface ILevelRegister<L extends ILevel> extends Register {

	/**
	 * Add a level to the register
	 *
	 * @param l
	 */
	public void addLevel(L l);

	/**
	 *
	 * @param level
	 */
	public boolean contains(L level);

	/**
	 * Choose a random level from the register
	 *
	 * @param rand
	 * @return l
	 */
	public L getLevel(Random rand);

	/**
	 * Choose a random but weighted level from the register
	 *
	 * @param rand
	 * @param f
	 * @return l
	 */
	public L getLevel(Random rand, Function f);

	/**
	 * Return a list that contains all levels (subdivision is possible)
	 *
	 * @return levels
	 */
	@SuppressWarnings("unchecked")
	public List getLevels();

	/**
	 * Return a set of levels that has this size
	 *
	 * @param size
	 * @return levels
	 */
	public Set<Level> getLevels(int size);

	/**
	 *
	 * @return number of levels in the register
	 */
	@Override
	public int getSize();

	/**
	 * The top level is that level that is the highest superlevel and thus
	 * contains all other levels
	 *
	 * @return toplevel
	 */
	public Level getToplevel();

	/**
	 * return a outputlust
	 */
	public List<OutputList<Double>> output();

	/**
	 * remove a level from the register
	 *
	 * @param l
	 * @return true if the level was present and is removed
	 */
	public boolean removeLevel(L l);

	/**
	 *
	 * @return table
	 */
	public Table toTable();

}
