/*
 * This file is part of SIMCOLEP, a simulation model developed by the Department of Public Health, 
 * Erasmus MC, University Medical Center Rotterdam, Rotterdam, The Netherlands.
 *
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/ 
 * or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA
 *
 * You are free to:
 * Share � copy and redistribute the material in any medium or format
 * Under the following terms:
 * Attribution � You must give appropriate credit, provide a link to the license, and indicate if changes were 
 * made. You may do so in any reasonable manner, but not in any way that suggests the licensor endorses
 * you or your use.
 * NonCommercial � You may not use the material for commercial purposes.
 * NoDerivatives � If you remix, transform, or build upon the material, you may not distribute the modified material.
 */

package simcolep.demo;

/**
 * @author ir E.A.J. Fischer
 *
 */
public abstract class AbstractPopulationRegister implements IPopulationRegister <Person>{

	/**
	 * width of the age classes in the population registers
	 */
	public static double ageclasswidth = 5.0;//size of an age class
	/**
	 * maximum age of a person
	 */
	public static double ageclassmax = 105.0;//the lower limit of the highest age class
	
	/**
	 * Returns a string with the names of the categories with the set ageclass max and width
	 * With following format 'x*ageclasswidth - (x+1)*ageclasswidth
	 * @return names
	 */
	public static String[] getCategoryNames() {
		int string_size = (int) (AbstractPopulationRegister.ageclassmax/AbstractPopulationRegister.ageclasswidth);
		
		String[] names = new String[string_size];
		
		for(int i = 0; i < string_size;i++)
			{names[i] = "'" +i*AbstractPopulationRegister.ageclasswidth +" - " +(i+1)*AbstractPopulationRegister.ageclasswidth;}
		
		return names;
	}

}
