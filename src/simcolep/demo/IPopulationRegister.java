/*
 * This file is part of SIMCOLEP, a simulation model developed by the Department of Public Health,
 * Erasmus MC, University Medical Center Rotterdam, Rotterdam, The Netherlands.
 *
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/
 * or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA
 *
 * You are free to:
 * Share � copy and redistribute the material in any medium or format
 * Under the following terms:
 * Attribution � You must give appropriate credit, provide a link to the license, and indicate if changes were
 * made. You may do so in any reasonable manner, but not in any way that suggests the licensor endorses
 * you or your use.
 * NonCommercial � You may not use the material for commercial purposes.
 * NoDerivatives � If you remix, transform, or build upon the material, you may not distribute the modified material.
 */

/*
 * Created on 04-Nov-2005
 *
 */
package simcolep.demo;

import java.io.IOException;
import java.io.Writer;
import java.util.Collection;
import java.util.List;
import java.util.Random;

import simcolep.exceptions.NotInRegisterException;
import simcolep.tools.io.Table;

/**
 *
 * @author ir. E.A.J. Fischer
 * @version 1.0
 * @param <P>
 *            - type of human object
 */
public interface IPopulationRegister<P> extends Register<P> {

	/**
	 * add a Person to the register
	 *
	 * @param p
	 */
	public void addPerson(P p);

	/**
	 *
	 * @param p
	 * @return true if this register contains p false otherwise
	 */
	public boolean contains(P p);

	/**
	 * Get a Person currently at index
	 *
	 * @param index
	 * @return
	 */
	public Person getPerson(int index);

	/**
	 * Selecting a random person from the population
	 *
	 * @param rand
	 * @return randomly selected person
	 */
	public P getPerson(Random rand);

	/**
	 * Selection of a random person from the population, but excluding one
	 * specific person
	 *
	 * @param rand
	 * @param excluded
	 *            from selection, e.g. source of infection
	 * @return randomly selected person
	 */
	public P getPerson(Random rand, Collection<P> excluded);

	/**
	 * Selecting a random person from the population of sex and in age class a
	 *
	 * @param rand
	 * @param sex
	 * @param a
	 *            ageclass
	 * @return randomly selected person
	 */
	public P getPerson(Random rand, Sex sex, int a);

	/**
	 * Return a list with the Persons between the ages from and to and of sex
	 * <br>
	 * By convention <br>
	 * 0 = male<br>
	 * 1 = female <br>
	 * In this particular case another value for sex would indicate that sex
	 * does not matter
	 *
	 * @param from
	 * @param to
	 * @param male
	 * @return list of persons
	 */
	@SuppressWarnings("unchecked")
	public List<P> getPersons(double from, double to, Sex male);

	/**
	 * A table containing the movement per age class
	 *
	 * @return table
	 */
	public Table movementTable();

	/**
	 * Remove person p
	 *
	 * @param p
	 * @return true if present in the population false otherwise
	 * @throws NotInRegisterException
	 */
	public boolean removePerson(P p) throws NotInRegisterException;

	/**
	 * Write this register to a file using this writer
	 *
	 * @param run
	 * @param writer
	 * @throws IOException
	 */
	public void toDatabase(int run, Writer writer) throws IOException;

	/**
	 * Create a table for the specified sex and its age distribution
	 *
	 * @param sex
	 *
	 * @return table
	 */
	public Table toTable(Sex sex);

}
