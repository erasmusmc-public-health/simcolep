/*
 * This file is part of SIMCOLEP, a simulation model developed by the Department of Public Health,
 * Erasmus MC, University Medical Center Rotterdam, Rotterdam, The Netherlands.
 *
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/
 * or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA
 *
 * You are free to:
 * Share � copy and redistribute the material in any medium or format
 * Under the following terms:
 * Attribution � You must give appropriate credit, provide a link to the license, and indicate if changes were
 * made. You may do so in any reasonable manner, but not in any way that suggests the licensor endorses
 * you or your use.
 * NonCommercial � You may not use the material for commercial purposes.
 * NoDerivatives � If you remix, transform, or build upon the material, you may not distribute the modified material.
 */

/*
 * Created on 19-Oct-2005
 *
 */
package simcolep.demo;

import java.io.Serializable;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import java.util.Set;

import nl.mgz.simulation.discreteevent.Target;
import simcolep.asgard.Odin;
import simcolep.exceptions.SimulationException;
import simcolep.fun2.Archimedes;
import simcolep.sim.Herbarium;
import simcolep.tools.History;
import simcolep.tools.Identifyable;
import simcolep.tools.PN;
import simcolep.tools.message.Message;
import simcolep.tools.message.Ratatosk;

/**
 * Level is a generic object implementing Levelable that contians either other
 * levels or inhabitants.<br>
 * The structure of all levels is always equal the most bottom (lowest ranking
 * level) contains inhabitants. Subsequent layers of levels contain a number of
 * these levels.
 *
 *
 * @author ir. E.A.J. Fischer
 * @version 1.0 -- Last Modified 19-09-2006
 */
public final class Level extends Target implements ILevel, Serializable, Identifyable {

	private static final String RANK_TEXT = "\trank:\t"; //$NON-NLS-1$
	private static final String SIZE_TEXT = "\tsize:\t"; //$NON-NLS-1$
	private static final String LEVEL_HEADERTEXT = "Level:\t"; //$NON-NLS-1$

	/** serialVersionUID */
	private static final long serialVersionUID = 1002884433430830860L;

	/**
	 * The rank that a dwelling has this means that this contains inhabitants
	 * and not levelables
	 */
	public static final int DWELLING_RANK = 0;

	/***/
	private static int LAYERS = -1;

	/** block removal of (trees of) levels when empty */
	public static boolean BLOCK_REMOVAL = false;

	/** save the history of this level */
	public static boolean saveLevelHist = false;

	/** rank of the level */
	protected int rank = Level.DWELLING_RANK;

	/** The level to which this level belongs */
	protected Level superlevel = null;

	/** Identification number */
	protected int id = -3210;// start with this id to check if the ID was set
								// properly

	/** sub levels of this level */
	@SuppressWarnings("unchecked")
	private LinkedHashSet<ILevel> members = null;

	/** inhabitants of this and thus its sublevels */
	private LinkedHashSet<Inhabitant> inhabs = null;

	/** inhab_size of the level when it is a dwelling */
	private int inhab_size = 0;

	/**
	 * capacity of the level is the number of members it can contain. As default
	 * there is no constraint on the number of members
	 */
	private double capacity = Double.POSITIVE_INFINITY;

	/** history of this level */
	private transient History LevelHist = null;

	/** true when removed and subtracted from counter */
	public transient boolean putBack = false;

	/** Has or does not have the D-factor <i>i.e.</i> Disease-factor */
	private boolean[] dFactor = null;

	/**
	 * @param popnum
	 * @param rank
	 */
	@SuppressWarnings("unchecked")
	public Level(int rank) {

		if (rank == DWELLING_RANK) {
			this.inhabs = new LinkedHashSet<Inhabitant>();
		} else
			this.members = new LinkedHashSet<ILevel>();

		this.rank = rank;

		this.dFactorize();
	}

	/**
	 * @param popnum
	 * @param rank
	 * @param superlevel
	 *
	 * @throws java.lang.IllegalArgumentException
	 */
	@SuppressWarnings("unchecked")
	public Level(int rank, Level superlevel) {

		this.rank = rank;
		// check rank consistency
		if (superlevel.getRank() <= this.rank)
			throw new IllegalArgumentException("cannot have a super level" + " with lower or equal rank"
					+ superlevel.getRank() + "<=" + this.rank);
		// get the right stuff
		if (rank == DWELLING_RANK) {
			this.inhabs = new LinkedHashSet<Inhabitant>();
		} else
			this.members = new LinkedHashSet<ILevel>();
		// set the super level
		this.superlevel = superlevel;

		this.dFactorize();
	}

	/**
	 * add an inhabitant to this level
	 *
	 * @throws IllegalArgumentException
	 *             if the rank is not 0
	 * @throws IllegalArgumentException
	 *             if the inhabitant already belongs to this level
	 * @see simcolep.demo.ILevel#addInhabitant(simcolep.demo.Person)
	 */
	@Override
	public boolean addInhabitant(Inhabitant inhab) {

		// if the rank is larger than 0 adding inhabitants is not allowed (in
		// this version anyway)
		if (this.rank > 0)
			throw new IllegalArgumentException("cannot add an inhabitant" + " to a level with rank > 0");

		// add the inhabitant to the map of inhabitants
		if (this.inhabs.contains(inhab))
			throw new IllegalArgumentException("cannot add the same inhabitant twice" + " to this level: " + inhab);

		// only allow adding if the capacity is not reached yet
		if (this.inhabs.size() >= this.capacity)
			return false;

		// add
		boolean b = this.inhabs.add(inhab);

		// set this level to the inhabitant
		inhab.setLevel(this);

		// add one to the inhab_size of this level
		this.changeInhabCount(+1);

		// save to history
		this.addLevelHist(Odin.getCurrentTime(), "add inhabitant " + inhab.getID());

		// notification of change of inhab_size (add inhabitant abreviated to
		// ai)
		Ratatosk.notify(Message.formulate(Message.ADDINHABITANT), inhab, this);

		return b;
	}

	/**
	 * Add an inhabitant without sending a message through the software. Be
	 * carefull this means that this level has to be added to the cadaster AFTER
	 * adding this inhabitant or notification should be done later
	 *
	 * @param p
	 */
	public boolean addInhabitantWithoutNotification(Inhabitant person) {
		// if the rank is larger than 0 adding inhabitants is not allowed (in
		// this version anyway)
		if (this.rank > 0)
			throw new IllegalArgumentException("cannot add an inhabitant" + " to a level with rank > 0");

		// add the inhabitant to the map of inhabitants
		if (this.inhabs.contains(person))
			throw new IllegalArgumentException("cannot add the same inhabitant twice" + " to this level: " + person);

		// only allow adding if the capacity is not reached yet
		if (this.inhabs.size() >= this.capacity)
			return false;

		// add
		boolean b = this.inhabs.add(person);

		// set this level to the inhabitant
		person.setLevel(this);

		// add one to the inhab_size of this level
		this.changeInhabCount(+1);

		// save to history
		this.addLevelHist(Odin.getCurrentTime(), "add inhabitant " + person.getID());

		return b;
	}

	/**
	 * Add the string to the list with LevelHistory
	 *
	 * @param t
	 * @param s
	 *
	 * @see simcolep.demo.ILevel#addLevelHist(double, java.lang.String)
	 */
	public void addLevelHist(double t, String s) {
		if (!Level.saveLevelHist)
			return;

		if (this.LevelHist == null)
			this.LevelHist = new History();

		this.LevelHist.add(t, s);

	}

	/**
	 * Add a member
	 *
	 * @param member
	 *
	 *            If previously this key (new Integer(member.getID())was not
	 *            associated return true
	 * @see simcolep.demo.ILevel#addMember(simcolep.demo.ILevel)
	 */
	@Override
	public boolean addMember(ILevel member) {

		// don't add members twice
		if (this.hasMember(member))
			throw new SimulationException("level already contains " + member);

		// only allow adding if the capacity is not reached yet
		if (this.members.size() >= this.capacity)
			return false;

		// check ranking
		if (member.getRank() >= this.rank)
			throw new SimulationException("cannot add a member to a level of the same rank");

		// add to the list of members
		boolean b = this.members.add(member);

		// add the number of inhabitants to this level
		this.changeInhabCount(member.getHabitationSize());

		// update level history
		this.addLevelHist(Odin.getCurrentTime(), "add member " + member.getID());

		// return the results
		return b;

	}

	/**
	 * The capacity of this specific level
	 *
	 * @see Level#capacity
	 * @return capacity
	 */
	public double getCapacity() {
		return this.capacity;
	}

	/**
	 *
	 * @param i
	 *            - layer
	 * @return this.dFactor[i]
	 */
	public boolean getDFactor(int i) {
		return this.dFactor[i];
	}

	/**
	 *
	 * @return this.dFactor[i]
	 */
	public boolean[] getDFactors() {
		return this.dFactor;
	}

	/**
	 * @return the number of inhabitants
	 * @see simcolep.demo.ILevel#getHabitationSize()
	 */
	@Override
	public int getHabitationSize() {
		// if the rank is the lowest return the inhab_size
		return this.inhab_size;

	}

	/**
	 * @return the identification of this level
	 * @see simcolep.demo.ILevel#getID()
	 */
	@Override
	public int getID() {
		return this.id;
	}

	/**
	 * if rank == DWELLING_RANK it return the index<sup>th</sup> inhabitant else
	 * if creates a list of all inhabitants and returns the index<sup>th</sup>
	 *
	 * @param index
	 *
	 * @see simcolep.demo.ILevel#getInhabitant(double)
	 */
	@Override
	public Inhabitant getInhabitant(int index) {
		// index not to large
		if (index > this.getHabitationSize())
			throw new IndexOutOfBoundsException("index: " + index + ",inhabitants: " + this.getHabitationSize());

		// return number so and so
		if (rank == DWELLING_RANK) {
			LinkedList<Inhabitant> list = new LinkedList<Inhabitant>();
			list.addAll(this.inhabs);
			if (list.size() == 0) {
				return null;
			}
			return list.get(index);

		} else {
			// iterate the members until the one containing this index
			int iteration_index = 0;
			for (ILevel member : this.members) {
				iteration_index += member.getHabitationSize();
				if (iteration_index >= index) {
					iteration_index -= member.getHabitationSize();

					return member.getInhabitant(index - iteration_index);
				}
			}
			return null;
		}
	}

	/**
	 * i
	 *
	 * @see simcolep.demo.ILevel#getInhabitant(double)
	 */
	@Override
	public Inhabitant getInhabitant(Random r) {
		return this.getInhabitant(r.nextInt(this.getHabitationSize()));
	}

	/**
	 * Get the set of the lowest rank and collect all inhabitants of these
	 * lowest levels if this is the lowest level return the set of inhabitants
	 *
	 * @return inhabs
	 * @see simcolep.demo.ILevel#getInhabitants()
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Set<Inhabitant> getInhabitants() {

		if (rank == Level.DWELLING_RANK)
			return new LinkedHashSet<Inhabitant>(this.inhabs);
		else {
			Set<Inhabitant> set = new LinkedHashSet<Inhabitant>();

			Iterator<ILevel> it = this.members.iterator();

			while (it.hasNext())
				set.addAll(it.next().getInhabitants());

			return set;
		}

	}

	/**
	 * @see simcolep.demo.ILevel#getLevelHist()
	 */
	@SuppressWarnings("unchecked")
	public List getLevelHist() {

		return this.LevelHist;
	}

	/**
	 * @return this.members
	 * @see simcolep.demo.ILevel#getMembers()
	 */
	@Override
	public Set<ILevel> getMembers() {

		return this.members;
	}

	/**
	 * Return the levels in the members set upto the specified rank. (Thus the
	 * set of levels of one rank lower!)
	 *
	 * @see simcolep.demo.ILevel#getMembers(int)
	 * @throws java.lang.IllegalArgumentException
	 *             when the rank parameter is larger than the rank of the level
	 *             or smaller than 0
	 *
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Set<ILevel> getMembers(int rank) {

		// if the rank is higher than the rank of this level throw
		// IllegalArgumentException
		if (rank + 1 > this.rank)
			throw new IllegalArgumentException("rank to large " + rank + " >= " + this.rank);
		// if the rank is smaller than 0 is not allowed
		if (rank < 0)
			throw new IllegalArgumentException("rank to small " + rank + " <  0");
		// if the rank equals this rank return the memberset of this level
		else if (rank + 1 == this.rank)
			return this.getMembers();
		// else return getMemberSet(int rank) of all subordinate Levels
		else {
			Iterator<ILevel> it = this.members.iterator();

			Set<ILevel> sublevels = new LinkedHashSet<ILevel>();

			while (it.hasNext())
				sublevels.addAll(it.next().getMembers(rank));

			return sublevels;
		}
	}

	/**
	 * @return rank
	 * @see simcolep.demo.ILevel#getRank()
	 */
	@Override
	public int getRank() {
		return this.rank;
	}

	/**
	 * @return this.members.size()
	 * @see simcolep.demo.ILevel#getSize()
	 */
	@Override
	public int getSize() {
		if (this.rank == DWELLING_RANK)
			return this.inhab_size;
		return this.members.size();
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see simcolep.demo.Levelable#getSuperlevel()
	 */
	@Override
	public ILevel getSuperlevel() {

		return this.superlevel;
	}

	/**
	 * if this is a level of rank 0 check the set of inhabitants<br>
	 * otherwise check lower levels that are members
	 *
	 * @see simcolep.demo.ILevel#hasInhabitant(simcolep.demo.Person)
	 */
	@Override
	@SuppressWarnings("unchecked")
	public boolean hasInhabitant(Inhabitant inhabitant) {

		// for the lowest rank we know the following
		if (rank == 0)
			return this.inhabs.contains(inhabitant);

		Iterator<ILevel> it = this.members.iterator();
		while (it.hasNext()) {
			if (it.next().hasInhabitant(inhabitant))
				return true;
		}

		return false;
	}

	/**
	 * True if member is present false if not
	 *
	 * @param member
	 * @return true if member is member of this level
	 * @see simcolep.demo.ILevel#hasMember(simcolep.demo.ILevel)
	 */
	@Override
	public boolean hasMember(ILevel member) {
		return this.members.contains(member);
	}

	/**
	 * @return this.superlevel == null
	 * @see simcolep.demo.ILevel#isHighest()
	 */
	@Override
	public boolean isHighest() {

		return this.superlevel == null;
	}

	/**
	 * @return rank ==0
	 * @see simcolep.demo.ILevel#isLowest()
	 */
	@Override
	public boolean isLowest() {

		return this.rank == DWELLING_RANK;
	}

	/**
	 * @see simcolep.demo.ILevel#removeInhabitant(simcolep.demo.Inhabitant)
	 */
	@Override
	public boolean removeInhabitant(Inhabitant inhab) {
		return this.removeI(inhab, true);
	}

	/**
	 * Removes all inhabitants from this level
	 *
	 */
	public void removeInhabitants() {

		if (this.rank != DWELLING_RANK)
			throw new IllegalArgumentException("cannot remove all inhabitants " + "from a non-dwelling rank");

		// remove all inhabitants
		for (Inhabitant inhab : this.getInhabitants()) {
			this.removeInhabitant(inhab);
		}

	}

	/**
	 * Remove without notifying others in the simulation
	 *
	 * @param inhab
	 */
	public boolean removeInhabitantWithoutNotification(Inhabitant inhab) {
		return this.removeI(inhab, false);

	}

	/**
	 * remove the member
	 *
	 * @param member
	 *
	 * @see simcolep.demo.ILevel#removeMember(simcolep.demo.ILevel)
	 */
	@Override
	public boolean removeMember(ILevel member) {

		// remove the member from the set
		boolean b = this.members.remove(member);

		// history writing
		if (b)
			this.addLevelHist(Odin.getCurrentTime(), "removed member " + member.getID());

		this.changeInhabCount(-member.getHabitationSize());
		// remove empty level (tree)
		if (b)
			this.remove_tree();

		// return result
		return b;

	}

	/**
	 * Remove the super level of this level
	 *
	 */
	@Override
	public void removeSuperlevel() {
		this.superlevel = null;

		this.addLevelHist(Odin.getCurrentTime(), "removed superlevel");
	}

	/**
	 * Set the capacity of this specific level
	 *
	 * @see Level#capacity
	 * @param cap
	 */
	public void setCapacity(double cap) {
		// set the capacity
		this.capacity = cap;

	}

	/**
	 * set the id
	 *
	 * @param id
	 * @see simcolep.demo.ILevel#setID(int)
	 */
	@Override
	public void setID(int id) {
		this.id = id;

	}

	/**
	 * set rank
	 *
	 * @param rank
	 * @see simcolep.demo.ILevel#setRank(int)
	 */
	@Override
	public void setRank(int rank) {
		this.rank = rank;
	}

	/**
	 * @see simcolep.demo.ILevel#setSuperlevel(simcolep.demo.ILevel)
	 */
	@Override
	public void setSuperlevel(ILevel new_super) {

		// add to the history of this level
		if (new_super == null) {
			throw new SimulationException("new super = null");
		} else {
			this.addLevelHist(Odin.getCurrentTime(), "super level set to: " + new_super.getID());
		}
		// check
		if (this.superlevel != null)
			throw new SimulationException("First remove current superlevel" + " than add new");

		// set the superlevel
		this.superlevel = (Level) new_super;

	}

	/**
	 *
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuffer s = new StringBuffer(32);
		s.append(LEVEL_HEADERTEXT + this.id + SIZE_TEXT + this.getHabitationSize());

		// if there are more ranks let it be added to the string
		if (this.rank == DWELLING_RANK) {
			if (this.superlevel != null)
				s.append(RANK_TEXT + DWELLING_RANK);
		} else if (this.members != null)
			s.append(RANK_TEXT + this.rank);

		return s.toString();

	}

	/**
	 * Change the counter of inhabitants of this level
	 *
	 * @param i
	 */
	private void changeInhabCount(int i) {

		this.inhab_size += i;

		if (this.superlevel != null)
			this.superlevel.changeInhabCount(i);

	}

	/**
	 * Determine for each layer, whether it contains the 'D-Factor'
	 */
	private void dFactorize() {
		if (LAYERS < 1)
			LAYERS = Archimedes.askInteger(PN.L);

		this.dFactor = new boolean[LAYERS];

		// obtain random number generator
		Random rand = Herbarium.rand(Herbarium.DIS);

		double dfactor = Archimedes.askDouble(PN.DFACTOR);

		for (int i = 0; i < LAYERS; i++) {

			if (rand.nextDouble() < dfactor) {
				this.dFactor[i] = true;
			} else
				this.dFactor[i] = false;
		}
	}

	/**
	 * Remove this level from its superlevel The iterative process that starts
	 * will remove all empty levels except for the top level Process can be
	 * blocked by BLOCK_REMOVAL to true
	 *
	 */
	private void remove_tree() {
		// blockage of removal ...
		if (BLOCK_REMOVAL)
			return;

		// do not remove the highest level
		if (this.isHighest())
			return;

		// if no members left remove from the superlevel
		if (this.members.isEmpty())
			if (!this.superlevel.removeMember(this))
				throw new SimulationException("did not remove from tree");
	}

	/**
	 * Remove an inhabitant from this and sub-ordinate levels. Whether or not to
	 * notify others in the program can be given as parameter
	 *
	 * @param inhab
	 * @param notify
	 * @return true if removed
	 */
	private boolean removeI(Inhabitant inhab, boolean notify) {

		boolean b = false;

		if (this.rank == DWELLING_RANK) {
			if (this.inhabs.contains(inhab)) {
				b = this.inhabs.remove(inhab);

				// remove reference in the inhabitant
				inhab.removeLevel(this);

				// subtract one from the inhab_size
				this.changeInhabCount(-1);

				if (this.inhab_size - this.inhabs.size() != 0)
					throw new IllegalArgumentException(
							"inhab_size does not correspond to " + "the number of inhabitants");

				if (notify) {
					// add to level history
					this.addLevelHist(Odin.getCurrentTime(), "Person " + inhab.getID() + " removed");

					// notify that the inhab_size of the level has changed
					// (remove inhabitant abrev. ri)
					Ratatosk.notify(Message.formulate(Message.REMOVEINHABITANT), inhab, this);

				}
			}
			return b;
		} else {
			Iterator<ILevel> it = this.members.iterator();

			while (it.hasNext()) {
				b = ((Level) it.next()).removeI(inhab, notify);
				if (b)
					return b;
			}
			return b;
		}
	}

}
