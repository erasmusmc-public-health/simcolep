/*
 * This file is part of SIMCOLEP, a simulation model developed by the Department of Public Health,
 * Erasmus MC, University Medical Center Rotterdam, Rotterdam, The Netherlands.
 *
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/
 * or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA
 *
 * You are free to:
 * Share � copy and redistribute the material in any medium or format
 * Under the following terms:
 * Attribution � You must give appropriate credit, provide a link to the license, and indicate if changes were
 * made. You may do so in any reasonable manner, but not in any way that suggests the licensor endorses
 * you or your use.
 * NonCommercial � You may not use the material for commercial purposes.
 * NoDerivatives � If you remix, transform, or build upon the material, you may not distribute the modified material.
 */

/*
 * Created on 09-Nov-2005
 *
 */
package simcolep.demo;

import simcolep.asgard.Loki;
import simcolep.asgard.Odin;
import simcolep.exceptions.SimulationException;
import simcolep.fun2.Archimedes;
import simcolep.lepra.mendel.Genomable;
import simcolep.tools.PN;

/**
 * A female person
 *
 * @author ir. E.A.J. Fischer
 * @version 1.0
 *
 */
public class Female extends Person {

	/**
	 *
	 */
	private static final long serialVersionUID = -8973299710000985407L;

	/**
	 * Strip a female of specific female objects
	 *
	 * @param p
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public final static Person stripFemale(Person p) {
		if (p.getSex() != Sex.FEMALE)
			return p;

		((Female) p).spouse = null;
		((Female) p).spousetype = new Genomable[Archimedes.askInteger(PN.L)];

		return p;
	}

	/** spouse */
	private Male spouse = null;

	/** original disease type of last spouse */
	private Genomable[] spousetype = null;

	/**
	 *
	 * @param hubby
	 */
	@Override
	@SuppressWarnings("unchecked")
	public final void addSpouse(Person hubby) {
		// check if marrying to a null
		if (hubby == null)
			throw new SimulationException("cannot marry null");
		// two spouses not allowed
		if (this.spouse != null)
			throw new SimulationException("cannot add two spouses to a women");
		// set spouse
		this.spouse = (Male) hubby;

		// set spouse disease type
		this.spousetype = new Genomable[Archimedes.askInteger(PN.L)];

		for (int i = 0; i < this.spousetype.length; i++)
			this.spousetype[i] = this.spouse.getMorbus(i).getGenome();

		// check if moving when widowed
		this.movesaswidow = Loki.getMovesasWidow(this);

		// set evermarried to true
		this.evermarried = true;
		this.engaged = false;

		this.addtoHistory(Odin.getCurrentTime(), "spouse added: ", spouse);

	}

	/**
	 * Additionally removes herself from children
	 *
	 * @see simcolep.demo.Person#die()
	 */

	@Override
	public final void die() {
		if (this.children != null) {
			for (Person child : this.children) {
				child.removeMother();
			}
		}

		super.die();
	}

	/**
	 * @return 1 = FEMALE
	 * @see simcolep.demo.Person#getSex()
	 */
	@Override
	public final Sex getSex() {
		return Sex.FEMALE;
	}

	/**
	 *
	 * @return spouse of this female
	 */
	@Override
	public final Person getSpouse() {
		return this.spouse;
	}

	/**
	 * Returns the original type of the last male this women was married to
	 *
	 * @param index
	 *
	 * @return spousetype
	 */
	public Genomable getSpouseGenotype(int index) {
		return this.spousetype[index];
	}

	/**
	 * Remove the spouse
	 */
	@Override
	@SuppressWarnings("unchecked")
	public final void removeSpouse() {
		// spouse is a null
		this.spouse = null;

		// removing and adding to singles list etc. is done after execution of
		// spouse death event
		// this enables us to be more flexible for example with widowers that do
		// nothing until a certain time.
		this.addtoHistory(Odin.getCurrentTime(), "spouse removed");

	}

}
