/*
 * This file is part of SIMCOLEP, a simulation model developed by the Department of Public Health,
 * Erasmus MC, University Medical Center Rotterdam, Rotterdam, The Netherlands.
 *
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/
 * or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA
 *
 * You are free to:
 * Share � copy and redistribute the material in any medium or format
 * Under the following terms:
 * Attribution � You must give appropriate credit, provide a link to the license, and indicate if changes were
 * made. You may do so in any reasonable manner, but not in any way that suggests the licensor endorses
 * you or your use.
 * NonCommercial � You may not use the material for commercial purposes.
 * NoDerivatives � If you remix, transform, or build upon the material, you may not distribute the modified material.
 */

/*
 * Created on 07-Nov-2005
 *
 */
package simcolep.demo;

import java.io.IOException;
import java.io.Serializable;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;

import simcolep.asgard.Odin;
import simcolep.demo.event.Age;
import simcolep.demo.event.Death;
import simcolep.exceptions.NotInRegisterException;
import simcolep.exceptions.SimulationException;
import simcolep.tools.PN;
import simcolep.tools.io.DataFormat;
import simcolep.tools.io.OutputElement;
import simcolep.tools.io.OutputList;
import simcolep.tools.io.Table;
import simcolep.tools.message.Message;
import simcolep.tools.message.Notifyable;
import simcolep.tools.message.Ratatosk;

/**
 * Implementation of the population register PopRegister
 *
 * @author ir. E.A.J. Fischer
 * @version 1.0
 */

public final class PopulationRegister extends AbstractPopulationRegister
		implements IPopulationRegister<Person>, Notifyable, Serializable {

	private static final String OUTPUT_SUMSTRING = "sum"; //$NON-NLS-1$
	/**
	 * serialVersionUID computed 15-11-2005
	 */
	private static final long serialVersionUID = 2699918049290051674L;
	/**
	 * Population listens to calls from Ratatosk with messages<br>
	 * listensTo
	 */
	private static final String[] listensTo = { Message.DEATH, Message.AGE };
	/** store data for output */
	private boolean storeData = false;

	/**
	 * Main register in a LinkedHashSet
	 */
	private LinkedHashSet<Person> mainregister = new LinkedHashSet<Person>();

	/**
	 * Males sorted by age
	 */
	protected ArrayList<Set<Person>> males = null;

	/**
	 * Women sorted by age
	 */
	protected ArrayList<Set<Person>> females = null;

	/**
	 * Output list contains the numbers of women and men at different times per
	 * age class
	 */

	@SuppressWarnings("unchecked")
	private transient List<OutputList>[] outpop = null;

	/** output of growth of the population */
	private transient OutputList<Double> growth = null;

	/**
	 * Constructor for an empty population. Registers to Ratatosk for messages:
	 * Odin#listensTo
	 *
	 *
	 * @param popnum
	 *            sets population to this integer value
	 *
	 */
	@SuppressWarnings("unchecked")
	public PopulationRegister() {

		// register
		this.mainregister = new LinkedHashSet<Person>();

		// sorted by age
		this.males = new ArrayList<Set<Person>>();
		this.females = new ArrayList<Set<Person>>();
		this.outpop = new ArrayList[2];
		this.outpop[0] = new ArrayList<OutputList>();
		this.outpop[1] = new ArrayList<OutputList>();
		this.growth = new OutputList<Double>(PN.POPSIZE);
		// instantiate the list with sets of age classes
		for (long x = 0; x < AbstractPopulationRegister.ageclassmax; x += AbstractPopulationRegister.ageclasswidth) {
			this.males.add(new LinkedHashSet<Person>());
			this.outpop[0].add(new OutputList<Double>(Sex.MALE.toString() + x));
			this.females.add(new LinkedHashSet<Person>());
			this.outpop[1].add(new OutputList<Double>(Sex.FEMALE.toString() + x));

		}

		// register to listen to messages
		for (String s : listensTo) {
			Ratatosk.register(Message.formulate(s), this);
		}

	}

	/**
	 *
	 * @param p
	 */
	@Override
	@SuppressWarnings("unchecked")
	public void addPerson(Person p) {
		// check if the register already contains this person
		if (Odin.inSafeMode())
			if (this.mainregister.contains(p))
				throw new SimulationException("cannot add the same person twice to the population");

		// add to the register
		if (!this.mainregister.add(p))
			throw new SimulationException(SimulationException.NOTADDED);

		// check
		if (Odin.inSafeMode())
			if (!p.isTargetOf(Death.class.getName()))
				throw new SimulationException(SimulationException.NODEATHSET);

		// check if this person is target of an ageing event if not schedule one
		if (!p.isTargetOf(Age.class.getName())) {
			// add an Ageing event to this addPerson This implementation of a
			// population needs it
			// schedule aging event
			double exetime = Age.calcAgeTime(AbstractPopulationRegister.ageclasswidth, p.getAge(Odin.getCurrentTime()),
					Odin.getCurrentTime());

			if (exetime <= p.getDeathTime()) {
				// new age event
				Age age = new Age(this, p, exetime);
				Odin.getEventManager().schedule(age);
			}
		} else
			throw new SimulationException("no one other's task" + " but Population to add age events");

		// get age class
		int ageclass = p.getAgeclass();
		// increase size of age class registers if necessary
		while (ageclass > this.males.size()) {
			this.females.add(new LinkedHashSet<Person>());
			this.outpop[Sex.FEMALE.getValue()].add(new OutputList<Double>(Sex.FEMALE.toString() + this.females.size()));
			this.males.add(new LinkedHashSet<Person>());
			this.outpop[Sex.MALE.getValue()].add(new OutputList<Double>(Sex.MALE.toString() + this.females.size()));
		}
		// add to male
		if (p.getSex() == Sex.MALE) {
			this.males.get(ageclass).add(p);
			return;
		} // or female list
		else if (p.getSex() == Sex.FEMALE) {
			this.females.get(ageclass).add(p);
			return;
		} else
			throw new SimulationException(SimulationException.NOSEX);
	}

	/**
	 * @see simcolep.demo.Register#collectData(double)
	 */
	@Override
	@SuppressWarnings({ "unchecked", "unchecked" })
	public void collectData(double time) {
		// if data collection is not turned on don't bother
		if (!this.storeData)
			return;

		double size = 0;
		// males
		for (int x = 0; x < males.size(); x++) {
			OutputElement<Double> e = new OutputElement<Double>(Odin.getCurrentTime(),
					Integer.valueOf(this.males.get(x).size()).doubleValue());
			this.outpop[Sex.MALE.getValue()].get(x).add(e);

			size += e.getValue().doubleValue();
		}
		// females
		for (int x = 0; x < females.size(); x++) {
			OutputElement<Double> e = new OutputElement<Double>(Odin.getCurrentTime(),
					Integer.valueOf(this.females.get(x).size()).doubleValue());
			this.outpop[Sex.FEMALE.getValue()].get(x).add(e);
			size += e.getValue().doubleValue();
		}
		// total size
		OutputElement<Double> e = new OutputElement<Double>(time, size);
		this.growth.add(e);

	}

	/**
	 * Sets all value to the error code
	 *
	 * @param time
	 *            time interval to 'collect'
	 * @param as
	 *            number representing an error
	 * @param times
	 *            to collect
	 *
	 */
	@Override
	@SuppressWarnings("unchecked")
	public void collectDataAtTimeAs(double time, double as) {

		// males
		for (int x = 0; x < males.size(); x++) {
			OutputElement<Double> e = new OutputElement<Double>(time, as);
			this.outpop[Sex.MALE.getValue()].get(x).add(e);

		}
		// females
		for (int x = 0; x < females.size(); x++) {
			OutputElement<Double> e = new OutputElement<Double>(time, as);
			this.outpop[Sex.FEMALE.getValue()].get(x).add(e);
		}
		// total size
		OutputElement<Double> e = new OutputElement<Double>(time, as);
		this.growth.add(e);

	}

	/**
	 * @see simcolep.demo.IPopulationRegister#contains(Person p)
	 */
	@Override
	public boolean contains(Person p) {
		return this.mainregister.contains(p);
	}

	@Override
	public Person getPerson(int index) {
		// make a list of the collection
		List<Person> pl = new ArrayList<Person>(this.mainregister);

		// no body left return
		if (pl.isEmpty())
			return null;

		// draw person from list
		Person p = pl.get(index);

		// return the selected person
		return p;
	}

	/**
	 * Selects a random person from the mainregister
	 *
	 * @param rand
	 * @return randomly choosen person
	 *
	 * @see simcolep.demo.IPopulationRegister#getPerson(Random)
	 */
	@Override
	public Person getPerson(Random rand) {
		return this.selectPerson(this.mainregister, rand, null);
	}

	/**
	 * @see simcolep.demo.IPopulationRegister#getPerson(java.util.Random,
	 *      java.lang.Object)
	 */
	@Override
	public Person getPerson(Random rand, Collection<Person> excluded) {
		return this.selectPerson(this.mainregister, rand, excluded);
	}

	/**
	 *
	 * @see simcolep.demo.IPopulationRegister#getPerson(java.util.Random, int,
	 *      int)
	 */
	@Override
	public Person getPerson(Random rand, Sex sex, int a) {
		if (sex == Sex.MALE) {
			return this.selectPerson(this.males.get(a), rand, null);

		} else if (sex == Sex.FEMALE) {
			return this.selectPerson(this.females.get(a), rand, null);

		} else if (sex == Sex.BOTH) {

			// choose between males or females == true
			boolean fsx = rand.nextBoolean();
			if (fsx)
				return this.selectPerson(this.females.get(a), rand, null);
			else
				return this.selectPerson(this.males.get(a), rand, null);

		}
		throw new SimulationException("sex > " + Sex.BOTH);
	}

	/**
	 * A list might be more usefull than a HashMap this will return the result
	 * of <code>list.addAll(this.mainregister.values())</code>
	 *
	 * @return list the mainregister as list
	 * @see simcolep.demo.IPopulationRegister#getPersons()
	 */
	public Collection<Person> getPersons() {
		return this.mainregister;
	}

	/**
	 * Population uses age classes.<br>
	 * This algorithm:<br>
	 * <li>determine to which age class <i>from</i> belongs*
	 * <li>determine to which age class <i>to</i> belongs*
	 * <li>get those and all intermediate age classes
	 * <li>add to a list
	 * <li>return that list <br>
	 * <br>
	 * *calculation to which age class double d belongs is:<br>
	 * (d-d%ageclasswidth)/ageclasswidth<br>
	 *
	 * @see simcolep.demo.IPopulationRegister#getPersons(double, double, int)
	 */
	@Override
	public List<Person> getPersons(double from, double to, Sex sex) {

		if (from > to)
			throw new SimulationException("from > to");

		// get people of sex born between birthdates:
		int classfrom = Double.valueOf(
				(from - from % (AbstractPopulationRegister.ageclasswidth)) / (AbstractPopulationRegister.ageclasswidth))
				.intValue();

		int classto = Double.valueOf(
				(to - to % (AbstractPopulationRegister.ageclasswidth)) / (AbstractPopulationRegister.ageclasswidth))
				.intValue();
		// put these and all class inbetween in a list for men, women or both

		if (sex == Sex.MALE) {
			// create list
			List<Person> l = new ArrayList<Person>();
			// add all from class from to class to
			for (; classfrom <= classto; classfrom++)
				l.addAll(this.males.get(classfrom));
			// return list
			return l;

		}
		if (sex == Sex.FEMALE) {
			// create list
			List<Person> l = new ArrayList<Person>();
			// add all from class from to class to
			for (; classfrom <= classto; classfrom++)
				l.addAll(this.females.get(classfrom));
			// return list
			return l;
		}
		if (sex == Sex.BOTH) {
			List<Person> l = new ArrayList<Person>();
			// add all from class from to class to
			for (; classfrom <= classto; classfrom++) {
				l.addAll(this.males.get(classfrom));
				l.addAll(this.females.get(classfrom));
			}
			// return list
			return l;
		}
		return null;
	}

	/**
	 * @see simcolep.demo.IPopulationRegister#getPersons(int)
	 */
	public Collection<? extends Person> getPersons(Sex sex) {

		if (sex == Sex.MALE) {
			List<Person> l = new ArrayList<Person>();
			for (Set<Person> s : this.males) {
				l.addAll(s);
			}
			return l;
		}
		if (sex == Sex.FEMALE) {
			List<Person> l = new ArrayList<Person>();
			for (Set<Person> s : this.females) {
				l.addAll(s);
			}
			return l;
		}
		if (sex == Sex.BOTH)
			return this.getPersons();

		return null;
	}

	/**
	 * @return mainregister A linkedhashset contain all people in the population
	 * @see simcolep.demo.Register#getRegister()
	 */
	@Override
	public Iterable<Person> getRegister() {

		return this.mainregister;
	}

	/**
	 * @see simcolep.demo.IPopulationRegister#getSize()
	 */
	@Override
	public int getSize() {

		return this.mainregister.size();
	}

	/**
	 *
	 * @see simcolep.demo.IPopulationRegister#movementTable()
	 */
	@Override
	public Table movementTable() {// columns
		int c = (int) (AbstractPopulationRegister.ageclassmax / AbstractPopulationRegister.ageclasswidth);
		int r = 25;
		// create a table
		Table table = new Table(r, c);
		// set names
		// columns
		for (int i = 0; i < c; i++) {
			table.setColumnName(i, "'" + (i * AbstractPopulationRegister.ageclasswidth) + " - "
					+ ((i + 1) * AbstractPopulationRegister.ageclasswidth) + "'");
		}

		// iterate the list and add one to each cell if needed
		for (Person p : this.mainregister) {
			// cut off at 25 for life_time_level
			int i_r = Math.min(p.life_time_level, 25);
			// previous entry
			table.addtoCell(i_r, p.getAgeclass(), 1.0);
		}
		// return the table
		return table;
	}

	/**
	 *
	 * @see simcolep.tools.message.Notifyable#notify(simcolep.tools.message.Message,
	 *      java.lang.Object, java.lang.Object)
	 */
	@Override
	public void notify(Message message, Object subject, Object source) throws Exception {
		if (message.equals(Message.DEATH)) {
			Person p = (Person) subject;
			boolean b = this.removePerson(p);
			if (!b)
				throw new SimulationException("not " + "possible to remove this person");

		}
		// change of age class
		else if (message.equals(Message.AGE)) {
			this.nextAgeClass((Person) subject);
		} else
			throw new SimulationException("register notified by an unknown message");

	}

	/**
	 *
	 * @param p
	 * @return this.mainregister.remove(Integer.valueOf(p.getID()))!= null
	 * @throws NotInRegisterException
	 */
	@Override
	@SuppressWarnings("unchecked")
	public boolean removePerson(Person p) throws NotInRegisterException {

		// if not found in main register
		if (!this.mainregister.remove(p))
			throw new SimulationException(SimulationException.NOTREMOVED);

		// get age class
		int ageclass = p.getAgeclass();
		// remove from age class
		boolean b = this.males.get(ageclass).remove(p);
		if (!b) {
			b = this.females.get(ageclass).remove(p);
			if (!b)
				throw new NotInRegisterException(p + "not found in populationlist age class " + ageclass);
		}

		// return
		return b;
	}

	/**
	 * @see simcolep.demo.Register#storeData(boolean)
	 */
	@Override
	public void storeData(boolean b) {
		this.storeData = b;
	}

	@Override
	public void toDatabase(int run, Writer writer) throws IOException {
		String sepsign = ",";
		// if this run is the first add column headings
		if (run <= 1)
			writer.append(
					"run" + sepsign + Person.DATABASEHEADING.replace("*sep*", sepsign) + DataFormat.LINESEPARATOR);
		// iterate the register
		for (Person p : this.mainregister) {
			writer.append(run + sepsign + p.databaseEntry(sepsign) + DataFormat.LINESEPARATOR);
		}

	}

	/**
	 *
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return this.toString(OUTPUT_SUMSTRING);
	}

	/**
	 *
	 * @see simcolep.gui.viewer.ToStringSwitchable#toString(java.lang.String)
	 */
	public String toString(String to) {
		StringBuffer outbuffer = new StringBuffer();

		if (to.contains(OUTPUT_SUMSTRING)) {
			outbuffer.append("Total size is:" + PN.TAB + this.mainregister.size() + PN.LINESEPARATOR);
			int numOfclass = Math.max(this.males.size(), this.females.size());
			outbuffer.append("Age classes:" + PN.LINESEPARATOR + PN.TAB);
			for (double y = 0; y < numOfclass; y++) {
				outbuffer.append(y * AbstractPopulationRegister.ageclasswidth + PN.TAB);
			}
			outbuffer.append(PN.LINESEPARATOR + "Males:" + PN.TAB);
			for (Set<Person> s : this.males) {
				outbuffer.append(s.size() + PN.TAB);
			}
			outbuffer.append(PN.LINESEPARATOR + "Females:" + PN.TAB);
			for (Set<Person> s : this.females) {
				outbuffer.append(s.size() + PN.TAB);
			}
			return outbuffer.toString();
		} else if (to.contains("full")) {
			if (!to.contains("force")) {
				int i = 0;
				for (Person p : this.mainregister) {
					outbuffer.append(p.toString() + PN.LINESEPARATOR);
					i++;
					if (i > 1000) {
						outbuffer.append("..." + PN.LINESEPARATOR);
						break;
					}
				}
			} else {
				for (Person p : this.mainregister) {
					outbuffer.append(p.toString() + PN.LINESEPARATOR);
				}
			}
			return outbuffer.toString();
		} else
			return null;
	}

	/**
	 * @param sex
	 * @return table
	 *
	 * @see simcolep.demo.IPopulationRegister#toTable(int)
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Table toTable(Sex sex) {

		if (sex == Sex.BOTH)
			throw new SimulationException("not implemented request output " + "for sexes separately");

		// get lists
		List<OutputList> columns = this.outpop[sex.getValue()];

		// if no lists return
		if (columns.size() <= 0)
			return null;

		// get size
		int rows = columns.get(0).size();

		// set table
		Table table = new Table(rows, columns.size());

		// set row names
		for (int index = 0; index < rows; index++) {
			table.setRowName(index, Double.toString(this.outpop[sex.getValue()].get(0).get(index).getTime()));
		}

		// add data
		for (int index = 0; index < columns.size(); index++) {
			// list
			OutputList<Double> ol = columns.get(index);
			// set column name
			table.setColumnName(index, ol.getName());

			// set values
			table.setColumn(index, ol);
		}
		// return table
		return table;
	}

	/**
	 *
	 * @param person
	 */

	@SuppressWarnings("unchecked")
	protected void nextAgeClass(Person person) {

		// get age class that it used to be a member of
		int ageclass = person.getAgeclass();
		// if this age class does not exist create it
		while (ageclass + 2 > males.size()) {
			AbstractPopulationRegister.ageclassmax += AbstractPopulationRegister.ageclasswidth;
			this.males.add(new LinkedHashSet<Person>());
			this.outpop[Sex.MALE.getValue()].add(new OutputList<Double>(Sex.MALE.toString() + this.males.size()));
			this.females.add(new LinkedHashSet<Person>());
			this.outpop[Sex.FEMALE.getValue()].add(new OutputList<Double>(Sex.FEMALE.toString() + this.females.size()));
		}
		// remove from old class add to new class
		if (person.getSex() == Sex.MALE) {
			// remove from previous age class
			this.males.get(ageclass).remove(person);
			// add to the current age class
			this.males.get(ageclass + 1).add(person);
		} else if (person.getSex() == Sex.FEMALE) {
			// remove from previous age class
			this.females.get(ageclass).remove(person);
			// add to the current age class
			this.females.get(ageclass + 1).add(person);
		} else
			throw new SimulationException(SimulationException.NOSEX);

	}

	/**
	 * Select the person in the collection at index rand * size of the
	 * collection
	 *
	 * @param c
	 *            collection of persons
	 * @param rand
	 *            random generated number
	 * @param exclude
	 *            excluded person
	 * @return p
	 */
	private Person selectPerson(java.util.Collection<? extends Person> c, Random rand,
			Collection<? extends Person> excluded) {

		// make a list of the collection
		List<Person> pl = new ArrayList<Person>(c);

		// remove excluded persons
		if (excluded != null)
			pl.removeAll(excluded);

		// no body left return
		if (pl.isEmpty())
			return null;

		// get a random index
		int randomIndex = pl.size();

		// more than 0 persons in the collection thus select one randomly
		if (randomIndex > 0)
			randomIndex = rand.nextInt(randomIndex);
		else // first of course
			randomIndex = 0;

		// draw person from list
		Person p = pl.get(randomIndex);

		// return the selected person
		return p;

	}
}
