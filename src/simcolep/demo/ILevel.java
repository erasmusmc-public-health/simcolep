/*
 * This file is part of SIMCOLEP, a simulation model developed by the Department of Public Health,
 * Erasmus MC, University Medical Center Rotterdam, Rotterdam, The Netherlands.
 *
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/
 * or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA
 *
 * You are free to:
 * Share � copy and redistribute the material in any medium or format
 * Under the following terms:
 * Attribution � You must give appropriate credit, provide a link to the license, and indicate if changes were
 * made. You may do so in any reasonable manner, but not in any way that suggests the licensor endorses
 * you or your use.
 * NonCommercial � You may not use the material for commercial purposes.
 * NoDerivatives � If you remix, transform, or build upon the material, you may not distribute the modified material.
 */

/*
 * Created on 30-Aug-2005
 *
 */
package simcolep.demo;

import java.io.Serializable;
import java.util.Random;
import java.util.Set;

/**
 * This interface is used by classes that want to be used in a data structure
 * with levels (of mixing). <br>
 * The levels have an hierarchy according to their rank. The rank is an integer
 * from 0 to the highest rank (n). The lowest ranking (0) should not have its
 * own members. The highest ranking should not contain a superlevel. (it
 * wouldn't be the highest in rank than would it?!)<br>
 * Note the definition of <br>
 * <li>Member: The direct subordinate in the hierarchial structure of a
 * Levelable
 * <P>
 * <li>Inhabitant: A member of the lowest ranking Levelable
 * <P>
 *
 * It is not allowed to add members that are not Levelable
 * <P>
 * to Objects of Levelable
 * <P>
 * with a rank higher than 0. An example would be for instance Person's living
 * in household that are set in a compound, which is a structure with two
 * levels. Where compound is the highest level and household the lowest.
 *
 *
 * @author ir E.A.J. Fischer
 * @version 0.1.2 -- Last Modified 30-Aug-2005
 * @param P
 *            - type of Inhabitant
 *
 */
public interface ILevel extends Serializable {

	/**
	 * Add an inhabitant to this Levelable
	 * <P>
	 * 
	 * @param inhab
	 * @return true if the inhabitant was added
	 */
	public boolean addInhabitant(Inhabitant inhab);

	/**
	 * Add to the members
	 * 
	 * @param member
	 * @return true if the Collection of members has changed, false if it did
	 *         not change
	 */
	public boolean addMember(ILevel member);

	/**
	 * Convenience instead of getInhabitants.size();
	 * 
	 * @return number of members of all the lowest ranking Levelable
	 *         <P>
	 * 		s
	 */
	public int getHabitationSize();

	/**
	 * 
	 * @return identification number of this level.
	 */
	public int getID();

	/**
	 * Get a inhabitant of this level at index i
	 * 
	 * @param i
	 * @return the inhabitant
	 */

	public Inhabitant getInhabitant(int i);

	/**
	 * Get a inhabitant of this level
	 * 
	 * @param r
	 * @return the inhabitant
	 */

	public Inhabitant getInhabitant(Random r);

	/**
	 * A set of all inhabitants of this and underlying levels
	 * 
	 * @return all the members of the lowest ranking Levelable
	 *         <P>
	 * 		s that are members of this or sublevels
	 */
	public Set<Inhabitant> getInhabitants();

	/**
	 * Return the members of this level
	 * 
	 * @return members
	 */
	public Set<ILevel> getMembers();

	/**
	 * To get the members with rank
	 * 
	 * @param rank
	 * @return members
	 */
	public Set<ILevel> getMembers(int rank);

	/**
	 * Get the rank (in the hierarchy of this level)
	 * 
	 * @return rank of this level (0....n) for n levels
	 */
	public int getRank();

	/**
	 * The size of a levelable is determined by the number of members, which are
	 * either levelables of one rank lower or inhabitants
	 * 
	 * @return number of members
	 */
	public int getSize();

	/**
	 * 
	 * @return the level to which this level belongs
	 */
	public ILevel getSuperlevel();

	/**
	 * Compares to an object
	 * 
	 * @param inhab
	 * @return true if member is a member of one of the lowest ranking Levelable
	 *         <P>
	 *         that is a member of this of its sublevels
	 */
	public boolean hasInhabitant(Inhabitant inhab);

	/**
	 * 
	 * @param member
	 * @return true if a member false if not
	 */
	public boolean hasMember(ILevel member);

	/**
	 * The number of ranks is free. This method will give the result whether or
	 * not a superlevel is set (it infers from a superlevel == null that this
	 * level is the highest)
	 * 
	 * @return true if superlevel == null false otherwise
	 */
	public boolean isHighest();

	/**
	 * If one wants to whether or not the Levelable has the lowest rank
	 * 
	 * @return true if rank = 0 false if rank >0
	 */
	public boolean isLowest();

	/**
	 * 
	 * @param inhab
	 * @return true if the inhabitant was removed
	 */
	public boolean removeInhabitant(Inhabitant inhab);

	/**
	 * Remove from members
	 * 
	 * @param member
	 * @return true if the Collection of members has changed, false if it did
	 *         not change
	 */
	public boolean removeMember(ILevel member);

	/**
	 * Remove the current superlevel from this level
	 */
	public void removeSuperlevel();

	/**
	 * Set's the identification number of this level
	 * 
	 * @param id
	 */
	public void setID(int id);

	/**
	 * Set the level's rank starting from the lowest (=0) <br>
	 * 
	 * @param level
	 */
	public void setRank(int level);

	/**
	 * 
	 * @param superLevel
	 * 
	 */
	public void setSuperlevel(ILevel superLevel);

}
