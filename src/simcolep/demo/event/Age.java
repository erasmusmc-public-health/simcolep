/*
 * This file is part of SIMCOLEP, a simulation model developed by the Department of Public Health,
 * Erasmus MC, University Medical Center Rotterdam, Rotterdam, The Netherlands.
 *
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/
 * or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA
 *
 * You are free to:
 * Share � copy and redistribute the material in any medium or format
 * Under the following terms:
 * Attribution � You must give appropriate credit, provide a link to the license, and indicate if changes were
 * made. You may do so in any reasonable manner, but not in any way that suggests the licensor endorses
 * you or your use.
 * NonCommercial � You may not use the material for commercial purposes.
 * NoDerivatives � If you remix, transform, or build upon the material, you may not distribute the modified material.
 */

/*
 * Created on 10-Nov-2005
 *
 */
package simcolep.demo.event;

import nl.mgz.simulation.discreteevent.Event;
import nl.mgz.simulation.discreteevent.Targetable;
import simcolep.asgard.Odin;
import simcolep.demo.AbstractPopulationRegister;
import simcolep.demo.Person;
import simcolep.exceptions.SimulationException;
import simcolep.tools.message.Message;
import simcolep.tools.message.Ratatosk;

/**
 *
 * This Event solely notifies that the target has gone to one higher age class
 *
 *
 * @author ir E.A.J. Fischer
 * @version 0.1.2 -- Last Modified 10-Nov-2005
 */

public final class Age extends Event {

	/** serial Version uid */
	private static final long serialVersionUID = 6611542062092888880L;

	/**
	 * The higher the priority the more important and sooner an event is
	 * executed as compared to events at exactly the same moment.<br>
	 * To have a consistancy in prioritizing the following rules are used;
	 * <li>Priority from high to low: Simulation events > Disease events >
	 * Demographic
	 * <li>Priority of simulation events > 500
	 * <li>Priority of disease events > 200
	 * <li>Priority of demographic events > 100
	 * <li>further ordering is logical in biological or epidemiological sense
	 * 
	 * @see nl.mgz.simulation.discreteevent.Event.priority
	 */
	private static final int PRIORITY = 101;

	/**
	 * 
	 * @param classWidth
	 * @param age
	 * @param currenttime
	 * @return
	 */
	public static double calcAgeTime(double classWidth, double age, double currenttime) {
		/* the current time */
		double nextTime = currenttime;
		/* add the class widht */
		nextTime += classWidth;
		/*
		 * subtract the part of the class width that was already passed due for
		 * some one of this age
		 */
		nextTime -= age % classWidth;
		/* return this time */
		return nextTime;
	}

	/**
	 * 
	 *
	 */
	public Age() {

	}

	/**
	 * 
	 * @param source
	 * @param target
	 * @param time
	 */
	public Age(Object source, Targetable target, double time) {

		super(source, target, time);

	}

	@Override
	public void execute() {

		// check whether this person already died
		Person target = (Person) super.getTarget();

		if (Odin.inSafeMode())
			if (target.getDeathTime() <= this.getTime())
				throw new SimulationException(SimulationException.SHOULDBEDEAD);

		// call that this person has changed age class
		Ratatosk.notify(Message.formulate(Message.AGE), super.getTarget(), this);

		// move to next age class
		// add one to the age class
		target.setAgeclass(target.getAgeclass() + 1);

		// schedule the next for this person if not already dead
		if (super.getTime() + AbstractPopulationRegister.ageclasswidth > target.getBdate() + target.getDeathAge())
			return;

		// next ageing event
		Age a = new Age(this, super.getTarget(), super.getTime() + AbstractPopulationRegister.ageclasswidth);
		Odin.getEventManager().schedule(a);

	}

	/**
	 * @see nl.mgz.simulation.discreteevent.Event#getPriority()
	 */
	@Override
	public int getPriority() {
		return PRIORITY;
	}

}
