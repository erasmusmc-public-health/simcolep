/*
 * This file is part of SIMCOLEP, a simulation model developed by the Department of Public Health, 
 * Erasmus MC, University Medical Center Rotterdam, Rotterdam, The Netherlands.
 *
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/ 
 * or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA
 *
 * You are free to:
 * Share � copy and redistribute the material in any medium or format
 * Under the following terms:
 * Attribution � You must give appropriate credit, provide a link to the license, and indicate if changes were 
 * made. You may do so in any reasonable manner, but not in any way that suggests the licensor endorses
 * you or your use.
 * NonCommercial � You may not use the material for commercial purposes.
 * NoDerivatives � If you remix, transform, or build upon the material, you may not distribute the modified material.
 */

/*
 * Created on 04-Nov-2005
 *
 */
package simcolep.demo.event;


import java.io.Serializable;

import nl.mgz.simulation.discreteevent.Event;
import nl.mgz.simulation.discreteevent.Targetable;

import simcolep.asgard.Odin;
import simcolep.demo.Person;

/**
 * Random movement event will be executed if the settings and variables for movement after marriage allow it. 
 * (Either not allowed but unmarried or always allowed)
 * @author ir. E.A.J. Fischer
 * @version 1.0
 */
public final class Move extends Event implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -5325656980266217451L;
    
    /**
	 * The higher the priority the more important and sooner an event is executed as compared to events at exactly the same moment.<br>
     * To have a consistancy in prioritizing the following rules are used;
     * <li> Priority from high to low: Simulation events > Disease events > Demographic
     * <li> Priority of simulation events > 500
     * <li> Priority of disease events > 200
     * <li> Priority of demographic events > 100
     * <li> further ordering is logical in biological or epidemiological sense
     *  
     * @see nl.mgz.simulation.discreteevent.Event.priority
     * */
	private static final int PRIORITY = 104;

    /**
     * 
     *
     */
    public Move()
    {
     
    }
    /**
     * 
     * @param source
     * @param target
     * @param time
     */
    public Move(Object source, Targetable target, double time) {
        super(source, target, time);
       
    }

    @SuppressWarnings("unchecked")
	@Override
    public void execute() {
	    //cast the target
		Person p = (Person)super.getTarget();
	            
	      //execute the move
	    Odin.getOdin().getLoki().moveRandom(p);
    }

    /**
     * @see nl.mgz.simulation.discreteevent.Event#getPriority()
     */
     @Override
     public int getPriority()
     {
     	return PRIORITY;
     }

}
