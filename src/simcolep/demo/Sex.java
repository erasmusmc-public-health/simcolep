package simcolep.demo;

public enum Sex {

	MALE(0), FEMALE(1), BOTH(2);

	private final int value;

	private Sex(int value) {
		this.value = value;
	}

	public int getValue() {
		return value;
	}
}
