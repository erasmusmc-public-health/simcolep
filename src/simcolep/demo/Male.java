/*
 * This file is part of SIMCOLEP, a simulation model developed by the Department of Public Health,
 * Erasmus MC, University Medical Center Rotterdam, Rotterdam, The Netherlands.
 *
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/
 * or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA
 *
 * You are free to:
 * Share � copy and redistribute the material in any medium or format
 * Under the following terms:
 * Attribution � You must give appropriate credit, provide a link to the license, and indicate if changes were
 * made. You may do so in any reasonable manner, but not in any way that suggests the licensor endorses
 * you or your use.
 * NonCommercial � You may not use the material for commercial purposes.
 * NoDerivatives � If you remix, transform, or build upon the material, you may not distribute the modified material.
 */

/*
 * Created on 09-Nov-2005
 *
 */
package simcolep.demo;

import simcolep.asgard.Loki;
import simcolep.asgard.Odin;
import simcolep.exceptions.SimulationException;

/**
 * A male person
 *
 * @author ir. E.A.J. Fischer
 * @version 1.0
 *
 */
public class Male extends Person {

	/** serialVersionUID */
	private static final long serialVersionUID = -7553052289868838098L;

	/**
	 *
	 */
	@SuppressWarnings("unchecked")
	public final static Person stripMale(Person p) {
		if (p.getSex() != Sex.MALE)
			return p;

		((Male) p).spouse = null;

		return p;
	}

	/** spouse of this male */
	private Female spouse = null;

	/**
	 *
	 * @param chick
	 * @throws NullPointerException
	 *             - if chick == null
	 * @throws IllegalArgumentException
	 *             - if spouse !=null (cannot add two spouses)
	 */
	@Override
	@SuppressWarnings("unchecked")
	public final void addSpouse(Person chick) {

		if (chick == null)
			throw new SimulationException("cannot marry null");

		if (this.spouse != null)
			throw new SimulationException("cannot add two spouses to a man");

		// set spouse
		this.spouse = (Female) chick;
		// set evermarried
		this.evermarried = true;
		// not engaged anymore
		this.engaged = false;

		// check if moving when widowed
		this.movesaswidow = Loki.getMovesasWidow(this);

		this.addtoHistory(Odin.getCurrentTime(), "spouse added", spouse);
	}

	@Override
	public final void die() {
		if (this.children != null) {
			for (Person child : this.children) {
				child.removeFather();
			}
		}

		super.die();
	}

	/**
	 * Sex
	 */
	@Override
	public final Sex getSex() {
		return Sex.MALE;
	}

	/**
	 *
	 * @return spouse
	 */
	@Override
	public final Person getSpouse() {
		return this.spouse;
	}

	/**
	 * remove the spouse by setting the reference to null
	 *
	 */
	@Override
	@SuppressWarnings("unchecked")
	public final void removeSpouse() {
		// remove the reference to the spouse
		this.spouse = null;

		// removing and adding to singles list etc. is done after execution of
		// spouse death event
		// this enables us to be more flexible for example with widowers that do
		// nothing until a certain time.
		this.addtoHistory(Odin.getCurrentTime(), "spouse removed");
	}

}
