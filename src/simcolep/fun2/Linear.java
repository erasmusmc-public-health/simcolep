/*
 * This file is part of SIMCOLEP, a simulation model developed by the Department of Public Health, 
 * Erasmus MC, University Medical Center Rotterdam, Rotterdam, The Netherlands.
 *
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/ 
 * or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA
 *
 * You are free to:
 * Share � copy and redistribute the material in any medium or format
 * Under the following terms:
 * Attribution � You must give appropriate credit, provide a link to the license, and indicate if changes were 
 * made. You may do so in any reasonable manner, but not in any way that suggests the licensor endorses
 * you or your use.
 * NonCommercial � You may not use the material for commercial purposes.
 * NoDerivatives � If you remix, transform, or build upon the material, you may not distribute the modified material.
 */

/**
 * created 18/11/04
 */
package simcolep.fun2;


/**
 *Continuous Function with:<br></br>
 *<i>Y = intercept + slope* X</i> 
 * @author ir E.A.J.Fischer
 * @version 0.1.1 -- Last Modified 29-04-05
 */
public final class Linear 
			extends ContinuousFunction 
				implements Distributionable{

	private static final String SLOPE = "slope"; //$NON-NLS-1$
	
	private static final String INTERCEPT = "intercept"; //$NON-NLS-1$
	/**
	 * 
	 */
	private static final long serialVersionUID = 3729609336411873075L;
	/**
	 * intercept
	 */
	protected double intercept = 0.0;
	/**
	 * slope 
	 */
	protected double slope = 0.0;
	/**
	 * 
	 */
	public Linear() {
		super();
		
	}
	/**
	 * 
	 * @param intercept
	 * @param slope
	 */
	public Linear(double intercept, double slope) 
	{
		super();
		this.intercept = intercept;
		this.slope = slope;
	}
	/**
	 * intercept default = 0.0
	 * @param slope
	 */
	public Linear(double slope) 
	{
		super();
		this.slope = slope;
	}
	
	/**
	 * 
	 * @param cfi
	 */
	public Linear(ContinuousFunctionInput cfi) 
	{
		super();
		setFunction(cfi);
	}
	
	/**
	 * @param name
	 * @param value
	 * @see simcolep.fun2.ContinuousFunction#changeParameter(String, double)
	 */
	public void changeParameter(String name, double value)
	{
		if(name.equals(INTERCEPT))
		{
			this.intercept = value;
		}else if (name.equals(SLOPE))
		{
			this.slope = value;
		}else if(name.equalsIgnoreCase("N0"))
        {
		    this.intercept = value;
        }else throw new IllegalArgumentException("unknown parameter: " + name);
	}
	
	/**
	 * @see simcolep.fun2.ContinuousFunction#setFunction(ContinuousFunctionInput)
	 */
	public void setFunction(ContinuousFunctionInput cfi)
	{
		this.intercept = cfi.getParameter(INTERCEPT).doubleValue();
		this.slope = cfi.getParameter(SLOPE).doubleValue();
	}
	

	/** 
	 * @see simcolep.fun2.RateFunctionable#getY(double)
	 */
	public double getY(double X) {
		
		return (this.intercept+X*this.slope);
	}

	

	/**
	 * @see simcolep.fun2.Function#getX(double)
	 */
	public double getX(double y) {
		return ((y-this.intercept)/this.slope);
	}

	
	/**
	 * 
	 */
	public double getXmax()
	{
		return super.getXmax();
	}
	/** 
	 * @see simcolep.fun2.RateFunctionable#getMax()
	 */
	public double getMax() {
		if (slope < 0.0){return intercept;}
		return Double.POSITIVE_INFINITY;
	}
	
	/**
	 * @see simcolep.fun2.Function#getDerivative(double[])
	 */
	public double getDerivative(double x) {
		return this.slope;
	}
	/** 
	 * @see simcolep.fun2.Function#getIntegrated(double[])
	 */
	public double getIntegrated(double x) {
		return this.intercept*x +0.5*this.slope*Math.pow(x,2);
		
	}
	/** 
	 * @see simcolep.fun2.Function#getInvIntegrated(double[])
	 */
	public double getInvIntegrated(double y) {
		
		return (1/this.slope)*(this.intercept+Math.sqrt(Math.pow(this.intercept,2)+2*this.slope*y));
	}
	/**
	 * @see simcolep.fun2.ContinuousFunction#getParameter(java.lang.String)
	 */
	public double getParameter(String name) {
		if(name.equalsIgnoreCase(INTERCEPT)) 
			return this.intercept;
		if(name.equalsIgnoreCase(SLOPE))
			return this.slope;
		return Double.NaN;
	}
    
	/**
     * 
     * @param y
     * @return x = getX(y)
     */
    public double drawX(double y) {
        if(y>1||y<0)
        	return Double.NaN;
        return getX(y);
    }
	/**
     * 
     * @param y
     * @return x = getX(y)
     */
    public double drawX(double[] y) {
        return this.drawX(y[0]);
    }
    
    /**
     * @see simcolep.fun2.Distributionable#drawXconditional(double[])
     */
    public double drawXconditional(double[] v) {
        double[] newY = {
        	getF(new double[]{v[1]})+v[0]*(1-getF(new double[]{v[1]}))};
        return drawX(newY);

    }
    
    /**
     * 
     * @see simcolep.fun2.Distributionable#mean()
     */
    public double mean() {
        
        return Double.NaN;
    }
    @Override
    public Linear clone() {
        
        return new Linear(this.intercept,this.slope);
    }

    
    /**
     * @see simcolep.fun2.Function#equalsFunction(simcolep.fun2.Function)
     */
     public boolean equalsFunction(Function f) {
        if(!f.getClass().equals(this.getClass()))return false;
        Linear lf = (Linear)f;
               
        return (Math.abs(this.intercept - lf.intercept)<Pythagoras.getDefaultPrecision() &&Math.abs(this.slope - lf.slope)<Pythagoras.getDefaultPrecision());
    }
     /**
      * @see simcolep.fun2.Distributionable#getF(double)
      */
 	public double getF(double d) {
 			return this.getIntegrated(d);
 	}
    /**
     * @see simcolep.fun2.Distributionable#getF(double)
     */
	public double getF(double[] d) {
			return this.getIntegrated(d[0]);
	}
	

}
