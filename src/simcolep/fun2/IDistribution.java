/*
 * This file is part of SIMCOLEP, a simulation model developed by the Department of Public Health, 
 * Erasmus MC, University Medical Center Rotterdam, Rotterdam, The Netherlands.
 *
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/ 
 * or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA
 *
 * You are free to:
 * Share � copy and redistribute the material in any medium or format
 * Under the following terms:
 * Attribution � You must give appropriate credit, provide a link to the license, and indicate if changes were 
 * made. You may do so in any reasonable manner, but not in any way that suggests the licensor endorses
 * you or your use.
 * NonCommercial � You may not use the material for commercial purposes.
 * NoDerivatives � If you remix, transform, or build upon the material, you may not distribute the modified material.
 */

package simcolep.fun2;


import simcolep.asgard.Odin;
import simcolep.exceptions.SimulationException;


/**
 * Interval dependent distribution. 
 * This distribution will use the distribution in the current interval and adapt to distribution of next intervals if value extends current interval 
 *
 *
 * @author ir E.A.J. Fischer
 * @version 1.2
 *
 */
public class IDistribution
				extends cFunction
					implements Distributionable
						 {


	/**
	 *
	 */
	private static final long serialVersionUID
								= 1992184524261486395L;

	/**
	 *
	 * @param starts
	 * @param functions
	 */
	public IDistribution(double[] starts, Function[] functions) {
		super(starts,functions);
	}

	/**
	 *
	 *
	 */
	public IDistribution()
	{
		super();
	}

	/**
	 * @param p: p[0] Random uniform variate p[1] current value of 0 
	 */
	public double drawX(double p) {
		return this.drawX(new double[]{p,0});
	}
	/**
	 * @param p: p[0] Random uniform variate p[1] current value of x 
	 */
	public double drawX(double[] p) {
		if(p[0] < 0.0 || p[0] > 1.0)
			throw new SimulationException(
					SimulationException.RANDOMVARIATEINTERVAL + p[0]);

		//get the interval of the current time
		Interval i =
			this.getInterval(p[1]);

		//get current function
		Distributionable d
				= (Distributionable)i.f;

		//draw
		double value = d.drawX(new double[]{p[0]});
	
		//check if within interval or smaller
		while(i.compareTo(value + p[1])>0)
		{

			//get the border of this interval
			double[] a_border = {i.end-p[1]};

			//else determine remaining variate at end interval
			double chi = 
				((Distributionable)i.f).getF(a_border);

			//get next interval
			if(this.funcs.indexOf(i)+1<this.funcs.size())
				i = this.funcs.get(this.funcs.indexOf(i)+1);
			else 
				throw new IndexOutOfBoundsException();

			double chi_accent
					= ((Distributionable)i.f).getF(a_border);

			//if the border is infinite => chi = 1 and chi_accent = 1
			//as all distributions go to 1 in the limit to infinity
			if(Double.isInfinite(a_border[0]))
				{
					chi = 1.0;
					chi_accent =1.0;
				}

			//determine the remainder
			double y = p[0] - chi;

			//calculate the new variate
			double[] U_ac =
				{chi_accent + y * (1.0-chi_accent) / (1.0-chi)};

			//when chi = 1.0 U_ac equals 1
			if(!(chi < 1.0))
				U_ac[0] = chi_accent;

			value =
				((Distributionable) i.f).drawX(U_ac);


			if(Double.isNaN(value))
				throw new SimulationException("value = "+ value);
		}
		//return the value
		return value;
	}



	/**
	 * @param p: p[0] random variate, p[1] condition on x, p[2] current value of x
	 * @see simcolep.fun2.Distributionable#drawXconditional(double[])
	 */
    public double drawXconditional(double[] p) {
        double[] newY = 
        	{getF(new double[]{p[1]})+p[0]*(1-getF(new double[]{p[1]})),p[2]};
        return drawX(newY);

    }
    /**
     * get the Y-value for the interval containing p
     */
    @Override
    public double getY(double p)
    {
    	
    	Interval i = this.getInterval(p);
    	
    	return i.f.getY(p);
    }
	/**
	 * @see simcolep.fun2.Distributionable#mean()
	 */
	public double mean() {
		return ((Distributionable) this.getInterval(Odin.getCurrentTime()).f).mean();
	}

	/**
	 * Return the cumulative function value of this for value of d
	 */
	public double getF(double d) {

		//get the interval of the current time
		Interval i =
			this.getInterval(0);

		//return
		return((Distributionable)i.f).getF(d);

	}
	/**
	 * Return the cumulative function value of this for value of d
	 */
	public double getF(double[] d) {

		//get the interval of the current time
		Interval i =
			this.getInterval(d[0]);

		//return
		return((Distributionable)i.f).getF(d);

	}

}
