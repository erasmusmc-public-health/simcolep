/*
 * This file is part of SIMCOLEP, a simulation model developed by the Department of Public Health, 
 * Erasmus MC, University Medical Center Rotterdam, Rotterdam, The Netherlands.
 *
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/ 
 * or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA
 *
 * You are free to:
 * Share � copy and redistribute the material in any medium or format
 * Under the following terms:
 * Attribution � You must give appropriate credit, provide a link to the license, and indicate if changes were 
 * made. You may do so in any reasonable manner, but not in any way that suggests the licensor endorses
 * you or your use.
 * NonCommercial � You may not use the material for commercial purposes.
 * NoDerivatives � If you remix, transform, or build upon the material, you may not distribute the modified material.
 */

/*
 * Created on 6-jul-2005
 *
 *
 */
package simcolep.fun2;

import java.util.ListIterator;

/**
 * A function to draw from a set of fractions
 * 
 * @author ir. E.A.J. Fischer
 *@version 0.1.1 -- Last Modified: 6-jul-2005
 */
public final class QFraction 
			extends QDiscreteFunction 
			implements DiscreteInputable{
    /**
	 * 
	 */
	private static final long serialVersionUID = 
		526749785322958337L;
	
	/**
	 *  method modifies the function after given input sothat the function becomes cumulative
	 *  
	 */
	private static DiscreteFunctionInput modifyFunction(DiscreteFunctionInput function) {
	   //there is a slight inconvenience in QDiscreteFunction that one using it as distibrution 
		//should always add a first value that is 0.0 
		//For fractions we can do this automatically
		
		function.addFirst(new DiscreteFunctionInputElement(0, 0));
		
		if(function.isCum())
	    	return (DiscreteFunctionInput)function.clone();
		
		double next = 0.0;
	  
	    ListIterator<DiscreteFunctionInputElement> it = 
	    	function.listIterator();	
	    while(it.hasNext())
	    {
	        
	        //current element
	        DiscreteFunctionInputElement el = 
	        	(DiscreteFunctionInputElement)it.next();
	        //determine the value of the NEXT element
	        next += el.value;
	        //set the current el
	        el.value = next;
	        
	    }
	    
	    function.setCum(true);
	        
	    return (DiscreteFunctionInput)function.clone();
	  }

	/**
	 * 
	 * @param fractions
	 * @return discrete function input with these fractions
	 */
	private static DiscreteFunctionInput createInput(double[][]fractions)
	{
	    DiscreteFunctionInput function = 
	    	new DiscreteFunctionInput(true, null);
	    for(int x =0; x< fractions.length;x++)
	    {
	        function.add(
	        		new DiscreteFunctionInputElement(fractions[x][0],fractions[x][1]));
	    }
	    return function;
	}

	/**
     * @param function
     */
    public QFraction(DiscreteFunctionInput function) {
       super(modifyFunction(function));
       super.interpolate(false);
         }
    
    /**
     * 
     * @param fractions
     */
    public QFraction(double[][] fractions)
    {
        super(modifyFunction(createInput(fractions)));
        super.interpolate(false);
        
        
        
    }
    
  
	@Override
    public DiscreteInputable setFunction(DiscreteFunctionInput dfi)
    {
        return 
        	super.setFunction(modifyFunction(dfi));
        
    }
    
    /**
     * 
     *
     */
    public QFraction()
    {
    	
    }
    
   
    
   
	
	
}
    
    


