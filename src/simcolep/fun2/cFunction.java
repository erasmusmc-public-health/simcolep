/*
 * This file is part of SIMCOLEP, a simulation model developed by the Department of Public Health,
 * Erasmus MC, University Medical Center Rotterdam, Rotterdam, The Netherlands.
 *
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/
 * or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA
 *
 * You are free to:
 * Share � copy and redistribute the material in any medium or format
 * Under the following terms:
 * Attribution � You must give appropriate credit, provide a link to the license, and indicate if changes were
 * made. You may do so in any reasonable manner, but not in any way that suggests the licensor endorses
 * you or your use.
 * NonCommercial � You may not use the material for commercial purposes.
 * NoDerivatives � If you remix, transform, or build upon the material, you may not distribute the modified material.
 */

/**
 * created: 18-Apr-2007
 */
package simcolep.fun2;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import simcolep.exceptions.SimulationException;
import simcolep.tools.PN;

/**
 * cFunction combines different Functions to one function. The value of the
 * cFunction is determined by the functions it combines in discrete time
 * periods.<br>
 * e.g The functions starts with a constant value for a certain time followed by
 * a linear increasing function. The user should take notice to define each
 * function as if it started from x = 0. Thus in our example (now with numerical
 * values) let's say that between x = and x = 10 the function has value 1 and
 * than increases with a coefficient of 0.5. The linear function should than be
 * defined as y = 0.5*(x-10) + 1 or y = 0.5*x - 4 as both give the same value at
 * x = 10 and beyond, starting from value y = 1.
 * 
 * <br>
 * <br>
 * Definition of this function is done by defining a set of function and the
 * value of x at which they start.
 * 
 * @author ir. E.A.J. Fischer
 * @version 1.1
 */
public class cFunction extends Function {

	protected final class Interval implements Comparable<Interval> {
		/** start of the interval */
		public double start = 0;

		/** end of the interval */
		public double end = 0;

		/** value of the integral of this function from start to end */
		private double integral = Double.NaN;

		/** function with this interval */
		public Function f = null;

		/**
		 *
		 * @param start
		 * @param end
		 * @param func
		 */
		public Interval(double start, double end, Function func) {
			this.start = start;
			this.end = end;
			this.f = func;
		}

		/**
		 * Compare a double with the interval
		 * 
		 * @param d
		 * @return -1 when d <= start, 1 when d > end and 0 if start < d <= end
		 */
		public int compareTo(double d) {

			if (d <= this.start)
				return -1;
			else if (d > this.end)
				return 1;
			else
				return 0;
		}

		/**
		 * returns 1 if the start is larger than the other interval, 0 if equal
		 * -1 if smaller
		 * 
		 * @see java.lang.Comparable#compareTo(T)
		 */
		@Override
		public int compareTo(Interval o) {

			if (o.start < this.start)
				return 1;
			else if (o.start > this.start)
				return -1;
			else
				return 0;

		}

		/**
		 * Calculates the integral of this interval if not done before and
		 * returns it.
		 * 
		 * @return this.integral
		 */
		public double Integral() {
			if (Double.isNaN(this.integral))
				this.integral = this.f.getIntegrated(this.end) - this.f.getIntegrated(this.start);

			return this.integral;
		}

		/**
		 * @see java.lang.Object#toString()
		 */
		@Override
		public String toString() {
			return "[ " + this.start + " - " + this.end + " ]: " + this.f;
		}

		/**
		 * True if d > start and <= end False otherwise
		 * 
		 * @return b
		 **/
		public boolean within(double d) {
			boolean b = (this.compareTo(d) == 0);

			return b;
		}
	}

	/** serialVersionUID */
	private static final long serialVersionUID = 6219817185067082821L;

	/** functions with respectively intervals */
	protected List<Interval> funcs = new LinkedList<Interval>();

	/**
	 *
	 */
	public cFunction() {
		super();
	}

	/**
	 *
	 * @param starts
	 *            start of the interval
	 * @param fs
	 *            functions
	 */
	public cFunction(double[] starts, Function[] fs) {
		if (starts.length != fs.length)
			throw new IllegalArgumentException("number of interval starts and functions " + "does not correspond!");

		for (int i = 0; i < starts.length; i++) {
			double start = starts[i];
			double end = 0;
			if (i + 1 == starts.length)
				end = Double.POSITIVE_INFINITY;
			else
				end = starts[i + 1];

			Interval interval = new Interval(start, end, fs[i]);

			funcs.add(interval);
		}

	}

	/**
	 * @see simcolep.fun2.Function#changeParameter(java.lang.String, double)
	 */
	@Override
	public void changeParameter(String name, double value) {
		throw new SimulationException(SimulationException.NOTIMPLEMENTED);

	}

	/**
	 * @see simcolep.fun2.Function#clone()
	 */
	@Override
	public Function clone() {
		double[] starts = new double[this.funcs.size()];
		Function[] functionados = new Function[this.funcs.size()];

		int index = -1;

		for (Interval i : this.funcs) {
			starts[index++] = i.start;
			functionados[index] = i.f.clone();
		}
		return new cFunction(starts, functionados);
	}

	/**
	 * @see simcolep.fun2.Function#equalsFunction(simcolep.fun2.Function)
	 */
	@Override
	public boolean equalsFunction(Function f) {
		if (!f.getClass().equals(this.getClass()))
			return false;

		return f.equals(this);
	}

	/**
	 * @see simcolep.fun2.Function#getDerivative(double)
	 */
	@Override
	public double getDerivative(double X) {
		return this.getInterval(X).f.getDerivative(X);
	}

	/**
	 * @see simcolep.fun2.Function#getIntegrated(double)
	 */
	@Override
	public double getIntegrated(double X) {

		double res = 0;

		for (Interval i : funcs) {
			if (i.end <= X)
				res += i.Integral();
			else {
				res += (i.f.getIntegrated(X) - i.f.getIntegrated(i.start));
				break;
			}
		}
		return res;
	}

	/**
	 * @see simcolep.fun2.Function#getInvIntegrated(double)
	 */
	@Override
	public double getInvIntegrated(double Y) {
		if (true)
			throw new SimulationException(SimulationException.NOTIMPLEMENTED);
		Interval i = this.getIntervalInverseIntegration(Y);

		// get adjusted y -value
		double y_ac = Y - (this.getIntegrated(i.start) - i.f.getIntegrated(i.start));

		// return the value
		return i.f.getInvIntegrated(y_ac);

	}

	/**
	 * @see simcolep.fun2.Function#getX(double)
	 */
	@Override
	public double getX(double y) {
		throw new SimulationException(SimulationException.NOTIMPLEMENTED);
	}

	/**
	 * @see simcolep.fun2.Function#getY(double)
	 */
	@Override
	public double getY(double X) {
		// return the value of that function
		return this.getInterval(X).f.getY(X);
	}

	/**
	 * @see simcolep.fun2.Function#inputGroup()
	 */
	@Override
	public int inputGroup() {
		return Function.COMBINED_FUNCTION_INPUT;
	}

	/**
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {

		String combFunction = this.getName() + PN.LINESEPARATOR + "[";

		for (Interval i : funcs) {
			combFunction += PN.TAB + i.toString() + PN.LINESEPARATOR;
		}

		return "]" + combFunction;
	}

	/**
	 * Get the interval in which x is an element
	 * 
	 * @param x
	 * @return i
	 */
	protected Interval getInterval(double x) {
		// get the correct function
		Iterator<Interval> fit = funcs.iterator();

		Interval i = fit.next();

		while (i.end <= x && fit.hasNext()) {
			i = fit.next();
		}
		return i;
	}

	/**
	 * Get the interval in which x is an element
	 *
	 * @param x
	 * @return i
	 */
	private Interval getIntervalInverseIntegration(double y) {
		// get the correct function
		Iterator<Interval> fit = funcs.iterator();

		Interval i = fit.next();

		double Fx = 0;

		while (Fx < y && fit.hasNext()) {
			i = fit.next();
			// add the value of the integral
			Fx += i.integral;
		}
		return i;
	}
}
