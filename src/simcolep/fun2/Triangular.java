/*
 * This file is part of SIMCOLEP, a simulation model developed by the Department of Public Health, 
 * Erasmus MC, University Medical Center Rotterdam, Rotterdam, The Netherlands.
 *
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/ 
 * or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA
 *
 * You are free to:
 * Share � copy and redistribute the material in any medium or format
 * Under the following terms:
 * Attribution � You must give appropriate credit, provide a link to the license, and indicate if changes were 
 * made. You may do so in any reasonable manner, but not in any way that suggests the licensor endorses
 * you or your use.
 * NonCommercial � You may not use the material for commercial purposes.
 * NoDerivatives � If you remix, transform, or build upon the material, you may not distribute the modified material.
 */
package simcolep.fun2;

/**
 * <i>f(x) =
 * <li> 0 if x < start
 * <li> area * 2 *(x-start)/((end-start)*(max-start)) if start <= x <= max
 * <li> area* 2*(end-x))/((end-start)*(end-max)) if max< x<=end
 * <li> 0 of x > end  
 * </i>
 * @author ir E.A.J.Fischer
 * @version 0.1.1 -- Last Modified 29-04-05
 * 
 */
public final class Triangular 
			extends ContinuousFunction 
					implements Distributionable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 
		-9008691796883536753L;
	/**start of the triangle*/
	protected double start=0;
	/**end of the triangle*/
	protected double end=0;
	/**area underneath the triangle*/
	protected double area =1.0;
	/**x value where the peek of the triangle is*/
	protected double max =0;	
	/**
	 * Method Triangular
	 *
	 *
	 * @param start
	 * @param end
	 * @param max <br>
	 * <b>area</b> = default = 1.0
	 *
	 */
	public Triangular(double start, double end,	double max) 
	{
		this.start = start;
		this.end = end;
		this.area = 1.0;
		this.max = max;
	}
/**
	 *
	 *For an area under the function of more than one use this constructor
	 *which is for RateFunctions usefull for example birthrate
	 *(e.g. not 1 child in life time)
	 *
	 * @param start
	 * @param end
	 * @param area 
	 * @param max
	 *
	 */
	public Triangular(double start, double end, double max, 
			double area) {
		this.start = start;
		this.end = end;
		this.area = area;
		this.max = max;
	}
	
	/**
	 *
	 *
	 * @param cfi
	 *
	 */
	public Triangular(ContinuousFunctionInput cfi) {
		setFunction(cfi);
	}
	/**
	 *
	 *
	 */
	public Triangular() {
		
	}

	/**
	 * @param cfi
	 * @see ContinuousFunction#setFunction(ContinuousFunctionInput)
	 */
	public void setFunction(ContinuousFunctionInput cfi)
	{
		this.start = cfi.getParameter("start").doubleValue();
		this.end = cfi.getParameter("end").doubleValue();
		this.max = cfi.getParameter("max").doubleValue();
		if(cfi.getParameter("area")!=null)
		{
			this.area = cfi.getParameter("area").doubleValue();
		}
	}
	
	/**
	 * @param name
	 * @param value
	 * 
	 * @see ContinuousFunction#changeParameter(String, double)
	 */
	public void changeParameter(String name, double value)
	{
		if(name.equals("start")){
			this.start = value;
		}else if(name.equals("end"))
		{
			this.end = value;
		}else if(name.equals("max"))
		{
			this.max = value;
		}else if(name.equals("area"))
		{
			this.area = value;
		}else if(name.equalsIgnoreCase("N0"))
        {
            return;
        }else 
        	throw new IllegalArgumentException("unknown parameter: " + name);
	}
	/**
	 * 
	 * @param name
	 * @return parameter value
	 */
	public double getParameter(String name)
	{
		if(name.equalsIgnoreCase("start"))
		{
			return this.start;
		}else if(name.equalsIgnoreCase("end"))
		{
			return this.end;
		}else if(name.equalsIgnoreCase("max"))
		{
			return this.max;
		}else if (name.equalsIgnoreCase("area"))
		{
			return this.area;
		}else{throw new IllegalArgumentException("parameter not found");}
	}
	

	/**
	 * @param X
	 *
	 * @return Yvalue
	 * 
	 *@see Function#getY(double)
	 */
	public double getY(double X) 
	{
		double Yvalue = 0.0;
		if(X >= this.start && X <= this.end)
		{
			if (X > this.max)
		{
			Yvalue = this.area*( (2*(this.end-X))/((this.end-this.start)*(this.end-this.max)));
		} else if (X ==this.max)
		{
			Yvalue = getMax();
		} else if (X < this.max)
		{
			Yvalue = this.area*((2*(X-this.start))/((this.end-this.start)*(this.max-this.start)));
		} } else{Yvalue = 0.0;}
		return Yvalue;
	}

	/**
	 *
	 *
	 * @param Y
	 *
	 * @return Xvalue
	 *
	 *@see Function#getX(double)
	 */
	public double getX(double Y) {
		if (Y > getMax())
		{
			return (0.5*area*Y*(this.end-this.start)*(this.end-this.max));
		} else if (Y == getMax())
		{
			return this.max;
		} else if (Y < getMax())
		{
			return (this.start + (0.5/area)*Y*(this.end+this.start)*(this.max-this.start)); 
		}
		return Double.POSITIVE_INFINITY;

	}

	 /**
	 * Sets area to 1 during the use of this method
	 * 
	 * @return getInvIntegrated(y)
	 * 
	 * @see simcolep.fun2.Distributionable#drawX(double)
	 */
	public double drawX(double y) {
	    double local_area = this.area;
	    this.area = 1;
	    double x = getInvIntegrated(y);
	    this.area = local_area;
	    return x;
	}
	
	/**
	 * Sets area to 1 during the use of this method
	 * 
	 * @return getInvIntegrated(y)
	 * 
	 * @see simcolep.fun2.Distributionable#drawX(double)
	 */
	public double drawX(double[] y) {
	    return this.drawX(y[0]);
	}
	/**
     * @see simcolep.fun2.Distributionable#drawXconditional(double, double)
     */
    public double drawXconditional(double[] v) {
    	double[] newY = {
        	getF(new double[]{v[1]})+v[0]*(1-getF(new double[]{v[1]}))};
        return drawX(newY);

    }
	/**
	 * 
	 * @return maximum
	 */
	public double getMax()
	{
		double maximum = this.area*(2/(this.end-this.start));
		return maximum;	
	}
	/**
	 * @param x
	 * @return slope for that Xvalue
	 * @see simcolep.fun2.Function#getDerivative(double)
	 */
	public double getDerivative(double x) {
		if(x<this.start||x>this.end)return 0.0;
		if(x<this.max) return this.getY(this.max)/(this.max-this.start);
		if(x>this.max)return -this.getY(this.max)/(this.end-this.max);
		return 0;
	}
	/**
	 * @param x
	 * @return the integration from 0 to Xvalue
	 * @see simcolep.fun2.Function#getIntegrated(double)
	 */
	public double getIntegrated(double x) {
		if(x<this.start) 
		{
			return 0.0;
		}
		else if(x>=this.start&&x<=this.max)
		{
			return 0.5*this.getY(x)*(x-this.start);
		}else if(x>=this.max&&x<this.end)
		{
			return this.area-0.5*this.getY(x)*(this.end-x);
		}else if(x>=this.end) 
		{
			return this.area;
		}
		return Double.NaN;
	}
	/**
	 * @param y
	 * @return the Xvalue for which the integration from 0 to this Xvalue is Yvalue
	 * @see simcolep.fun2.Function#getInvIntegrated(double)
	 */
	public double getInvIntegrated(double y) {
		if(y <0) return Double.NaN;
		else if (y>this.area) return Double.POSITIVE_INFINITY;
		else if (y<=this.getY(this.max))
		{
			return this.start + Math.sqrt((this.end-this.start)*(this.max-this.start)*y/this.area);
		}else if(y> this.getY(this.max)&& y<this.area)
		{
			return this.end - Math.sqrt((1-(y/this.area))*((this.end-this.start)*(this.end-this.max)));
		}else return this.end;
	}
	

    /**
     * @return start+max+end *(1/3) 
     * @see simcolep.fun2.Distributionable#mean()
     */
    public double mean() {
       
        return (1/3)*(this.start+this.max+this.end);
    }
    
    @Override
    public Triangular clone() {
        
        return new Triangular(this.start,this.end,this.max,this.area);
    }
    
    @Override
    public boolean equalsFunction(Function f) {
        if(!this.getClass().equals(f.getClass()))return false;
        Triangular td =(Triangular)f;
        return (Math.abs(this.area-td.area)<Pythagoras.getDefaultPrecision()&&Math.abs(this.end-td.end)<Pythagoras.getDefaultPrecision()&&Math.abs(this.max-td.max)<Pythagoras.getDefaultPrecision()&&Math.abs(this.start-td.start)<Pythagoras.getDefaultPrecision());
    }
   
    /**
     * @see simcolep.fun2.Distributionable#getF(double)
     */
	public double getF(double d) {
		return this.getIntegrated(d);
	}
    /**
     * @see simcolep.fun2.Distributionable#getF(double)
     */
	public double getF(double[] d) {
		return this.getIntegrated(d[0]);
	}
}
