/*
 * This file is part of SIMCOLEP, a simulation model developed by the Department of Public Health, 
 * Erasmus MC, University Medical Center Rotterdam, Rotterdam, The Netherlands.
 *
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/ 
 * or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA
 *
 * You are free to:
 * Share � copy and redistribute the material in any medium or format
 * Under the following terms:
 * Attribution � You must give appropriate credit, provide a link to the license, and indicate if changes were 
 * made. You may do so in any reasonable manner, but not in any way that suggests the licensor endorses
 * you or your use.
 * NonCommercial � You may not use the material for commercial purposes.
 * NoDerivatives � If you remix, transform, or build upon the material, you may not distribute the modified material.
 */

/*
 * Created on 24-Nov-2004
 *
 *
 */
package simcolep.fun2;



/**
 * Quadratic Function given by:
 * <br></br>
 * <i><b>Y</b> = a*<b>X</b><sup>2</sup> +b*<b>X</b>+c</i>
 * 
 * @author ir E.A.J.Fischer
 * @version 0.1.1 -- created 24 november 2004
 *
 * 
 */
public final class Quadratic 
				extends ContinuousFunction 
							{

    /**
	 * 
	 */
	private static final long serialVersionUID = 
		8605553317216577863L;

	
    
	/**
	 * 
	 */
	protected double a = 1;
	/**
	 * 
	 */
	protected double b = 0;
	/**
	 * 
	 */
	protected double c = 0;
	
	/**
	 * default values:
	 * <ul>	
	 * <li>a =1
	 * <li>b = 0
	 *<li>c = 0
	 *</ul>
	 */
	public Quadratic() {
		
	}
	
	/**
	 * default values:
	 * <ul>	
	 * <li>b = 0
	 * 
	 *<li>c = 0
	 *</ul>
	 *@param b
	 */
	public Quadratic(double a)
	{
		this.a = a;
	}
	
	/**
	 * default values:
	 * <ul>	
	 * 
	 *<li>c = 0
	 *</ul>
	 *@param a
	 *@param b
	 */
	public Quadratic(double a, double b)
	{
		this.a = a;
		this.b = b;
	}

	/**
	 * 
	 * @param a
	 * @param b
	 * @param c
 	*/	
	public Quadratic(double a, double b, double c)
	{
		this.a = a;
		this.b = b;
		this.c = c;
	}
	
	/**
	 * 
	 * @param cfi
	 */
	public Quadratic(ContinuousFunctionInput cfi) 
	{
		super();
		setFunction(cfi);
	}
	
	/**
	 * @param name
	 * @param value
	 * @see simcolep.fun2.ContinuousFunction#changeParameter(String, double)
	 */
	public void changeParameter(String name, double value)
	{
		if(name.equals("a")){
			this.a = value;
		}else if(name.equals("b"))
		{
			this.b = value;
		}else if(name.equals("c"))
		{
			this.c = value;
		}else if(name.equalsIgnoreCase("N0"))
        {
            this.c = value;
        }else throw new IllegalArgumentException("unknown parameter: " + name);
	}
	
	/**
	 * @param cfi
	 * @see simcolep.fun2.ContinuousFunction#setFunction(ContinuousFunctionInput)
	 */
	public void setFunction(ContinuousFunctionInput cfi)
	{
		this.a = cfi.getParameter("a").doubleValue();
		this.b = cfi.getParameter("b").doubleValue();
		this.c = cfi.getParameter("c").doubleValue();
	}
	
	
	/** 
	 *<i>	Y = a*X<sup>2</sup> + b*X + c</i> 
	 * @see simcolep.fun2.Function#getY(double)
	 */
	public double getY(double X) 
	{
		return this.a*Math.pow(X,2)+this.b*X+c;	
		
	}

	

	/** 
	 *Gives the Xvalue that is positive and the closest to 0 or the closest to zero when both are positive or negative.
	 *with:<br>
	 *<i>Xvalue = <sup>1</sup>/<sub>2*a</sub>*(-b+/-sqrt(b^2 - 4ac + 4ay)))<i>
	 * @see simcolep.fun2.Function#getX(double)
	 */
	public double getX(double y) {
		double[] x = getXvalues(y);
		if(x[0]>= 0 && x[1]<=0)
			return x[0];
		if(x[0]<=0 && x[1]>=0)
			return x[1];
		if(x[0]<=0 && x[1]<=0)
			return Math.max(x[0], x[1]);
		if(x[0]>=0 && x[1]>=0)
			return Math.min(x[0], x[1]);
		return Double.NaN;
		 
	}

	/**
	 * 
	 * @param Yvalue
	 * @return
	 */
	public double[]getXvalues(double Y)
	{ 	double[] x = new double[2];
		x[0] = (1/(2*this.a))*(-this.b+Math.sqrt(Math.pow(this.b,2)-4*this.a*this.c+4*this.a*Y));
		x[1] = (1/(2*this.a))*(-this.b-Math.sqrt(Math.pow(this.b,2)-4*this.a*this.c+4*this.a*Y));
		return x;
	}
	
	/**
	 * 
	 */
	public double getXmax()
	{
		return super.getXmax();
	}
	/** 
	 * @see simcolep.fun2.RateFunctionable#getMax()
	 */
	public double getMax() {
		if (this.b > 0)
		{
			return Double.POSITIVE_INFINITY;
		} else if (this.b == 0)
		{
			return a;
		} else if (this.b < 0)
		{
			return getY(this.c);
		}
		
		
		return 0;
	}
	
	/**
	 * 
	 * @return nullpoints <br>
	 * i.e.<i> Y = 0.0</i>
	 */
	public double[] getNullPoints()
	{
		double[] nullpoint = {0.0,0.0}; 
		if (this.b > 0.0 && this.a > 0.0)
		{
		    nullpoint[0] = Double.NaN;
		    nullpoint[1] = Double.NaN;
			return nullpoint;
		}else
		{
			nullpoint[0] = (this.c-Math.sqrt(((0-this.a)/(this.b))));
			nullpoint[1] = (this.c+Math.sqrt(((0-this.a)/(this.b))));
			return nullpoint;
		}
		
	}

	/**
	 * @param x
	 * @return 2*a*X+b
	 * @see simcolep.fun2.Function#getDerivative(double)
	 */
	public double getDerivative(double X) {
		
		return 2.0*this.a*X+this.b;
	}

	/**
	 * @param x
	 * @return 1/3 * a*Xvalue^3+1/2*b*Xvalue^2+c*x
	 * @see simcolep.fun2.Function#getIntegrated(double)
	 */
	public double getIntegrated(double X) {
		
		return (1.0/3.0)*this.a*Math.pow(X,3)+(1.0/2.0)*this.b*Math.pow(X,2)+this.c*X;
	}

	/**
	 * Returning the Xvalue that is <b>NOT</b> a complex number 
	 * @param y
	 * @return the non-complex Xvalue
	 * @see simcolep.fun2.Function#getInvIntegrated(double)
	 */
	public double getInvIntegrated(double Y) {
		
		double Xvalue =0.0;
		if(Y == 0)return Xvalue;
		else {
			Xvalue = (1.0/(6.0*this.a))*((-3.0*this.b+9.0*(Pythagoras.pow(this.b,2)-4*this.a*this.c))/Pythagoras.pow(-27*Pythagoras.pow(this.b,3.0)+162.0*this.a*this.b*this.c+324*Pythagoras.pow(this.a,2.0)*Y+Math.sqrt(Pythagoras.pow(-9*Pythagoras.pow(this.b,2)+36*this.a*this.c,3)+729*(Pythagoras.pow(this.b,3)-6*this.a*this.b*this.c-12*Pythagoras.pow(this.a*Y,2.0))),(1.0/3.0))+Pythagoras.pow(-27.0*Pythagoras.pow(this.b,3.0)+162*this.a*this.b*this.c+324*(Pythagoras.pow(this.a,2.0)*Y)+Math.sqrt(Pythagoras.pow(-9*Pythagoras.pow(this.b,2.0)+36*this.a*this.c,3.0)+729*(Pythagoras.pow(this.b,3.0)-6*this.a*this.b*this.c-12.0*Pythagoras.pow(this.a*Y,2.0))),(1.0/3.0)));
				return Xvalue;
		}
		
		
	}

	/* (non-Javadoc)
	 * @see simcolep.fun2.ContinuousFunction#getParameter(java.lang.String)
	 */
	public double getParameter(String name) {
		if(name.equalsIgnoreCase("a"))return this.a;
		if(name.equalsIgnoreCase("b"))return this.b;
		if(name.equalsIgnoreCase("c"))return this.c;
		return Double.NaN;
	}

    /**
     * @see simcolep.fun2.Distributionable#mean()
     */
    public double mean() {
        
        return Double.NaN;
    }

    @Override
    public Quadratic clone() {
        return new Quadratic(this.a,this.b, this.c);
    }

    @Override
    public boolean equalsFunction(Function f) {
        if(!f.getClass().equals(this.getClass()))return false;
        Quadratic qf = (Quadratic)f;
        
        return (Math.abs(this.a-qf.a)<Pythagoras.getDefaultPrecision()&&Math.abs(this.b - qf.b)<Pythagoras.getDefaultPrecision()&&Math.abs(this.c - qf.c)<Pythagoras.getDefaultPrecision());
    }

   

}
