/*
 * This file is part of SIMCOLEP, a simulation model developed by the Department of Public Health, 
 * Erasmus MC, University Medical Center Rotterdam, Rotterdam, The Netherlands.
 *
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/ 
 * or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA
 *
 * You are free to:
 * Share � copy and redistribute the material in any medium or format
 * Under the following terms:
 * Attribution � You must give appropriate credit, provide a link to the license, and indicate if changes were 
 * made. You may do so in any reasonable manner, but not in any way that suggests the licensor endorses
 * you or your use.
 * NonCommercial � You may not use the material for commercial purposes.
 * NoDerivatives � If you remix, transform, or build upon the material, you may not distribute the modified material.
 */

/*
 * Created on 21-Feb-2006
 *
 */
package simcolep.fun2;

import umontreal.iro.lecuyer.probdist.GammaDist;

/**
 * This class uses the distribution from umontreal.iro.lecuyer.probdist and adds the functionality so that we can use it in SIMCOLEP
 * 
 * 
 * @author ir E.A.J. Fischer
 * @version 0.1.2 -- Last Modified 21-Feb-2006
 */
public final class Gamma extends ContinuousFunction 
		implements Distributionable{

    /**serial Version UID*/
    private static final long serialVersionUID = -5232088397317670212L;
    /**the distribution*/
    private transient GammaDist function = null;
    /**alpha*/
    protected double alpha = 1.0;//default alpha = 1.0
    /**lambda*/
    protected double lambda = 1.0; //default lambda = 1.0
    

    /**
     * 
     */
    public Gamma() {
        super();
        this.function = new GammaDist(alpha, lambda);
        
    }

    /**
     * @param cfi
     */
    public Gamma(ContinuousFunctionInput cfi) {
        this.setFunction(cfi);
        
    }

    /**
     * @param cf
     */
    public Gamma(ContinuousFunction cf) {
        if(cf.getClass().equals(Gamma.class))
        {
            this.alpha = ((Gamma)cf).alpha;
            this.lambda = ((Gamma)cf).lambda;
            this.function = ((Gamma)cf).function;
        }else 
        	throw new IllegalArgumentException(
        			"cf is not Gamma "+ cf);
        
    }
    /**
     * 
     * @param alpha
     * @param lambda
     */
    public Gamma(double alpha, double lambda)
    {
    	this.alpha = alpha;
        this.lambda = lambda;
        this.function = new GammaDist(this.alpha,this.lambda);
        
    }

    /**
     * @see simcolep.fun2.ContinuousFunction#setFunction(simcolep.fun2.ContinuousFunctionInput)
     */
    @Override
    public void setFunction(ContinuousFunctionInput CFI) {
       this.alpha = CFI.getParameter("alpha");
       this.lambda = CFI.getParameter("lambda");
       this.function = new GammaDist(this.alpha,this.lambda);

    }

    /**
     * @see simcolep.fun2.ContinuousFunction#changeParameter(java.lang.String, double)
     */
    @Override
    public void changeParameter(String name, double value) {
        if(name.equalsIgnoreCase("alpha"))
        {
            this.alpha = value;
            this.function.setParams(value,this.function.getLambda(), this.function.decPrec);
        }else if(name.equalsIgnoreCase("lambda"))
        {
            this.lambda = value;
            this.function.setParams(this.function.getAlpha(),value, this.function.decPrec);
        }

    }

    /**
     * @see simcolep.fun2.ContinuousFunction#getParameter(java.lang.String)
     */
    @Override
    public double getParameter(String name) {
        if(name.equalsIgnoreCase("alpha")) return this.alpha;
        else if(name.equalsIgnoreCase("lambda")) return this.lambda;
        else throw new IllegalArgumentException("unknown parameter "+name );
    }

    /**
     * @see simcolep.fun2.ContinuousFunction#getY(double)
     */
    @Override
    public double getY(double X) {
          return this.function.density(X);
    }

    /**
     * @see simcolep.fun2.ContinuousFunction#getX(double)
     */
    @Override
    public double getX(double y) {
       
        return -Double.NaN;
    }



    /**
     * @see simcolep.fun2.Function#getDerivative(double)
     */
    @Override
    public double getDerivative(double x) {
        return -Double.NaN;
    }

    /**
     * @see simcolep.fun2.Function#getIntegrated(double)
     */
    @Override
    public double getIntegrated(double x) {
      
        return this.function.cdf(x);
    }

    /**
     * @see simcolep.fun2.Function#getInvIntegrated(double)
     */
    @Override
    public double getInvIntegrated(double y) {
       
    	if(y < 0) return 0;
    	if(y >= 1 ) return Double.POSITIVE_INFINITY;
        return this.function.inverseF(y);
    }

    /**
     * @see simcolep.fun2.Function#clone()
     */
    @Override
    public Function clone() {
        Gamma g = new Gamma(this.alpha,this.lambda);
        return g;
    }

    /**
     * @see simcolep.fun2.Function#equalsFunction(simcolep.fun2.Function)
     */
    @Override
    public boolean equalsFunction(Function f) {
        boolean b = f.getClass().equals(this.getClass());
        if(!b)return b;
        Gamma g =(Gamma) f;
         b = (this.alpha-g.alpha)<Pythagoras.getDefaultPrecision();
         if(!b)return b;
         b = (this.lambda-g.lambda)<Pythagoras.getDefaultPrecision();
         if(!b)return b;
        return b;
    }

    /**
     * @see simcolep.fun2.Distributionable#drawX(double)
     */
    public double drawX(double y) {
         return this.function.inverseF(y);
           }

    /**
     * @see simcolep.fun2.Distributionable#drawX(double)
     */
    public double drawX(double[] y) {
         return this.function.inverseF(y[0]);
           }
    /**
     * @see simcolep.fun2.Distributionable#drawXconditional(double[])
     */
    public double drawXconditional(double[] v) {
        double[] newY = 
        	{getF(new double[]{v[1]})+v[0]*(1-getF(new double[]{v[1]}))};
        return drawX(newY);

    }
   /**
    * 
    * @return this.alpha*this.lambda
    */
    public double mean() {
        return this.alpha/this.lambda;
    }
    
    /**
     * @see simcolep.fun2.Distributionable#getF(double)
     */
    public double getF(double d) {
			return this.function.cdf(d);
	 }
    /**
     * @see simcolep.fun2.Distributionable#getF(double)
     */
    public double getF(double[] d) {
			return this.function.cdf(d[0]);
	 }

    
}
