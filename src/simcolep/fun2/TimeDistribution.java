/*
 * This file is part of SIMCOLEP, a simulation model developed by the Department of Public Health, 
 * Erasmus MC, University Medical Center Rotterdam, Rotterdam, The Netherlands.
 *
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/ 
 * or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA
 *
 * You are free to:
 * Share � copy and redistribute the material in any medium or format
 * Under the following terms:
 * Attribution � You must give appropriate credit, provide a link to the license, and indicate if changes were 
 * made. You may do so in any reasonable manner, but not in any way that suggests the licensor endorses
 * you or your use.
 * NonCommercial � You may not use the material for commercial purposes.
 * NoDerivatives � If you remix, transform, or build upon the material, you may not distribute the modified material.
 */
package simcolep.fun2;

import simcolep.asgard.Idunn;

/**
 * @author Egil
 *
 */
public class TimeDistribution 
	extends IDistribution 	
		implements Distributionable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5671092707690230428L;

	/**
	 * @param starts
	 * @param functions
	 */
	public TimeDistribution(double[] starts, Function[] functions) {
		super(starts, functions);
			}

	/**
	 * 
	 */
	public TimeDistribution() {
			}
	
	/**
	 * @param p: p[0] Random uniform variate p[1] current value of x 
	 */
	public double drawX(double[] p) {
			return super.drawX(new double[]{p[0],Idunn.getCurrentTime()});
	}

	/**
	 * @param p: p[0] random variate, p[1] condition on x, p[2] current value of x
	 * @see simcolep.fun2.Distributionable#drawXconditional(double[])
	 */
	public double drawXconditional(double[] p) {
	    
		return super.drawXconditional(new double[]{p[0],p[1],Idunn.getCurrentTime()});
			
	}
}
