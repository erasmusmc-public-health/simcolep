/*
 * This file is part of SIMCOLEP, a simulation model developed by the Department of Public Health,
 * Erasmus MC, University Medical Center Rotterdam, Rotterdam, The Netherlands.
 *
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/
 * or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA
 *
 * You are free to:
 * Share � copy and redistribute the material in any medium or format
 * Under the following terms:
 * Attribution � You must give appropriate credit, provide a link to the license, and indicate if changes were
 * made. You may do so in any reasonable manner, but not in any way that suggests the licensor endorses
 * you or your use.
 * NonCommercial � You may not use the material for commercial purposes.
 * NoDerivatives � If you remix, transform, or build upon the material, you may not distribute the modified material.
 */

package simcolep.fun2;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import simcolep.tools.PN;

/**
 * Class <code><b>Archimedes</b></code> (named after the great ancient Greek
 * engineer "Eureka, I've got it") is a class that allows the user to specify
 * function and distributions and place them with a name in the Archimedes
 * class. After 'telling' Archi (Method tellArchi) you can 'ask' Archi to give
 * the function again and use it (Method askArchi) <br>
 *
 * @author ir E.A.J.Fischer
 * @version 0.1.1 -- Last Modified 30-12-04
 */

public final class Archimedes {
	/**
	 * Library of implemented functions.
	 * 
	 * @see simcolep.fun2.Binomial
	 * @author ir. E.A.J. Fischer
	 * @version 1.0
	 *
	 */
	public enum Library {
		Binomial(/** @see simcolep.fun2.Binomial */
				new Binomial()), Constant(new Constant()),
		/** @see simcolep.fun2.Constant */
		Erlang(new Erlang()),
		/** @see simcolep.fun2.Erlang */
		Exponential(new Exponential()),
		/** @see simcolep.fun2.Exponential */
		Gamma(new Gamma()),
		/** @see simcolep.fun2.Gamma */
		Normal(new Normal()),
		/** @see simcolep.fun2.Normal */
		Power(new Power()),
		/** @see simcolep.fun2.Power */
		Quadratic(new Quadratic()),
		/** @see simcolep.fun2.Quadratic */
		Triangular(new Triangular()),
		/** @see simcolep.fun2.Triangular */
		Uniform(new Uniform()),
		/** @see simcolep.fun2.Uniform */
		Linear(new Linear()),
		/** @see simcolep.fun2.Linear */
		Weibull(new Weibull()),
		/** @see simcolep.fun2.Weibull */
		QDiscreteFunction(new QDiscreteFunction()),
		/** @see simcolep.fun2.QDiscreteFunction */
		QFractionsFunction(new QFraction()),
		/** @see simcolep.fun2.QFraction */
		cFunction(new cFunction()),
		/** @see simcolep.fun2.cFunction */
		IDistribution(new IDistribution()),
		/** @see simcolep.fun2.IDistribution */
		TimeDistribution(new TimeDistribution());
		/** @see simcolep.fun2.TimeDistribution */
		;
		/**
		 * Function
		 */
		private Function f;

		/**
		 * 
		 * @param f
		 */
		private Library(Function f) {
			this.f = f;
		}

		/**
		 * 
		 * @return new instance of the function
		 * @throws IllegalAccessException
		 * @throws InstantiationException
		 */
		public Function getInstance() throws InstantiationException, IllegalAccessException {

			return this.f.getClass().newInstance();

		}
	}

	private static final String FUNCTIONSTEXT = "Functions:"; //$NON-NLS-1$

	private static final String PARAMETERSTEXT = "Parameters:"; //$NON-NLS-1$

	private static final String BRAINTEXT = "***********Archi's Brain***************"; //$NON-NLS-1$

	/***/
	public static final int PARAMETER = 1;

	/***/
	public static final int FUNCTION = 0;

	/** message exception */
	private static final String MESSAGE_USE_CONVINCE = "in Archimedes replace function by method convinceArchi(..)for ";

	private static final String MESSAGE_BRAIN_NOT_STARTED = " Archi's brain not started";

	private static final String MESSAGE_NOT_IN_BRAIN = " Not in Archi's brain";

	private static final String ENDTEXT = "*********************Archimedes thougths end here****************************";
	/**
	 * functions and parameters (index = 0 -> functions, index = 1 ->
	 * Parameters)
	 */
	private static List<LinkedHashMap<String, Object>> ArchisBrain = new LinkedList<LinkedHashMap<String, Object>>();

	/**
	 * 
	 * @return ArchisBrain
	 */
	public static List<? extends Map<String, Object>> archisBrain() {

		return ArchisBrain;
	}

	/**
	 * Return that part of Archi's brain that is requested
	 *
	 * @param i
	 *            - @see simcolep.fun2.Archimedes.FUNCTION and @see
	 *            simcolep.fun2.Archimedes.PARAMETER
	 * @return Functions in Archis brain
	 */
	public static Map<String, Object> archisBrain(int i) {
		return ArchisBrain.get(i);
	}

	/**
	 *
	 * Let it return a <code> Function</code>
	 *
	 * @param name
	 *
	 * @return function
	 * @throws NullPointerException
	 */
	public static Function ask(String name) {
		try {
			Function function = (Function) ArchisBrain.get(FUNCTION).get(name);
			if (function == null)
				throw new IllegalArgumentException(name + MESSAGE_NOT_IN_BRAIN);
			return function;

		} catch (NullPointerException ne) {
			throw new NullPointerException(name + MESSAGE_BRAIN_NOT_STARTED);
		}
	}

	/**
	 * *Let it return a distribution
	 * 
	 * @param name
	 * @return distribution
	 * @throws NullPointerException
	 */
	public static Distributionable askDistribution(String name) {
		try {
			Distributionable distribution = (Distributionable) ArchisBrain.get(FUNCTION).get(name);
			if (distribution == null)
				throw new IllegalArgumentException(name + MESSAGE_NOT_IN_BRAIN);
			return distribution;

		} catch (NullPointerException ne) {
			throw new NullPointerException(name + MESSAGE_BRAIN_NOT_STARTED);
		}
	}

	/**
	 *
	 * @param name
	 * @return value
	 */
	public static double askDouble(String name) {

		try {
			double value = ((Double) ArchisBrain.get(PARAMETER).get(name)).doubleValue();
			return value;
		} catch (NullPointerException ne) {
			throw new NullPointerException(name + MESSAGE_NOT_IN_BRAIN);
		}

	}

	/**
	 * @see Archimedes#askDouble(String)
	 * @param string
	 * @return value as a int
	 *
	 */
	public static int askInteger(String string) {
		return new Double(askDouble(string)).intValue();
	}

	/**
	 * @see Archimedes#askDouble(String)
	 * @param string
	 * @return value as long
	 */
	public static long askLong(String string) {
		return new Double(askDouble(string)).longValue();
	}

	/**
	  *
	  */
	@SuppressWarnings(PN.UNCHECKED)
	public static String BraintoString() {
		StringBuffer brain = new StringBuffer(BRAINTEXT + PN.LINESEPARATOR);
		String[] what = { PARAMETERSTEXT + PN.LINESEPARATOR, FUNCTIONSTEXT + PN.LINESEPARATOR };
		// Parameters
		brain.append(what[1]);
		Iterator it = ArchisBrain.get(PARAMETER).keySet().iterator();
		while (it.hasNext()) {
			Object next = it.next();
			brain.append(next + PN.TAB + ArchisBrain.get(PARAMETER).get(next) + PN.LINESEPARATOR);
		}

		// Functions
		brain.append(what[0]);
		it = ArchisBrain.get(FUNCTION).keySet().iterator();
		while (it.hasNext()) {
			Object next = it.next();
			brain.append(next + PN.TAB + ArchisBrain.get(FUNCTION).get(next) + PN.LINESEPARATOR);
		}

		brain.append(ENDTEXT);
		return brain.toString();
	}

	/**
	 * 
	 * @param string
	 * @param fOrP
	 *            function or parameter
	 * @return true if ArchisBrain contains a function or parameter with this
	 *         name
	 */
	public static boolean contains(String string, int fOrP) {

		return ArchisBrain.get(fOrP).containsKey(string);

	}

	/**
	 *
	 * Override an existing parameter value
	 *
	 * @param value
	 * @param name
	 * @see Archimedes#tell(Function, String)
	 */
	public static void convince(String name, double value) {
		ArchisBrain.get(PARAMETER).put(name, new Double(value));
	}

	/**
	 *
	 * Override an existing function
	 *
	 * @param function
	 * @param name
	 * @see Archimedes#tell(Function, String)
	 */
	public static void convince(String name, Function function) {
		ArchisBrain.get(FUNCTION).put(name, function);
	}

	/**
	 * Resets Archi's brain
	 *
	 */
	public static void reset() {
		if (!ArchisBrain.isEmpty()) {
			ArchisBrain.clear();
		}
	}

	/**
	 *
	 * @param value
	 * @param name
	 */
	public static void tell(String name, double value) {
		if (ArchisBrain.isEmpty()) {
			ArchisBrain.add(new LinkedHashMap<String, Object>());
			ArchisBrain.add(new LinkedHashMap<String, Object>());
		}

		if (!ArchisBrain.get(PARAMETER).containsKey(name)) {
			ArchisBrain.get(PARAMETER).put(name, new Double(value));
		} else
			throw new IllegalArgumentException(MESSAGE_USE_CONVINCE + name);

	}

	/**
	 *
	 * Enter a new function into Archi's brain<br>
	 * Note that this should be a function with a name that is not present in
	 * Archi's brain
	 *
	 * @param function
	 * @param name
	 *
	 * @throws <code>IllegalArgumentException</code>
	 *             if a function with the same name already exists
	 *
	 */
	public static void tell(String name, Function function) {
		if (ArchisBrain.isEmpty()) {
			ArchisBrain.add(new LinkedHashMap<String, Object>());
			ArchisBrain.add(new LinkedHashMap<String, Object>());
		}

		if (!ArchisBrain.get(FUNCTION).containsKey(name)) {
			ArchisBrain.get(FUNCTION).put(name, function);
		} else
			throw new IllegalArgumentException(MESSAGE_USE_CONVINCE + name);

	}

}
