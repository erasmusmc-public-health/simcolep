/*
 * This file is part of SIMCOLEP, a simulation model developed by the Department of Public Health, 
 * Erasmus MC, University Medical Center Rotterdam, Rotterdam, The Netherlands.
 *
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/ 
 * or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA
 *
 * You are free to:
 * Share � copy and redistribute the material in any medium or format
 * Under the following terms:
 * Attribution � You must give appropriate credit, provide a link to the license, and indicate if changes were 
 * made. You may do so in any reasonable manner, but not in any way that suggests the licensor endorses
 * you or your use.
 * NonCommercial � You may not use the material for commercial purposes.
 * NoDerivatives � If you remix, transform, or build upon the material, you may not distribute the modified material.
 */

/*
 * Created on 26-Apr-2005
 *
 *
 */
package simcolep.fun2;

/**
 * @author ir E.A.J.Fischer
 * @version 0.1.1--Last Modified: 26-04-05
 *
 */
public final class CinlarsNonStatPoisson {
	/**rate of the non stationary poisson process*/
	private Function labda = null;
	
	/**
	 * 
	 * @param labda
	 */
	public CinlarsNonStatPoisson(Function labda)
	{
		this.labda=labda;
		
	}
	/**
	 * 
	 * @param time time of last event 
	 * @param z random variate
	 * @return
	 * 
	 */
	public double next(double time, double z)
	{
	    //change the time to the last time accent
        time = labda.getIntegrated(time);
		
        //add the random increment of a Poisson process
        double time_ac = time - Math.log(z);
		
        //get the inversed time
        double res =labda.getInvIntegrated(time_ac);
       
        //return it
        return res;
	}
	/**
	 * 
	 * @param time last event
	 * @param z random variate [0,1]
	 * @param function rate function
	 * @return next time
	 * 
	 */
	public static double next(double time, double z, Function function)
	{
        //get integrated time
        double t = 
        	function.getIntegrated(time);
        
        //add a random variate
	    double time_ac = 
	    	t - Math.log(z);
	   	
	    //get the x-value of this value of the integration
	    double res = 
	    	function.getInvIntegrated(time_ac);
	    
	    if(res < time)
	    	throw new IllegalArgumentException(
	    			"Error in Poisson process");
	    else
	    	return res;
	   	}
    
    /**
     * 
     * @param ratefunction
     */
    public void setFunction(Function labda) {
        this.labda = labda;
        
    }
	
}
