/*
 * This file is part of SIMCOLEP, a simulation model developed by the Department of Public Health, 
 * Erasmus MC, University Medical Center Rotterdam, Rotterdam, The Netherlands.
 *
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/ 
 * or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA
 *
 * You are free to:
 * Share � copy and redistribute the material in any medium or format
 * Under the following terms:
 * Attribution � You must give appropriate credit, provide a link to the license, and indicate if changes were 
 * made. You may do so in any reasonable manner, but not in any way that suggests the licensor endorses
 * you or your use.
 * NonCommercial � You may not use the material for commercial purposes.
 * NoDerivatives � If you remix, transform, or build upon the material, you may not distribute the modified material.
 */

/*
 * Created on 19-Nov-2004
 *
 */
package simcolep.fun2;

/**
 * <i>Y = Yvalue for X =[min, max]</i>
 * @default <i>min = -infinity, max = infinity</i>
 * 
 * @author ir E.A.J. Fischer
 * @version 0.1.1--Last Modified 29-04-05
 *
 * 
 */
public final class Uniform 
			extends ContinuousFunction 
				implements Distributionable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 
		8252260884366674509L;

	/**value*/
	protected double Yvalue = 0.0;
	
	/**minimum x value: result of the function is 0 for x < min */
	protected double min = Double.NEGATIVE_INFINITY;
	
	/**maximum y value: result of the function is 0 for x > max */
	protected double max = Double.POSITIVE_INFINITY;
	
	/**
	 * 
	 */
	public Uniform() {
		
	}
/**
 * 
 * @param Yvalue
 */
	public Uniform(double Yvalue)
	{
		this.Yvalue= Yvalue;
	}
	/**
	 * 
	 * @param Yvalue
	 * @param min
	 */
	public Uniform(double Yvalue, double min)
	{
		this.Yvalue= Yvalue;
		this.min = min;
	}
	
	/**
	 * 
	 * @param Y, min, max
	 */
		public Uniform(double Y, double min, double max)
		{
			this.Yvalue= Y;
			
			this.min = min;
			
			this.max = max;
			
			if (min>max)
				throw new IllegalArgumentException("min is larger than max");
		}
	/**
	 * 
	 * @param cfi
	 */
	public Uniform(ContinuousFunctionInput cfi)
	{
		setFunction(cfi);
	}
	
	/**
	 * @see ContinuousFunction#changeParameter(String, double)
	 * Note that as a Uniform has only one parameter 
	 * it does not matter to check which name is given 
	 */
	public void changeParameter(String name, double value)
	{
		if(name.equalsIgnoreCase("Yvalue"))
		{
			this.Yvalue=value;
		}else if(name.equalsIgnoreCase("max"))
		{
			this.max =value;
		}else if (name.equalsIgnoreCase("min"))
		{
			this.min =value;
		}else if(name.equalsIgnoreCase("N0"))
        {
            return;
        }else 
        	throw new IllegalArgumentException("unknown parameter: " + name);
		
	}
	
	/**
	 * @see ContinuousFunction#setFunction(ContinuousFunctionInput)
	 */
	public void setFunction(ContinuousFunctionInput cfi)
	{
		this.Yvalue = cfi.getParameter("Yvalue").doubleValue();
		try{
			this.min =cfi.getParameter("min").doubleValue();
			this.max =cfi.getParameter("max").doubleValue();
		}catch (NullPointerException e){}
	}
	

	

	/**
	 * @see simcolep.fun2.ContinuousFunction#getY(double[])
	 */
	public double getY(double X) {
		if(X >=this.min &&X<= this.max)
		{
			return this.Yvalue;
		}
		return 0.0;
	}

	/** 
	 * 
	 * @see simcolep.fun2.Function#getX(double)
	 */
	public double getX(double y) {
		return Double.POSITIVE_INFINITY;
	}

	/**
	 * @see simcolep.fun2.Distributionable#drawX(double[])
	 */
	public double drawX(double y) {
	    
	    return getInvIntegrated(y);
	}
	/**
	 * @see simcolep.fun2.Distributionable#drawX(double[])
	 */
	public double drawX(double[] y) {
	    
	    return getInvIntegrated(y[0]);
	}
	/** 
	 * @see simcolep.fun2.Distributionable#drawXconditional(double, double)
	 */
	public double drawXconditional(double[] v) {
		double[] newY = {
	        	getF(new double[]{v[1]})+v[0]*(1-getF(new double[]{v[1]}))};
	        return drawX(newY);
	}




	/**
	 * @param x
	 * @see simcolep.fun2.Function#getDerivative(double)
	 */
	public double getDerivative(double x) {
		return 0;
		
	}
	/**
	 * @param x
	 * @see simcolep.fun2.Function#getIntegrated(double)
	 */
	public double getIntegrated(double x) {
		if(x < this.min) 
			return 0.0;
		if(x <= this.max) 
			return this.Yvalue*(x-this.min);
		else 
			return this.Yvalue*(this.max-this.min);
	}
	/**
	 * 
	 * @see simcolep.fun2.Function#getInvIntegrated(double)
	 */
	public double getInvIntegrated(double y) {
		
	    if(y <= 0.0) 
	    	return this.min;
		if(y <= getIntegrated(this.max))
			return (y/this.Yvalue)+this.min;
		else 
			return Double.POSITIVE_INFINITY;
		
	}
	/**
	 * @see simcolep.fun2.ContinuousFunction#getParameter(java.lang.String)
	 */
	public double getParameter(String name) {
		if(name.equalsIgnoreCase("Yvalue")) return this.Yvalue;
		if(name.equalsIgnoreCase("min")) return this.min;
		if(name.equalsIgnoreCase("max"))return this.max;
		return Double.NaN;
	}
    
    /**
     * @return (min+max)/2 or 0 if min and max are not set
     * @see simcolep.fun2.Distributionable#mean()
     */
    public double mean() {
       if(min!=Double.NEGATIVE_INFINITY)
       {
           if(max!=Double.POSITIVE_INFINITY)
           {
               return (this.min+this.max)/2;
           }else return Double.POSITIVE_INFINITY/2;
       }else if (max!=Double.POSITIVE_INFINITY)return Double.NEGATIVE_INFINITY/2;
        return 0;
    }
    
    @Override
    public Uniform clone() {
        
        return new Uniform(this.Yvalue, this.min, this.max);
    }
    
    @Override
    public boolean equalsFunction(Function f) {
        if(!this.getClass().equals(f.getClass()))return false;
        Uniform ud =(Uniform)f;
        return (Math.abs(this.max-ud.max)<Pythagoras.getDefaultPrecision()&&Math.abs(this.min-ud.min)<Pythagoras.getDefaultPrecision()&&Math.abs(this.Yvalue-ud.Yvalue)<Pythagoras.getDefaultPrecision());
    }
    /**
	 * @see simcolep.fun2.Distributionable#getF(double)
	 */
	public double getF(double d) {
			return this.getIntegrated(d);
	}
	/**
	 * @see simcolep.fun2.Distributionable#getF(double)
	 */
	public double getF(double[] d) {
			return this.getIntegrated(d[0]);
	}
}
