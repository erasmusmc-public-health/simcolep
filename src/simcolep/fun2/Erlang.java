/*
 * This file is part of SIMCOLEP, a simulation model developed by the Department of Public Health, 
 * Erasmus MC, University Medical Center Rotterdam, Rotterdam, The Netherlands.
 *
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/ 
 * or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA
 *
 * You are free to:
 * Share � copy and redistribute the material in any medium or format
 * Under the following terms:
 * Attribution � You must give appropriate credit, provide a link to the license, and indicate if changes were 
 * made. You may do so in any reasonable manner, but not in any way that suggests the licensor endorses
 * you or your use.
 * NonCommercial � You may not use the material for commercial purposes.
 * NoDerivatives � If you remix, transform, or build upon the material, you may not distribute the modified material.
 */

/*
 * Created on 25-Jul-2005
 *
 */
package simcolep.fun2;

import simcolep.exceptions.SimulationException;
import umontreal.iro.lecuyer.probdist.ErlangDist;

/**
 * This distribution is given by the Erlang distribution:<br>
 * Erlang(1,r) equals Exponential(r)
 * 
 * 
 * 
 * @author ir E.A.J. Fischer
 * @version 0.1.2 -- Last Modified 25-Jul-2005
 */
public final class Erlang 
		extends ContinuousFunction 
			implements Distributionable {
	/**serial version UID*/
     private static final long serialVersionUID = -4331345345761500816L;
    /**phase*/
    protected double m = 1.0;
    
    /**rate*/
    protected double r = 1.0;

    /**Distribution */
	private ErlangDist e_dist = null;
    
  

    /**
     * 
     */
    public Erlang(double m, double r) {
        super();
        this.m = m;
        this.r =r;
        //this method checks whether m is an integer. 
        check();
        this.e_dist = new ErlangDist((int) this.m, this.r);
    }
    /**
     * 
     * @param r
     */
    public Erlang(double r)
    {
        super();
        this.r = r;
        this.m = 1;
        //this method checks whether m is an integer. 
        this.e_dist = new ErlangDist(1, this.r);
    }

   
    /**
     * @param cfi
     */
    public Erlang(ContinuousFunctionInput cfi) {
        super();
        if(cfi.containsKey("m"))
        {
            this.m = cfi.getParameter("m").doubleValue();
        }
        if(cfi.containsKey("r"))
        {
            this.r = cfi.getParameter("r").doubleValue();
        }
//      this method checks whether m is an integer. 
        check();
        this.e_dist = new ErlangDist((int) this.m, this.r);
    }

    /**
     * @param cf
     */
    public Erlang(ContinuousFunction cf) {
        super();
        this.m = ((Erlang)cf).m;
        this.r = ((Erlang)cf).r;
//      this method checks whether m is an integer. 
        check();
        this.e_dist = new ErlangDist((int) this.m, this.r);
    }
    
    /**
     * 
     *
     */
    public Erlang()
    {
    	
    }

    /**
     * @see simcolep.fun2.ContinuousFunction#setFunction(simcolep.fun2.ContinuousFunctionInput)
     */
    public void setFunction(ContinuousFunctionInput CFI) {
        this.m = CFI.getParameter("m").doubleValue();
        this.r = CFI.getParameter("r").doubleValue();
//      this method checks whether m is an integer. 
        check();
        this.e_dist = new ErlangDist((int) this.m, this.r);
    }

    /**
     * check whether parameter m is equivalent to an integer
     */
    private void check() {
        if(Math.round(this.m)!=this.m)
        	throw new SimulationException(
        			"m is for convenience a double " +
        			"but should be an integer value");
        
    }
    /**
     * @param name
     * @param value
     */
    public void changeParameter(String name, double value) {
       if(name.equalsIgnoreCase("m"))
       {
           this.m = value;
       }else if(name.equalsIgnoreCase("r"))
       {
           this.r = value;
       }else if(name.equalsIgnoreCase("N0")){return;}
       else throw new SimulationException(
    		   "unknown parameter: " +name);
       
       this.e_dist = new ErlangDist((int) this.m, this.r);

    }

    /**
     * 
     * @see simcolep.fun2.ContinuousFunction#getParameter(java.lang.String)
     */
    public double getParameter(String name) {
        if(name.equalsIgnoreCase("m"))
        {
            return this.m;
        }else if(name.equalsIgnoreCase("r"))
        {
            return this.r;
        }else throw new SimulationException(
        		"unknown parameter");
    }

    /**
     * @return (r <sup>-m</sup> Xvalue<sup>m-1</sup> e<sup>-Xvalue/r</sup>)/(m-1)! if x>0
     * <br> Double.NaN otherwise 
     * @see simcolep.fun2.Function#getY(double)
     */
    public double getY(double X) {
        if(X<0)
        	return Double.NaN;
        return e_dist.density(X);
      
    }

    /**
     * @see simcolep.fun2.Function#getXvalue(double)
     */
    public double getX(double y) {
       if(this.m>1){
           System.out.println("multi-phase Erlang has two or more X-values for each Yvalue method returns X value closest to 0");
           return Pythagoras.numericgetXvalue(this,y,5);
       }
       
       if(y< 0) return Double.NaN;
       return -(1/this.r)*Math.log((1/this.r)*y);
       
    }

   

    /**
     *@return -(Math.exp((1-(Xvalue/this.r)))*(Math.pow(this.r,-(1+this.m)))*Math.pow(Xvalue, this.m-2.0)*(this.r-this.m*this.r+Xvalue))/Pythagoras.factorial(this.m-1); 
     * @see simcolep.fun2.Function#getDerivative(double)
     */
    public double getDerivative(double x) {
        if(x < 0)
        	return Double.NaN;
        return -(Math.exp((1-(x*this.r)))*(Pythagoras.pow(1/this.r,-(1+this.m)))*Pythagoras.pow(x, this.m-2.0)*(1/this.r-this.m/this.r+x))/Pythagoras.factorial(this.m-1);
    }

    /**
     * @return 
     * @see simcolep.fun2.Function#getIntegrated(double)
     */
    public double getIntegrated(double x) {
       if(x < 0) return 0;
      return e_dist.cdf(x);
    }

    /**
     * @see simcolep.fun2.Function#getInvIntegrated(double)
     */
    public double getInvIntegrated(double y) {
        if(y <0.0) return Double.NaN;
        if(y >= 1.0)return Double.POSITIVE_INFINITY;
      
        return e_dist.inverseF(y);
    }

   

  
    /**
     * @param y
     * @return x - getInvIntegrated(x)
     * 
     * @see simcolep.fun2.Distributionable#drawX(double)
     */
    public double drawX(double y) {
        if(this.r == 0.0)
        	return Double.POSITIVE_INFINITY;
        
        if(Double.isInfinite(this.r))
        	return 0.0;
     
        //draw x
        double x = e_dist.inverseF(y);
        if(Double.MAX_VALUE== x)
        	return Double.POSITIVE_INFINITY*Math.signum(x);
        else
        	return x;
        
    }
    /**
     * @param y
     * @return x - getInvIntegrated(x)
     * 
     * @see simcolep.fun2.Distributionable#drawX(double)
     */
    public double drawX(double[] y) {
        if(this.r == 0.0)
        	return Double.POSITIVE_INFINITY;
        
        if(Double.isInfinite(this.r))
        	return 0.0;
     
        //draw x
        double x = e_dist.inverseF(y[0]);
        if(Double.MAX_VALUE== x)
        	return Double.POSITIVE_INFINITY*Math.signum(x);
        else
        	return x;
        
    }
    
    /**
     * @see simcolep.fun2.Distributionable#drawXconditional(double[])
     */
    public double drawXconditional(double[] p) {
        p[0] = 
        	getF(new double[]{p[1]})+p[0]*(1-getF(new double[]{p[1]}));
        return drawX(p);

    }
        
    /**
     * @return m*r
     * @see simcolep.fun2.Distributionable#mean()
     */
    public double mean() {
   
        return this.m*this.r;
    }
    
    @Override
    public Erlang clone() {
        
        return new Erlang(this.m, this.r);
    }
    
    @Override
    public boolean equalsFunction(Function f) {
        if(!f.getClass().equals(this.getClass()))return false;
        Erlang e = (Erlang)f;
        return (Math.abs(this.m-e.m)<Pythagoras.getDefaultPrecision() && Math.abs(this.r-e.m)<Pythagoras.getDefaultPrecision());
    }
  

	/**
	 * @see simcolep.fun2.Distributionable#getF(double[])
	 */
	public double getF(double d) {
	        //draw x
        return e_dist.cdf(d);
	}
	
	/**
	 * @see simcolep.fun2.Distributionable#getF(double[])
	 */
	public double getF(double[] d) {
	        //draw x
        return e_dist.cdf(d[0]);
	}
}
