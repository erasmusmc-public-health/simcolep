/*
 * This file is part of SIMCOLEP, a simulation model developed by the Department of Public Health,
 * Erasmus MC, University Medical Center Rotterdam, Rotterdam, The Netherlands.
 *
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/
 * or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA
 *
 * You are free to:
 * Share � copy and redistribute the material in any medium or format
 * Under the following terms:
 * Attribution � You must give appropriate credit, provide a link to the license, and indicate if changes were
 * made. You may do so in any reasonable manner, but not in any way that suggests the licensor endorses
 * you or your use.
 * NonCommercial � You may not use the material for commercial purposes.
 * NoDerivatives � If you remix, transform, or build upon the material, you may not distribute the modified material.
 */

package simcolep.fun2;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;

import simcolep.exceptions.SimulationException;
import simcolep.tools.PN;

/**
 * A table with X and Y values, that can interpolate between the values
 * Important note to this function is that input table should be specified in
 * such a way that the start value is the value that with interpolation is found
 * exactly at that moment. If you want mid-point values calculate them
 * yourselves
 *
 * The Q because it searches fastre than an simple list implementation as it is
 * a sorted of unbalanced tree
 *
 * @author ir. E.A.J. Fischer
 * @version 1.0
 */
public class QDiscreteFunction extends Function implements Distributionable, DiscreteInputable {
	/**
	 *
	 * Element of a Quick discrete function. The elements form an (unbalanced)
	 * tree
	 *
	 *
	 * @author ir E.A.J. Fischer
	 * @version 0.1.2 -- Last Modified 21-Dec-2005
	 */
	private final class QElement implements Serializable {
		/**
		 *
		 */
		private static final long serialVersionUID = -8479892549833932518L;
		/**
		 * minimum value
		 */
		double min = Double.NaN;
		/**
		 * maximum value
		 */
		double max = Double.NaN;
		/**
		 * intercept when interpolating
		 */
		double intercept = Double.NaN;
		/**
		 * slope when interpolating
		 */
		double slope = Double.NaN;
		/**
		 * element to the right
		 */
		QElement right = null;
		/**
		 * element to the left
		 */
		QElement left = null;
		/**
		 * boundary between right and left
		 */
		double boundary = 0;

		/**
		 *
		 * @param min
		 * @param max
		 * @param intercept
		 * @param slope
		 */
		public QElement(double min, double max, double intercept, double slope) {
			this.min = min;
			this.max = max;
			this.intercept = intercept;
			this.slope = slope;

		}

		/**
		 *
		 * @param left
		 * @param right
		 */
		public QElement(QElement left, QElement right) {
			this.right = right;
			this.left = left;
			this.boundary = this.left.getMax();

		}

		/**
		 * Interpolates between the min and max value using the slope
		 *
		 * @param d
		 * @return intercept+(d-this.min)*slope
		 */
		public double get(double d) {
			// if both are null this element is the last of the branch ie.
			// containing data
			if (this.right == null && this.left == null) {
				if (d >= this.max)
					return intercept;
				else
					return intercept + (d - this.min) * slope;
			}
			// go left if no right
			if (this.right == null)
				return this.left.get(d);
			// go right if no left
			if (this.left == null)
				return this.right.get(d);
			// if a choice has to be made between right and left
			// go right if the value is larger
			// than the boundary between the two values
			if (d >= this.boundary)
				return this.right.get(d);
			else
				return this.left.get(d);
		}

		/**
		 *
		 * @param d
		 * @return
		 */
		public double getSlope(double d) {

			if (this.right == null && this.left == null)
				return this.slope;
			// left slope
			if (this.right == null)
				return this.left.getSlope(d);

			// right slope
			if (this.left == null)
				return this.right.getSlope(d);
			// the right or left side of the boundary
			if (d > this.boundary)
				return this.right.getSlope(d);
			else
				return this.left.getSlope(d);
		}

		/**
		 *
		 * @return max
		 */
		private double getMax() {
			if (this.right == null && this.left == null) {
				return this.max;
			} else if (this.right == null) {
				return this.left.getMax();
			} else
				return this.right.getMax();

		}

	}

	/**
	    *
	    */
	public static final long serialVersionUID = -5709241180125932159L;

	/**
	 * first element of the QDiscreteFunction x to y
	 */
	private QElement functionXtoY = null;
	/**
	 * first element of the QDiscreteFunction y to x
	 */
	private QElement functionYtoX = null;
	/**
	 * input
	 */
	private DiscreteFunctionInput input = null;
	/**
	 * default <b> false</b>
	 */
	private boolean interpolate = false;

	/**
	 * cumulative function
	 *
	 */
	private boolean cum = false;

	/**
	 *
	 *
	 */
	public QDiscreteFunction() {

	}

	/**
	 *
	 * @param dfi
	 */
	public QDiscreteFunction(DiscreteFunctionInput dfi) {
		this.input = dfi;
		this.cum = dfi.isCum();
		setFunction(dfi);
	}

	@Override
	public void changeParameter(String name, double value) {
		throw new IllegalArgumentException("don't change it!");

	}

	@Override
	public Function clone() {
		throw new SimulationException(SimulationException.NOTIMPLEMENTED);
	}

	/**
	 * @see simcolep.fun2.Distributionable#drawX(double[])
	 */
	@Override
	public double drawX(double v) {
		// only cumulative functions can be used to have a correct from y to x
		// draw methiod
		if (!cum)
			throw new IllegalArgumentException("Function is not cumulative!");

		return this.getX(v);
	}

	/**
	 * @see simcolep.fun2.Distributionable#drawX(double[])
	 */
	@Override
	public double drawX(double[] v) {
		// only cumulative functions can be used to have a correct from y to x
		// draw methiod
		if (!cum)
			throw new IllegalArgumentException("Function is not cumulative!");

		return this.drawX(v[0]);
	}

	/**
	 * @see simcolep.fun2.Distributionable#drawXconditional(double, double)
	 */
	@Override
	public double drawXconditional(double[] v) {
		double[] newY = { getF(new double[] { v[1] }) + v[0] * (1 - getF(new double[] { v[1] })) };
		return drawX(newY);

	}

	@Override
	public boolean equalsFunction(Function f) {
		if (!f.getClass().equals(this.getClass()))
			return false;
		// otherwise return true for all functions
		return true;
	}

	@Override
	public double getDerivative(double X) {

		return this.functionXtoY.getSlope(X);
	}

	/**
	 *
	 * @return input
	 */
	@Override
	public DiscreteFunctionInput getDiscreteFunctionInput() {
		return this.input;
	}

	/**
	 * @see simcolep.fun2.Distributionable#getF(double)
	 */
	@Override
	public double getF(double d) {
		return this.getY(d);
	}

	/**
	 * @see simcolep.fun2.Distributionable#getF(double)
	 */
	@Override
	public double getF(double[] d) {
		return this.getY(d[0]);
	}

	@Override
	public double getIntegrated(double X) {
		throw new SimulationException(SimulationException.NOTIMPLEMENTED);
	}

	@Override
	public double getInvIntegrated(double Y) {
		throw new SimulationException(SimulationException.NOTIMPLEMENTED);
	}

	@Override
	public double getX(double Y) {
		return this.functionYtoX.get(Y);
	}

	@Override
	public double getY(double X) {
		return this.functionXtoY.get(X);
	}

	@Override
	public int inputGroup() {
		return Function.DISCRETE_FUNCTION_INPUT;
	}

	/**
	 * Change interpolation of this function off or on
	 *
	 * @param interpolate
	 */
	public void interpolate(boolean interpolate) {

		this.interpolate = interpolate;
		// if the function was already set change it
		if (input != null)
			this.setFunction(this.input);

	}

	/**
	 *
	 * @return mean
	 */
	@Override
	public double mean() {

		return this.getX(0.5);
	}

	/**
	 *
	 * @param dfi
	 */
	@Override
	public DiscreteInputable setFunction(DiscreteFunctionInput dfi) {
		if (dfi.isEmpty())
			return this;

		// set input
		this.input = dfi;

		// cumulativei
		this.cum = dfi.isCum();

		// set the function either interpolated or not
		if (interpolate)
			return setFunctionInterpolated(dfi);
		else
			return setFunctionNonInterpolated(dfi);
	}

	/**
	 *
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {

		StringBuffer StrBf = new StringBuffer(
				this.getName() + " cumulative: " + this.cum + Function.TAB + "interpolate: " + this.interpolate);

		for (DiscreteFunctionInputElement oe : this.input) {
			StrBf.append(PN.LINESEPARATOR + PN.TAB + oe.toString());
		}

		return StrBf.toString();

	}

	/**
	 * The entries should be given in ascending order for both x and y! Only
	 * cumulative distributions are accepted
	 *
	 * @param dfi
	 */
	@SuppressWarnings("unchecked")
	private DiscreteInputable setFunctionInterpolated(DiscreteFunctionInput dfi) {

		if (dfi.isEmpty())
			return this;

		// determine minimum maximum slope and intercept
		// create all entries for the tree
		ListIterator<DiscreteFunctionInputElement> it = dfi.listIterator();
		// temp x to y
		List<QElement> tempXtoY = new LinkedList<QElement>();

		// temp y to x
		List<QElement> tempYtoX = new LinkedList<QElement>();

		// minumum of the element
		double qmin = 0.0;
		// maximum of an element
		double qmax = 0.0;
		// minimum of y of an element
		double miny = 0.0;
		// maximum of y of an element
		double maxy = 0.0;

		// iterate the list of discrete input elements
		while (it.hasNext()) {
			// first in line
			DiscreteFunctionInputElement e = it.next();
			// second in line
			DiscreteFunctionInputElement enext;
			if (it.hasNext()) // if present
				enext = it.next();
			else // else the first === second
				enext = e;
			// thrid in line
			DiscreteFunctionInputElement enextnext;
			if (it.hasNext())// if present
				enextnext = it.next();
			else// else second in line
				enextnext = enext;
			// first entry this starts assuming that x = 0
			if (tempXtoY.isEmpty()) {
				// midpoint of X value
				qmax = enext.min;
				// intercept wit Y -axis
				double qintercept = e.min;
				// slope
				double qslope = (enext.value) / (qmax - qmin);
				// create the element
				QElement q = new QElement(qmin, qmax, qintercept, qslope);
				tempXtoY.add(q);

				// entry for the y to x function
				miny = e.value;
				maxy = enext.value;
				double intercepty = (qmin);
				double yslope = 1 / qslope;
				// prevent dividing by zero
				if (qslope == 0)
					yslope = 0;

				// create the inverse element
				QElement qy = new QElement(miny, maxy, intercepty, yslope);
				tempYtoX.add(qy);

				qmin = qmax;
				miny = maxy;

			}
			// entries for the xtoy function
			qmax = enext.min;
			double qintercept = e.value;
			double qslope = (enext.value - e.value) / (qmax - qmin);
			if (qmax == qmin)
				qslope = (enext.value - e.value);
			// create an element with minimum, maximum X values an intercept at
			// qmin and a slope
			QElement q = new QElement(qmin, qmax, qintercept, qslope);
			// add to the temporary list
			tempXtoY.add(q);

			// entry for the y to x function
			miny = e.value;
			maxy = enext.value;
			double intercepty = qmin;
			double slopey = 1 / qslope;
			if (Double.isInfinite(slopey))
				slopey = 0;
			// create an element that is the inverse of the X tot Y element
			QElement qy = new QElement(miny, maxy, intercepty, slopey);
			// add to the temporary list
			tempYtoX.add(qy);

			// wind back
			if (!e.equals(enext))// second in line
				it.previous();
			if (!enext.equals(enextnext))// third in line
				it.previous();

			// set new values
			qmin = qmax;

			miny = maxy;
		}

		// create the tree for x to y
		while (tempXtoY.size() != 1) {
			List list = new LinkedList();
			ListIterator<QElement> qit = tempXtoY.listIterator();
			while (qit.hasNext()) {
				QElement left = qit.next();
				QElement right = null;
				if (qit.hasNext())
					right = qit.next();
				QElement q = new QElement(left, right);
				list.add(q);
			}
			tempXtoY = list;
		}
		this.functionXtoY = tempXtoY.get(0);

		// create the tree for y to x
		while (tempYtoX.size() != 1) {
			List list = new LinkedList();
			ListIterator<QElement> qit = tempYtoX.listIterator();
			while (qit.hasNext()) {
				QElement left = qit.next();
				QElement right = null;
				if (qit.hasNext())
					right = qit.next();
				QElement q = new QElement(left, right);
				list.add(q);
			}
			tempYtoX = list;
		}

		this.functionYtoX = tempYtoX.get(0);

		return this;
	}

	/**
	 *
	 * @param dfi
	 */
	@SuppressWarnings("unchecked")
	private DiscreteInputable setFunctionNonInterpolated(DiscreteFunctionInput dfi) {
		// determine minimum maximum slope and intercept
		// create all entries for the tree
		ListIterator<DiscreteFunctionInputElement> it = dfi.listIterator();
		// temp x to y
		List<QElement> tempXtoY = new LinkedList<QElement>();

		// temp y to x
		List<QElement> tempYtoX = new LinkedList<QElement>();

		while (it.hasNext()) {
			DiscreteFunctionInputElement e = it.next();
			DiscreteFunctionInputElement enext;
			if (it.hasNext())
				enext = it.next();
			else
				enext = e;
			DiscreteFunctionInputElement enextnext;
			if (it.hasNext())
				enextnext = it.next();
			else
				enextnext = enext;

			// entries for the xtoy function
			double qmin = e.min;
			double qmax = enext.min;
			double qintercept = e.value;
			double qslope = 0;

			QElement q = new QElement(qmin, qmax, qintercept, qslope);
			tempXtoY.add(q);

			// entry for the y to x function
			double miny = e.value;
			double maxy = enext.value;
			double intercepty = enext.min;

			QElement qy = new QElement(miny, maxy, intercepty, 0);
			tempYtoX.add(qy);

			// wind back
			if (!e.equals(enext)) {
				it.previous();
			}
			if (!enext.equals(enextnext)) {
				it.previous();
			}

		}

		// create the tree for x to y
		while (tempXtoY.size() != 1) {
			List list = new LinkedList();
			ListIterator<QElement> qit = tempXtoY.listIterator();
			while (qit.hasNext()) {
				QElement left = qit.next();
				QElement right = null;
				if (qit.hasNext())
					right = qit.next();
				QElement q = new QElement(left, right);
				list.add(q);
			}
			tempXtoY = list;
		}
		this.functionXtoY = tempXtoY.get(0);

		// create the tree for y to x
		while (tempYtoX.size() != 1) {
			List list = new LinkedList();
			ListIterator<QElement> qit = tempYtoX.listIterator();
			while (qit.hasNext()) {
				QElement left = qit.next();
				QElement right = null;
				if (qit.hasNext())
					right = qit.next();
				QElement q = new QElement(left, right);
				list.add(q);
			}
			tempYtoX = list;
		}

		this.functionYtoX = tempYtoX.get(0);

		return this;

	}
}
