/*
 * This file is part of SIMCOLEP, a simulation model developed by the Department of Public Health, 
 * Erasmus MC, University Medical Center Rotterdam, Rotterdam, The Netherlands.
 *
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/ 
 * or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA
 *
 * You are free to:
 * Share � copy and redistribute the material in any medium or format
 * Under the following terms:
 * Attribution � You must give appropriate credit, provide a link to the license, and indicate if changes were 
 * made. You may do so in any reasonable manner, but not in any way that suggests the licensor endorses
 * you or your use.
 * NonCommercial � You may not use the material for commercial purposes.
 * NoDerivatives � If you remix, transform, or build upon the material, you may not distribute the modified material.
 */

/*
 * Created on 23-Dec-2004
 *
 * Exponential function with parameter r, N0, a and t0 
 * Y = a + N0*e^(r*(t-t0))
 */
package simcolep.fun2;

import simcolep.exceptions.SimulationException;


/**
 * Exponential function with parameter r, N0 and a
 * <br></br> 
 * <i>Y = a + N0*base<sup>(t-t0)</sup></i>
 * <br>by default<i> base = e<sup> r</sup></i> where <i> r </i> should be set
 * 
 * @author ir E.A.J. Fischer
 *
 * @version 0.1.2 Last Modified 7-december-2005
 */
public final class Exponential 
		extends ContinuousFunction 
			implements Distributionable{

	/*Parameter names*/
	/**r*/
	private static final String p_r= "r"; //$NON-NLS-1$
	/**N0*/
	private static final String p_N0= "N0"; //$NON-NLS-1$
	/**t0*/
	private static final String p_t0= "t0"; //$NON-NLS-1$
	/**a = constant*/
	private static final String p_a= "a"; //$NON-NLS-1$
	/**base*/
	private static final String p_base= "base"; //$NON-NLS-1$
	/**
	 * 
	 */
	private static final long serialVersionUID = -473404914089948704L;
	
	/**rate*/
    protected double r = 0.0;
    /**value at time t = 0*/
	protected double N0 = 1.0;
	/**start time */
	protected double t0 = 0.0;
	/**constant a*/
	protected double a = 0.0;
	/**base*/
    protected double base = Math.E;
	
	/**
	 * 
	 * @param r
	 * @param N0
	 * @param a
	 * @param t0
	 */
	public Exponential(double r, double N0, double a, double t0) {
		super();
		this.t0 = t0;
		this.r = r;
		this.N0 = N0;
		this.a = a;
		}
	/**
	 * Constructor with default t0 = 0.0
	 * 
	 * @param r
	 * @param N0
	 * @param a
	 */
	public Exponential(double r, double N0, double a) {
		super();
		this.r = r;
		this.N0 = N0;
		this.a = a;
		}
	/**
	 * Constructor with 
	 * <ul>
	 * a = 0.0<br>
	 * t0 = 0.0
	 * </ul>
	 * 
	 * @param r
	 * @param N0
	 * 
	 * 
	 */
	public Exponential(double r, double N0){
		super();
		this.r = r;
		this.N0 = N0;
		}
	/**
	 * Constructor with default:
	 * <ul>
	 * a  = 0.0<br>
     * N0= 1.0<br>
     * t0 = 0.0
	 * </ul>
	 * @param r
	 */
	public Exponential(double r){
		super();
		this.r = r;
		
		}
    /**
     * Recalculate the growth rate such that the new base results in the same growth curve
     * 
     * @param base
     */
    public void setBase(double base)
    {
        //determine whether this base is larger than Euler's E
        if(Math.abs(base-Math.E)>Pythagoras.getDefaultPrecision())
        {
            this.base = base;
            //recalculate r
            this.r = Math.log(base);            
        }
    }
	/**
	 * 
	 * @param cfi
	 */
	public Exponential(ContinuousFunctionInput cfi)
	{
		setFunction(cfi);
	}
	
	/**
	 * 
	 *
	 */
	public Exponential()
	{
		super();
	}
	/**
	 * The parameters of the function can be changed.<br>
	 * Valid names for the parameters are:
	 * <ul>
	 * <li> r</li>
	 * <li>N0</li>
	 * <li>a</li>
	 * <li>t0</li>
	 * </ul>
	 * 
	 * @param name
	 * @param value
	 */
	public void changeParameter(String name, double value)
	{
		if(name.equals(p_r))
		{
			this.r = value;
		}else if (name.equals(p_N0))
		{
			this.N0 = value;
		}else if (name.equals(p_a))
		{
			this.a = value;
		}else if(name.equals(p_t0))
		{
			this.t0 = value;
			
		}else if(name.equals(p_base))
          {
            this.setBase(value);
          }else 
        	  throw new IllegalArgumentException(name +"is not a parameter of Exponential");
		
	}
	
	/**
	 * The parameters of the function can be requested.<br>
	 * Valid names for the parameters are:
	 * <ul>
	 * <li> r</li>
	 * <li>N0</li>
	 * <li>a</li>
	 * <li>t0</li>
	 * </ul>
	 * 
	 * @param name
	 * @param value
	 */
	public double getParameter(String name)
	{
		if(name.equals(p_r))
		{
			return this.r;
		}else if (name.equals(p_N0))
		{
			return this.N0;
		}else if (name.equals(p_a))
		{
			return this.a;
		}else if(name.equals(p_t0))
		{
			return this.t0;
			
		}else if(name.equalsIgnoreCase(p_base))
        {
		    return this.base;
        }throw new IllegalArgumentException(name +" is not a parameter of Exponential");
		
	}
	/** 
	 * @see simcolep.fun2.ContinuousFunction#setFunction(simcolep.fun2.ContinuousFunctionInput)
	 */
	public void setFunction(ContinuousFunctionInput CFI) {
		this.r = CFI.getParameter(p_r).doubleValue();
		if(CFI.getParameter(p_N0)!=null)
		{
			this.N0 = 
				CFI.getParameter(p_N0).doubleValue();
		} else {this.N0 = 1.0;}
		if(CFI.getParameter(p_a)!=null)
		{
		this.a = CFI.getParameter(p_a).doubleValue();
		}else {this.a=0.0;}
		if(CFI.getParameter(p_t0)!=null)
		{
			this.t0= CFI.getParameter(p_t0).doubleValue();
		}else{this.t0=0.0;}

	}

	

	/**
	 * @see simcolep.fun2.Function#getY(double)
	 */
	public double getY(double X) {
		
		return (this.a+this.N0*Math.exp(this.r*(X-t0)));
	}

	
	/**
     * @return t0+(1/this.r)*Math.log((Yvalue-this.a)/this.N0)
	 * @see simcolep.fun2.Function#getX(double)
	 */
	public double getX(double y) {
        //if N0 equals 0 return Double.POSITIVE_INFINITY
        if(this.N0==0.0) 
        	return Double.POSITIVE_INFINITY;
        //if Y < a return Positive or Negative infinity
        if(y<=this.a) 
        	return -Math.signum(this.r)*Double.POSITIVE_INFINITY;
        //else return the X for which the function has value Y
		return t0+(1/this.r)*Math.log((y-this.a)/this.N0);
	}
	

	/** 
	 * <i>f'(Xvalue) = -N<sub>0</sub>*r*e<sup>-r(Xvalue-t<sub>0</sub>)</sup></i>
	 * @see simcolep.fun2.Function#getDeritive(double)
	 */
	public double getDerivative(double x) {
		
		return this.r*this.N0*Math.exp(this.r*(x-this.t0));
	}
	/**
	 * 
	 * Gives the integration from 0 to Xvalue:<br>
     * for r &lt 0: F(X) = (1/-r)*(e<sup>-r*t0</sup>*N0-e<sup>-r*(t0-X)</sup>*N0 + a*X<br>
     * for r &gt 0: F(X) = (1/r)*e<sup>-r*t0</sup>*(e<sup>r*Xvalue</sup>-1)*N0 + a*Xvalue<br>
     * 
	 * @see simcolep.fun2.Function#getIntegrated(double)
	 */
	public double getIntegrated(double x) {
        if(this.r<0)
        {
            return (1/-this.r)*(Math.exp((-this.r*this.t0)-Math.exp(-this.r*(this.t0-x))))+this.a*x - (1/-this.r);
        }else if (this.r>0)
        {
            return(1/this.r)*(Math.exp(-this.r*this.t0)*(Math.exp(this.r*x)))*this.N0 +this.a*x - (1/this.r);
        } else return (this.a+this.N0)*x;
		
        
	}
	/**
	 * Gives the invers of the integrated or more specifically:<br>
	 * the Xvalue for which the integration from 0 equals Yvalue
	 * <br>for a == 0<br>
	 * 
	 * <i>x = t<sub> 0</sub> +<sup>1</sup>/<sub>r</sub> .ln( e<sup>-r*t0</sup> + (r.y/N0) ) </i> 
	 * <br> for a != 0 the numeric invintegration of Pythogoras is used
	 * @see simcolep.fun2.Function#getInvIntegrated(double)
     * @see simcolep.fun2.Pythagoras#numericInvIntegrated(Function, double, double) with start value 0
	 */
	public double getInvIntegrated(double y) {
		if(a!=0.0) 
			return Pythagoras.numericInvIntegrated(this,y,0);
		
        return this.t0 + (1/this.r)*Math.log(Math.exp(-this.r*this.t0)+(this.r*y)/this.N0);
		 
	}
    
	/**
     * 
     * @param y
     * @return x for this value of y in an exponential distribution.<br><b>Warning:</b> 
     * <li>This method does not use the parameters a, t0 or N0
     * <li>if r &gt 0 this method returns as if r = -r 
     * 
     * @see simcolep.fun2.Distributionable#drawX(double)
     *
     */
    public double drawX(double y) {
        if((y<0.0||y>1.0))
        	throw new IllegalArgumentException("y > 1 OR y < 0: " + y);
        //positive r
        if(this.r>0)
        	throw new SimulationException(
        			"Parameter r of the exponential distribution" +
        			" is greater than 0.0");
      
        return (1/this.r)*Math.log(1.0-y);
       
    }
    /**
     * 
     * @param y
     * @return x for this value of y in an exponential distribution.<br><b>Warning:</b> 
     * <li>This method does not use the parameters a, t0 or N0
     * <li>if r &gt 0 this method returns as if r = -r 
     * 
     * @see simcolep.fun2.Distributionable#drawX(double)
     *
     */
    public double drawX(double[] y) {
        if((y[0]<0.0||y[0]>1.0))
        	throw new IllegalArgumentException("y > 1 OR y < 0: " + y);
        //positive r
        if(this.r>0)
        	throw new SimulationException(
        			"Parameter r of the exponential distribution" +
        			" is greater than 0.0");
      
        return (1/this.r)*Math.log(1.0-y[0]);
       
    }
    
    /**
     * @see simcolep.fun2.Distributionable#drawXconditional(double, double)
     */
    public double drawXconditional(double[] p) {
        double[] newY = 
        	{getF(new double[]{p[1]})+p[0]*(1-getF(new double[]{p[1]}))};
        return drawX(newY);

    }
    
    /**
     *@return mean of an exponential distribution = Math.abs(1/this.r)
     * @see simcolep.fun2.Distributionable#mean()
     */
    public double mean() {
        
        return Math.abs(1.0/this.r);
    }
    
    @Override
    public Exponential clone() {
        
        return new Exponential(this.r, this.N0, this.a, this.t0);
    }
    
    @Override
    public boolean equalsFunction(Function f) {
        if(!this.getClass().equals(f.getClass()))return false;
        Exponential ef =(Exponential)f;
        //if one of these is not the same return false
        if(!(Math.abs(this.a-ef.a)<Pythagoras.getDefaultPrecision()&&Math.abs(this.N0-ef.N0)<Pythagoras.getDefaultPrecision()&&Math.abs(this.t0 - ef.t0)<Pythagoras.getDefaultPrecision())) 
        	return false;
        //if these are the same return true
        if(Math.abs(this.base-ef.base)<Pythagoras.getDefaultPrecision()&&Math.abs(this.r-ef.r)<Pythagoras.getDefaultPrecision())
        	return true;
        //even with the different base and different r the same function can be described
        if(Math.abs(Math.exp(this.r)-ef.base)<Pythagoras.getDefaultPrecision())
        	return true;
        //r is positive or negative infinity and base is either positive infinity or zero
        if(Double.isInfinite(this.r)&&Double.isInfinite(ef.r))
        	return true;
        if(Double.isInfinite(this.r)&&this.r>0&&Double.isInfinite(ef.base))
        	return true;
        if(Double.isInfinite(this.r)&&this.r<0&&ef.base<Pythagoras.getDefaultPrecision()) 
        	return true;
        if(Double.isInfinite(ef.r)&&ef.r>0&&Double.isInfinite(this.base)) 
        	return true;
        if(Double.isInfinite(ef.r)&&ef.r<0&&this.base<Pythagoras.getDefaultPrecision()) 
        	return true;
        
        return false;
       
    }
    /**
     * @see simcolep.fun2.Distributionable#getF(double[])
     */
    public double getF(double d) {
		if(d < 0)
			return 0.0;
		
	    //positive r
        if(this.r>0)
        	throw new SimulationException(
        			"Parameter r of the exponential distribution" +
        			" is greater than 0.0");
		
		return 1 - Math.exp(d*r);
		
	}
    /**
     * @see simcolep.fun2.Distributionable#getF(double[])
     */
    public double getF(double[] d) {
		if(d[0] < 0)
			return 0.0;
		
	    //positive r
        if(this.r>0)
        	throw new SimulationException(
        			"Parameter r of the exponential distribution" +
        			" is greater than 0.0");
		
		return 1 - Math.exp(d[0]*r);
		
	}

}
