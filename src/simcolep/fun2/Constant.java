/*
 * This file is part of SIMCOLEP, a simulation model developed by the Department of Public Health, 
 * Erasmus MC, University Medical Center Rotterdam, Rotterdam, The Netherlands.
 *
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/ 
 * or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA
 *
 * You are free to:
 * Share � copy and redistribute the material in any medium or format
 * Under the following terms:
 * Attribution � You must give appropriate credit, provide a link to the license, and indicate if changes were 
 * made. You may do so in any reasonable manner, but not in any way that suggests the licensor endorses
 * you or your use.
 * NonCommercial � You may not use the material for commercial purposes.
 * NoDerivatives � If you remix, transform, or build upon the material, you may not distribute the modified material.
 */

/*
 * Created on 18-Jul-2005
 *
 */
package simcolep.fun2;


/**
 * This class always returns a constant 
 * 
 * 
 * @author ir E.A.J. Fischer
 * @version 0.1.2 -- Last Modified 18-Jul-2005
 */
public final class Constant 
		extends ContinuousFunction 
			implements Distributionable{

	/**name of the parameter*/
	private static final String C = "c"; 

	/***/
    public static final long serialVersionUID = -7630775500739872724L;
    
    /**value of a constant*/
    protected double c = 0.0;
    
    /**
     * 
     *
     */
    public Constant()
    {
    	
    }
    /**
     * 
     * @param c
     */
    public Constant(double c)
    {
        this.c = c;
    }
    /**
     * @see simcolep.fun2.Function#changeParameter(String, double)
     */
    public void changeParameter(String name, double value)
    {
        this.c = value;
    }
    /**
     * 
     * @param cfi
     */
    public Constant(ContinuousFunctionInput cfi) {
        this.c = cfi.getParameter(C).doubleValue();
        
       
    }

    /**
     * @see simcolep.fun2.Function#getYvalue(double)
     */
    public double getY(double X) {
        
        return c;
    }

    /**
     * @see simcolep.fun2.Function#getXvalue(double)
     */
    public double getX(double y) {
       
        return c;
    }

    /**
     * @see simcolep.fun2.Function#getDerivative(double)
     */
    public double getDerivative(double x) {
       
        return c;
    }

    /**
     * @see simcolep.fun2.Function#getIntegrated(double)
     */
    public double getIntegrated(double x) {
        
        return c;
    }

    /**
     * @see simcolep.fun2.Function#getInvIntegrated(double)
     */
    public double getInvIntegrated(double y) {
       
        return c;
    }
   
    /**
     * @see simcolep.fun2.Distributionable#drawX(double)
     */
     public double drawX(double y) {
         
         return c;
     }

   /**
    * @see simcolep.fun2.Distributionable#drawX(double)
    */
    public double drawX(double[] y) {
        
        return c;
    }
    
    /**
     * @see simcolep.fun2.Function#drawXconditional(double, double)
     */
    public double drawXconditional(double[] p)
    {
        return c;
    }
    
    /**
     * @return c
     */
    
    public double mean() {
        
        return c;
    }
    
    /**
     * 
     * @see java.lang.Object#clone()
     */
    @Override
    public Constant clone() {
        
        return new Constant(this.c);
    }
    /**
     * @see simcolep.fun2.Function#equalsFunction(simcolep.fun2.Function)
     */
    @Override
    public boolean equalsFunction(Function f) {
        if(!f.getClass().equals(this.getClass()))return false;
        
        if(Double.isInfinite(this.c)&&Double.isInfinite(((Constant)f).c)) return true;
        
        return(Math.abs(this.c - ((Constant)f).c)<Pythagoras.getDefaultPrecision());
        
    }
    
 
    
    @Override
    public double getParameter(String pname) {
        
        return this.c;
    }

  
    
	@Override
	public void setFunction(ContinuousFunctionInput cfi) {
		 this.c = cfi.getParameter(C).doubleValue();
		
	}
	
	/**
	 * Only method of Constant that does not return c but 0 or 1
	 *  
	 * 
	 * @see simcolep.fun2.Distributionable#getF(double)
	 */
	public double getF(double d) {
		if(Math.abs(d - c) < Pythagoras.getDefaultPrecision())
			return 1;
		else 
			return 0;
		
	}
	
	/**
	 * Only method of Constant that does not return c but 0 or 1
	 *  
	 * 
	 * @see simcolep.fun2.Distributionable#getF(double)
	 */
	public double getF(double[] d) {
		if(Math.abs(d[0] - c) < Pythagoras.getDefaultPrecision())
			return 1;
		else 
			return 0;
		
	}
}
