/*
 * This file is part of SIMCOLEP, a simulation model developed by the Department of Public Health, 
 * Erasmus MC, University Medical Center Rotterdam, Rotterdam, The Netherlands.
 *
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/ 
 * or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA
 *
 * You are free to:
 * Share � copy and redistribute the material in any medium or format
 * Under the following terms:
 * Attribution � You must give appropriate credit, provide a link to the license, and indicate if changes were 
 * made. You may do so in any reasonable manner, but not in any way that suggests the licensor endorses
 * you or your use.
 * NonCommercial � You may not use the material for commercial purposes.
 * NoDerivatives � If you remix, transform, or build upon the material, you may not distribute the modified material.
 */

/*
 * Created on 19-Nov-2004
 *
 */
package simcolep.fun2;

import java.awt.Point;

/**
 * @author ir. E.A.J. Fischer
 * @version 1.2 -- Last Modified 12-04-07
 *
 *<br>a quote from P. Erdos:
<BLOCKQUOTE>
"A mathematician is a device for turning coffee into theorems."
</BLOCKQUOTE>
 */
public final class Pythagoras {

    private static double DefaultPrecision = 1E-9; 
    private static double DefaultStep = 1E-05;
	private static double DefaultRepeats = 1E10;
	private static double DefaultDecimals = 1E9;//corresponds to precision 
	
    
    /**Gives the value of the numeric integral for function
	 * from a to b. The number of steps, m = 2* (b-a)/DefaultStep
	 * <br> this method uses the Simpson-rule 
     * 
     * 
	 * 
     * @param function
     * @param a
     * @param b
	 * @return integral from a to b
	 */
	public static double numericIntegration(ContinuousFunction function, double a, double b)
	{
        double integral = function.getY(a)+function.getY(b);
        double m = (b-a)/DefaultStep;
        double h = (b-a)/2*m;
		for(int j =1;j<2*m;j++)
        {
            double x = a + j*h;
		    if(j%2!=0) integral +=4*function.getY(x);
            else integral +=2*function.getY(x);
        }
        integral += function.getY(b);
		integral = (1.0/3.0)*DefaultStep*integral;
		return integral;
	}
	

	/**
	 * 
	 * @param value
	 * @param under
	 * @param upper
	 * @return
	 */
	public static boolean between(double value, double under, double upper)
	{
		return (value >= under && value <= upper);
	}
	/**
	 * 
	 * @param point1
	 * @param point2
	 * @return
	 */
	public static double slope(Point point1, Point point2)
	{
		if(point1.equals(point2))
		{
			System.out.println("Two equal points. Slope defined as 0.0 ");
			return 0.0;
		}
		return (point1.getY()-point2.getY())/(point1.getX()-point2.getX());
	}
	
	/**
	 * 
	 * @param v
	 * @return normalised vector v
	 */
	public static double[] NormaliseVector(double[]v)
	{
		double sum=0;
		for(int x =0; x<v.length;x++)
		{
			sum+=v[x];
		}
		for(int x =0; x<v.length;x++)
		{
			v[x]=v[x]/sum;
		}
		
		return v;
	}
	/**
	 * 
	 * @param base
	 * @param exp
	 * @return power of base to the exp
	 */
	public static double pow(double base, double exp)
	{
		double value = Math.pow(base,exp);
		
		if(new Double(value).equals(new Double(Double.NaN)))
		{
			//a negative base
			if(base < 0)
			{
				//and the absolute value of the power that is smaller than 1
			if(Math.abs(exp) < 1)
				{
				//if 1/exp is an odd integer number 
					if((1/exp)%2==1.0)
					{
						value = -Math.pow(-base,exp);
					}
					
				}
				
			}
						
		}
		
		return value;
	}

	/**
	 * 
	 * @param n
	 * @return n!
	 */
    public static double factorial(double n) {
        if      (n <  0) 
        	throw new RuntimeException("Underflow error in factorial");
        else if (n > 170) 
        	throw new RuntimeException("Overflow error in factorial");
        else if (n < 1) return 1;
        else  return n * factorial(n-1);
    }
    
    /**
	 * 
	 * @param n
	 * @return n!
	 */
    public static int factorial(int n) {
        if      (n <  0) throw new RuntimeException("Underflow error in factorial");
        else if (n > 13) throw new RuntimeException("Overflow error in factorial");
        else if (n < 1) return 1;
        else             return n * factorial(n-1);
    }
    
    /**
	 * 
	 * @param n
	 * @return n!
	 */
    public static long factorial(long n) {
        if      (n <  0) throw new RuntimeException("Underflow error in factorial");
        else if (n > 20) throw new RuntimeException("Overflow error in factorial");
        else if (n < 1) return 1;
        else             return n * factorial(n-1);
    }

    
   
    /**
     * @param d
     * @param e
     * @param function
     * @return
     */
    public static double sum(double i, double n, Function function) {
        double sum = 0;
        while(i<=n)
        {
            
            sum = sum+function.getY(i);
            i++;
        }
        
        return sum;
    }
    
    /**
     * <b><i>!! WARNING !!</i></b> This method only works properly for continuously decreasing or increasing functions. Otherwise it will return only one of the (multiple) Xvalue corresponding to the Yvalue of the integration!!<br><br>
     * The method tries to find the Xvalue for which:<br><br>
     * <i>function(Xvalue)-Yvalue = 0</i>
     * Uses the Newton(-Raphson) method.
     * @param function
     * @param start 
     * 
     * @return InvIntegrated of function for Yvalue
     */
    public static double numericgetXvalue(Function function, double Yvalue, double start)
    {
        double x =  start;
        int SAFE =0;
        while(Math.abs(function.getY(x)-Yvalue)>DefaultPrecision&&SAFE<DefaultRepeats)
        {
            SAFE +=1;
            x = x-(function.getY(x)-Yvalue)/(function.getDerivative(x));
        }
        if(Math.abs(function.getY(x)-Yvalue)>DefaultPrecision)return Double.NaN;
        return x;
    }
    /**
     * <b><i>!! WARNING !!</i></b> This method only works properly in the complete range of X values for continuously decreasing or increasing functions (monotone functions). Otherwise it will return only one of the (multiple) Xvalue corresponding to the Yvalue of the integration!! <br>When the functions has no value of X for which the integration is Y it returns Double.NaN<br><br>
     * The method finds the Xvalue for which:<br><br>
     * <i>function(Xvalue)-Yvalue = 0</i>
     * Uses the Newton(-Raphson) method.
     * 
     * @param function
     * @return InvIntegrated of function for Yvalue
     */
    public static double numericInvIntegrated(Function function, double Yvalue, double start)
    {
    	
        double x =  start;
        int SAFE=0;
        while(Math.abs(function.getIntegrated(x)-Yvalue)>DefaultPrecision&&SAFE<DefaultRepeats)
        {
            SAFE+=1;
            
            if(function.getY(x)==0.0)return Double.POSITIVE_INFINITY;
            x = x-(function.getIntegrated(x)-Yvalue)/(function.getY(x));
        }
        if(Math.abs(function.getIntegrated(x)-Yvalue)>DefaultPrecision) 
        	{
        		System.out.println("point not found for " +function.toString());
        		return Double.NaN;
        	}
        return x;
    }
   
    /**
     * 
     *sets the DefaultPrecision for Pythagoras
     *@param d -- new precision
     *@return p -- old default precision (Default = 10E-10)
     */
    public static double setDefaultPrecision(double d)
    {
        double p = DefaultPrecision;
        DefaultPrecision =d;
        return p;
    }
    /**
     * 
     *sets the DefaultRepeats for Pythagoras
     *@param d -- new precision
     *@return p -- old default precision (Default = 10E-10)
     */
    public static double setDefaultRepeats(double d)
    {
        double p = DefaultRepeats;
        DefaultRepeats = d;
        return p;
    }

    /**
     * @return DefaultPrecision = 1E-9
     */
    public static double getDefaultPrecision() {
        
        return DefaultPrecision;
    }
    /**
     * @return DefaultDecimals = 1E9
     */
    public static double getDefaultDecimals() {
        
        return DefaultDecimals;
    }

    /**
     * 
     * @param x
     * @param i
     * @return exponential sum function for x and i
     */
    public static double expsumf(double x, int i) {
        double sum = 0;
        for(int j =0; j<i;j++)
        {
            sum+=pow(x,j)/factorial(j);
        }
        return sum;
        
    }

    /**
     * Returns the distance between two point using the Pythagorion theorem a<sup>2</sup> + b<sup>2</sup> = c<sup>2</sup>
     * 
     * @param p1
     * @param p2
     */
    public static double distance(Point p1, Point p2) {
        
        return p1.distance(p2);
        
    }
}
