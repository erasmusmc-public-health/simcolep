/*
 * This file is part of SIMCOLEP, a simulation model developed by the Department of Public Health,
 * Erasmus MC, University Medical Center Rotterdam, Rotterdam, The Netherlands.
 *
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/
 * or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA
 *
 * You are free to:
 * Share � copy and redistribute the material in any medium or format
 * Under the following terms:
 * Attribution � You must give appropriate credit, provide a link to the license, and indicate if changes were
 * made. You may do so in any reasonable manner, but not in any way that suggests the licensor endorses
 * you or your use.
 * NonCommercial � You may not use the material for commercial purposes.
 * NoDerivatives � If you remix, transform, or build upon the material, you may not distribute the modified material.
 */

/*
 * Created on 16-dec-2004
 *
 */
package simcolep.fun2;

import java.io.Serializable;
import java.util.LinkedList;

import simcolep.tools.PN;

/**
 * Input of discrete functions
 * 
 * @author ir E.A.J. Fischer
 * @version 0.1.1 -- Last Modified 16-12-04
 */
public final class DiscreteFunctionInput extends LinkedList<DiscreteFunctionInputElement> implements Serializable {

	/**	 */
	private static final long serialVersionUID = 6259930100430046376L;

	/** cumulative function */
	private boolean cum = false;

	/**
	 * 
	 * @param cum
	 */
	public DiscreteFunctionInput(boolean cum) {
		this.cum = cum;
	}

	/**
	 *
	 * @param cum
	 */
	public DiscreteFunctionInput(boolean cum, double[][] input) {
		this.cum = cum;

		for (double[] e : input) {
			this.add(new DiscreteFunctionInputElement(e[0], e[1]));
		}
	}

	/**
	 * Add a DiscreteFunctionElement to the DiscreteFunctionInput class Checks
	 * whether a cumulative function gets cumulative values
	 * 
	 * @param element
	 * @return <code>boolean</code>
	 * @throws <code>IllegalArgumentException</code>
	 */

	@Override
	public boolean add(DiscreteFunctionInputElement element) {
		if (cum) {
			if (!super.isEmpty()) {
				if (super.getLast().value > element.value) {
					throw new IllegalArgumentException(
							super.getLast().value + " > " + element.value + " is not cumulative");
				}
			}
			return super.add(element);

		} else {
			return super.add(element);
		}

	}

	/**
	 * 
	 * @param dfi
	 * @return true if equivalent
	 */
	public boolean equivalent(DiscreteFunctionInput dfi) {
		if (Math.abs(dfi.size() - this.size()) > 0)
			return false;

		for (int x = 0; x < this.size(); x++) {
			if (Math.abs(dfi.get(x).value - this.get(x).value) > Pythagoras.getDefaultPrecision())
				return false;
			if (Math.abs(dfi.get(x).min - this.get(x).min) > Pythagoras.getDefaultPrecision())
				return false;
		}

		return true;
	}

	/**
	 *
	 * @return truw when the function elements are cumulative
	 */
	public boolean isCum() {
		return this.cum;
	}

	/**
	 * 
	 * @param cum
	 */
	public void setCum(boolean cum) {
		this.cum = cum;

	}

	/**
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		String string = new String();
		for (int x = 0; x < this.size(); x++) {
			string = string + this.get(x) + PN.LINESEPARATOR;
		}
		return string;
	}

}
