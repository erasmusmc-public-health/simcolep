/*
 * This file is part of SIMCOLEP, a simulation model developed by the Department of Public Health,
 * Erasmus MC, University Medical Center Rotterdam, Rotterdam, The Netherlands.
 *
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/
 * or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA
 *
 * You are free to:
 * Share � copy and redistribute the material in any medium or format
 * Under the following terms:
 * Attribution � You must give appropriate credit, provide a link to the license, and indicate if changes were
 * made. You may do so in any reasonable manner, but not in any way that suggests the licensor endorses
 * you or your use.
 * NonCommercial � You may not use the material for commercial purposes.
 * NoDerivatives � If you remix, transform, or build upon the material, you may not distribute the modified material.
 */

package simcolep.fun2;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;

import simcolep.exceptions.SimulationException;
import simcolep.tools.XMLable;

/**
 * Abstract superclass for all continuous function to be written
 *
 * @author ir. E.A.J. Fischer
 * @version 0.1.1--Last Modified 16 august 2004
 */

public abstract class ContinuousFunction extends Function {
	private static final String WITH_PARAMETERS = " with parameters:";
	/***/
	public static final long serialVersionUID = -2504399264868758974L;

	/**
	 *
	 *
	 */
	public ContinuousFunction() {

	}

	/**
	 *
	 * @param cf
	 */
	public ContinuousFunction(ContinuousFunction cf) {
	}

	/**
	 *
	 * @param cfi
	 */
	public ContinuousFunction(ContinuousFunctionInput cfi) {
		this.setFunction(cfi);
	}

	/**
	 *
	 * @param name
	 * @param value
	 */
	@Override
	public abstract void changeParameter(String name, double value);

	/**
	 *
	 * @param name
	 * @return value of parameter name
	 */
	@Override
	public abstract double getParameter(String name);

	/**
	 * get Xvalue given Y value
	 *
	 *
	 * @param y
	 *
	 * @return <code><b>double</b></code> Xvalye
	 * @see Function#getX(doubl[])
	 */
	@Override
	public abstract double getX(double y);

	/**
	 * @return Double.POSITIVE_INFINITY
	 */
	@Override
	public double getXmax() {
		return Double.POSITIVE_INFINITY;
	}

	/**
	 * get Y value given X value
	 * 
	 * @param X
	 * @return Yvalue
	 *
	 * @see Function#getY(double[])
	 */
	@Override
	public abstract double getY(double X);

	/**
	 * @see simcolep.fun2.Function#inputGroup()
	 */
	@Override
	public int inputGroup() {
		return Function.CONTINUOUS_FUNCTION_INPUT;
	}

	/**
	 *
	 * @param CFI
	 */
	public abstract void setFunction(ContinuousFunctionInput CFI);

	/**
	 * @see java.lang.Object#toString()
	 *
	 */
	@Override
	public String toString() {
		String fs = (this.getClass().getSimpleName() + WITH_PARAMETERS + Function.TAB);

		for (Field f : this.getClass().getDeclaredFields()) {

			int modifier = f.getModifiers();
			if (modifier == Modifier.PROTECTED || modifier == Modifier.PUBLIC)
				try {
					fs += f.getName() + Function.TAB + f.get(this) + Function.TAB;
				} catch (IllegalAccessException e) {
					throw new SimulationException(e);
				}
		}

		return fs;

	}

	/**
	 *
	 * @see simcolep.fun2.Function#toXMLElement()
	 */
	public org.jdom.Element toXMLElement() {
		// create element
		org.jdom.Element element = new org.jdom.Element(XMLable.FUNCTION);

		// add attribute
		element.setAttribute(XMLable.TYPE, this.getName());

		// add parameters as attribute
		for (Field param : this.getClass().getDeclaredFields()) {
			if (!Modifier.isStatic(param.getModifiers()))
				// parameters are primitives
				if (param.getType().isPrimitive()) {
					// get parameter name
					String parameter = param.getName();
					// set teh atrtibute
					element.setAttribute(parameter, Double.toString(this.getParameter(parameter)));
				}
		}

		// and return the element
		return element;

	}
}
