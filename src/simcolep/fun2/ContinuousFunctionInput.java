/*
 * This file is part of SIMCOLEP, a simulation model developed by the Department of Public Health, 
 * Erasmus MC, University Medical Center Rotterdam, Rotterdam, The Netherlands.
 *
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/ 
 * or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA
 *
 * You are free to:
 * Share � copy and redistribute the material in any medium or format
 * Under the following terms:
 * Attribution � You must give appropriate credit, provide a link to the license, and indicate if changes were 
 * made. You may do so in any reasonable manner, but not in any way that suggests the licensor endorses
 * you or your use.
 * NonCommercial � You may not use the material for commercial purposes.
 * NoDerivatives � If you remix, transform, or build upon the material, you may not distribute the modified material.
 */

/*
 * Created on 16-dec-2004
 *
 */
package simcolep.fun2;


import java.util.Map;
import java.util.LinkedHashMap;

/**
 * Contains the information that a continuous function needs
 * 
 * @author ir E.A.J. Fischer
 * @version 0.1.1 -- Last Modified 16-dec-2004
 *
 */

public final class ContinuousFunctionInput 
			extends LinkedHashMap<String,Double> {

	
	/**
     * 
     */
    private static final long serialVersionUID = 1L;
    /**
	 * 
	 */
	public ContinuousFunctionInput() {
			
	}
	/**
	 * Sets the continuous function input given the parameters 
	 * 
	 * @param parameters
	 */
	public ContinuousFunctionInput(Map<String,Double> parameters)
	{
		this.putAll(parameters);
	}
	/**
	 * Put one parameter (name and value) in the continuous function input
	 * 
	 * @param  name
	 * @param  value
	 */
	public void putParameter(String name, double value)
	{
		this.put(name,new Double(value));
	}
	/**
	 * 
	 * @param name
	 * @return <code> Double</code> parameter
	 */
	public Double getParameter(String name)
	{
		if(!this.containsKey(name))
		{
			return null;
		}
		return ((Double)this.get(name));
	}

}
