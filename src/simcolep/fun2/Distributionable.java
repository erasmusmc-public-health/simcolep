/*
 * This file is part of SIMCOLEP, a simulation model developed by the Department of Public Health,
 * Erasmus MC, University Medical Center Rotterdam, Rotterdam, The Netherlands.
 *
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/
 * or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA
 *
 * You are free to:
 * Share � copy and redistribute the material in any medium or format
 * Under the following terms:
 * Attribution � You must give appropriate credit, provide a link to the license, and indicate if changes were
 * made. You may do so in any reasonable manner, but not in any way that suggests the licensor endorses
 * you or your use.
 * NonCommercial � You may not use the material for commercial purposes.
 * NoDerivatives � If you remix, transform, or build upon the material, you may not distribute the modified material.
 */

/*
 * Created Date: 17 august 2004
 */

package simcolep.fun2;

import java.io.Serializable;

/**
 *
 * @author ir. E.A.J. Fischer
 * @version 0.1.1 -- Last modified 29-04-05
 *
 */

public interface Distributionable extends Serializable {

	/**
	 * Distributionables should implement this method to draw a (random) value
	 * for x from the distribution that they implement. For every separate
	 * distribution this should be determined (for some this will be getX for
	 * others this will be getInvIntegrated) The value of u can sometimes be
	 * bounded to the range 0-1
	 * 
	 * @param u
	 * @return the x corresponding to u
	 */
	public double drawX(double u);

	/**
	 * Distributionables should implement this method to draw a (random) value
	 * for x from the distribution that they implement. For every separate
	 * distribution this should be determined (for some this will be getX for
	 * others this will be getInvIntegrated) The value of u can sometimes be
	 * bounded to the range 0-1
	 * 
	 * @param u
	 * @return the x corresponding to u
	 */
	public double drawX(double[] u);

	/**
	 * Use this distribution with a condition on the Xvalue
	 *
	 *
	 * @param u
	 * @param p
	 *            - containing at least two parameters, a random value between
	 *            0-1 and a condition
	 * @return Xvalue
	 */
	public double drawXconditional(double[] p);

	/**
	 * Get the value of the cumulative distribution function
	 * 
	 * @param d
	 * @return
	 */
	public abstract double getF(double p);

	/**
	 * Get the value of the cumulative distribution function
	 * 
	 * @param d
	 * @return
	 */
	public abstract double getF(double[] p);

	/**
	 * 
	 * @return mean of this distribution
	 */
	public abstract double mean();

}
