/*
 * This file is part of SIMCOLEP, a simulation model developed by the Department of Public Health,
 * Erasmus MC, University Medical Center Rotterdam, Rotterdam, The Netherlands.
 *
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/
 * or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA
 *
 * You are free to:
 * Share � copy and redistribute the material in any medium or format
 * Under the following terms:
 * Attribution � You must give appropriate credit, provide a link to the license, and indicate if changes were
 * made. You may do so in any reasonable manner, but not in any way that suggests the licensor endorses
 * you or your use.
 * NonCommercial � You may not use the material for commercial purposes.
 * NoDerivatives � If you remix, transform, or build upon the material, you may not distribute the modified material.
 */

/*
 * Created on 16-dec-2004
 *
 */
package simcolep.fun2;

import java.io.Serializable;

import simcolep.exceptions.SimulationException;
import simcolep.tools.PN;

/**
 * This class is used as an data element for a discrete function An element has
 * three values that it can return
 *
 * @author ir E.A.J. Fischer
 * @version 0.1.1 -- Last Modified 16/12/04
 *
 *
 */
public final class DiscreteFunctionInputElement implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 5712169229739616882L;

	private static final String MIN = "Minimum: ";
	private static final String MAX = "Maximum: ";
	private static final String VALUE = "Value: ";

	/** minimum value of this element */
	public double min = 0;

	/** maximum value of this element */
	public double max = Double.POSITIVE_INFINITY;

	/** the Y-value of this element */
	public double value = 0;

	/**
	 * 
	 * @param element
	 */
	public DiscreteFunctionInputElement(DiscreteFunctionInputElement element) {
		this.min = element.min;
		this.value = element.value;
		this.max = element.max;
	}

	/**
	 *
	 * @param min
	 * @param value
	 */
	public DiscreteFunctionInputElement(double min, double value) {
		this.min = min;
		this.value = value;
	}

	public DiscreteFunctionInputElement(double min, double max, double value) {
		this.min = min;
		this.max = max;
		this.value = value;
		// min and max are not allowed to have the same value or max to be
		// smaller than min
		if (min >= max) {
			throw new SimulationException(this + " max >= min");

		}
	}

	/**
	 *
	 * @return <code> boolean</code>
	 */
	public boolean hasmax() {
		return !Double.isInfinite(this.max);
	}

	/**
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuffer strbuf = new StringBuffer(PN.RIGHT_SQUARE_BRACKET + Function.TAB);
		strbuf.append(MIN + Function.TAB + this.min + PN.SEMICOLON);
		if (this.hasmax())
			strbuf.append(MAX + Function.TAB + this.max + PN.SEMICOLON);
		strbuf.append(VALUE + Function.TAB + this.value + PN.LEFT_SQUARE_BRACKET);
		return strbuf.toString();
	}

}
