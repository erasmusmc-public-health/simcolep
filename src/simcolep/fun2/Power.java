/*
 * This file is part of SIMCOLEP, a simulation model developed by the Department of Public Health, 
 * Erasmus MC, University Medical Center Rotterdam, Rotterdam, The Netherlands.
 *
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/ 
 * or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA
 *
 * You are free to:
 * Share � copy and redistribute the material in any medium or format
 * Under the following terms:
 * Attribution � You must give appropriate credit, provide a link to the license, and indicate if changes were 
 * made. You may do so in any reasonable manner, but not in any way that suggests the licensor endorses
 * you or your use.
 * NonCommercial � You may not use the material for commercial purposes.
 * NoDerivatives � If you remix, transform, or build upon the material, you may not distribute the modified material.
 */

/*
 * Created on 30-jun-2005
 *
 * 
 */
package simcolep.fun2;

/**
 * 
 * Power Function given by:
 * <br></br>
 * <i><b>Y</b> = a (x+b)<sup>c</sup> + d</i>
 * <br>
 *  @author ir. E.A.J. Fischer
 * @version 0.1.1 --Last Modified 30-06-2005
 *
 *
 */

public final class Power 
		extends ContinuousFunction {
    /**
	 * 
	 */
	private static final long serialVersionUID = -4974941959646484612L;
	
	private static final String A = "a";
	private static final String B = "b";
	private static final String C = "c";
	private static final String D = "d";
	/**default 1*/
	protected double a =1;
    /**default 0*/
	protected double b =0;
    /**default 1*/
	protected double c =1;
    /**default 0*/
	protected double d = 0;
	
    /**
	 * 
	 */
	public Power() {
		super();
		
	}
	/**
	 * 
	 * @param a
	 * @param b
	 * @param c
	 * @param d
	 */
	public Power(double a,double b,double c,double d)
	{
		this.a = a;
		this.b = b;
		this.c = c;
		this.d = d;
	}

	/**
	 * 
	 * @param a
	 * @param b
	 * @param c
	 */
	public Power(double a,double b,double c)
	{
		this.a = a;
		this.b = b;
		this.c = c;
	}
	
	/**
	 * 
	 * @param a
	 * @param c
	 */
	public Power(double a,double c)
	{
		this.a = a;
		this.c = c;
	}
	/**
     * 
     * @param a
	 */
    public Power(double c) {
       this.c =c;
    }
	/**
	 * @param cfi
	 */
	public Power(ContinuousFunctionInput cfi) {
		setFunction(cfi);
	}

	/**
	 * @param cf
	 */
	public Power(ContinuousFunction cf) {
		this.a= cf.getParameter(A);
		this.b= cf.getParameter(B);
		this.c = cf.getParameter(C);
		this.d = cf.getParameter(D);
	}

	
    /**
	 * @see simcolep.fun2.ContinuousFunction#setFunction(simcolep.fun2.ContinuousFunctionInput)
	 */
	public void setFunction(ContinuousFunctionInput cfi) {
		this.a = cfi.getParameter(A).doubleValue();
		if(cfi.containsKey(A))
		{
			this.b = cfi.getParameter(B).doubleValue();
		}
		if(cfi.containsKey(C))
		{
		this.c = cfi.getParameter(C).doubleValue();
		}
		if(cfi.containsKey(D))
		{
			this.d = cfi.getParameter(D).doubleValue();
		}
		

	}

	/**
	 * @see simcolep.fun2.ContinuousFunction#changeParameter(java.lang.String, double)
	 */
	public void changeParameter(String name, double value) {
		if(name.equalsIgnoreCase(A))
		{
			this.a = value;
		}else if(name.equalsIgnoreCase(B))
		{
			this.b = value;
		}else if(name.equalsIgnoreCase(C))
		{
			this.c = value;
		}else if(name.equalsIgnoreCase(D))
		{
			this.d = value;
		}else if(name.equalsIgnoreCase("N0"))
        {
            this.a = value-this.d;
        }else throw new IllegalArgumentException("unknown parameter: " + name);
				

	}

	/**
	 * @see simcolep.fun2.ContinuousFunction#getParameter(java.lang.String)
	 */
	public double getParameter(String name) {
		if(name.equalsIgnoreCase(A))
		{
			return this.a;
		}else if(name.equalsIgnoreCase(B))
		{
			return this.b;
		}else if(name.equalsIgnoreCase(C))
		{
			return this.c;
		}else if(name.equalsIgnoreCase(D))
		{
		    return this.d;
		}else throw new IllegalArgumentException("unknown parameter: " + name);
	}

	/**
	 * @return <i><b>Yvalue</b> = a<sup><b>Xvalue</b>-b</sup> +c</i>
	 * @see simcolep.fun2.Function#getY(double)
	 */
	public double getY(double X) {
        //we are not going into complex numbers!
        if(Math.abs(c)<1.0 && X<0)return Double.NaN;
        
		return a*Math.pow((X+b),c) + d;
	}

	/** 
	 * 
	 * @return <i><b>Xvalue</b> =(ln(<b>Yvalue</b>-c)/ln(a))-b</i>  
	 * @see simcolep.fun2.Function#getX(double)
	 */
	public double getX(double y) {
	    //we are not going into complex numbers!
        if(Math.abs(1/c)<1.0 && y-this.d<0)return Double.NaN;
        
		return Pythagoras.pow(((y-this.d)/this.a),(1/this.c))-this.b;
	}

	

	/**
	 * @return f'(x)
	 * @see simcolep.fun2.Function#getDerivative(double)
	 */
	public double getDerivative(double x) {
		
		return this.a*this.c*Pythagoras.pow((x+this.b),this.c-1);
	}

	/**
	 * @return <i><b>Yvalue</b> = (a<sup><b>Xvalue</b>+b</sup> / ln(a))+c*<b>Xvalue</b></i>
	 * @see simcolep.fun2.Function#getIntegrated(double)
	 */
	public double getIntegrated(double x) {
		
		return ((1+this.c)*this.d*x +a*(Pythagoras.pow((this.b+x),(1+this.c))-Pythagoras.pow(-this.b,1+this.c)))/(1-this.c);
	}

	/**
	 * @return NaN to be implemented when necessary 
	 * @see simcolep.fun2.Function#getInvIntegrated(double)
	 */
	public double getInvIntegrated(double y) {
		
		return Pythagoras.numericInvIntegrated(this,y, 0);
	}
 
    
  
      
    /**
     *  
     * @see simcolep.fun2.Distributionable#mean()
     */
    public double mean() {
    
        return Double.NaN;
    }
    
    @Override
    public Power clone() {
       
        return new Power(this.a,this.b,this.c,this.d);
    }
    
    @Override
    public boolean equalsFunction(Function f) {
       //same object thus same function
    	if(this.equals(f))
    	   return true;
    	
    	//test if a power
    	if(!f.getClass().equals(Power.class))
    		return false;
    	//test each parameter whether or not the same 
    	String[] params = {A,
    			B,
    			C,
    			D};
    	
    	for(String p : params)
    	{
    		double tester = this.getParameter(p) - f.getParameter(p); 
    		
    		if(Math.abs(tester)>Pythagoras.getDefaultPrecision())
    			return false;
    	}
        
    	return true;
    }

}
