/*
 * This file is part of SIMCOLEP, a simulation model developed by the Department of Public Health, 
 * Erasmus MC, University Medical Center Rotterdam, Rotterdam, The Netherlands.
 *
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/ 
 * or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA
 *
 * You are free to:
 * Share � copy and redistribute the material in any medium or format
 * Under the following terms:
 * Attribution � You must give appropriate credit, provide a link to the license, and indicate if changes were 
 * made. You may do so in any reasonable manner, but not in any way that suggests the licensor endorses
 * you or your use.
 * NonCommercial � You may not use the material for commercial purposes.
 * NoDerivatives � If you remix, transform, or build upon the material, you may not distribute the modified material.
 */

/*
 * Created on 06-Dec-2005
 *
 */
package simcolep.fun2;


import umontreal.iro.lecuyer.probdist.NormalDist;
/**
 * 
 * @author ir. E.A.J. Fischer
 * @version 1.0
 */
public final class Normal 
		extends ContinuousFunction 
			implements Distributionable {
    
	/**
	 * 
	 */
	private static final long serialVersionUID = 948413583656750013L;

	/**
	 * normal distribution
	 */
	private transient NormalDist normal = null;
    
	/**
	 * mean
	 */
    protected double mean = 0.0;
    /**
     * variance
     */
    protected double var = 1.0;
    /**
     * 
     *
     */
    public Normal() {

        normal = new NormalDist(this.mean,this.var);
    }

    /**
     * 
     * @param mean
     * @param var
     */
    public Normal(double mean, double var)
    {
        this.mean = mean;
        this.var = var;
        normal = new NormalDist(mean,var);
    }
    
    @Override
    public void changeParameter(String name, double value) {
        if(name.equalsIgnoreCase("mean"))
        {
            normal.setParams(value,normal.getSigma());
            this.mean = value;
        }else if(name.equalsIgnoreCase("var"))
        {
            normal.setParams(normal.getMu(),value);
            this.var = value;
        }else
        	throw new IllegalArgumentException(
        			"unknown parameter: " + name);

    }

    @Override
    public double getParameter(String pname) {
        if(pname.equalsIgnoreCase("mean"))
        	return mean;
        else if(pname.equalsIgnoreCase("var"))
        	return var;
        else throw new IllegalArgumentException(pname + " is unknown for Normal");
     
    }
    
    @Override
    public double getY(double X) {
        return normal.density(X);
    }

    @Override
    public double getX(double y) {
        throw new IllegalArgumentException("");
    }

    @Override
    public double getDerivative(double x) {
        throw new IllegalArgumentException("");
    }

    @Override
    public double getIntegrated(double x) {
        return normal.cdf(x);
    }

    @Override
    public double getInvIntegrated(double y) {
        return normal.inverseF(y);
    }

    @Override
    public Function clone() {
        
        return null;
    }

    /**
     * @see simcolep.fun2.Distributionable#drawX(double)
     */
    public double drawX(double y) {
          return this.normal.inverseF(y);
    }
    /**
     * @see simcolep.fun2.Distributionable#drawX(double)
     */
    public double drawX(double[] y) {
          return this.normal.inverseF(y[0]);
    }

    /**
     * @see simcolep.fun2.Distributionable#drawXconditional(double[])
     */
    public double drawXconditional(double[] v) {
        double[] newY = 
        	{getF(new double[]{v[1]})+v[0]*(1-getF(new double[]{v[1]}))};
        return drawX(newY);

    }

    /**
     * @see simcolep.fun2.Distributionable#mean()
     */
    public double mean() {
         return normal.getMu();
    }

    @Override
    public boolean equalsFunction(Function f) {
        if(!f.getClass().equals(this.getClass()))return false;
        Normal nd = (Normal)f;
        
        return (nd.normal.getMu()==nd.normal.getMu()&&nd.normal.getSigma()==nd.normal.getSigma());
    }



	@Override
	public void setFunction(ContinuousFunctionInput CFI) {
		this.mean = CFI.getParameter("mean");
		
		this.var = CFI.getParameter("var");
		
		normal = new NormalDist(this.mean,this.var);
	}
	
	/**
	 * @see simcolep.fun2.Distributionable#getF(double)
	 */
	public double getF(double d) {
		return this.normal.cdf(d);
	}
	/**
	 * @see simcolep.fun2.Distributionable#getF(double)
	 */
	public double getF(double[] d) {
		return this.normal.cdf(d[0]);
	}
    
}
