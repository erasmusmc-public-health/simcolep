/*
 * This file is part of SIMCOLEP, a simulation model developed by the Department of Public Health,
 * Erasmus MC, University Medical Center Rotterdam, Rotterdam, The Netherlands.
 *
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/
 * or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA
 *
 * You are free to:
 * Share � copy and redistribute the material in any medium or format
 * Under the following terms:
 * Attribution � You must give appropriate credit, provide a link to the license, and indicate if changes were
 * made. You may do so in any reasonable manner, but not in any way that suggests the licensor endorses
 * you or your use.
 * NonCommercial � You may not use the material for commercial purposes.
 * NoDerivatives � If you remix, transform, or build upon the material, you may not distribute the modified material.
 */

/*
 * Created on 16-dec-2004
 *
 */
package simcolep.fun2;

import java.io.Serializable;

/**
 * A mathematical function that can return its numerical value for values in x
 * also for the derivative and the integration. Furtermore it can return values
 * of x for given y and the inverse of the integration.
 *
 * @author ir. E.A.J Fischer
 * @version 0.1.1 --Last Modified 16-12-04
 *
 *
 */
public abstract class Function implements Serializable {

	/** serial number */
	public static final long serialVersionUID = -5736078880508894554L;

	/** integer value representing continuous functions */
	public static final int CONTINUOUS_FUNCTION_INPUT = 0;

	/** integer value representing discrete functions */
	public static final int DISCRETE_FUNCTION_INPUT = 1;

	/** integer value representing combined functions */
	public static final int COMBINED_FUNCTION_INPUT = 2;

	/** integer value representing combined functions */
	public static final int DISEASE_FUNCTION_INPUT = 3;

	/** tab */
	public static final String TAB = "\t";

	/***/
	/**
	 *
	 */
	public Function() {

	}

	/**
	 * Change a parameter of this function
	 *
	 * @param name
	 * @param value
	 */
	public abstract void changeParameter(String name, double value);

	/**
	 * 
	 */
	@Override
	public abstract Function clone();

	/**
	 * Determines whether to Functions describe the same function<br>
	 * e.g. f(x) == g(x) for all values of x
	 * 
	 * @param f
	 * @return true of f(x) == g(x)
	 */
	public abstract boolean equalsFunction(Function f);

	/**
	 *
	 * @param x
	 * @return the value of the derivative of the function in x
	 */
	public abstract double getDerivative(double x);

	/**
	 * 
	 * @param x
	 * @return defined integral from 0 to Xvalue for the function
	 */
	public abstract double getIntegrated(double x);

	/**
	 *
	 * @param y
	 * @return the X for which the defined integral from 0 to X equals Yvalue
	 */
	public abstract double getInvIntegrated(double y);

	/**
	 * 
	 * @return simple class name
	 */
	public String getName() {
		return this.getClass().getSimpleName();
	}

	/**
	 * 
	 * @param pname
	 * @return parameter value
	 */
	public double getParameter(String pname) {
		return 0;
	}

	/**
	 * Calculates the Xvalue that corresponds to the Yvalue parameter
	 * 
	 * @param y
	 * @return Xvalue
	 */
	public abstract double getX(double y);

	/**
	 * Not all classes have to be able to do this but it makes it easier to use
	 * it for other classes for whom we do like to have this method they just
	 * override it
	 *
	 */
	public double getXmax() {
		return Double.POSITIVE_INFINITY;
	}

	/**
	 * Calculates the Yvalue that corresponds to the Xvalue parameter
	 *
	 * @param X
	 * @return Yvalue
	 */
	public abstract double getY(double X);

	/**
	 * Read the correct type of input from a file, specified for a certain type
	 * of function
	 * 
	 * @return
	 */
	public abstract int inputGroup();
}
