/*
 * This file is part of SIMCOLEP, a simulation model developed by the Department of Public Health, 
 * Erasmus MC, University Medical Center Rotterdam, Rotterdam, The Netherlands.
 *
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/ 
 * or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA
 *
 * You are free to:
 * Share � copy and redistribute the material in any medium or format
 * Under the following terms:
 * Attribution � You must give appropriate credit, provide a link to the license, and indicate if changes were 
 * made. You may do so in any reasonable manner, but not in any way that suggests the licensor endorses
 * you or your use.
 * NonCommercial � You may not use the material for commercial purposes.
 * NoDerivatives � If you remix, transform, or build upon the material, you may not distribute the modified material.
 */

/*
 * Created on 21-Feb-2006
 *
 */
package simcolep.fun2;


import umontreal.iro.lecuyer.probdist.WeibullDist;
/**
 * Weibull
 * 
 * 
 * @author ir E.A.J. Fischer
 * @version 0.1.2 -- Last Modified 21-Feb-2006
 */
public final class Weibull 
				extends ContinuousFunction 
						implements Distributionable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 
		-5228255210354843310L;
	/**the weibull distribution*/
    private transient WeibullDist w;
    /**alpha*/
    protected double alpha = 1.0;
    /**lambda*/
    protected double lambda = 1.0;
    /**delta */
    protected double delta = 0.0;
    
    /**
     * 
     */
    public Weibull() {
        super();
        this.w = new WeibullDist(this.alpha, this.lambda,this.delta);
    }

    /**
     * @param cfi
     */
    public Weibull(ContinuousFunctionInput cfi) {
        setFunction(cfi);
    }

    /**
     * @param cf
     */
    public Weibull(ContinuousFunction cf) {
        super(cf);
        if(!cf.getClass().equals(this.getClass())) throw new IllegalArgumentException("not a Weibull "+cf);
        
        Weibull cfw = (Weibull) cf;
        this.alpha = cfw.alpha;
        this.delta = cfw.delta;
        this.lambda = cfw.lambda;
        this.w = cfw.w;
    }

    /**
     * 
     * @param alpha
     * @param lambda
     * @param delta
     */
    public Weibull(double alpha, double lambda, double delta) {
        this.alpha = alpha;
        this.lambda = lambda;
        this.delta = delta;
        this.w = new WeibullDist(this.alpha,this.lambda,this.delta);
     
    }

    /**
     * @see simcolep.fun2.ContinuousFunction#setFunction(simcolep.fun2.ContinuousFunctionInput)
     */
    @Override
    public void setFunction(ContinuousFunctionInput CFI) {
        this.alpha = CFI.getParameter("alpha");
        this.lambda = CFI.getParameter("lambda");
        this.delta = CFI.getParameter("delta");
        this.w = new WeibullDist(this.alpha,this.lambda,this.delta);

    }

    /**
     * @see simcolep.fun2.ContinuousFunction#changeParameter(java.lang.String, double)
     */
    @Override
    public void changeParameter(String name, double value) {
        if(name.equalsIgnoreCase("alpha"))
        {
            this.alpha = value;
            this.w.setParams(value,this.w.getLambda(), this.w.getDelta());
        }else if(name.equalsIgnoreCase("lambda"))
        {
            this.lambda = value;
            this.w.setParams(this.w.getAlpha(),value, this.w.getDelta());
        }else if (name.equalsIgnoreCase("delta"))
        {
            this.lambda = value;
            this.w.setParams(this.w.getAlpha(),this.w.getLambda(),value);
        }else 
        	throw new IllegalArgumentException("unknown parameter " + name);
    }

    /* (non-Javadoc)
     * @see simcolep.fun2.ContinuousFunction#getParameter(java.lang.String)
     */
    @Override
    public double getParameter(String name) {
        if(name.equalsIgnoreCase("alpha")) return this.alpha;
        else if(name.equalsIgnoreCase("lambda")) return this.lambda;
        else if(name.equalsIgnoreCase("delta")) return this.delta;
        else throw new IllegalArgumentException("unknown parameter "+name );
    }

    /**
     * @see simcolep.fun2.ContinuousFunction#getY(double)
     */
    @Override
    public double getY(double X) {
        
        return this.w.density(X);
    }

    /**
     * @see simcolep.fun2.ContinuousFunction#getX(double)
     */
    @Override
    public double getX(double y) {
        
        return this.w.inverseF(y);
    }

   

    /**
     * @see simcolep.fun2.Function#getDerivative(double)
     */
    @Override
    public double getDerivative(double x) {
        
        return Double.NaN;
    }

    /**
     * @see simcolep.fun2.Function#getIntegrated(double)
     */
    @Override
    public double getIntegrated(double x) {
        
        return this.w.cdf(x);
    }

    /**
     * @see simcolep.fun2.Function#getInvIntegrated(double)
     */
    @Override
    public double getInvIntegrated(double y) {
        if(y<0) return 0;
        if(y>=1) return Double.POSITIVE_INFINITY;
        return this.w.inverseF(y);
    }

    /**
     * @see simcolep.fun2.Function#clone()
     */
    @Override
    public Function clone() {
        Weibull weibull= new Weibull(this.alpha, this.lambda,this.delta);
        return weibull;
    }

    /**
     * @see simcolep.fun2.Function#equalsFunction(simcolep.fun2.Function)
     */
    @Override
    public boolean equalsFunction(Function f) {
        if(!f.getClass().equals(this.getClass()))return false;
        if(Math.abs(f.getParameter("alpha") - this.alpha)>Pythagoras.getDefaultPrecision())return false;
        if(Math.abs(f.getParameter("lambda") - this.lambda)>Pythagoras.getDefaultPrecision())return false;
        if(Math.abs(f.getParameter("delta") - this.delta)>Pythagoras.getDefaultPrecision())return false;
        
        return true;
    }

    /**
     * @see simcolep.fun2.Distributionable#drawX(double)
     */
    public double drawX(double y) {
        
        return this.w.inverseF(y);
    }
    
    /**
     * @see simcolep.fun2.Distributionable#drawX(double)
     */
    public double drawX(double[] y) {
        
        return this.w.inverseF(y[0]);
    }

    /**
     * @see simcolep.fun2.Distributionable#drawXconditional(double, double)
     */
    public double drawXconditional(double[] v) {
        double[] newY = {
        	getF(new double[]{v[1]})+v[1]*(1-getF(new double[]{v[1]}))};
        return drawX(newY);

    }

    /**
     * @see simcolep.fun2.Distributionable#mean()
     */
    public double mean() {
        
        return this.w.inverseF(0.5);
    }

    /**
     * @see simcolep.fun2.Distributionable#getF(double)
     */
 	public double getF(double d) {
 		return this.w.cdf(d);
 	}
   /**
    * @see simcolep.fun2.Distributionable#getF(double)
    */
	public double getF(double[] d) {
		return this.w.cdf(d[0]);
	}

}
