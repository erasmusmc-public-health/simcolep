/*
 * This file is part of SIMCOLEP, a simulation model developed by the Department of Public Health, 
 * Erasmus MC, University Medical Center Rotterdam, Rotterdam, The Netherlands.
 *
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/ 
 * or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA
 *
 * You are free to:
 * Share � copy and redistribute the material in any medium or format
 * Under the following terms:
 * Attribution � You must give appropriate credit, provide a link to the license, and indicate if changes were 
 * made. You may do so in any reasonable manner, but not in any way that suggests the licensor endorses
 * you or your use.
 * NonCommercial � You may not use the material for commercial purposes.
 * NoDerivatives � If you remix, transform, or build upon the material, you may not distribute the modified material.
 */

/*
 * Created on 05-Dec-2005
 *
 */
package simcolep.fun2;

import java.util.Random;

import simcolep.exceptions.SimulationException;
import umontreal.iro.lecuyer.probdist.BinomialDist;
import umontreal.iro.lecuyer.probdist.NormalDist;

/**
 * Binomial distribution
 *
 *
 * @author ir E.A.J. Fischer
 * @version 0.1.2 -- Last Modified 05-Dec-2005
 */
public final class Binomial
			extends ContinuousFunction
				implements Distributionable {

    private static final String n_string = "n"; //$NON-NLS-1$

    private static final String p_string = "p";
	/**
	 *
	 */
	private static final long serialVersionUID = 1L;



	/**probability*/
	protected double p = 0.0;

	/**number of trials*/
    protected double n = 0.0;



    /**
     *
     */
    public Binomial() {
        super();

    }

    /**
     *
     * @param n
     * @param p
     */
    public Binomial(double n, double p) {
        this.n = n;
        this.p = p;

    }



    /**
     * @see simcolep.fun2.Function#changeParameter(java.lang.String, double)
     */
    @Override
    public void changeParameter(String name, double value) {
       if(name.equalsIgnoreCase(n_string))
       {
           this.n = value;
       }else if(name.equalsIgnoreCase(p_string))
       {
           this.p = value;
       }else 
    	   throw new SimulationException(SimulationException.UNKNOWNPARAMETER_MESSAGE + name);

    }

    /**
     * @see simcolep.fun2.Function#getY(double)
     */
    @Override
    public double getY(double X) {
        if(X<0)
        	return 0;

        if(X>this.n)
        	return 0;

        double res =
        	Binomial.BinCoef(this.n,X)*Math.pow(this.p,X)*Math.pow((1-this.p),this.n-X);

        return res;
    }

    /**
     * @see simcolep.fun2.Function#getX(double)
     */
    @Override
    public double getX(double y) {
               return drawX(y);
    }

    /**
     * @see simcolep.fun2.Function#getDerivative(double)
     */
    @Override
    public double getDerivative(double X) {
             return Double.POSITIVE_INFINITY;
    }

    /**
     * @see simcolep.fun2.Function#getIntegrated(double)
     */
    @Override
    public double getIntegrated(double X) {
        if(X<0)
        	return 0;
        if(X>this.n)
        	return 1.0;

        double sum =0;
        double i =0;

        while(i<=X)
        {
            sum+=this.getY(i);
            i++;
        }

        return sum;
    }

    /**
     * @see simcolep.fun2.Function#getInvIntegrated(double)
     */
    @Override
    public double getInvIntegrated(double y) {
        if(y<=0) return 0;
        if(y>=1.0)return this.n;

        double sum =0;
        double i =0;
        while(sum<=y)
        {
            sum+=this.getY(i);
            i++;
        }

        return sum;
    }

    /**
     *
     * @see java.lang.Object#clone()
     */
    @Override
    public Function clone() {
          return new Binomial(this.n,this.p);
    }

    /**
     * @see simcolep.fun2.Distributionable#drawX(double)
     */
    public double drawX(double Y) {
       //quick method does not use random variate y but random number generator rand

        return Binomial.drawBin(this.n,this.p,Y);
    }
    /**
     * @see simcolep.fun2.Distributionable#drawX(double)
     */
    public double drawX(double[] Y) {
       //quick method does not use random variate y but random number generator rand
        return this.drawX(Y[0]);
    }

    /**
     * @see simcolep.fun2.Distributionable#drawXconditional(double, double)
     */
    public double drawXconditional(double[] p) {
        double[] newY = 
        	{getF(new double[]{p[1]})+p[0]*(1-getF(new double[]{p[1]}))};
        return drawX(newY);

    }

    /**
     * @see simcolep.fun2.Distributionable#mean()
     */
    public double mean() {
         return this.n*this.p;
    }

    /**
     *
     * @param n
     * @param k
     */
    public static double BinCoef(double n, double k)
    {
        double c =
        	Pythagoras.factorial(n)/(Pythagoras.factorial(k)*Pythagoras.factorial(n-k));
        return c;
    }


    /**
     *
     * @param n
     * @param p
     * @param rand
     */
    public static double drawBin(double n, double p, double rand)
    {

        if(rand<=0)
        	return 0;
        if(rand>=1.0)
        	return n;

        double sum =0;
        double i =0;
        while(sum<=rand)
        {
            if(i<0){sum+=0;}
            else if(i>n){sum+=0;}
            else {
                double c =Binomial.BinCoef(n,i);
                double psucces =Math.pow(p,i);
                double pfailure =Math.pow((1-p),n-i) ;
                sum+=c*psucces*pfailure;
                }
            if(sum>rand)
            	return i;
            i++;
        }

        return i;

    }

    /**
     *
     * @param n
     * @param p
     * @param rand
     */
    public static double drawBin(double n, double p, Random rand)
    {
        if(p<=Pythagoras.getDefaultPrecision())
        	return 0;
        //if larger than a million
        if(n>1E6)
        	{
        	if(ruleOfThumb(n,p))
        		return NormalApprox(n,p,rand.nextDouble());

        	return BinomialDist.inverseF((int)n,p,rand.nextDouble());
        	}

        double X =0;
        for(int i =0; i<n;i++)
        {
            if(rand.nextDouble()<p)X++;

        }
        return X;
    }





    /**
     * Draw with rand as random number the number of successes with a probability p for n trials
     * If n >170 it will return a random variate of a Binomial distribution with a random number
     * if n > 1E6 it will return the Normal approximation if 0.1 &lt p &lt 0.9
     * @param p
     * @param n
     * @param rand
     */
    public static long drawBin_applyRuleOfThumb(double p, double n, Random rand)
    {
        if(Binomial.ruleOfThumb(n,p))
        	return Binomial.NormalApprox(n,p,rand.nextDouble());

        if(n>=170)
        	return (long)Binomial.drawBin(n,p,rand);

        return (long) Binomial.drawBin(n,p,rand.nextDouble());
    }
    /**
     * Gets the normal approximation of a binomial distribution
     *
     * @param n
     * @param p
     * @param rand
     * @return
     */
    public static int NormalApprox(double n, double p, double rand) {
       //mean
    	double mean = n*p;
    	//variance
        double var = Math.sqrt(n*p*(1-p));
        //Normal distribtuion
        NormalDist nd = new  NormalDist(mean,var);
        //value
        double approx =nd.inverseF(rand);
        //round down
        approx = approx-approx%1;
        //return value
        return (int)approx;
    }
    /**
     *
     * @param n
     * @param p
     * @return
     */
    public static boolean ruleOfThumb(double n, double p)
    {
    	boolean b = n*p>10;
    	if(b)b=n*(1-p)>10;

    	return b;
   }

    @Override
    public boolean equalsFunction(Function f) {
       boolean b = this.getClass().equals(f.getClass());
       if(!b)return b;
       Binomial bin = (Binomial) f;
       return(Math.abs(this.n - bin.n) <Pythagoras.getDefaultPrecision()&&Math.abs(this.p-bin.p)<Pythagoras.getDefaultPrecision());

    }


   /**
    *
    * @see simcolep.fun2.Function#getParameter(java.lang.String)
    */
    public double getParameter(String pname) {
        if(pname.equalsIgnoreCase(n_string))
        	return this.n;
        else if(pname.equalsIgnoreCase(p_string))
        	return this.p;
        else
        	return -100;
    }

	@Override
	public void setFunction(ContinuousFunctionInput CFI) {
		this.n = CFI.getParameter(n_string);
		this.p = CFI.getParameter(p_string);

	}
	
	/**
	 * return getIntegrated(double)
	 * @see simcolep.fun2.Distributionable#getF(double)
	 */
	public double getF(double d) {
			return this.getIntegrated(d);
	}
	/**
	 * return getIntegrated(double)
	 * @see simcolep.fun2.Distributionable#getF(double)
	 */
	public double getF(double[] d) {
			return this.getF(d[0]);
	}
}
