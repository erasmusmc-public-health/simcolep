/*
 * This file is part of SIMCOLEP, a simulation model developed by the Department of Public Health,
 * Erasmus MC, University Medical Center Rotterdam, Rotterdam, The Netherlands.
 *
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/
 * or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA
 *
 * You are free to:
 * Share � copy and redistribute the material in any medium or format
 * Under the following terms:
 * Attribution � You must give appropriate credit, provide a link to the license, and indicate if changes were
 * made. You may do so in any reasonable manner, but not in any way that suggests the licensor endorses
 * you or your use.
 * NonCommercial � You may not use the material for commercial purposes.
 * NoDerivatives � If you remix, transform, or build upon the material, you may not distribute the modified material.
 */

package simcolep.lepra;

import java.io.IOException;
import java.io.Writer;

import simcolep.asgard.Idunn;
import simcolep.demo.Person;
import simcolep.demo.Sex;

/**
 * @author ir E.A.J. Fischer
 * @version 1.4.3 - 17-2-2008
 */
public class ExtendedClinicRegister extends ClinicRegister {
	/**
	 *
	 * @author Egil
	 *
	 */
	private final class Entry {

		private static final String COLUMNSEPARATOR = ",";

		private StringBuffer entry = null;

		public Entry() {
			entry = new StringBuffer();
		}

		public Entry(int i) {
			entry = new StringBuffer(i + COLUMNSEPARATOR);
		}

		public Entry(String s) {
			entry = new StringBuffer(s + COLUMNSEPARATOR);
		}

		public void append(boolean b) {
			entry.append(b + COLUMNSEPARATOR);

		}

		public void append(double d) {
			entry.append(d + COLUMNSEPARATOR);
		}

		public void append(int i) {
			entry.append(i + COLUMNSEPARATOR);
		}

		public void append(Sex i) {
			entry.append(i + COLUMNSEPARATOR);
		}

		public void append(String s) {
			entry.append(s + COLUMNSEPARATOR);
		}

		@Override
		public String toString() {
			return entry.toString();
		}

	}

	private static final String COLUMNHEADING = "RUN,Person,Layer,Level," + "REGTIME,HHSIZE,BTIME,SEX,"
			+ "INFTIME,ISTATE,LATENCY, IN_PB, INC_MB, DIAGNOSIS,"
			+ "DETECTIONTIME,DETECTIONMETHOD,ORIGIN,TRANSMISSIONLEVEL,VACCINATED, VACTIME";

	private static final String LINESEPARATOR = System.getProperty("line.separator");

	private int runnumber = -1;

	private Writer writer = null;

	private boolean opened = false;

	/**
	 *
	 * @param repeat
	 * @param writer2
	 */
	public ExtendedClinicRegister(int repeat, Writer writer2) {
		this.runnumber = repeat;
		this.writer = writer2;
	}

	/**
	 * @see simcolep.lepra.ClinicRegister#addDetected(simcolep.lepra.Morbus,
	 *      int, int, int)
	 */
	@Override
	public void addDetected(Morbus m, int relation, Sex sex_index, int reason) {
		super.addDetected(m, relation, sex_index, reason);

		try {
			this.register(m);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 *
	 * @throws IOException
	 */
	@Override
	public void open() throws IOException {
		if (writer == null)
			return;

		if (runnumber == 1)
			writer.append(COLUMNHEADING + LINESEPARATOR);

		this.opened = true;
	}

	/**
	 *
	 * @param m
	 * @throws IOException
	 */
	public void register(Morbus m) throws IOException {
		if (!opened)
			return;
		Entry entry = new Entry(runnumber);
		// identification
		Person p = m.getOwner();
		entry.append(p.getID());
		entry.append(m.getLayer());
		entry.append(p.getLevel().getID());
		// time
		entry.append(Idunn.getCurrentTime());
		// demographic information
		entry.append(p.getLevel().getHabitationSize());
		entry.append(p.getBdate());
		entry.append(p.getSex());
		// infection information
		entry.append(m.getInfectionTime());
		entry.append(m.getState());
		entry.append(m.getLatencyPeriod());
		entry.append(m.getIncPBPeriod());
		entry.append(m.getIncMBPeriod());
		entry.append(m.getDiagnosis());
		entry.append(m.getDetectionTime());
		entry.append(m.getDetectionMethod());
		entry.append(m.getStrain().getOrigin());
		entry.append(m.getStrain().getArctype());
		entry.append(m.isVaccinated());
		if (Double.isInfinite(m.getVacinationTime())) {
			entry.append("NA");
		} else
			entry.append(m.getVacinationTime());

		writer.append(entry.toString() + LINESEPARATOR);
		writer.flush();

	}

	/**
	 *
	 */
	@Override
	public void storeData(boolean b) {
		super.storeData(b);
	}

}
