/*
 * This file is part of SIMCOLEP, a simulation model developed by the Department of Public Health,
 * Erasmus MC, University Medical Center Rotterdam, Rotterdam, The Netherlands.
 *
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/
 * or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA
 *
 * You are free to:
 * Share � copy and redistribute the material in any medium or format
 * Under the following terms:
 * Attribution � You must give appropriate credit, provide a link to the license, and indicate if changes were
 * made. You may do so in any reasonable manner, but not in any way that suggests the licensor endorses
 * you or your use.
 * NonCommercial � You may not use the material for commercial purposes.
 * NoDerivatives � If you remix, transform, or build upon the material, you may not distribute the modified material.
 */
package simcolep.lepra.mendel;

import java.util.Random;

import simcolep.demo.Sex;
import simcolep.fun2.Archimedes;
import simcolep.tools.PN;
import simcolep.tools.io.Table;

/**
 * Random inheritance model
 *
 * @author ir. E.A.J. Fischer
 * @version 1.1
 */
public final class RandomModel extends InheritanceModel {
	private static final int GENES = 1;
	private double[][][] gene_count = null;

	/**
	 *
	 * @param f
	 */
	public RandomModel() {
	}

	@Override
	public int genes() {
		return GENES;
	}

	/**
	 * @see simcolep.lepra.mendel.InheritanceModel#getGenotype(Genome, Genome,
	 *      java.util.Random)
	 */
	@Override
	public Genomable getGenotype(Genomable mother_genome, Genomable father_genome, Random rand) {

		int[][] genes = new int[2][GENES];

		genes[0][0] = (int) Archimedes.askDistribution(PN.GENE + 0).drawX(rand.nextDouble());
		genes[1][0] = genes[0][0];

		// the gene counter
		if (gene_count == null) {
			int alleles = Archimedes.askInteger(PN.NUMOFALLELES + 0);
			gene_count = new double[GENES][1][alleles];
		}

		// do not register father or mother genomes as it does noet matter
		gene_count[0][0][genes[Sex.MALE.getValue()][0]] += 1;
		gene_count[0][0][genes[Sex.FEMALE.getValue()][0]] += 1;

		return Genome.getGenome(genes);
	}

	/**
	 * In this model this method is actual equal to the normal method
	 *
	 * @see simcolep.lepra.mendel.InheritanceModel#getRandomGenotype(java.util.Random)
	 */
	@Override
	public Genomable getRandomGenotype(Random rand) {

		return this.getGenotype(null, null, rand);
	}

	/**
	 * @see simcolep.lepra.mendel.InheritanceModel#inheritancetable(int)
	 */
	@Override
	public Table inheritancetable(int gene) {
		return new Table(this.gene_count[gene]);
	}

	/**
	 * Set model parameters in this model no additional parameters
	 * 
	 * @see simcolep.lepra.mendel.InheritanceModel#setModel(double[])
	 */
	@Override
	public void setModel(double[] parameters) {
		return;
	}

}
