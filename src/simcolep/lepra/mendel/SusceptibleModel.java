/*
 * This file is part of SIMCOLEP, a simulation model developed by the Department of Public Health, 
 * Erasmus MC, University Medical Center Rotterdam, Rotterdam, The Netherlands.
 *
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/ 
 * or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA
 *
 * You are free to:
 * Share � copy and redistribute the material in any medium or format
 * Under the following terms:
 * Attribution � You must give appropriate credit, provide a link to the license, and indicate if changes were 
 * made. You may do so in any reasonable manner, but not in any way that suggests the licensor endorses
 * you or your use.
 * NonCommercial � You may not use the material for commercial purposes.
 * NoDerivatives � If you remix, transform, or build upon the material, you may not distribute the modified material.
 */
/**
 * created: 25-Apr-2007
 */
package simcolep.lepra.mendel;

import java.util.Random;


import simcolep.fun2.Archimedes;
import simcolep.lepra.Morbus;
import simcolep.tools.PN;
import simcolep.tools.io.SimulationLog;
import simcolep.tools.io.Table;


/**
 * Inheritance of susceptibility, type of disease is drawn
 * This model is of a class of models that do not really use Mendelian inheritance but let traits inherit not genes 
 * <br> This model is based on two 'genes'. First gene determines susceptibility the second 'gene' is than randomly drawn. 
 * @author ir. E.A.J. Fischer
 * @version 1.1
 */
public final class SusceptibleModel
	extends InheritanceModel
{
	
	/**number of genes*/
	private static final int GENES = 2;
	/**Susceptible gene*/
	private static final int SUSCEPTIBLE_ALLELE = 1;

	/**
	 * 
	 * @param f
	 */
	public SusceptibleModel()
	{
		super();
		
	
	}
	
	/**
	 * @see simcolep.lepra.mendel.InheritanceModel#getGenotype(Genomable, Genomable, java.util.Random)
	 */
	public Genomable getGenotype(Genomable mother_genome,
			Genomable father_genome, 
			Random rand)
	{
	
		
		/*choose parent to inherit from */
		Genomable parent_type = null;
	
		if(this.dismom < rand.nextDouble())
			parent_type = father_genome;
		else 
			parent_type = mother_genome;
		
		//create haploid gene map 
		int[][] genes = new int[2][GENES];
		
		
		if(parent_type.getPhenotype() == Morbus.IMMUNE_TYPE)
			{
				genes[0][0] = Morbus.IMMUNE_TYPE;
				genes[1][0] = Morbus.IMMUNE_TYPE;
				genes[0][1] = Morbus.IMMUNE_TYPE;
				genes[1][1] = Morbus.IMMUNE_TYPE;
		}
		else{
			genes[0][0] = SUSCEPTIBLE_ALLELE;
			genes[1][0] = SUSCEPTIBLE_ALLELE;
			genes[0][1] = 
				(int)Archimedes.askDistribution(PN.GENE+1).drawX(rand.nextDouble());
			genes[1][1] = genes[0][1];
		}
			
		
		return Genome.getGenome(genes);
	}
	
	@Override
	public int genes() {
			return GENES;
	}

	@Override
	public Genomable getRandomGenotype(Random rand) {
		
		//values
		int[][]genes = new int [2][GENES];

		//random susceptibility
		genes[0][0] = 
			(int)Archimedes.askDistribution(PN.GENE+0).drawX(rand.nextDouble());
		genes[1][0] = genes[0][0];
		 
		if(genes[0][0]== SUSCEPTIBLE_ALLELE)
		{	//random type
			genes[0][1] = 
				(int)Archimedes.askDistribution(PN.GENE+1).drawX(rand.nextDouble());
			genes[1][1] = genes[0][1];
		}else
		{
			genes[0][1] = Morbus.IMMUNE_TYPE;
			genes[1][1] = Morbus.IMMUNE_TYPE;
		}
		
		//draw susceptibility
		return Genome.getGenome(genes);
	}	
	
	/**
	 * Set model parameters in this model the probability to inherit from mothers
	 * @see simcolep.lepra.mendel.InheritanceModel#setModel(double[])
	 */
	public void setModel(double[] parameters) {
		this.dismom = parameters[0];
	}

	@Override
	public Table inheritancetable(int gene) {
		SimulationLog.log("not implemented inheritance table");
		return new Table(new double[1][1]); 
	}
	
}
