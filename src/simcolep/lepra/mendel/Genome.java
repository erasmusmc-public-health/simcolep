/*
 * This file is part of SIMCOLEP, a simulation model developed by the Department of Public Health,
 * Erasmus MC, University Medical Center Rotterdam, Rotterdam, The Netherlands.
 *
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/
 * or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA
 *
 * You are free to:
 * Share � copy and redistribute the material in any medium or format
 * Under the following terms:
 * Attribution � You must give appropriate credit, provide a link to the license, and indicate if changes were
 * made. You may do so in any reasonable manner, but not in any way that suggests the licensor endorses
 * you or your use.
 * NonCommercial � You may not use the material for commercial purposes.
 * NoDerivatives � If you remix, transform, or build upon the material, you may not distribute the modified material.
 */
package simcolep.lepra.mendel;

import java.util.LinkedHashMap;
import java.util.Map;

import simcolep.demo.Sex;
import simcolep.fun2.Archimedes;
import simcolep.tools.PN;

/**
 * A genome contains a genotype which is an 2-dimensional array of int and can
 * translate it to a phenotype of which the values should be in Archimedes brain
 *
 *
 * @author ir E.A.J. Fischer
 *
 *
 */
public final class Genome implements Genomable {

	/** Map containing all readily available genomes */
	private static Map<String, Genome> genomeBank = new LinkedHashMap<String, Genome>();

	/**
	 *
	 * @param genes
	 * @return
	 */
	public static Genome getGenome(int[][] genes) {
		Genome genome = Genome.genomeBank.get(getGene_Sequence(genes));

		if (genome == null) {
			// if not present create it
			genome = new Genome(genes);
			// add it to the genome bank
			Genome.genomeBank.put(getGene_Sequence(genes), genome);
		}

		return genome;

	}

	/**
	 *
	 * @param genes
	 * @return loc_geneseq
	 */
	private static String getGene_Sequence(int[][] genes) {
		String loc_geneseq = PN.GENE;

		for (int i = 0; i < genes[0].length; i++) {
			loc_geneseq += genes[Sex.MALE.getValue()][i];
			loc_geneseq += genes[Sex.FEMALE.getValue()][i];
		}
		return loc_geneseq;
	}

	/** genotype */
	private int[][] genotype = null;

	/** phenotype */
	private int phenotype = -1;
	/** string representation of the genome */
	private String geneseq = null;

	/**
	 *
	 */
	private Genome(int[][] genes) {
		this.setGenotype(genes);
	}

	/**
	 * @see simcolep.lepra.mendel.Genomable#getGenotype()
	 */
	@Override
	public int[][] getGenotype() {
		return this.genotype;
	}

	/**
	 * @see simcolep.lepra.mendel.Genomable#getPhenotype()
	 */
	@Override
	public int getPhenotype() {
		return this.phenotype;
	}

	/**
	 * @see simcolep.lepra.mendel.Genomable#setGenotype(int[][])
	 */
	@Override
	public void setGenotype(int[][] genes) {
		// set genotype
		this.genotype = genes;

		// determine the phenotype
		this.phenotype = Archimedes.askInteger(this.getGene_Sequence());
	}

	/**
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return this.getGene_Sequence();
	}

	/**
	 *
	 * @return geneseq
	 */
	private String getGene_Sequence() {

		if (this.geneseq == null) {
			this.geneseq = getGene_Sequence(this.genotype);
		}

		return this.geneseq;

	}

}
