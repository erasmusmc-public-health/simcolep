/*
 * This file is part of SIMCOLEP, a simulation model developed by the Department of Public Health, 
 * Erasmus MC, University Medical Center Rotterdam, Rotterdam, The Netherlands.
 *
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/ 
 * or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA
 *
 * You are free to:
 * Share � copy and redistribute the material in any medium or format
 * Under the following terms:
 * Attribution � You must give appropriate credit, provide a link to the license, and indicate if changes were 
 * made. You may do so in any reasonable manner, but not in any way that suggests the licensor endorses
 * you or your use.
 * NonCommercial � You may not use the material for commercial purposes.
 * NoDerivatives � If you remix, transform, or build upon the material, you may not distribute the modified material.
 */

/**
 * created: 25-Apr-2007
 */
package simcolep.lepra.mendel;

import java.util.Random;

import simcolep.tools.io.Table;



/**
 * @author ir. E.A.J. Fischer
 * @version 1.1
 */
public abstract class InheritanceModel {

	/**probability to inherit mothers type*/
	public double dismom = 0.5; 
	
	/**
	 * Determines type of new born  
	 * @param mother_genome type of mother
	 * @param father_genome type of father 
	 * @param rand random number generator
	 * 
	 * @return type
	 */
	public abstract Genomable getGenotype(Genomable mother_genome,
			Genomable father_genome, 
			Random rand);
	
	/**
	 * Set parameters for this inheritance model
	 * 
	 * @param parameters
	 */
	public abstract void setModel(double[] parameters );


	/**
	 * The number of genes involved in this model
	 * 
	 */
	public abstract int genes();

	/**
	 * Initialize the genomes without parents. Use for instance when initializing the population 
	 * 
	 * @param rand
	 * @return
	 */
	public abstract Genomable getRandomGenotype(Random rand);

	/**
	 * 
	 */
	public void reset() {
			}

	/**
	 * 
	 * @param i
	 * @return
	 */
	public abstract Table inheritancetable(int gene);
	

}