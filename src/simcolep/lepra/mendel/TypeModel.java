/*
 * This file is part of SIMCOLEP, a simulation model developed by the Department of Public Health, 
 * Erasmus MC, University Medical Center Rotterdam, Rotterdam, The Netherlands.
 *
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/ 
 * or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA
 *
 * You are free to:
 * Share � copy and redistribute the material in any medium or format
 * Under the following terms:
 * Attribution � You must give appropriate credit, provide a link to the license, and indicate if changes were 
 * made. You may do so in any reasonable manner, but not in any way that suggests the licensor endorses
 * you or your use.
 * NonCommercial � You may not use the material for commercial purposes.
 * NoDerivatives � If you remix, transform, or build upon the material, you may not distribute the modified material.
 */
package simcolep.lepra.mendel;

import java.util.Random;

import simcolep.fun2.Archimedes;
import simcolep.tools.PN;
import simcolep.tools.io.SimulationLog;
import simcolep.tools.io.Table;




/**
 * Inheritance of type
 * 
 * @author ir. E.A.J. Fischer
 * @version 1.1
 */
public final class TypeModel 
		extends InheritanceModel
{
	
	/**number of genes*/
	private static final int GENES = 1;

	/**
	 * 
	 *
	 */
	public TypeModel()
	{super();}
	
	/**
	 * @see simcolep.lepra.mendel.InheritanceModel#getGenotype(Genome, Genome, java.util.Random)
	 */
	public Genomable getGenotype(Genomable mother_genome,
			Genomable father_genome, 
			Random rand)
	{
		/*choose and return genome*/
		if(rand.nextDouble()< dismom)
			return mother_genome;
		else
			return father_genome;
	}

	/**
	 * @see simcolep.lepra.mendel.InheritanceModel#genes()
	 */
	public int genes() {
			return GENES;
	}

	/**
	 * @see simcolep.lepra.mendel.InheritanceModel#getRandomGenotype(java.util.Random)
	 */
	public Genomable getRandomGenotype(Random rand) {
		
		int[][]genes = new int[2][GENES];
		
		genes[0][0] = 
			(int)Archimedes.askDistribution(PN.GENE+0).drawX(rand.nextDouble());
		genes[1][0] = genes[0][0];
					
		return Genome.getGenome(genes);
	}
	/**
	 * Set model parameters in this model the probability to inherit from mothers
	 * @see simcolep.lepra.mendel.InheritanceModel#setModel(double[])
	 */
	public void setModel(double[] parameters) {
		this.dismom = parameters[0];
	}

	@Override
	public Table inheritancetable(int gene) {
		SimulationLog.log("not implemented inheritance table");
		return new Table(new double[1][1]);
	}
	
}