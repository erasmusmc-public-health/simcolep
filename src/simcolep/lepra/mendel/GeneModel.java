/*
 * This file is part of SIMCOLEP, a simulation model developed by the Department of Public Health,
 * Erasmus MC, University Medical Center Rotterdam, Rotterdam, The Netherlands.
 *
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/
 * or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA
 *
 * You are free to:
 * Share � copy and redistribute the material in any medium or format
 * Under the following terms:
 * Attribution � You must give appropriate credit, provide a link to the license, and indicate if changes were
 * made. You may do so in any reasonable manner, but not in any way that suggests the licensor endorses
 * you or your use.
 * NonCommercial � You may not use the material for commercial purposes.
 * NoDerivatives � If you remix, transform, or build upon the material, you may not distribute the modified material.
 */
package simcolep.lepra.mendel;

import java.util.Random;

import simcolep.demo.Sex;
import simcolep.fun2.Archimedes;
import simcolep.fun2.Distributionable;
import simcolep.tools.PN;
import simcolep.tools.io.Table;

/**
 * Inheritance model with a free number of genes which should be indicated by a
 * parameter
 *
 * @author ir E.A.J. Fischer
 *
 */
public final class GeneModel extends InheritanceModel {

	private static double[][][] gene_count = null;
	public static boolean starthomozygous = true;

	/**
	 * Number of genes
	 */
	private int GENES = 2;

	/**
	 *
	 */
	public GeneModel() {

	}

	/**
	 * @see simcolep.lepra.mendel.InheritanceModel#genes()
	 */
	@Override
	public int genes() {
		return GENES;
	}

	/**
	 * Get from both genomes at random one allele per gene
	 *
	 * @see simcolep.lepra.mendel.InheritanceModel#getGenotype(simcolep.lepra.mendel.Genomable,
	 *      simcolep.lepra.mendel.Genomable, java.util.Random)
	 */
	@Override
	public Genomable getGenotype(Genomable mother_genome, Genomable father_genome, Random rand) {

		int[][] genes = new int[2][GENES];
		// the gene counter assumes 2 alleles per gene!
		if (gene_count == null)
			gene_count = new double[GENES][6][3];

		int[][] fathergenes = father_genome.getGenotype();
		int[][] mothergenes = mother_genome.getGenotype();

		for (int i = 0; i < GENES; i++) {
			// the gene counter assumes 2 alleles per gene!
			int sumfgenes = fathergenes[0][i] + fathergenes[1][i];
			int summgenes = mothergenes[0][i] + mothergenes[1][i];
			// determine one of six possibilities
			int index = sumfgenes + summgenes;
			if (index > 2) {
				index++;

			} else if (index == 2) {
				if (sumfgenes == 1)// heterozygote
					index = 2;
				else
					index = 3;
			}

			// determine father index
			int f_index = -1;
			double p = rand.nextDouble();
			if (p < 0.5)
				f_index = 0;
			else if (p >= 0.5)
				f_index = 1;

			// father
			genes[Sex.MALE.getValue()][i] = fathergenes[f_index][i];

			// determine mother index
			int m_index = -1;
			p = rand.nextDouble();
			if (p < 0.5)
				m_index = 0;
			else if (p >= 0.5)
				m_index = 1;

			// mother
			genes[Sex.FEMALE.getValue()][i] = mothergenes[m_index][i];

			gene_count[i][index][genes[0][i] + genes[1][i]] += 1;
		}

		return Genome.getGenome(genes);
	}

	/**
	 * @see simcolep.lepra.mendel.InheritanceModel#getRandomGenotype(java.util.Random)
	 */
	@Override
	public Genomable getRandomGenotype(Random rand) {
		if (starthomozygous)
			return getRandomHomozygousGenotype(rand);

		int[][] genes = new int[2][GENES];

		for (int i = 0; i < GENES; i++) {

			Distributionable gene_freq = Archimedes.askDistribution(PN.GENE + i);

			// father
			genes[Sex.MALE.getValue()][i] = (int) gene_freq.drawX(rand.nextDouble());

			// mother
			genes[Sex.FEMALE.getValue()][i] = (int) gene_freq.drawX(rand.nextDouble());
		}

		return Genome.getGenome(genes);
	}

	/**
	 *
	 * @return
	 */
	@Override
	public Table inheritancetable(int gene) {
		return new Table(gene_count[gene]);
	}

	@Override
	public void reset() {
		gene_count = null;
	}

	/**
	 * Set model parameters in this model the number of genes
	 * 
	 * @see simcolep.lepra.mendel.InheritanceModel#setModel(double[])
	 */
	@Override
	public void setModel(double[] parameters) {
		GENES = (int) parameters[0];

	}

	/**
	 *
	 * @param rand
	 * @return
	 */
	private Genomable getRandomHomozygousGenotype(Random rand) {
		int[][] genes = new int[2][GENES];

		for (int i = 0; i < GENES; i++) {

			Distributionable gene_freq = Archimedes.askDistribution(PN.GENE + i);

			// draw gene
			int gene = (int) gene_freq.drawX(rand.nextDouble());
			// father
			genes[Sex.MALE.getValue()][i] = gene;

			// mother
			genes[Sex.FEMALE.getValue()][i] = gene;
		}

		return Genome.getGenome(genes);
	}

}
