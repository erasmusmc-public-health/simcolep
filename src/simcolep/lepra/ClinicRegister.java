/*
 * This file is part of SIMCOLEP, a simulation model developed by the Department of Public Health,
 * Erasmus MC, University Medical Center Rotterdam, Rotterdam, The Netherlands.
 *
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/
 * or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA
 *
 * You are free to:
 * Share � copy and redistribute the material in any medium or format
 * Under the following terms:
 * Attribution � You must give appropriate credit, provide a link to the license, and indicate if changes were
 * made. You may do so in any reasonable manner, but not in any way that suggests the licensor endorses
 * you or your use.
 * NonCommercial � You may not use the material for commercial purposes.
 * NoDerivatives � If you remix, transform, or build upon the material, you may not distribute the modified material.
 */

/*
 * Created on 28-November-2005
 *
 */
package simcolep.lepra;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import simcolep.asgard.Idunn;
import simcolep.demo.Person;
import simcolep.demo.Register;
import simcolep.demo.Sex;
import simcolep.exceptions.SimulationException;
import simcolep.fun2.Archimedes;
import simcolep.tools.PN;
import simcolep.tools.io.OutputList;
import simcolep.tools.io.Table;
import simcolep.tools.message.Message;
import simcolep.tools.message.Ratatosk;

/**
 * This class registers Morbus objects incidence, prevalence and transmission
 *
 *
 * @author ir E.A.J. Fischer
 * @version 0.1.2 -- Last Modified 28-Nov-2005
 */
@SuppressWarnings("unchecked")
public class ClinicRegister implements Register {
	/** serial Version UID */
	private static final long serialVersionUID = 5151596943150217315L;

	/**
	 *
	 */
	private static final String DIAGNOSIS = "diagnosis";
	/**
	 *
	 */
	private static final String INFECTED = "infected";

	/** String representation of prevalence */
	private static final String PREV_STRING = "p";

	/** String representing incidence of relapses */
	public static final String RELINC = "i_rel";

	/** String representing incidence of passive detection */
	public static final String PASDETINC = "i_det_pas";

	/** String representing incidence of active case finding during surveys */
	public static final String SURVEYDETINC = "i_det_survey";
	/**
	 * String representing number examinated of active case finding during
	 * surveys
	 */
	public static final String SURVEYDETEX = "i_ex_survey";

	/** string representing the size of household of the first cases found */
	public static final String PRIMCASEHH = "hhsizePrimary";

	/**
	 * String representing incidence of active case finding during contact
	 * tracing
	 */
	public static final String CONTACTDETINC = "i_det_contact";

	/** String representing number examinated by contact tracing */
	public static final String CONTACTDETEX = "i_ex_contact";

	/**
	 * String representing incidence of active case finding during contact
	 * tracing
	 */
	public static final String FUDETINC = "i_det_fu";

	/** String representing number examinated by contact tracing */
	public static final String FUEX = "i_ex_fu";

	/** String representing incidence of diagnosis */
	public static final String DIAGINC = " i_diag";

	/** String representing incidence of transmission */
	public static final String TRANSINC = " i_trans";

	/** String representing incidence of states */
	public static final String STATEINC = " i_state";

	/** String representing age of detected for males */
	public static final String AGEINCMALETOTAL = "age_itotal_male";

	/** String representing age of detected for males */
	public static final String AGEINCFEMALETOTAL = "age_itotal_female";

	/** String representing age of detected for males */
	public static final String AGEINCMALEPAS = "age_ipas_male";

	/** String representing age of detected for males */
	public static final String AGEINCFEMALEPAS = "age_ipas_female";

	/** String representing age of detected for males */
	public static final String AGEINCMALECON = "age_icon_male";

	/** String representing age of detected for males */
	public static final String AGEINCFEMALECON = "age_icon_female";

	/** String representing age of detected for males */
	public static final String AGEINCMALEEX = "age_excon_male";

	/** String representing age of detected for males */
	public static final String AGEINCFEMALEEX = "age_excon_female";

	/** String representing relationships of contacts */
	public static final String RELATION_INC_MALE = "relation_i_male";

	/** String representing relationships of contacts */
	public static final String RELATION_EX_MALE = "relation_e_male";

	/** String representing relationships of contacts */
	public static final String RELATION_INC_FEMALE = "relation_i_female";

	/** String representing relationships of contacts */
	public static final String RELATION_EX_FEMALE = "relation_e_female";

	/** String representing incidence of diagnosis */
	public static final String DIAGPREV = " p_diag";

	/** String representing prevalence of states */
	public static final String STATEPREV = " p_state";

	/** String representation of gene prevalence */
	public static final String GENEPREV = "genefreq ";

	/** String representation of type prevalence */
	public static final String TYPEPREV = "type";

	/** String representing incidence of states */
	public static final String POPCOUNT = "count";

	/** string representing extinction output */
	public static final String EXTINCTION = "Extinct";

	public static final String SURVEYTOTALEX = "total_exsur";
	public static final String CONTACTTOTALEX = "total_excon";
	public static final String FUTOTALEX = "total_exfu";
	public static final String CHEMO = "chemo_state";

	public static final String SURVEYTOTALINC = "total_surinc";
	public static final String CONTACTTOTALINC = "total_coninc";
	public static final String FUTOTALINC = "total_fuinc";
	public static final String PASTOTALINC = "total_pasinc";
	public static final String DIAGTOTALINC = "total_diaginc";
	public static final String CHEMOTOTAL = "total_chemo";

	/** Detection methods */
	/** Passive */
	public static final int PASSIVE_DETECTION = 0;

	/** Survey */
	public static final int SURVEY_DETECTION = 1;

	/** Contact tracing */
	public static final int CONTACT_DETECTION = 2;

	/** Follow up of detected patient for possible relapse */
	public static final int PATIENTFOLLOWUP = 3;

	/** Follow up of contact of a patient */
	public static final int CONTACTFOLLOWUP = 4;

	private static final int TOTAL = 5;

	private static final int MAXHH = 25;

	/** Monitor whether a removal of a infected means extinction of infection */
	private boolean Monitor_Extinction = false;

	/** updating prevalence files yes/no */
	private boolean save = false;

	/** the register of morbus types */
	private List<LinkedHashSet<Morbus>> types = null;

	/** the registration of passive detection incidence NCDR */
	private List<int[]> pasdetinc = null;

	/** the registration of active detection during surveys */
	private List<int[]> surdetinc = null;

	/** the registration of active detection during contact tracing */
	private List<int[]> contactdetinc = null;

	/** the registration of actiove detection by relationship */
	private List<int[]>[] contactrelinc = null;

	/** the registration of active detection during followup of contacts */
	private List<int[]> fudetinc = null;

	/** the registration of detection by diagnosis */
	private List<int[]> diaginc = null;

	/** the registration of prevalence by diagnosis */
	private List<int[]> diagprev = null;

	/** the registration of prevalent states */
	private List<int[]> stateprev = null;
	/** gene frequencies */
	private List<int[]> geneprev = null;
	/** the registration of types */
	private List<int[]> typeprev = null;

	/** the registration of incident of state changes */
	private List<int[]> stateinc = null;

	/** the registration of detected relapses */
	private List<int[]> relinc = null;

	/** recorded at times */
	private List<Double> times = null;

	/** transmission */
	private List<int[]> transinc = null;

	/** population count */
	private int[] pop_count = null;

	/** extinction counter */
	private List<int[]> extinct = null;

	/** list with population counts */
	private List<int[]> popsize = null;

	/** examination for surveys */
	private List<int[]> surdet_examination = null;

	/** examination for contact tracing */
	private List<int[]> contactdet_examination = null;

	/** examination by relation contact tracing */
	private List<int[]>[] contactrelex = null;

	/** examination during follow up of contacts */
	private List<int[]> fudet_examination = null;

	/** administered chemoprophylactic treatment by state */
	private List<int[]> chemo = null;

	/**
	 * count the number of infected (not susceptible not cured, not 'immune')
	 */
	private int infected = 0;

	/** maximum number of each state and time it was reached */
	private int[] maxima = new int[State.MAX_STATE];
	private double[] maxtimes = new double[State.MAX_STATE];

	/** Counter for contact tracing during last time interval */
	private List<int[]> detcontact_groupsize = null;

	/** last saved output of contact groups */
	private List<int[]> saved_detcon_groups = null;

	/* total incidence counters */
	/** total number detections by passive detection */
	private List<int[]> totalpasinc = null;
	/** total number detections by contact detection */
	private List<int[]> totalconinc = null;
	/** total number detections by follow up of contacts */
	private List<int[]> totalfuinc = null;
	/** total number detections by survey detection */
	private List<int[]> totalsurinc = null;
	/** total number of positive diagnoses */
	private List<int[]> totaldiaginc = null;
	/** total number examinations by contact detection */
	private List<int[]> totalconex = null;
	/** total number examinations during follow up */
	private List<int[]> totalfuex = null;
	/** total number examination during surveys */
	private List<int[]> totalsurex = null;
	/** total number of chemoprophylactic treatments */
	private List<int[]> totalchemo = null;

	private boolean closed = false;

	/** number detected by age class and detection mode */
	private List[][] age_inc = null;

	/** number examined by age class and detection mode */
	private List[][] age_ex = null;

	public int ageclasses = 21;

	/**
	 * counts the sizes of households of cases detected other than by contact
	 * tracing
	 */
	private ArrayList<int[]> primcasehh = null;

	/**
	 *
	 * @param stepsize
	 */
	public ClinicRegister() {
		super();

		// initiate lists
		this.types = new ArrayList<LinkedHashSet<Morbus>>();
		for (int x = 0; x < Morbus.DISEASE_TYPES; x++) {
			this.types.add(new LinkedHashSet<Morbus>());

		}

		// detected per state
		// passive
		pasdetinc = new ArrayList<int[]>();
		pasdetinc.add(new int[State.MAX_STATE]);

		// survey
		surdetinc = new ArrayList<int[]>();
		surdetinc.add(new int[State.MAX_STATE]);

		this.surdet_examination = new ArrayList<int[]>();
		this.surdet_examination.add(new int[State.MAX_STATE]);
		// contact tracing
		contactdetinc = new ArrayList<int[]>();
		contactdetinc.add(new int[State.MAX_STATE]);

		this.primcasehh = new ArrayList<int[]>();
		this.primcasehh.add(new int[MAXHH]);

		// relation ships
		this.contactrelinc = new ArrayList[2];
		this.contactrelinc[Sex.MALE.getValue()] = new ArrayList<int[]>();
		this.contactrelinc[Sex.FEMALE.getValue()] = new ArrayList<int[]>();
		this.contactrelinc[Sex.MALE.getValue()].add(new int[Person.RELATIONS]);
		this.contactrelinc[Sex.FEMALE.getValue()].add(new int[Person.RELATIONS]);
		// examination of contact tracing
		this.contactdet_examination = new ArrayList<int[]>();
		this.contactdet_examination.add(new int[State.MAX_STATE]);

		// relation ships
		this.contactrelex = new ArrayList[2];
		this.contactrelex[Sex.MALE.getValue()] = new ArrayList<int[]>();
		this.contactrelex[Sex.FEMALE.getValue()] = new ArrayList<int[]>();
		this.contactrelex[Sex.MALE.getValue()].add(new int[Person.RELATIONS]);
		this.contactrelex[Sex.FEMALE.getValue()].add(new int[Person.RELATIONS]);

		// follow up
		// contact tracing
		fudetinc = new ArrayList<int[]>();
		fudetinc.add(new int[State.MAX_STATE]);

		// examination of contact tracing
		this.fudet_examination = new ArrayList<int[]>();
		this.fudet_examination.add(new int[State.MAX_STATE]);

		// examination of contact tracing
		this.chemo = new ArrayList<int[]>();
		this.chemo.add(new int[State.MAX_STATE]);

		// diagnosis per diagnosis type
		diaginc = new ArrayList<int[]>();
		diaginc.add(new int[Clinic.DIAGNOSIS]);

		// relapsed per type
		relinc = new ArrayList<int[]>();
		relinc.add(new int[Morbus.DISEASE_TYPES]);

		this.age_inc = new ArrayList[Sex.BOTH.getValue()][6];
		// age at detection males
		for (int i = 0; i < 6; i++) {
			this.age_inc[Sex.MALE.getValue()][i] = new ArrayList<int[]>();
			ageclasses = (int) Math
					.round(Archimedes.askDouble(PN.AGECLASSMAX) / Archimedes.askDouble(PN.AGECLASSWIDTH));
			this.age_inc[Sex.MALE.getValue()][i].add(new int[ageclasses]);
			// age at detection females
			this.age_inc[Sex.FEMALE.getValue()][i] = new ArrayList<int[]>();
			ageclasses = (int) Math
					.round(Archimedes.askDouble(PN.AGECLASSMAX) / Archimedes.askDouble(PN.AGECLASSWIDTH));
			this.age_inc[Sex.FEMALE.getValue()][i].add(new int[ageclasses]);
		}
		this.age_ex = new ArrayList[Sex.BOTH.getValue()][6];
		// age at detection males
		for (int i = 0; i < 6; i++) {
			this.age_ex[Sex.MALE.getValue()][i] = new ArrayList<int[]>();
			ageclasses = (int) Math
					.round(Archimedes.askDouble(PN.AGECLASSMAX) / Archimedes.askDouble(PN.AGECLASSWIDTH));
			this.age_ex[Sex.MALE.getValue()][i].add(new int[ageclasses]);
			// age at detection females
			this.age_ex[Sex.FEMALE.getValue()][i] = new ArrayList<int[]>();
			ageclasses = (int) Math
					.round(Archimedes.askDouble(PN.AGECLASSMAX) / Archimedes.askDouble(PN.AGECLASSWIDTH));
			this.age_ex[Sex.FEMALE.getValue()][i].add(new int[ageclasses]);
		}

		// detected per diagnosis
		this.diagprev = new ArrayList<int[]>();
		this.diagprev.add(new int[Clinic.DIAGNOSIS]);

		// prevalence of states
		this.stateprev = new ArrayList<int[]>();
		// the last entry is the sum of all others to count population size
		this.stateprev.add(new int[State.MAX_STATE]);

		// prevalence of states
		this.typeprev = new ArrayList<int[]>();
		// the last entry is the sum of all others to count population size
		this.typeprev.add(new int[Morbus.DISEASE_TYPES]);

		// prevalent gene frequencies
		int genes = Archimedes.askInteger(PN.NUMOFGENES);
		int alleles = 0;
		for (int i = 0; i < genes; i++) {
			alleles += Archimedes.askInteger(PN.NUMOFALLELES + i);
		}
		this.geneprev = new ArrayList<int[]>();
		this.geneprev.add(new int[alleles]);

		// incidence of states
		this.stateinc = new ArrayList<int[]>();
		// the last entry is the sum of all others to count population size
		this.stateinc.add(new int[State.MAX_STATE]);

		// transmission
		this.transinc = new ArrayList<int[]>();
		this.transinc.add(new int[Math.max(Archimedes.askInteger(PN.LEVELDEPTH), 2)]);

		// totals
		totalpasinc = new ArrayList<int[]>();
		totalpasinc.add(new int[1]);
		totalconinc = new ArrayList<int[]>();
		totalconinc.add(new int[1]);
		totalfuinc = new ArrayList<int[]>();
		totalfuinc.add(new int[1]);
		totalsurinc = new ArrayList<int[]>();
		totalsurinc.add(new int[1]);
		totaldiaginc = new ArrayList<int[]>();
		totaldiaginc.add(new int[1]);
		totalconex = new ArrayList<int[]>();
		totalconex.add(new int[1]);
		totalfuex = new ArrayList<int[]>();
		totalfuex.add(new int[1]);
		totalsurex = new ArrayList<int[]>();
		totalsurex.add(new int[1]);
		totalchemo = new ArrayList<int[]>();
		totalchemo.add(new int[1]);
		// population count
		this.pop_count = new int[1];
		this.popsize = new ArrayList<int[]>();
		this.popsize.add(new int[1]);
		// extinction counter
		int[] ext = { 1 };
		this.extinct = new ArrayList<int[]>();
		this.extinct.add(ext);

	}

	/**
	 *
	 * @param m
	 */
	public void addChemo(Morbus m) {
		if (closed)
			return;

		if (save) {
			this.chemo.get(chemo.size() - 1)[m.getState()] += 1;
			this.totalchemo.get(totalchemo.size() - 1)[0] += 1;
		}
		return;
	}

	/**
	 * Register the result of examining certain group and the result of the
	 * number of detected
	 *
	 * @param groupsize
	 * @param detected
	 */
	public void addContactTracingResult(int groupsize, int detected) {
		if (groupsize < detected)
			throw new SimulationException("cannot detect more contacts than level inhabitants!");

		if (this.detcontact_groupsize == null)
			this.detcontact_groupsize = new ArrayList<int[]>();

		while (this.detcontact_groupsize.size() < groupsize) {
			this.detcontact_groupsize.add(new int[this.detcontact_groupsize.size() + 2]);
		}
		// add one (levelsize-1 one because levels of size 0 are of no interest
		this.detcontact_groupsize.get(groupsize - 1)[detected] += 1;
	}

	/**
	 * Add m to the list of detected and add one to the detection incidence
	 *
	 * @param m
	 * @param relation
	 *            null if not contact tracing otherwise index case
	 * @param reason
	 */
	public void addDetected(Morbus m, int relation, Sex sex_index, int reason) {
		if (closed)
			return;

		if (m.relapsing())
			if (save)
				relinc.get(relinc.size() - 1)[m.getType()] += 1;

		// add to the list of diagnosed
		this.addDiagnosed(m);

		// add one
		if (save) {
			this.addAgeclass(m, reason);

			switch (reason) {
			case PASSIVE_DETECTION: {
				this.pasdetinc.get(pasdetinc.size() - 1)[m.getState()] += 1;
				this.totalpasinc.get(totalpasinc.size() - 1)[0] += 1;
				int hhsize = Math.min(m.getOwner().getLevel().getSize(), MAXHH - 1);
				this.primcasehh.get(primcasehh.size() - 1)[hhsize] += 1;
				break;
			}
			case SURVEY_DETECTION: {
				this.surdetinc.get(surdetinc.size() - 1)[m.getState()] += 1;
				this.totalsurinc.get(totalsurinc.size() - 1)[0] += 1;
				int hhsize = Math.min(m.getOwner().getLevel().getSize(), MAXHH - 1);
				this.primcasehh.get(primcasehh.size() - 1)[hhsize] += 1;
				break;
			}
			case CONTACT_DETECTION: {
				this.contactdetinc.get(contactdetinc.size() - 1)[m.getState()] += 1;
				this.totalconinc.get(totalconinc.size() - 1)[0] += 1;
				this.contactrelinc[sex_index.getValue()]
						.get(this.contactrelinc[sex_index.getValue()].size() - 1)[relation] += 1;
				break;
			}
			case CONTACTFOLLOWUP: {
				this.fudetinc.get(fudetinc.size() - 1)[m.getState()] += 1;
				this.totalfuinc.get(totalfuinc.size() - 1)[0] += 1;
				break;
			}
			default:
				return;

			}
		}

	}

	/**
	 * Count number of examined by type of examination
	 *
	 * @param m
	 * @param relation
	 *            - if an index case was present for contact tracing otherwise
	 *            null
	 * @param reason
	 */
	public void addExamined(Morbus m, int relation, Sex sex_index, int reason) {
		if (closed)
			return;

		if (!save)
			return;

		Person owner = m.getOwner();
		int ageclass = owner.getAgeclass();

		int[] ageexdm = (int[]) this.age_ex[owner.getSex().getValue()][reason]
				.get(this.age_ex[owner.getSex().getValue()][reason].size() - 1);

		if (ageclass >= ageexdm.length)
			ageclass = ageexdm.length - 1;

		ageexdm[ageclass] += 1;
		this.age_ex[owner.getSex().getValue()][reason].set(this.age_ex[owner.getSex().getValue()][reason].size() - 1,
				ageexdm);

		switch (reason) {

		case SURVEY_DETECTION: {
			this.surdet_examination.get(surdet_examination.size() - 1)[m.getState()] += 1;
			this.totalsurex.get(totalsurex.size() - 1)[0] += 1;
			break;
		}
		case CONTACT_DETECTION: {
			this.contactdet_examination.get(contactdet_examination.size() - 1)[m.getState()] += 1;
			this.totalconex.get(totalconex.size() - 1)[0] += 1;
			this.contactrelex[sex_index.getValue()]
					.get(this.contactrelex[sex_index.getValue()].size() - 1)[relation] += 1;
			break;
		}
		case CONTACTFOLLOWUP: {
			this.fudet_examination.get(fudet_examination.size() - 1)[m.getState()] += 1;
			this.totalfuex.get(totalfuex.size() - 1)[0] += 1;
			break;
		}
		default:
			return;

		}

	}

	/**
	 * Add to the types list
	 *
	 * @param m
	 */
	public void addMorbus(Morbus m) {
		if (closed)
			return;

		this.addMorbus(m, m.getType());
	}

	/**
	 *
	 * @param morbus
	 * @param from
	 * @param to
	 */
	public void changeType(Morbus m, int from, int to) {
		if (closed)
			return;

		// add to register
		this.addMorbus(m, to);
		// remove from register
		this.remove(m, from);

	}

	/**
	 * Collect prevalence and incidence data
	 *
	 * @param time
	 */
	@Override
	public void collectData(double time) {
		// to save or not to save
		if (!save)
			return;

		// add to the times list
		if (this.times == null)
			this.times = new ArrayList<Double>();

		this.times.add(time);

		// prevalence per diagnosis add the last prevalence
		int[] diag_prev = this.diagprev.get(this.diagprev.size() - 1).clone();

		// prevalence of state
		this.diagprev.add(diag_prev);

		// prevalence per state add the last prevalence
		int[] state_prev = this.stateprev.get(this.stateprev.size() - 1).clone();

		// prevalence of state
		this.stateprev.add(state_prev);
		// prevalence per state add the last prevalence
		int[] type_prev = this.typeprev.get(this.typeprev.size() - 1).clone();

		// prevalence of state
		this.typeprev.add(type_prev);
		// prevalence per state add the last prevalence
		int[] gene_prev = this.geneprev.get(this.geneprev.size() - 1).clone();

		// prevalence of state
		this.geneprev.add(gene_prev);

		// set the counter to the last place of population size list
		this.popsize.set(this.popsize.size() - 1, this.pop_count);
		// create a new counter
		this.pop_count = new int[1];
		this.pop_count[0] = this.popsize.get(this.popsize.size() - 1)[0];
		this.popsize.add(this.pop_count);

		int[] ext = { Math.min(1, this.infected) };
		this.extinct.add(ext);

		// incidence per state
		this.stateinc.add(new int[State.MAX_STATE + 1]);
		for (int i = 0; i < 6; i++) {
			this.age_inc[Sex.MALE.getValue()][i].add(new int[ageclasses]);
			this.age_inc[Sex.FEMALE.getValue()][i].add(new int[ageclasses]);
			this.age_ex[Sex.MALE.getValue()][i].add(new int[ageclasses]);
			this.age_ex[Sex.FEMALE.getValue()][i].add(new int[ageclasses]);
		}
		/*
		 * add an array to each incidence list so that incidence of last period
		 * is saved
		 */
		// detection
		this.pasdetinc.add(new int[State.MAX_STATE]);
		this.surdetinc.add(new int[State.MAX_STATE]);
		this.contactdetinc.add(new int[State.MAX_STATE]);
		this.contactrelinc[Sex.MALE.getValue()].add(new int[Person.RELATIONS]);
		this.contactrelinc[Sex.FEMALE.getValue()].add(new int[Person.RELATIONS]);
		this.fudetinc.add(new int[State.MAX_STATE]);
		this.primcasehh.add(new int[MAXHH]);
		// examination
		this.contactdet_examination.add(new int[State.MAX_STATE]);
		this.contactrelex[Sex.MALE.getValue()].add(new int[Person.RELATIONS]);
		this.contactrelex[Sex.FEMALE.getValue()].add(new int[Person.RELATIONS]);
		this.fudet_examination.add(new int[State.MAX_STATE]);
		this.surdet_examination.add(new int[State.MAX_STATE]);
		this.chemo.add(new int[State.MAX_STATE]);
		// relapse
		this.relinc.add(new int[Morbus.DISEASE_TYPES]);
		// diagnosis
		this.diaginc.add(new int[Clinic.DIAGNOSIS]);
		// transmission
		this.transinc.add(new int[Math.max(Archimedes.askInteger(PN.LEVELDEPTH), 2)]);

		// totals
		totalpasinc.add(new int[1]);
		totalconinc.add(new int[1]);
		totalfuinc.add(new int[1]);
		totalsurinc.add(new int[1]);
		totaldiaginc.add(new int[1]);
		totalconex.add(new int[1]);
		totalfuex.add(new int[1]);
		totalsurex.add(new int[1]);
		totalchemo.add(new int[1]);

		// refresh the contact tracing lists
		this.saved_detcon_groups = this.detcontact_groupsize;
		this.detcontact_groupsize = new ArrayList<int[]>();
	}

	/**
	 * Sets all value to the error code
	 *
	 * @param time
	 *            time interval to 'collect'
	 * @param as_int
	 *            number representing an error
	 * @param times
	 *            to collect
	 *
	 */
	@Override
	public void collectDataAtTimeAs(double time, double as) {
		int as_int = (int) as;
		// to save or not to save
		if (!save)
			return;
		// add to the times list
		if (this.times == null)
			this.times = new ArrayList<Double>();

		this.times.add(time);

		// create data to be collected
		int[] states = new int[State.MAX_STATE];
		for (int i = 0; i < State.MAX_STATE; i++)
			states[i] = as_int;
		int[] types = new int[Morbus.DISEASE_TYPES];
		for (int i = 0; i < Morbus.DISEASE_TYPES; i++)
			types[i] = as_int;
		int[] diagnosis = new int[Clinic.DIAGNOSIS];
		for (int i = 0; i < Clinic.DIAGNOSIS; i++)
			diagnosis[i] = as_int;
		int[] onecell = { as_int };
		int[] age = new int[ageclasses];
		for (int i = 0; i < ageclasses; i++) {
			age[i] = as_int;
		}
		int[] relations = new int[Person.RELATIONS];
		for (int i = 0; i < Person.RELATIONS; i++) {
			relations[i] = as_int;
		}

		int[] genes = new int[this.geneprev.get(0).length];
		for (int i = 0; i < genes.length; i++)
			genes[i] = as_int;
		// prevalence of state
		this.diagprev.add(diagnosis);

		// prevalence of state
		this.stateprev.add(states);
		this.typeprev.add(types);
		this.geneprev.add(genes);
		this.popsize.add(onecell);
		this.extinct.add(onecell);

		// incidence per state
		this.stateinc.add(states);
		for (int i = 0; i < 6; i++) {
			this.age_inc[Sex.MALE.getValue()][i].add(age);
			this.age_inc[Sex.FEMALE.getValue()][i].add(age);
			this.age_ex[Sex.MALE.getValue()][i].add(age);
			this.age_ex[Sex.FEMALE.getValue()][i].add(age);
		}
		/*
		 * add an array to each incidence list so that incidence of last period
		 * is saved
		 */
		// detection
		this.pasdetinc.add(states);
		this.surdetinc.add(states);
		this.contactdetinc.add(states);
		this.contactrelinc[Sex.MALE.getValue()].add(relations);
		this.contactrelinc[Sex.FEMALE.getValue()].add(relations);
		this.fudetinc.add(states);
		this.primcasehh.add(new int[MAXHH]);
		// examination
		this.contactdet_examination.add(states);
		this.contactrelex[Sex.MALE.getValue()].add(relations);
		this.contactrelex[Sex.FEMALE.getValue()].add(relations);
		this.fudet_examination.add(states);
		this.surdet_examination.add(states);
		this.chemo.add(states);
		// relapse
		this.relinc.add(types);
		// diagnosis
		this.diaginc.add(diagnosis);
		// transmission
		this.transinc.add(new int[Math.max(Archimedes.askInteger(PN.LEVELDEPTH), 2)]);

		// totals
		totalpasinc.add(onecell);
		totalconinc.add(onecell);
		totalfuinc.add(onecell);
		totalsurinc.add(onecell);
		totaldiaginc.add(onecell);
		totalconex.add(onecell);
		totalfuex.add(onecell);
		totalsurex.add(onecell);
		totalchemo.add(onecell);
	}

	/**
	 * true if this M is currently registered any where in this register false
	 * otherwise
	 *
	 * @param m
	 * @return result
	 */
	public boolean contains(Morbus m) {

		if (this.types.get(m.getType()).contains(m))
			return true;

		return false;
	}

	/**
	 * Retreives all ever infected (state>Morbus.SUSCEPTIBLE and state !=
	 * Morbus.IMMUNE) disease objects (thus all types) and stuffs them into one
	 * list which is returned.
	 *
	 * @return a list with all infected disease objects
	 */
	public Set<Morbus> getInfected() {
		Set<Morbus> s = new LinkedHashSet<Morbus>();

		for (Set<Morbus> set : this.types) {
			for (Morbus m : set)
				if (m.getState() > State.SUSCEPTIBLE_STATE && m.getState() < State.NEVER_SUSCEPTIBLE_STATE)
					s.add(m);
		}
		return s;
	}

	/**
	 *
	 * @param type
	 * @return collection of this type
	 */
	public Collection<Morbus> getMorbiOfType(int type) {
		return this.types.get(type);
	}

	/**
	 * Combine the types archives to a list and return
	 *
	 * @return l
	 */
	@Override
	public Iterable getRegister() {
		List<Morbus> l = new ArrayList<Morbus>();
		for (Set<Morbus> s : this.types)
			l.addAll(s);

		return l;
	}

	/**
	 * Combine the types archives to a list and return
	 *
	 * @return l
	 */
	public List<Morbus> getRegisterList() {
		List<Morbus> l = new ArrayList<Morbus>();
		for (Set<Morbus> s : this.types)
			l.addAll(s);

		return l;
	}

	/**
	 * @see simcolep.demo.Register#getSize()
	 */
	@Override
	public int getSize() {
		return pop_count[0];
	}

	/**
	 *
	 * @return sum of all susceptibles
	 */
	public int getSusceptibles() {
		int s = 0;
		for (int t = 1; t < Morbus.DISEASE_TYPES; t++) {
			s += this.getType(t);
		}
		return s;
	}

	/**
	 * Get the number of morbi of this type
	 *
	 * @param type
	 * @return
	 *
	 */
	public int getType(int type) {
		return this.types.get(type).size();
	}

	/**
	 *
	 * @return a list containg maximum number of each state
	 *
	 */
	public String maxString() {

		StringBuffer max = new StringBuffer();
		for (int i = 0; i < this.maxima.length; i++) {
			max.append(this.maxima[i] + PN.LINESEPARATOR);
		}

		return max.toString();
	}

	/**
	 *
	 * @return a list containg maximum number of each state
	 *
	 */
	public String maxtimeString() {

		StringBuffer max = new StringBuffer();
		for (int i = 0; i < this.maxima.length; i++) {
			max.append(this.maxtimes[i] + PN.LINESEPARATOR);
		}

		return max.toString();
	}

	/**
	 * No database
	 *
	 * @throws IOException
	 */
	public void open() throws IOException {
	}

	/**
	 *
	 * @return table
	 */
	public Table outputContractTracing() {
		int size = 25;
		// no contacttracing registers return empty tables
		if (this.saved_detcon_groups == null)
			return new Table(new double[size][size]);
		else if (this.saved_detcon_groups.isEmpty())
			return new Table(new double[size][size]);

		// increase size if needed
		if (size < this.saved_detcon_groups.size())
			size = this.saved_detcon_groups.size();

		// table names
		String[] rownames = new String[size];
		for (int i = 0; i < rownames.length; i++)
			rownames[i] = "Size " + (i + 1);

		// create a table of dimensions size
		Table table = new Table(size, size + 1);
		table.setRowNames(rownames);
		for (int gsize = 0; gsize < this.saved_detcon_groups.size(); gsize++) {
			int[] res = this.saved_detcon_groups.get(gsize);
			for (int det = 0; det < gsize + 1; det++) {
				table.setCell(gsize, det, res[det]);
			}
		}

		return table;

	}

	/**
	 * Records the incidence of the change of one state to another and keeps
	 * track of the prevalence of the states
	 *
	 * @param current_state
	 * @param new_state
	 */
	public void recordStateChange(int current_state, int new_state) {
		if (closed)
			return;

		// count the prevalence
		stateprev.get(stateprev.size() - 1)[new_state] += 1;

		// subtract previous
		stateprev.get(stateprev.size() - 1)[current_state] -= 1;

		/* update maxima */
		if (this.stateprev.get(stateprev.size() - 1)[new_state] > this.maxima[new_state]) {
			this.maxima[new_state] = this.stateprev.get(stateprev.size() - 1)[new_state];
			this.maxtimes[new_state] = Idunn.getCurrentTime();
		}

		// if both infected
		boolean cur_inf = (current_state > State.SUSCEPTIBLE_STATE && current_state < State.NEVER_SUSCEPTIBLE_STATE);
		boolean new_inf = (new_state > State.SUSCEPTIBLE_STATE && new_state < State.NEVER_SUSCEPTIBLE_STATE);
		// if currently infected
		if (cur_inf == true && new_inf == false)
			this.infected--;
		else if (new_inf == true && cur_inf == false) {// if current not
														// infected and new
														// state is
			this.infected++;
		}
		// monitoring
		if (this.Monitor_Extinction) {
			if (this.infected <= 0) {
				Ratatosk.notify(Message.formulate(Message.EXTINCT), this, this);

				this.close();
			}
		}
		// saving incidence
		if (save) {
			// count for incidence of the new state
			stateinc.get(stateinc.size() - 1)[new_state] += 1;
		}
	}

	/**
	 * Registers a succesfull transmission within the given rank
	 *
	 * @param rank
	 */
	public void registerTransmission(int rank) {
		if (closed)
			return;

		if (save)
			// count transmission
			transinc.get(transinc.size() - 1)[rank] += 1;
	}

	/**
	 * Remove this morbidable from all lists
	 *
	 * @param m
	 * @return true if M was removed from the list of infecteds.
	 */

	public boolean remove(Morbus m) {
		if (closed)
			return false;

		// get type
		int type = m.getType();

		// remove
		return remove(m, type);
	}

	/***
	 * Turn monitoring of extinction on of off
	 *
	 * @param b
	 */
	public void setExtinctionMonitor(boolean b) {
		this.Monitor_Extinction = b;
	}

	/**
	 * @see simcolep.demo.Register#storeData()
	 */
	@Override
	public void storeData(boolean b) {
		if (save == b)
			return;
		// start saving stuff
		save = b;

		// set the times list
		this.times = new ArrayList<Double>();

	}

	/**
	 * Fully expression of the register
	 *
	 * @return s
	 */
	public String toFullString() {
		String s = this.toString();
		s += PN.LINESEPARATOR + "Cases: " + PN.LINESEPARATOR;
		for (Set<Morbus> set : this.types) {
			s += "type: " + this.types.indexOf(set) + PN.LINESEPARATOR;
			for (Morbus m : set) {
				s += m.toString() + PN.LINESEPARATOR;
			}
		}
		return s;
	}

	/**
	 * Create an outputlist of the subject
	 *
	 *
	 * @param subject
	 * @return ol
	 */

	@SuppressWarnings("unchecked")
	public OutputList<Integer>[] toOutputLists(String subject) {
		List<int[]> l = null;
		// determine which list
		if (subject.equals(RELINC)) {
			// incidence of relapse
			l = relinc;
		} else if (subject.equals(PASDETINC)) {
			// incidence of passive detection (detection rate)
			l = pasdetinc;
		} else if (subject.equals(SURVEYDETINC)) {
			// incidence of survey detection (detection rate)
			l = surdetinc;
		} else if (subject.equals(CONTACTDETINC)) {
			// incidence of contact tracing detection (detection rate)
			l = contactdetinc;
		} else if (subject.equals(PRIMCASEHH)) {
			// size of household of primary cases
			l = primcasehh;
		} else if (subject.equals(RELATION_INC_MALE)) {
			// incidence of contact tracing detection by relationship
			l = contactrelinc[Sex.MALE.getValue()];
		} else if (subject.equals(RELATION_INC_FEMALE)) {
			// incidence of contact tracing detection by relationship
			l = contactrelinc[Sex.FEMALE.getValue()];
		} else if (subject.equals(FUDETINC)) {
			// incidence of contact tracing detection (detection rate)
			l = fudetinc;
		} else if (subject.equals(DIAGINC)) {
			// states prevalence
			l = diaginc;
		} else if (subject.equals(AGEINCMALETOTAL)) {
			// ages
			l = this.age_inc[Sex.MALE.getValue()][TOTAL];
		} else if (subject.equals(AGEINCFEMALETOTAL)) {
			// ages
			l = this.age_inc[Sex.FEMALE.getValue()][TOTAL];
		} else if (subject.equals(AGEINCMALEPAS)) {
			// ages
			l = this.age_inc[Sex.MALE.getValue()][PASSIVE_DETECTION];
		} else if (subject.equals(AGEINCFEMALEPAS)) {
			// ages
			l = this.age_inc[Sex.FEMALE.getValue()][PASSIVE_DETECTION];
		} else if (subject.equals(AGEINCMALECON)) {
			// ages
			l = this.age_inc[Sex.MALE.getValue()][CONTACT_DETECTION];
		} else if (subject.equals(AGEINCFEMALECON)) {
			// ages
			l = this.age_inc[Sex.FEMALE.getValue()][CONTACT_DETECTION];
		} else if (subject.equals(AGEINCMALEEX)) {
			// ages
			l = this.age_inc[Sex.MALE.getValue()][CONTACT_DETECTION];
		} else if (subject.equals(AGEINCFEMALEEX)) {
			// ages
			l = this.age_inc[Sex.FEMALE.getValue()][CONTACT_DETECTION];
		} else if (subject.equals(TRANSINC)) {
			// incidence of diagnosis
			l = transinc;
		} else if (subject.equals(STATEINC)) {
			// states incidence
			l = stateinc;
		} else if (subject.equals(DIAGPREV)) {
			// states prevalence
			l = diagprev;
		} else if (subject.equals(STATEPREV)) {
			// prevalence of diagnosis
			l = stateprev;
		} else if (subject.equals(TYPEPREV)) {
			// prevalence of genes
			l = typeprev;
		} else if (subject.equals(GENEPREV)) {
			// prevalence of genes
			l = geneprev;
		} else if (subject.equals(POPCOUNT)) {
			// population count
			l = this.popsize;
		} else if (subject.equals(CONTACTDETEX)) {
			// incidence of contact tracing detection (detection rate)
			l = contactdet_examination;
		} else if (subject.equals(FUEX)) {
			// incidence of contact tracing detection (detection rate)
			l = fudet_examination;
		} else if (subject.equals(SURVEYDETEX)) {
			// incidence of contact tracing detection (detection rate)
			l = surdet_examination;
		} else if (subject.equals(CHEMO)) {
			// incidence of contact tracing detection (detection rate)
			l = this.chemo;
		} else if (subject.equals(PASTOTALINC)) {
			// incidence of contact tracing detection (detection rate)
			l = this.totalpasinc;
		} else if (subject.equals(CONTACTTOTALINC)) {
			// incidence of contact tracing detection (detection rate)
			l = this.totalconinc;
		} else if (subject.equals(FUTOTALINC)) {
			// incidence of followup detection (detection rate)
			l = this.totalfuinc;
		} else if (subject.equals(SURVEYTOTALINC)) {
			// incidence of contact tracing detection (detection rate)
			l = this.totalsurinc;
		} else if (subject.equals(DIAGTOTALINC)) {
			// incidence of contact tracing detection (detection rate)
			l = this.totaldiaginc;
		} else if (subject.equals(CONTACTTOTALEX)) {
			// incidence of contact tracing detection (detection rate)
			l = this.totalconex;
		} else if (subject.equals(RELATION_EX_MALE)) {
			// incidence of contact tracing examininations by relationship
			l = contactrelex[Sex.MALE.getValue()];
		} else if (subject.equals(RELATION_EX_FEMALE)) {
			// incidence of contact tracing examininations by relationship
			l = contactrelex[Sex.FEMALE.getValue()];
		} else if (subject.equals(FUTOTALEX)) {
			// incidence of contact tracing detection (detection rate)
			l = this.totalfuex;
		} else if (subject.equals(SURVEYTOTALEX)) {
			// incidence of contact tracing detection (detection rate)
			l = this.totalsurex;
		} else if (subject.equals(CHEMOTOTAL)) {
			// incidence of contact tracing detection (detection rate)
			l = this.totalchemo;
		} else if (subject.equals(EXTINCTION)) {
			l = this.extinct;

		} else
			throw new SimulationException(SimulationException.UNKNOWNOUTPUT_MESSAGE);

		// classes
		int classes = l.get(0).length;
		// number of collection moments
		int moments = this.times.size();

		// create the main output array with time in first column and values in
		// the second
		int[][] main_array = new int[classes][moments];
		Double[] time_array = new Double[this.times.size()];
		time_array = this.times.toArray(time_array);

		// put values in the array
		for (int j = 0; j < moments; j++) {
			int[] values = l.get(j);
			for (int i = 0; i < classes; i++) {
				main_array[i][j] = values[i];
			}
		}

		// outputlists
		OutputList<Integer>[] list_array = new OutputList[classes];
		// iterate
		for (int index = 0; index < classes; index++) {

			String listName = null;
			if (list_array.length > 1)
				listName = subject + "_" + index;
			else
				listName = subject;

			list_array[index] = new OutputList<Integer>(listName, time_array, main_array[index]);

		}

		return list_array;
	}

	/**
	 *
	 * @param subject
	 *            - what information do we put in the table
	 * @return table
	 */
	@SuppressWarnings("unchecked")
	public Table toTable(String subject) {
		// a list
		List l = null;

		// result table
		Table table = null;

		// prevalences in a table
		if (subject.equalsIgnoreCase(PREV_STRING)) {
			// create table
			table = new Table(5, 2);
			// set column names
			table.setColumnName(0, INFECTED);
			table.setColumnName(1, DIAGNOSIS);

			// set values
			for (int x = 0; x < 5; x++) {

				table.setCell(x, 0, this.types.get(x).size());

			}
			// return table
			return table;

		} else if (subject.equalsIgnoreCase(RELINC)) {
			// incidence of relaspe
			l = relinc;
		} else if (subject.equalsIgnoreCase(PASDETINC)) {
			// incidence of passive detection (detection rate)
			l = pasdetinc;
		} else if (subject.equalsIgnoreCase(SURVEYDETINC)) {
			// incidence of survey detection (detection rate)
			l = surdetinc;
		} else if (subject.equalsIgnoreCase(CONTACTDETINC)) {
			// incidence of contact tracing detection (detection rate)
			l = contactdetinc;
		} else if (subject.equalsIgnoreCase(CONTACTDETEX)) {
			// incidence of contact tracing detection (detection rate)
			l = contactdet_examination;
		} else if (subject.equalsIgnoreCase(RELATION_INC_MALE)) {
			// incidence of contact tracing detection (detection rate)
			l = contactrelinc[Sex.MALE.getValue()];
		} else if (subject.equalsIgnoreCase(RELATION_EX_MALE)) {
			// incidence of contact tracing detection (detection rate)
			l = contactrelex[Sex.MALE.getValue()];
		} else if (subject.equalsIgnoreCase(RELATION_INC_FEMALE)) {
			// incidence of contact tracing detection (detection rate)
			l = contactrelinc[Sex.FEMALE.getValue()];
		} else if (subject.equalsIgnoreCase(RELATION_EX_FEMALE)) {
			// incidence of contact tracing detection (detection rate)
			l = contactrelex[Sex.FEMALE.getValue()];
		} else if (subject.equalsIgnoreCase(FUDETINC)) {
			// incidence of contact tracing detection (detection rate)
			l = fudetinc;
		} else if (subject.equalsIgnoreCase(FUEX)) {
			// incidence of contact tracing detection (detection rate)
			l = fudet_examination;
		} else if (subject.equalsIgnoreCase(SURVEYDETEX)) {
			// incidence of contact tracing detection (detection rate)
			l = surdet_examination;
		} else
			l = new ArrayList<int[]>();
		// create table
		table = new Table(l.size(), 5);

		// iterate
		for (int[] values : (List<int[]>) l) {
			int row_index = l.indexOf(values);

			// row name
			table.setRowName(row_index, Double.toString(this.times.get(row_index)));

			for (int x = 0; x < values.length; x++) {
				table.setCell(row_index, x, values[x]);
			}
		}
		// return the table
		return table;
	}

	/**
	 * Release disease object from treatment, which is the same as removing it
	 * from the diagnosed list
	 *
	 * @param m
	 */
	protected void rft(Morbus m) {
		if (closed)
			return;

		// check if ever diagnosed
		if (m.getDiagnosis() < 0)
			return;
		// otherwise remove
		// add to the correct list
		this.diagprev.get(this.diagprev.size() - 1)[m.getDiagnosis()] -= 1;

		// release
		m.rft();

	}

	/**
	 * Add to the output for age classes
	 *
	 * @param m
	 * @param detmode
	 */
	private void addAgeclass(Morbus m, int detmode) {
		int ageclass = m.getOwner().getAgeclass();
		Sex sex = m.getOwner().getSex();

		int[] ageinctot = (int[]) this.age_inc[sex.getValue()][TOTAL]
				.get(this.age_inc[sex.getValue()][TOTAL].size() - 1);
		int[] ageincdm = (int[]) this.age_inc[sex.getValue()][detmode]
				.get(this.age_inc[sex.getValue()][detmode].size() - 1);

		if (ageclass >= ageinctot.length)
			ageclass = ageinctot.length - 1;
		// TOTAL = 5
		ageinctot[ageclass] += 1;
		ageincdm[ageclass] += 1;

		this.age_inc[sex.getValue()][detmode].set(this.age_inc[sex.getValue()][TOTAL].size() - 1, ageinctot);
		this.age_inc[sex.getValue()][detmode].set(this.age_inc[sex.getValue()][detmode].size() - 1, ageincdm);
	}

	/**
	 * Add to the diagnosis lists
	 *
	 * @param m
	 */
	private void addDiagnosed(Morbus m) {
		if (closed)
			return;

		// health person do not have to go into a clinic register
		if (m.getDiagnosis() == Morbus.HEALTHY)
			throw new SimulationException("do not register healthy people");

		// add to the correct list
		this.diagprev.get(this.diagprev.size() - 1)[m.getDiagnosis()] += 1;

		// count incidence
		if (save) {
			// add one
			diaginc.get(diaginc.size() - 1)[m.getDiagnosis()] += 1;
			// count if not healthy
			if (m.getDiagnosis() != Morbus.HEALTHY)
				this.totaldiaginc.get(totaldiaginc.size() - 1)[0] += 1;
			;
		}
	}

	/**
	 *
	 * @param m
	 * @param type
	 */
	private void addMorbus(Morbus m, int type) {
		if (closed)
			return;

		/** add to the list of types */
		this.types.get(type).add(m);

		/** record the current state */
		int state = m.getState();

		this.stateprev.get(stateprev.size() - 1)[state] += 1;

		this.typeprev.get(typeprev.size() - 1)[type] += 1;

		if (state > State.SUSCEPTIBLE_STATE && state < State.NEVER_SUSCEPTIBLE_STATE)
			this.infected++;

		if (m.isDetected())
			// add to the correct list
			if (m.getRFTtime() > Idunn.getCurrentTime()) {
				// subtract from prevalence
				this.diagprev.get(this.diagprev.size() - 1)[m.getDiagnosis()] += 1;
			}

		// count allele frequencies
		int[][] genes = m.getGenome().getGenotype();
		int previous = 0;
		for (int sex = 0; sex < genes.length; sex++) {
			for (int j = 0; j < genes[sex].length; j++) {
				this.geneprev.get(this.geneprev.size() - 1)[previous + genes[sex][j]] += 1;
				previous += Archimedes.askInteger(PN.NUMOFALLELES + j);
			}
			previous = 0;
		}
		/** update maxima */
		if (this.stateprev.get(stateprev.size() - 1)[state] > this.maxima[state]) {
			this.maxima[state] = this.stateprev.get(stateprev.size() - 1)[state];
			this.maxtimes[state] = Idunn.getCurrentTime();
		}

		/** add one to the population count */
		this.pop_count[0]++;
	}

	/**
	 * Close clinic for removals, addings or changes
	 */
	private void close() {
		this.closed = true;

	}

	/**
	 * Remove this morbus given that it is registered as type
	 *
	 * @param m
	 * @param type
	 */
	private boolean remove(Morbus m, int type) {

		if (closed)
			return false;

		// remove from types
		boolean b = this.types.get(type).remove(m);

		// remove from prevalence
		int state = m.getState();
		this.stateprev.get(stateprev.size() - 1)[state] -= 1;
		this.typeprev.get(typeprev.size() - 1)[type] -= 1;
		if (this.typeprev.get(typeprev.size() - 1)[type] < 0)
			System.out.print("?");

		if (state > State.SUSCEPTIBLE_STATE && state < State.NEVER_SUSCEPTIBLE_STATE)
			this.infected--;

		// count allele frequencies
		int[][] genes = m.getGenome().getGenotype();
		int previous = 0;
		for (int sex = 0; sex < genes.length; sex++) {
			for (int j = 0; j < genes[sex].length; j++) {
				this.geneprev.get(this.geneprev.size() - 1)[previous + genes[sex][j]] -= 1;
				previous += Archimedes.askInteger(PN.NUMOFALLELES + j);
			}
			previous = 0;
		}

		// remove from population count
		this.pop_count[0]--;

		// remove from diagnosed if treatment is completed
		if (m.isDetected()) {
			if (m.getRFTtime() > Idunn.getCurrentTime()) {
				// subtract from prevalence
				this.diagprev.get(this.diagprev.size() - 1)[m.getDiagnosis()] -= 1;
			}
		}

		// if set to check upon this
		if (this.Monitor_Extinction)
			if (this.infected <= 0) {
				Ratatosk.notify(Message.formulate(Message.EXTINCT), this, this);
				// close clinic for any removals, addings or changes
				this.close();
			}
		// return succes or not
		return b;

	}
}
