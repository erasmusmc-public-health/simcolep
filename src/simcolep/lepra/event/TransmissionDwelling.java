/*
 * This file is part of SIMCOLEP, a simulation model developed by the Department of Public Health, 
 * Erasmus MC, University Medical Center Rotterdam, Rotterdam, The Netherlands.
 *
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/ 
 * or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA
 *
 * You are free to:
 * Share � copy and redistribute the material in any medium or format
 * Under the following terms:
 * Attribution � You must give appropriate credit, provide a link to the license, and indicate if changes were 
 * made. You may do so in any reasonable manner, but not in any way that suggests the licensor endorses
 * you or your use.
 * NonCommercial � You may not use the material for commercial purposes.
 * NoDerivatives � If you remix, transform, or build upon the material, you may not distribute the modified material.
 */

/*
 * Created on 04-Nov-2005
 *
 */
package simcolep.lepra.event;


import nl.mgz.simulation.discreteevent.Targetable;

import simcolep.asgard.Idunn;
import simcolep.demo.Level;
import simcolep.exceptions.SimulationException;
import simcolep.lepra.Morbus;
/**
 * 
 * Transmission within the level. 
 * If both the source and the target have the same level this transmission is executed.
 * 
 * 
 * @author ir E.A.J. Fischer
 * @version 0.1.2 -- Last Modified 28-Nov-2005
 */
public class TransmissionDwelling extends Transmission {

  
    /**
	 * 
	 */
	private static final long serialVersionUID = 2335151392623049542L;
	/**
	 * The higher the priority the more important and sooner an event is executed as compared to events at exactly the same moment.<br>
     * To have a consistancy in prioritizing the following rules are used;
     * <li> Priority from high to low: Simulation events > Disease events > Demographic
     * <li> Priority of simulation events > 500
     * <li> Priority of disease events > 200
     * <li> Priority of demographic events > 100
     * <li> further ordering is logical in biological or epidemiological sense
     *  
     * @see nl.mgz.simulation.discreteevent.Event.priority
     * */
	private static final int PRIORITY = 210;

	
    /**
     * 
     * @param source
     * @param target
     * @param time
     */
    public TransmissionDwelling(Object source, Targetable target, double time) {
        super(source, target, time);

    }

    @Override
    public void execute() {
    	
    	if(Idunn.inSafeMode())
    	{    	//check whether the source is still alive
    	 if(((Morbus)this.getSource()).isDead) 
    		 throw new SimulationException(SimulationException.SHOULDBEDEAD);
    	
    	//in the same level
        if((((Morbus)super.getTarget()).getOwner()).getLevel().equals((((Morbus)super.getSource()).getOwner()).getLevel())== false)
        	throw new SimulationException(SimulationException.NOTSAMEDWELLING);
    	}
        
    	if(Idunn.getIdunn().transmit((Morbus)super.getTarget(),(Morbus)super.getSource(),Level.DWELLING_RANK))
                    Idunn.getIdunn().getClinic().getRegister().registerTransmission(Level.DWELLING_RANK);
               
        //otherwise do nothing
        //remove this event from the source
        ((Morbus)this.getSource()).removeSourcedEvent(this);

    }

    /**
	    * @see nl.mgz.simulation.discreteevent.Event#getPriority()
	    */
	    @Override
	    public int getPriority()
	    {
	    	return PRIORITY;
	    }

	@Override
	public int getTransmissionType() {
		return Transmission.LEVEL;
	}
}
