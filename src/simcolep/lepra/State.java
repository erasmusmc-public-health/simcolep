/*
 * This file is part of SIMCOLEP, a simulation model developed by the Department of Public Health, 
 * Erasmus MC, University Medical Center Rotterdam, Rotterdam, The Netherlands.
 *
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/ 
 * or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA
 *
 * You are free to:
 * Share � copy and redistribute the material in any medium or format
 * Under the following terms:
 * Attribution � You must give appropriate credit, provide a link to the license, and indicate if changes were 
 * made. You may do so in any reasonable manner, but not in any way that suggests the licensor endorses
 * you or your use.
 * NonCommercial � You may not use the material for commercial purposes.
 * NoDerivatives � If you remix, transform, or build upon the material, you may not distribute the modified material.
 */

/**
 * created: 11-Apr-2007
 */
package simcolep.lepra;


import simcolep.exceptions.SimulationException;
import simcolep.fun2.Archimedes;
import simcolep.fun2.Function;
import simcolep.tools.PN;



/**
 * A Disease state is an object containing a few attributes, determining the state (an integer), the detection probability
 * 
 * @author ir. E.A.J. Fischer
 * @version 1.1
 */
public final class State {

	

    /**mininum value of the state of a recovered (or immune)*/
    public static final int RECOVERED_STATE = 7;
    
    /**State describing total unchangeable inherited never susceptibel*/
    public static final int NEVER_SUSCEPTIBLE_STATE = 7;
    
    /**total number of states including susceptible state*/
    public static final int MAX_STATE = 10;

    /*State numbers*/
    /**state for susceptible*/
    public static final int SUSCEPTIBLE_STATE = 0;
    
    /**Latency state*/
    public static final int LATENT_STATE = 1;

    /**Asymptomatic contagious; not clinically apparent*/
	public static final int ASYMPTOMATIC_LOWCONTAGIOUS_STATE = 2;

	/**Symptomatic contagious; clinically apparent*/
	public static final int SYMPTOMATIC_LOWCONTAGIOUS_STATE = 3;

	/**Asymptomatic building up contagiousness; clinically not apparent*/
	public static final int ASYMPTOMATIC_BUILDINGUP_STATE = 4;

	/**Symptomatic building up contagiousness; clinically not apparent*/
	public static final int SYMPTOMATIC_BUILDINGUP_STATE = 5;

	/**Symptomatic contagious; clinically apparent*/
	public static final int SYMPTOMATIC_HIGHCONTAGIOUS_STATE = 6;
	
	/**self heal state*/ 
	public static int SELFHEAL_STATE = SUSCEPTIBLE_STATE;
    
	/**cure state*/
	public static int CURE_STATE = SUSCEPTIBLE_STATE;
	
	
	/**State objects*/
	private static State[] states = null;


	
	/**
	 * Set the following detection functions  to the states
	 * @param f
	 */
	public static void set(Function[][] f)
	{
		//determine the value of self heal and cure state
		State.SELFHEAL_STATE = 
			Archimedes.askInteger(PN.SELFHEAL);
		
		if(State.SELFHEAL_STATE>=MAX_STATE)
			throw new SimulationException("Selfheal state too large");
		
		State.CURE_STATE = 
			Archimedes.askInteger(PN.CURE);
		
		if(State.SELFHEAL_STATE>=MAX_STATE)
			throw new SimulationException("Cure state too large");
		
		//create states for each function
		State.states = 
			new State[f.length]; 
		
		for(int y =0 ; y < f.length; y++)
		{
			State.states[y] = new State(y, f[y]);
			
			if(y > State.LATENT_STATE && y < State.RECOVERED_STATE)
				State.states[y].infectious = true;
			if(y == State.SYMPTOMATIC_LOWCONTAGIOUS_STATE)
				State.states[y].symptomatic = true;
			if(y == State.SYMPTOMATIC_BUILDINGUP_STATE)
				State.states[y].symptomatic = true;
			if(y == State.SYMPTOMATIC_HIGHCONTAGIOUS_STATE)
				State.states[y].symptomatic = true;
				
		}
		
	}
	
	/**
	 * Return a state object of this class
	 * @param i
	 * @return
	 */
	public static State state(int i) {
				
			return states[i];
		
	}
	/**infectiousness of this state*/
	private boolean infectious = false;
	
	/**symptoms */
	private boolean symptomatic = false;
	
	/**state*/
	private int state = -1;
	
	/**detection probability*/
	private Function[] detProb = null;
	
	/**
	 * 
	 * @param state
	 * @param p
	 */
	public State(int state, Function[] p) {
		this.state = state;
		this.detProb = p;
			}
	
	/**
	 * 
	 * @return state
	 */
	public int getState()
	{
		return state;
	}
	
	/**
	 * Determines whether a morbus with this state is 
	 * OR can become infectious in the future
	 * @return infectious
	 */
	public boolean isInfectious()
	{
		return this.infectious;
	}
	/**
	 * Return the detection probability of this state a calendar time t
	 * 
	 * @param t calendar time
	 * @param fu number of follow up
	 */
	public double detProb(double t, int fu)
	{
		return detProb[fu].getY(t);
	}

	/**
	 * @see java.lang.Object#toString()
	 */
	public String toString()
	{
		return Integer.toString(this.state); 
	}

	/**
	 * True of clinically apparent state that causes voluntary reporting 
	 * @return 
	 */
	public boolean isSymptomatic() {
		return this.symptomatic;
	}

	

}
