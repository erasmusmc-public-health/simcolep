/*
 * This file is part of SIMCOLEP, a simulation model developed by the Department of Public Health,
 * Erasmus MC, University Medical Center Rotterdam, Rotterdam, The Netherlands.
 *
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/
 * or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA
 *
 * You are free to:
 * Share � copy and redistribute the material in any medium or format
 * Under the following terms:
 * Attribution � You must give appropriate credit, provide a link to the license, and indicate if changes were
 * made. You may do so in any reasonable manner, but not in any way that suggests the licensor endorses
 * you or your use.
 * NonCommercial � You may not use the material for commercial purposes.
 * NoDerivatives � If you remix, transform, or build upon the material, you may not distribute the modified material.
 */

/*
 * Created on 29-Nov-2005
 *
 */
package simcolep.lepra;

import java.util.LinkedHashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;

import nl.mgz.simulation.discreteevent.Target;
import simcolep.asgard.Idunn;
import simcolep.asgard.Odin;
import simcolep.demo.ILevel;
import simcolep.demo.Inhabitant;
import simcolep.demo.Level;
import simcolep.demo.Person;
import simcolep.demo.Sex;
import simcolep.exceptions.SimulationException;
import simcolep.fun2.Archimedes;
import simcolep.fun2.Function;
import simcolep.lepra.event.intervention.BCGCAMPAIGN;
import simcolep.lepra.event.intervention.Blanket;
import simcolep.lepra.event.intervention.FollowUp;
import simcolep.lepra.event.intervention.Survey;
import simcolep.sim.DiseaseRun;
import simcolep.sim.Herbarium;
import simcolep.sim.Main;
import simcolep.tools.PN;
import simcolep.tools.io.OutputManager;
import simcolep.tools.message.Message;
import simcolep.tools.message.Notifyable;
import simcolep.tools.message.Ratatosk;

/**
 * Contains a register for cases, infections etc. Governs intervention methods
 *
 *
 * @author ir E.A.J. Fischer
 * @version 1.0 -- Last Modified 31-03-2006
 *
 */
public class Clinic extends Target implements Notifyable {

	/** number of possible diagnoses */
	public static final int DIAGNOSIS = 5;
	/** diagnosis of state 3 */
	private static final int MILD_DIAG = 1;
	/** diagnosis of state 5 */
	private static final int INTERMEDIATE_DIAG = 2;
	/** diagnosis of state 6 */
	private static final int SEVERE_DIAG = 3;
	/** diagnosis of asymptomics (by some test) */
	public static final int ASYMP_DIAG = 4;

	/** @see simcolep.tools.message.Notifyable */
	private static final String[] listensTo = { Message.PASDETECTION, // passive
																		// detection
			Message.DEATH, // death
			Message.BLANKET, // blanket treatment
			Message.SURVEY, // survey
			Message.FOLLOWUP, // follow up
			Message.CONTACTTRACING, // contact tracing
			Message.TREATMENTEND, // end of treatment
			Message.BCGCAMPAIGN, // BCG campaign
			Message.MORBUSACTIVATION, // morbus activated
			Message.CHANGESTATE };// morbus changed state
	//

	public static final String[] DETECTIONMETHODS = { "V", "S", "C", "PF", "CF" };

	/** register */
	private ClinicRegister register = null;

	// interventions
	// Level compliance is always the lowest ranking i.e. dwelling
	private double tStart = 0;
	/**
	 * contact tracing on/off
	 */
	private boolean contact = false;
	/** contact tracing compliance at level */
	private double ccLevel = 0.0;
	/** contact tracing compliance individuals */
	private double ccIndividual = 0.0;
	/** start time of contact tracing */
	private double cStart = 0.0;

	/**
	 * contact prophylactics on/off
	 */
	private boolean prophylactics = false;
	/** prophylactics compliance at level */
	private double pcLevel = 0.0;
	/** prophylactics compliance individuals */
	private double pcIndividual = 0.0;
	/** start time of prophylactic treatment */
	private double pStart = 0.0;

	/**
	 * bcg vaccination of contacts on/off
	 */
	private boolean contactbcg = false;
	/** bcg vaccination compliance at level */
	private double vcLevel = 0.0;
	/** bcg vaccination compliance at individual */
	private double vcIndividual = 0.0;
	/** start time of vaccination of contact */
	private double vStart = 0.0;

	/**
	 * surveys on/off
	 */
	private boolean survey = false;
	/** survey compliance at level */
	private double scLevel = 0.0;
	/** survey compliance individuals */
	private double scIndividual = 0.0;
	/** survey frequency */
	private double sFreq = 0.0;
	/** start of survey campaign */
	private double sStart = 0.0;
	/** end of survey campaign */
	private double sEnd = 0.0;

	/**
	 * blanket treatment on/off
	 */
	private boolean blanket = false;
	/** blanket compliance Level */
	private double bcLevel = 0.0;
	/** blanket frequency */
	private double bFreq = 0.0;
	/** start of blanket treatment */
	private double bStart = 0.0;
	/** end of blanket treatment campaign */
	private double bEnd = 0.0;
	/** blanket compliance of individuals */
	private double bcIndividual = 0.0;

	/**
	 * follow up on/off
	 */
	private boolean fu = false;
	/** follow up frequency */
	private double fuFreq = Double.POSITIVE_INFINITY;

	/** compliance during each follow up round */
	private double fcIndividual = 0;

	/** upto which level rank do we consider some one a contact */

	/** contact vaccination rank */
	private int vcRank = 0;
	/** contact prophylacitc treatment */
	private int pcRank = 0;
	/** contact tracing rank */
	private int ccRank = 0;

	/**
	 * effect of revaccination if true same as vaccination, false(default) no
	 * effect
	 */
	public boolean revaccinate = false;

	/** BCG has protective effect */
	public boolean BCGprotect = false;

	/** BCG acts in shifting along the spectrum */
	public boolean BCGshift = false;

	/** campaigning during a certain period */
	private boolean campaignbcg = false;
	private double bcgLevel = 0;
	private double bcgStart = Double.POSITIVE_INFINITY;
	private double bcgEnd = Double.NEGATIVE_INFINITY;
	private double bcgFreq = 0;

	/**
	 *
	 */
	public Clinic() {
		super();

		// create registers
		register = new ExtendedClinicRegister(Main.currentrun(),
				OutputManager.getManager().getDatabaseWriter(DiseaseRun.LEPROSY_CODE));

		// register at Ratatosk
		for (String s : listensTo)
			Ratatosk.register(Message.formulate(s), this);

	}

	/**
	 * Getter for the register of this clinic
	 *
	 * @return clinic register i
	 */
	public ClinicRegister getRegister() {

		return this.register;
	}

	/**
	 *
	 */
	public double getStartTreatment() {
		return this.tStart;

	}

	/**
	 * TreatmentEnd time depends on the diagnosis of the disease
	 *
	 * @param morbus
	 * @return time between detection and end of treatment
	 */
	public double getTreatTime(Morbus morbus) {
		return Archimedes.ask(PN.TREATMENTDURATION).getY(morbus.getDiagnosis());
	}

	/**
	 * @see simcolep.tools.message.Notifyable#notify(simcolep.tools.message.Message,
	 *      java.lang.Object, java.lang.Object)
	 */

	@Override
	@SuppressWarnings("unchecked")
	public void notify(Message message, Object subject, Object source) {

		if (message.equalsIgnoreCase(Message.PASDETECTION)) {
			// double check whether this person is infected
			// if susceptible
			if (((Morbus) subject).getState() == State.SUSCEPTIBLE_STATE)
				return;
			// if recovered
			if (((Morbus) subject).getState() >= State.RECOVERED_STATE)
				return;

			// diagnose and set diagnosis to the morbus object
			this.diagnose((Morbus) subject);

			// detection thus add
			this.register.addDetected((Morbus) subject,
					0, /* no relation with one self */
					Sex.BOTH, /* no sex needed */
					ClinicRegister.PASSIVE_DETECTION);

			Morbus subm = (Morbus) subject;
			// treat the morbus
			subm.treat();

			// schedule follow ups and/or contact tracing
			this.tracing(subm);

			// vaccinate contacts
			this.vaccinatecontact(subm, !contact);

			// prophylactic treatment of contacts
			this.contact_prophylactics(subm, !contact);

			// follow up
			this.followUp(subm, true, ClinicRegister.PATIENTFOLLOWUP, subm.getFollowUp());

		} // DEATH
		else if (message.equalsIgnoreCase(Message.DEATH)) {
			for (int i = 0; i < Archimedes.askInteger(PN.L); i++) {
				// remove when dead
				Morbus m = (((Person) subject).getMorbus(i));
				// remove from register
				this.register.remove(m);

			}

		} // BLANKET
		else if (message.equalsIgnoreCase(Message.BLANKET)) {
			// preform blanket treatment
			this.blanket();
		} // SURVEY
		else if (message.equalsIgnoreCase(Message.SURVEY)) {
			// perform survey
			this.survey();
		} // FOLLOW UP
		else if (message.equalsIgnoreCase(Message.FOLLOWUP)) {
			// determine follow up reason
			int reason = ClinicRegister.CONTACTFOLLOWUP;
			if (((Morbus) subject).isDetected())
				reason = ClinicRegister.PATIENTFOLLOWUP;

			// do follow up of this disease object
			this.followUp((Morbus) subject, false, reason, ((Morbus) subject).getFollowUp());
		} // CONTACT TRACING
		else if (message.equalsIgnoreCase(Message.CONTACTTRACING)) {
			// trace contacts and examine
			this.tracing((Morbus) subject);
		} // END OF TREATMENT => RFT
		else if (message.equalsIgnoreCase(Message.TREATMENTEND))// end of
																// treatment
		{
			this.register.rft((Morbus) subject);

		} else if (message.equalsIgnoreCase(Message.CHANGESTATE))// state change
		{
			// check for additional information
			if (message.getInfo() == null)
				throw new SimulationException("no information on which " + "states the morbus is changing");
			// changes from
			int current_state = ((Integer) message.getInfo()[0]).intValue();
			// to
			int new_state = ((Integer) message.getInfo()[1]).intValue();

			// report a change of the state
			this.register.recordStateChange(current_state, new_state);
		} else if (message.equalsIgnoreCase(Message.BCGCAMPAIGN)) {
			this.bcgcampaign();
		} else
			throw new SimulationException(Ratatosk.UNKNOWNMESSAGE);
	}

	/**
	 * Sets the interventions
	 *
	 * @param contact
	 *            tracing
	 * @param prophylactics
	 * @param survey
	 * @param blanket
	 * @param fu
	 *            follow up
	 * @param BCGprotect
	 *            effect of vaccination = protection
	 * @param BCGshift
	 *            effect of vaccination = shift in leprosy spectrum
	 * @param contactbcg
	 */
	public void setInterventions(boolean contact, boolean prophylactics, boolean survey, boolean blanket, boolean fu,
			boolean revac, boolean BCGprotect, boolean BCGshift, boolean contactbcg, boolean campaignbcg) {
		// set start of normal treatment
		this.tStart = Archimedes.askDouble(PN.TSTART);

		// set contact tracing
		this.contact = contact;
		if (contact) {
			this.ccLevel = Archimedes.askDouble(PN.CCLEVEL);
			this.ccIndividual = Archimedes.askDouble(PN.CCINDIVIDUAL);
			this.cStart = Archimedes.askDouble(PN.CSTART);
			this.ccRank = Archimedes.askInteger(PN.CCRANK);
		}

		// set prophylactic treatment of contacts
		this.prophylactics = prophylactics;
		if (this.prophylactics) {
			this.pcLevel = Archimedes.askDouble(PN.PCLEVEL);
			this.pcIndividual = Archimedes.askDouble(PN.PCINDIVIDUAL);
			this.pStart = Archimedes.askDouble(PN.PSTART);
			this.pcRank = Archimedes.askInteger(PN.PCRANK);
		}

		// set survey
		this.survey = survey;
		if (this.survey) {
			this.scLevel = Archimedes.askDouble(PN.SCLEVEL);
			this.scIndividual = Archimedes.askDouble(PN.SCINDIVIDUAL);
			this.sFreq = Archimedes.askDouble(PN.SFREQ);
			this.sStart = Archimedes.askDouble(PN.SSTART);
			this.sEnd = Archimedes.askDouble(PN.SEND);
			// schedule first survey
			Survey surveyEvent = new Survey(this, this, this.sStart);
			Idunn.getEventManager().schedule(surveyEvent);
		}

		// set blanket
		this.blanket = blanket;
		if (this.blanket) {
			this.bcLevel = Archimedes.askDouble(PN.BCLEVEL);
			this.bcIndividual = Archimedes.askDouble(PN.BCINDIVIDUAL);
			this.bFreq = Archimedes.askDouble(PN.BFREQ);
			this.bStart = Archimedes.askDouble(PN.BSTART);
			this.bEnd = Archimedes.askDouble(PN.BEND);
			// schedule first blanket treatment
			Blanket blanketEvent = new Blanket(this, this, this.bStart);
			Idunn.getEventManager().schedule(blanketEvent);
		}

		// set follow up of contacts
		this.fu = fu;
		if (this.fu) {
			this.fuFreq = Archimedes.askDouble(PN.FUFREQ);

			this.fcIndividual = Archimedes.askDouble(PN.FCINDIVIDUAL);
		}

		// bcg vaccination of contact
		this.contactbcg = contactbcg;
		if (this.contactbcg) {
			this.vcLevel = Archimedes.askDouble(PN.VCLEVEL);
			this.vcIndividual = Archimedes.askDouble(PN.VCINDIVIDUAL);
			this.vStart = Archimedes.askDouble(PN.VSTART);
			this.vcRank = Archimedes.askInteger(PN.VCRANK);
		}

		// bcg vaccination campaign
		this.campaignbcg = campaignbcg;
		if (this.campaignbcg) {
			this.bcgLevel = Archimedes.askDouble(PN.BCGCAMPLEVEL);

			this.bcgStart = Archimedes.askDouble(PN.STARTBCGCAMPAIGN);
			this.bcgEnd = Archimedes.askDouble(PN.ENDBCGCAMPAIGN);
			this.bcgFreq = Archimedes.askDouble(PN.BCGCAMPFREQ);

			if (this.bcgFreq <= 0)
				throw new SimulationException("BCG campaigning frequency will cause trouble = " + this.bcgFreq);
			// schedule BCG campaigning events starting at startbcg campaign
			// with frequency until end.
			double time = this.bcgStart;
			while (time <= this.bcgEnd) {
				BCGCAMPAIGN event = new BCGCAMPAIGN(this, this, time);
				Idunn.getEventManager().schedule(event);

				time += this.bcgFreq;
			}

		}

		// effect of BCG vaccination
		this.revaccinate = revac;
		this.BCGprotect = BCGprotect;
		this.BCGshift = BCGshift;

	}

	/**
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		String s = this.register.toString();

		return s;
	}

	/**
	 *
	 * @param person
	 */
	@SuppressWarnings("unchecked")
	public void vaccination_at_birth(Person person) {
		// obtain random number generator
		Random rand = Herbarium.rand(Herbarium.DIS);
		// administer vaccination of new borns
		for (int i = 0; i < Idunn.LAYERS; i++) {
			if (rand.nextDouble() < Archimedes.ask(PN.BCGCOVERAGE).getY(Idunn.getCurrentTime())) {
				// vaccinate baby
				person.getMorbus(i).vaccinate(person.getBdate());
				// if type of campaign is vaccination of housemates (setting is
				// FALSE)of newly born (value =1)
				if (this.campaignbcg == false) {
					// during BCG campaign vaccinate other household members if
					// not vaccinated already
					double time = Idunn.getCurrentTime();
					if (time > Archimedes.askDouble(PN.STARTBCGCAMPAIGN)
							&& time <= Archimedes.askDouble(PN.ENDBCGCAMPAIGN)) {
						Function BCGcampaign = Archimedes.ask(PN.BCGCAMPAIGN);
						// iterate household members
						for (Inhabitant p : person.getLevel().getInhabitants()) {
							if (p != person) {
								Person per = (Person) p;
								if (!per.getMorbus(i).isVaccinated()) {
									if (rand.nextDouble() < BCGcampaign.getY(per.getAge(time))) {
										per.getMorbus(i).vaccinate(time);
									}
								}
							}
						}

					}
				}
			}
		}

	}

	/**
	 * Give BCG vaccination Compliance is measured on individual and level -
	 * level
	 *
	 */
	@SuppressWarnings("unchecked")
	private void bcgcampaign() {
		if (!this.campaignbcg)
			return;
		double time = Idunn.getCurrentTime();
		// if reach the end of the campaign return
		if (time > this.bcgEnd)
			return;

		// obtain random number generator
		Random rand = Herbarium.rand(Herbarium.DIS);

		// coverage by age
		Function age = Archimedes.ask(PN.BCGCAMPAIGN);

		// else execute blanket
		List<Set<Level>> list = Odin.getOdin().getLevelRegister().getLevels();

		for (Set<Level> l : list) {
			// iterate all levels
			for (Level level : l) {
				// iterate over the layers
				for (int index = 0; index < Archimedes.askInteger(PN.L); index++) {
					// check compliance/coverage for level
					if (rand.nextDouble() < this.bcgLevel) {
						for (Inhabitant p : level.getInhabitants()) {
							// check compliance/coverage for individual
							if (rand.nextDouble() < age.getY(((Person) p).getAge(time))) {
								// compliance of individual
								Morbus m = ((Person) p).getMorbus(index);

								m.vaccinate(time);

							}
							// iterating inhabitant
						}
						// level complied at this layer
					}
					// iterating layers
				}
				// iterating level
			}
			// iterating over lists with level size
		}
		return;
	}

	/**
	 * Administer blanket treatment (treat all people in the population)
	 * Compliance is measured on individual and level - level
	 *
	 */
	@SuppressWarnings("unchecked")
	private void blanket() {
		if (!blanket)
			return;
		// if reach the end of the campaign return
		if (Idunn.getCurrentTime() > this.bEnd)
			return;
		// if campaign has not yet started schedule the start
		double next = Idunn.getCurrentTime() + this.bFreq;

		if (next < this.bStart)
			next = this.bStart;

		Blanket b = new Blanket(this, this, next);
		Idunn.getEventManager().schedule(b);

		// obtain random number generator
		Random rand = Herbarium.rand(Herbarium.DIS);

		// else execute blanket
		List<Set<Level>> list = Odin.getOdin().getLevelRegister().getLevels();

		for (Set<Level> l : list) {
			// iterate all levels
			for (Level level : l) {
				// iterate over the layers
				for (int index = 0; index < Archimedes.askInteger(PN.L); index++) {
					// check compliance/coverage for level
					if (rand.nextDouble() < this.bcLevel) {
						for (Inhabitant p : level.getInhabitants()) {
							// check compliance/coverage for individual
							if (rand.nextDouble() < this.bcIndividual) {
								// compliance of individual
								Morbus m = ((Person) p).getMorbus(index);
								// chemo
								m.chemoprophylatics();
								this.register.addChemo(m);
							}
							// iterating inhabitant
						}
						// level complied at this layer
					}
					// iterating layers
				}
				// iterating level
			}
			// iterating over lists with level size
		}
		return;
	}

	/**
	 * Administer prophylactic-intervention to contacts
	 *
	 * @param morbus
	 * @param examine
	 */
	@SuppressWarnings("unchecked")
	private void contact_prophylactics(Morbus morbus, boolean examine) {
		// TODO remove the interwoveness of administering chemo and examination
		// no prophylactic treatment
		if (!prophylactics)
			return;

		// if the intervention has not yet started
		if (Idunn.getCurrentTime() < this.pStart)
			return;

		// obtain random number generator
		Random rand = Herbarium.rand(Herbarium.DIS);

		// index patient
		Person index = morbus.getOwner();
		// get set of levels that are considered contacts
		ILevel inf_level = index.getLevel();

		while (inf_level.getRank() < pcRank)
			inf_level = inf_level.getSuperlevel();

		// get the set of the lowest ranking levels
		Set<ILevel> bottom = null;

		if (inf_level.getRank() == Level.DWELLING_RANK) {
			bottom = new LinkedHashSet<ILevel>();
			bottom.add(inf_level);
		} else
			bottom = inf_level.getMembers(Level.DWELLING_RANK);

		// iterate over the sub-levels
		for (ILevel l : bottom) {
			// determine compliance
			if (rand.nextDouble() < this.pcLevel) {

				Set<Inhabitant> s = l.getInhabitants();

				for (Inhabitant p : s) {
					int relation = index.getRelationship((Person) p);
					if (rand.nextDouble() < this.pcIndividual) {
						Morbus m2 = ((Person) p).getMorbus(morbus.getLayer());
						// examination will lead to treating etc.
						boolean administer = true;
						if (examine) {
							administer = !this.examine(m2, Clinic.DETECTIONMETHODS[ClinicRegister.CONTACT_DETECTION],
									0);
							// register examination
							this.register.addExamined(m2, relation, index.getSex(), ClinicRegister.CONTACT_DETECTION);

						}
						if (administer) {
							m2.chemoprophylatics();
							this.register.addChemo(m2);
						} else {
							this.register.addDetected(m2, relation, index.getSex(), ClinicRegister.CONTACT_DETECTION);
						}

					}
				}

			}
		}
	}

	/**
	 * Set the diagnosis
	 * <li>1 = SYMPTOMATIC_LOWCONTAGIOUS_STATE
	 * <li>2 = SYMPTOMATIC_BUILDINGUP_STATE
	 * <li>3 = SYMPTOMATIC_HIGHCONTAGIOUS_STATE
	 * <li>4 = ASYMPTOMATIC (BUILDINGUP or LOWCONTAGIOUS)
	 * <li>HEALTHY otherwise
	 *
	 * @param m
	 */
	private void diagnose(Morbus m) {
		if (m.getState() == State.SYMPTOMATIC_LOWCONTAGIOUS_STATE)
			m.setDiagnosis(Idunn.getCurrentTime(), MILD_DIAG);
		else if (m.getState() == State.SYMPTOMATIC_BUILDINGUP_STATE)
			m.setDiagnosis(Idunn.getCurrentTime(), INTERMEDIATE_DIAG);
		else if (m.getState() == State.SYMPTOMATIC_HIGHCONTAGIOUS_STATE)
			m.setDiagnosis(Idunn.getCurrentTime(), SEVERE_DIAG);
		else if (m.getState() == State.ASYMPTOMATIC_BUILDINGUP_STATE)
			m.setDiagnosis(Idunn.getCurrentTime(), ASYMP_DIAG);
		else if (m.getState() == State.ASYMPTOMATIC_LOWCONTAGIOUS_STATE)
			m.setDiagnosis(Idunn.getCurrentTime(), ASYMP_DIAG);
		else if (m.getState() == State.LATENT_STATE)
			m.setDiagnosis(Idunn.getCurrentTime(), ASYMP_DIAG);
		else
			m.setDiagnosis(Idunn.getCurrentTime(), Morbus.HEALTHY);

	}

	/**
	 * Examine this morbus if the morbus is not already detected and infected
	 * return true
	 *
	 * @param m
	 * @return if lepra is found
	 */
	private boolean examine(Morbus m, String method, int fu) {

		// obtain random number generator
		Random rand = Herbarium.rand(Herbarium.DIS);

		boolean b = (rand.nextDouble() < m.getDetProb(Idunn.getCurrentTime(), fu));

		if (m.isDetected())
			return false;
		if (b) { // diagnose
			this.diagnose(m);
			// set active detection
			m.activeDetection(method);
		}
		return b;

	}

	/**
	 * Follow this morbus up
	 *
	 * @param m
	 * @param levelInhabs
	 *            - follow up level inhabitants
	 * @param followup_reason
	 */
	@SuppressWarnings("unchecked")
	private void followUp(Morbus m, boolean levelInhabs, int followup_reason, int followup_number) {
		if (!fu)
			return;

		if (levelInhabs) {
			Level l = m.getOwner().getLevel();

			for (Inhabitant p : l.getInhabitants()) {

				Morbus m2 = ((Person) p).getMorbus(m.getLayer());

				// determine whether or not to follow this person up
				boolean fu = m2.getFollowUp() < Idunn.MAXFU;

				if (m2.isDetected())
					fu = false;

				if (fu) {
					FollowUp fuEvent = new FollowUp(this, m2, Idunn.getCurrentTime() + this.fuFreq);

					Idunn.getEventManager().schedule(fuEvent);

					m2.setFollowUp(1);
				}
			}
		} else {
			// individual loss to follow up
			if (Herbarium.rand(Herbarium.DIS).nextDouble() < this.fcIndividual) {

				if (this.examine(m, Clinic.DETECTIONMETHODS[followup_reason], followup_number)) {
					this.register.addDetected(m, 0, Sex.MALE, followup_reason);
					// treat
					m.treat();
					// if detected and follow up
					return;
				}
				this.register.addExamined(m, 0, Sex.MALE, followup_reason);
			}

			if (m.getFollowUp() < Idunn.MAXFU) {
				FollowUp fu = new FollowUp(this, m, Idunn.getCurrentTime() + this.fuFreq);

				Idunn.getEventManager().schedule(fu);
				m.setFollowUp(m.getFollowUp() + 1);

			} else
				m.setFollowUp(0);
		}

	}

	/**
	 * execute survey and schedule next
	 *
	 */
	@SuppressWarnings("unchecked")
	private void survey() {

		if (!survey)
			return;
		// if reach the end of the campaign return
		if (Idunn.getCurrentTime() > this.sEnd)
			return;
		// if campaign has not yet started schedule the start
		double next = Idunn.getCurrentTime() + this.sFreq;

		if (next < this.sStart)
			next = this.sStart;

		Survey s = new Survey(this, this, next);
		Idunn.getEventManager().schedule(s);

		// obtain random number generator
		Random rand = Herbarium.rand(Herbarium.DIS);

		// else execute blanket and schedule next
		List<LinkedHashSet<Level>> list = Odin.getOdin().getLevelRegister().getLevels();

		for (LinkedHashSet<Level> l : list) {
			// iterating list with levels of certain size
			for (Level level : l) {
				// iterate over sets of levels
				for (int index = 0; index < Archimedes.askInteger(PN.L); index++) {
					// iterate over layers
					if (rand.nextDouble() < this.scLevel) {
						// compliance/coverage per level
						if (rand.nextDouble() < this.scIndividual) {
							// compliance/coverage per individual
							for (Inhabitant p : level.getInhabitants()) {
								Morbus m = ((Person) p).getMorbus(index);

								if (this.examine(m, Clinic.DETECTIONMETHODS[ClinicRegister.SURVEY_DETECTION], 0)) {
									this.register.addDetected(m, 0, Sex.MALE, ClinicRegister.SURVEY_DETECTION);
									m.treat();
								}
								this.register.addExamined(m, 0, Sex.MALE, ClinicRegister.SURVEY_DETECTION);
							}
							// compliance/coverage per individual
						}
						// compliance/coverage per level
					}
					// iterate over layers
				}
				// iterate over sets of levels
			}
			// iterating list with levels of certain size
		}
	}

	/**
	 * Examine all people in the level
	 *
	 * @param m
	 */
	@SuppressWarnings("unchecked")
	private void tracing(Morbus m) {
		if (!contact)
			return;
		// if the time is smaller
		if (Idunn.getCurrentTime() < this.cStart)
			return;

		// get set of levels that are considered contacts
		ILevel inf_level = m.getOwner().getLevel();
		while (inf_level.getRank() < ccRank)
			inf_level = inf_level.getSuperlevel();

		// get the set of the lowest ranking levels
		Set<ILevel> bottom = null;
		if (inf_level.getRank() == Level.DWELLING_RANK) {
			bottom = new LinkedHashSet<ILevel>();
			bottom.add(inf_level);
		} else
			bottom = inf_level.getMembers(0);

		// obtain random number generator
		Random rand = Herbarium.rand(Herbarium.DIS);

		// index patient
		Person index = m.getOwner();

		int contacts = 0;
		int detected = 0;
		// iterate over sub-levels
		for (ILevel l : bottom) {
			// trace the contacts of this m if complies
			if (rand.nextDouble() < this.ccLevel) {

				Set<Inhabitant> s = l.getInhabitants();

				for (Inhabitant p : s) {
					int relation = index.getRelationship((Person) p);
					// determine compliance of each individual
					if (rand.nextDouble() < this.ccIndividual) {
						Morbus m2 = ((Person) p).getMorbus(m.getLayer());
						// add one to the level size
						contacts++;
						// found during examination will lead to treatment etc.
						if (this.examine(m2, Clinic.DETECTIONMETHODS[ClinicRegister.CONTACT_DETECTION], 0)) {
							this.register.addDetected(m2, relation, index.getSex(), ClinicRegister.CONTACT_DETECTION);
							m2.treat();
							detected++;
						}
						// register examination
						if (!m2.equals(m))
							this.register.addExamined(m2, relation, index.getSex(), ClinicRegister.CONTACT_DETECTION);

						if (this.fu) {// if follow up and tracing are both on
										// tracing will cause follow up of
										// contacts
							FollowUp fu = new FollowUp(this, m2, Idunn.getCurrentTime() + this.fuFreq);

							Idunn.getEventManager().schedule(fu);
							m2.setFollowUp(1);

						}
					}
				}
			}
		}
		// register the number of contact traced and the number detected as a
		// group
		this.register.addContactTracingResult(contacts, detected);

	}

	/**
	 * Vaccinate the contact (like tracing)of this disease object
	 *
	 * @param m
	 * @param examine
	 */
	@SuppressWarnings("unchecked")
	private void vaccinatecontact(Morbus m, boolean examine) {
		// TODO remove interwovenness of examination and vaccination
		if (!contactbcg)
			return;
		// vaccination intervention is not yet started
		if (Idunn.getCurrentTime() < this.vStart)
			return;
		// obtain random number generator
		Random rand = Herbarium.rand(Herbarium.DIS);

		// get set of levels that are considered contacts
		ILevel inf_level = m.getOwner().getLevel();

		while (inf_level.getRank() < vcRank)
			inf_level = inf_level.getSuperlevel();

		// get the set of the one but lowest ranking levels
		Set<ILevel> bottom = null;
		if (inf_level.getRank() == Level.DWELLING_RANK) {
			bottom = new LinkedHashSet<ILevel>();
			bottom.add(inf_level);
		} else
			bottom = inf_level.getMembers(0);

		for (ILevel l : bottom) {
			// get level compliance
			if (rand.nextDouble() < this.vcLevel) {
				// else get compliance of each household member separately
				for (Inhabitant p : l.getInhabitants()) {
					if (rand.nextDouble() < this.vcIndividual) {

						Morbus m2 = ((Person) p).getMorbus(m.getLayer());
						// examination will lead to treating etc.
						boolean administer = true;
						if (examine) {
							administer = !this.examine(m2, Clinic.DETECTIONMETHODS[ClinicRegister.CONTACT_DETECTION],
									0);
							int relation = m.getOwner().getRelationship(m2.getOwner());

							// register examination
							this.register.addExamined(m2, relation, m.getOwner().getSex(),
									ClinicRegister.CONTACT_DETECTION);

						}
						if (administer) {
							m2.vaccinate(Idunn.getCurrentTime());// vaccinate
						} else {
							int relation = m.getOwner().getRelationship(m2.getOwner());

							this.register.addDetected(m2, relation, m.getOwner().getSex(),
									ClinicRegister.CONTACT_DETECTION);
						}
					}
				}
			}
		}
	}

}
