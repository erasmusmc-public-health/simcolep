/*
 * This file is part of SIMCOLEP, a simulation model developed by the Department of Public Health, 
 * Erasmus MC, University Medical Center Rotterdam, Rotterdam, The Netherlands.
 *
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/ 
 * or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA
 *
 * You are free to:
 * Share � copy and redistribute the material in any medium or format
 * Under the following terms:
 * Attribution � You must give appropriate credit, provide a link to the license, and indicate if changes were 
 * made. You may do so in any reasonable manner, but not in any way that suggests the licensor endorses
 * you or your use.
 * NonCommercial � You may not use the material for commercial purposes.
 * NoDerivatives � If you remix, transform, or build upon the material, you may not distribute the modified material.
 */

/**
 * created: 26-Apr-2007
 */
package simcolep.lepra.function;

import java.util.Random;

import simcolep.exceptions.SimulationException;
import simcolep.fun2.CinlarsNonStatPoisson;
import simcolep.lepra.Morbus;
import simcolep.tools.PN;

/**
 * Kernel for MB with steps towards the high infectiousness
 * @author ir. E.A.J. Fischer
 * @version 1.1
 */
public final class MBstepKernel 
			extends AbstractKernel {

	/** */
	private static final long serialVersionUID = -2583504249612071448L;

	/**beta when symptomatic*/
	protected double beta = 0.0;
	

	
	/**contact rate*/
	private double contactrate = 0.0;
	
	/**duration of the contact period*/
	private double incubation_period = 0.0;
	
	/**duration of the latency period*/
	private double latency_period = 0.0;

	/**true when periods are set*/
	private boolean set = false;

	/**length of each step*/
	protected double step_length = 0;

	/**number of steps*/
	protected double steps = 0;
	
	
	/**
	 * 
	 */
	public MBstepKernel() {
		super();
			}

	/**
	 * @see simcolep.fun2.Function#getY(double)
	 */
	@Override
	public double getY(double t) {
		if(t<this.latency_period)
			return 0.0;
		else if (t<= this.latency_period+this.incubation_period)
		{
			//detemine which step we are in 
			double step = 1 +
						((t-this.latency_period)
							-(t-this.latency_period)%this.step_length)/this.step_length;
			//return value
			return this.contactrate*this.beta*(step/this.steps);
		}
		else 
			return this.beta*this.contactrate;
	}

	/**
	 * @see simcolep.fun2.Function#getIntegrated(double)
	 */
	@Override
	public double getIntegrated(double t) {
		if(t<this.latency_period)
			return 0.0;
		else{
			//the 'slope'
			double s = 
				(this.beta*this.contactrate)/this.steps;
			
			//number of steps completed
			//detemine which steps are completed 
			double comp_steps =	((t-this.latency_period)
											-(t-this.latency_period)%this.step_length)/this.step_length;
			
			//integral of completed steps
			double int_comp_steps = 
				s*this.step_length*(((comp_steps/2)*(comp_steps+1)));
			//add remaining 
			if (comp_steps < this.steps)
			{
				//current step
				 double cur_step =
					 comp_steps +1;
				
				//last part
				double remaining_time =
					(t-this.latency_period)%this.step_length;
					
				double int_cur_step =
					(this.contactrate*this.beta*(cur_step/this.steps))*(remaining_time);
				
				//return value
				return int_comp_steps +int_cur_step;
			}//add more
			else 
			{
					return int_comp_steps + 
								this.beta*this.contactrate*((t-(this.latency_period+this.incubation_period)));
			}
		}
			
	}

	/**
	 * @see simcolep.fun2.Function#getInvIntegrated(double)
	 */
	@Override
	public double getInvIntegrated(double y) {
		if(y <= 0)
			return this.latency_period;
		else if(y>=this.getIntegrated(this.latency_period+this.incubation_period))
		{
			double after_inc = 
				y - this.getIntegrated(this.latency_period+this.incubation_period);
			
			return this.latency_period+this.incubation_period+after_inc/(this.contactrate*this.beta);
			
		}else{
			/*get the step in which y is a value of F(x)
			*F(x) increase with each step the value of the step 
			*in y-direction times the triangular number
			*of the step number (step*(step+1)*0.5)
			*/
			double s = 
				this.beta*this.contactrate/this.steps;
			
			double triangular_number =
				((y-(y%(s*this.step_length)))/(s*this.step_length));
			if(true)
				throw new SimulationException(
						"not yet properly implemented: " +
						"Wrong triangular number!");
			//determine step
			int cur_step = 1 +
				(int)(-1+Math.sqrt(1+8*triangular_number))/2;
			
			/*
			 * Return the x-value corresponding to the y-value
			 */
			double y_ac = 
				y -(s*this.step_length)*triangular_number;
			
			double x = this.latency_period + 
				(cur_step-1)*this.step_length + y_ac/(cur_step*s);
			
			return x;
		}
	}

	/**
	 * @see simcolep.lepra.function.Kernel#next(double, double, java.util.Random)
	 */
	public double next(double t, double c, Random rand) {
		if(!set)
			throw new SimulationException("Kernel is not set");
		
		//set the contact rate
		this.contactrate = c;
		
		  //next time depends on the rates but also on current time
        //with infinite contact rates this can go wrong because all events should have been 
        //done but in the case of movement this can cause a problem
        double next = t;
       
        if(!Double.isInfinite(this.contactrate))
        	next =
        		CinlarsNonStatPoisson.next(t, rand.nextDouble(), this);  
        else if(beta == 0)
        	next = 
        		Double.POSITIVE_INFINITY;
       
        
        //check whether this time is in the future
        if(next < t)
            	throw new SimulationException(
        			"next infection in the past!");
    
        if(Double.isNaN(next))
        	throw new SimulationException("next is NaN");
        
         //reset contact rate to 1
        this.contactrate =1.0;
        
        //return the next infection time
        return next;
	}

	/**
	 * @see simcolep.lepra.function.Kernel#set(simcolep.lepra.Morbus)
	 */
	public void set(Morbus m) {
		//latency period
		this.latency_period = 
			m.getLatencyPeriod();
		// incubation period
		this.incubation_period = 
			m.getIncMBPeriod();
		//calculate step length
		this.step_length =
			(this.incubation_period-this.latency_period)/this.steps;
		//if steps == 0 
		if(this.steps == 0)
			throw new SimulationException("MBstepKernel: steps = 0");
		
		//set
		this.set = true;

	}

	@Override
	public void changeParameter(String name, double value) {
        if(name.equalsIgnoreCase("steps"))
        {
            this.steps = value;
        }else if(name.equalsIgnoreCase("beta"))
        {
            this.beta = value;
        }
	}
	
	/**
	 * 
	 */
	public Kernel copy()
	{
		MBstepKernel copy = new MBstepKernel();
		copy.beta = this.beta;
		copy.steps = this.steps;
		return copy;
	}
	
	/**
	 * @see java.lang.Object#toString()
	 */
	public String toString()
	{
		String s = "MBKernel:"+PN.TAB+
		"beta:"+PN.TAB+this.beta +PN.TAB+
		"steps:"+PN.TAB+this.steps +PN.TAB;
		
		return s; 
	}
	
	
}
