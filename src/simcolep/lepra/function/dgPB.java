/*
 * This file is part of SIMCOLEP, a simulation model developed by the Department of Public Health, 
 * Erasmus MC, University Medical Center Rotterdam, Rotterdam, The Netherlands.
 *
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/ 
 * or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA
 *
 * You are free to:
 * Share � copy and redistribute the material in any medium or format
 * Under the following terms:
 * Attribution � You must give appropriate credit, provide a link to the license, and indicate if changes were 
 * made. You may do so in any reasonable manner, but not in any way that suggests the licensor endorses
 * you or your use.
 * NonCommercial � You may not use the material for commercial purposes.
 * NoDerivatives � If you remix, transform, or build upon the material, you may not distribute the modified material.
 */

package simcolep.lepra.function;

import java.util.Map;
import java.util.Random;

import simcolep.asgard.Idunn;
import simcolep.fun2.Distributionable;
import simcolep.fun2.Function;

import simcolep.lepra.Morbus;
import simcolep.lepra.State;


/**
 * Function for down grading PB
 * @author ir. E.A.J.Fischer
 * @version 0.2 -- Last Modified: 23/11/05
 *
 */
public final class dgPB 
		extends AbstractDiseaseHistoryFunction {
	
	
	


	/**
	 * 
	 */
	private static final long serialVersionUID = -7552983435372794244L;
	
	/**states of type 3: downgrading PB*/
	private static int[] TYPE3_STATES = 
							{State.SUSCEPTIBLE_STATE,
				State.LATENT_STATE,
				State.ASYMPTOMATIC_BUILDINGUP_STATE,
				State.SYMPTOMATIC_BUILDINGUP_STATE,
				State.SYMPTOMATIC_HIGHCONTAGIOUS_STATE};
	
	/**
	 * 
	 * @param funcs
	 */
	public dgPB(Map<String, Function> funcs) {
		super();
		this.set(funcs);
		
	}
	/**
	 * 
	 *
	 */
	public dgPB(){
		super();}


	/**
	 * @see simcolep.lepra.function.AbstractDiseaseHistoryFunction#getLatency(Random, double)
	 */
	@Override
	public double getLatency(Random rand, double age) {
		
		return this.lat.drawX(new double[]{rand.nextDouble(),age});
	}

	/**
	 * @see simcolep.lepra.function.AbstractDiseaseHistoryFunction#getIncubationPB(Random, double)
	 */
	@Override
	public double getIncubationPB(Random rand, double age) {
		
		return this.incpb.drawX(new double[]{rand.nextDouble(),age});
	}

	/**
	 * 
	 * @see simcolep.lepra.function.AbstractDiseaseHistoryFunction#getIncubationMB(Random, double)
	 */
	public double getIncubationMB(Random rand, double age) {
		
		return this.incmb.drawX(new double[]{rand.nextDouble(),age});
	}

	/**
	 * 
	 * @see simcolep.lepra.function.DiseaseHistory#getSelfHealing(Random, double)
	 */
	public double getSelfHealing(Random rand, double age) {
		
		return Double.POSITIVE_INFINITY;
	}

	/**
	 * 
	 * @see simcolep.lepra.function.AbstractDiseaseHistoryFunction#getPasDetection(simcolep.lepra.Morbus, Random)
	 */
	public double getPasDetection(Morbus m, Random rand) {
    
		return this.det.drawX(new double[]{rand.nextDouble(),m.getOwner().getAge(Idunn.getCurrentTime())});
	}
	
	/**
	 * 
	 */
	public AbstractDiseaseHistoryFunction set(Map<String, Function> funcs) {
		this.lat  = 
			(Distributionable)funcs.get(LAT);
		this.incpb = 
			(Distributionable)funcs.get(INCPB);
		this.incmb =
			(Distributionable)funcs.get(INCMB);
		this.det =
			(Distributionable)funcs.get(DET);
		this.kernel =
			(Kernel) funcs.get(KERNEL);
		return this;
	}
	@Override
	public int[] getStates() {
			return TYPE3_STATES;
	}

   

   

}
