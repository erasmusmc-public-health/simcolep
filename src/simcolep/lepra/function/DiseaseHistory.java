/*
 * This file is part of SIMCOLEP, a simulation model developed by the Department of Public Health,
 * Erasmus MC, University Medical Center Rotterdam, Rotterdam, The Netherlands.
 *
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/
 * or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA
 *
 * You are free to:
 * Share � copy and redistribute the material in any medium or format
 * Under the following terms:
 * Attribution � You must give appropriate credit, provide a link to the license, and indicate if changes were
 * made. You may do so in any reasonable manner, but not in any way that suggests the licensor endorses
 * you or your use.
 * NonCommercial � You may not use the material for commercial purposes.
 * NoDerivatives � If you remix, transform, or build upon the material, you may not distribute the modified material.
 */

/*
 * Created on 04-Nov-2005
 *
 */
package simcolep.lepra.function;

import java.util.Random;

import simcolep.lepra.Morbus;

/**
 *
 * @author ir. E.A.J. Fischer
 * @version 1.0
 */
public interface DiseaseHistory {

	/**
	 * Return duration of incubation to MB period
	 * 
	 * @param rand
	 * @param age
	 * @return length of incubation to MB
	 */
	public abstract double getIncubationMB(Random rand, double age);

	/**
	 * Return duration of incubation to PB period
	 * 
	 * @param rand
	 * @param age
	 * @return length of incubation until PB
	 */
	public abstract double getIncubationPB(Random rand, double age);

	/**
	 * Infectivity kernel of this disease history
	 * 
	 * @return kernel
	 */
	public abstract Kernel getKernel();

	/**
	 * Return duration of latency period
	 * 
	 * @param rand
	 * @param age
	 * @return length of the latency period
	 */
	public abstract double getLatency(Random rand, double age);

	/**
	 * Return duration to passive detection
	 * 
	 * @param rand
	 * @return detection delay or time until passive detection
	 */
	public abstract double getPasDetection(Morbus m, Random rand);

	/**
	 * Return duration to self healing
	 * 
	 * @param rand
	 * @param age
	 * @return length of self healing
	 */
	public abstract double getSelfHealing(Random rand, double age);

	/**
	 * An array containing the states of a disease history
	 * 
	 * @return
	 */
	public abstract int[] getStates();

}