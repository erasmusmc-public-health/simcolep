/*
 * This file is part of SIMCOLEP, a simulation model developed by the Department of Public Health, 
 * Erasmus MC, University Medical Center Rotterdam, Rotterdam, The Netherlands.
 *
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/ 
 * or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA
 *
 * You are free to:
 * Share � copy and redistribute the material in any medium or format
 * Under the following terms:
 * Attribution � You must give appropriate credit, provide a link to the license, and indicate if changes were 
 * made. You may do so in any reasonable manner, but not in any way that suggests the licensor endorses
 * you or your use.
 * NonCommercial � You may not use the material for commercial purposes.
 * NoDerivatives � If you remix, transform, or build upon the material, you may not distribute the modified material.
 */
package simcolep.lepra.function;

import java.util.Map;
import java.util.Random;

import simcolep.exceptions.SimulationException;
import simcolep.fun2.Distributionable;
import simcolep.fun2.Function;
import simcolep.lepra.Morbus;
import simcolep.tools.PN;

/**
 * Disease history functions determine the course of disease by the duration of states
 * 
 * @author ir. E.A.J. Fischer
 * @version 1.0
 */
public abstract class AbstractDiseaseHistoryFunction 
		extends Function 
			implements DiseaseHistory {
	

	/***/
    public static final long serialVersionUID = -5403748959290024892L;
	
    /**String representation of kernel*/
    public static final String KERNEL = "kernel"; 
    /**
	 * Name of self healing distribution
	 */
	public static final String SH = "sh";
	/**
	 * Name of latency distribution 
	 */
	public static final String LAT = "lat";

	/**name of function incubation to pb*/
	public static final String INCPB = "incpb"; 
	
	/**name of function incubation to mb*/
	public static final String INCMB = "incmb"; 
	
	/**name of function for detection*/
	public static final String DET = "det";
	
	
	/**duration distribution of latency*/
	protected Distributionable lat = null;
	
	/**duration distribution of incubation to pb*/
	protected Distributionable incpb  = null;
	
	/**duration distribution of incubation to mb*/
	protected Distributionable incmb  = null;
	
	/**duration of self healing period*/
	protected Distributionable sh = null;
	
	/**duration distribution of incubation to detection*/
	protected Distributionable det  = null;
	
	
	/**
	 * Infectivity kernel
	 */
	protected Kernel kernel = null;

	/**
	 * 
	 * @param funcs
	 */
	public AbstractDiseaseHistoryFunction(Map<String, Function> funcs) {
		super();		
	}

	/**
	 * 
	 *
	 */
	public AbstractDiseaseHistoryFunction()
	{
		super();
	}
	/**
	 * 
	 * @see simcolep.fun2.Function#changeParameter(java.lang.String, double)
	 */
	public void changeParameter(String name, double value)
	{	
		throw new SimulationException(SimulationException.NOTIMPLEMENTED);
	}
	
	
	@Override
	public double getY(double X) {
		
		return 0;
	}

	@Override
	public double getX(double y) {
		
		return 0;
	}

	@Override
	public double getDerivative(double x) {
		
		return 0;
	}

	@Override
	public double getIntegrated(double x) {
		
		return 0;
	}

	@Override
	public double getInvIntegrated(double y) {
		
		return 0;
	}

	@Override
	public Function clone() {
				return this;
	}

	/**
	 * @see simcolep.lepra.function.DiseaseHistory#getLatency(Random, double)
	 */
	public abstract double getLatency(Random rand, double age);
	
	/**
	 * @see simcolep.lepra.function.DiseaseHistory#getIncubationPB(Random, double)
	 */
	public abstract double getIncubationPB(Random rand, double age);

	/**
	 * @see simcolep.lepra.function.DiseaseHistory#getIncubationMB(Random, double)
	 */
	public abstract double getIncubationMB(Random rand, double age);
	

	/**
	 * @see simcolep.lepra.function.DiseaseHistory#getSelfHealing(Random, double)
	 */
	public abstract double getSelfHealing(Random rand, double age);

	/**
	 * @see simcolep.lepra.function.DiseaseHistory#getPasDetection(simcolep.lepra.Morbidable, Random)
	 */
	public abstract double getPasDetection(Morbus m, Random rand);

    
    /**
     * @see simcolep.fun2.Function#equalsFunction(simcolep.fun2.Function)
     */
    public boolean equalsFunction(Function f)
    {
        return this.getClass().equals(f.getClass());
    }
    
    /**
     * 
     * @see simcolep.fun2.Function#getParameter(java.lang.String)
     */
    public double getParameter(String pname)
    {
        throw new SimulationException(
        		SimulationException.NOTIMPLEMENTED);
    }
    
 

    /**
     * @see simcolep.fun2.Function#inputGroup()
     */
    @Override
    public int inputGroup()
    {
    	return Function.DISEASE_FUNCTION_INPUT;
    }
    
	

	/**
	 * return a copy of the kernel
	 * @see simcolep.lepra.function.DiseaseHistory#getKernel()
	 */
	public Kernel getKernel() {
			return this.kernel.copy();
	}
    
	/**
	 * return the sequence of states modeled by this disease history
	 * @return
	 */
	public abstract int[] getStates();
	/**
	 * Set this disease history
	 *
	 */
	public abstract AbstractDiseaseHistoryFunction set(Map<String, Function> funcs);
  
	/**
	 * 
	 */
    public String toString()
    {
    	String dis_hist = this.getName();
    	
    	//disease history functions
    	//latency
    	if(this.lat != null)
    		dis_hist += 
    			PN.LINESEPARATOR + LAT +PN.TAB+
    			this.lat.toString();
    	//incubation to PB
    	if(this.incpb != null)
    		dis_hist += 
    			PN.LINESEPARATOR + INCPB+PN.TAB+
    			this.incpb.toString();
    	//incubation to MB
    	if(this.incmb != null)
    		dis_hist += 
    			PN.LINESEPARATOR + INCMB+PN.TAB+
    			this.incmb.toString();
    	//self healing
    	if(this.sh != null)
    		dis_hist += 
    			PN.LINESEPARATOR + SH+PN.TAB+
    			this.sh.toString();
    	//kernel
    	if(this.kernel!= null)
    		dis_hist+=
    			PN.LINESEPARATOR + KERNEL+PN.TAB+
    			this.kernel.toString();
    	
    	//detection
    	if(this.det != null)
    		dis_hist += 
    			PN.LINESEPARATOR + DET + PN.TAB+
    			this.det.toString();
    	
    	return dis_hist;
    }
}
