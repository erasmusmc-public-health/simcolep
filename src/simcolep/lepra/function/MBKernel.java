/*
 * This file is part of SIMCOLEP, a simulation model developed by the Department of Public Health, 
 * Erasmus MC, University Medical Center Rotterdam, Rotterdam, The Netherlands.
 *
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/ 
 * or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA
 *
 * You are free to:
 * Share � copy and redistribute the material in any medium or format
 * Under the following terms:
 * Attribution � You must give appropriate credit, provide a link to the license, and indicate if changes were 
 * made. You may do so in any reasonable manner, but not in any way that suggests the licensor endorses
 * you or your use.
 * NonCommercial � You may not use the material for commercial purposes.
 * NoDerivatives � If you remix, transform, or build upon the material, you may not distribute the modified material.
 */

/**
 * created: 11-Apr-2007
 */
package simcolep.lepra.function;

import java.util.Random;


import simcolep.exceptions.SimulationException;
import simcolep.fun2.CinlarsNonStatPoisson;

import simcolep.lepra.Morbus;
import simcolep.tools.PN;

/**
 * @author ir. E.A.J. Fischer
 * @version 1.1
 */
public final class MBKernel extends AbstractKernel {

	/**serialVersionUID */
	private static final long serialVersionUID =
		-4974716683116020188L;
	
	/***/
	protected static final String BETAHIGH = "betahigh"; 
	/***/
	protected static final String BETALOW = "betalow"; 
	
	/**beta at start of infection*/
	protected double betalow = 0.0;
	
	/**beta when symptomatic*/
	protected double betahigh = 0.0;
	
	/**contact rate*/
	private double contactrate = 0.0;
	
	/**duration of the contact period*/
	private double incubation_period = 0.0;
	
	/**duration of the latency period*/
	private double latency_period = 0.0;

	/**true when periods are set*/
	private boolean set = false;
	/**
	 * 
	 */
	public MBKernel() {
		super();
		
	}


	@Override
	public void changeParameter(String name, double value) {
        if(name.equalsIgnoreCase(BETALOW))
        {
            this.betalow = value;
        }else if(name.equalsIgnoreCase(BETAHIGH))
        {
            this.betahigh = value;
        }

	}
	/**
	 * @see simcolep.fun2.Function#getY(double)
	 */
	@Override
	public double getY(double t) {
		if(t<this.latency_period)
			return 0.0;
		else if (t<= this.latency_period+this.incubation_period)
			return this.contactrate*(this.betalow +((((this.betahigh-this.betalow)/this.incubation_period)*(t-this.latency_period))));
		else 
			return this.betahigh*this.contactrate;
	}

	/**
     * @see simcolep.fun2.Function#getIntegrated(double)
     */
    public double getIntegrated(double x) {
        if(x < this.latency_period)
        {
            return 0;
        }//if smaller than the incubation time
        else if (x <= this.latency_period+this.incubation_period)
        {
        	//if beta low == zero => avoid infinity contact rate times zero betalow if betalow is zero infinity times beta low is still zero thus
        	if(betalow == 0)
        		return (1.0/2.0)*getY(x)*(x-this.latency_period);
        	//calculate the area under the function
            return (((1.0/2.0)*(getY(x)-this.contactrate*betalow))+this.contactrate*betalow)*(x-this.latency_period);
        }else 
        {
            //longer than the incubation time is until incubation + since incubation            
            return this.getIntegrated(this.incubation_period+this.latency_period)+(x-this.incubation_period-this.latency_period)*this.contactrate*betahigh;
        }
    }

    /**
     * @see simcolep.fun2.Function#getInvIntegrated(double)
     */
    public double getInvIntegrated(double y) {
        //never making contact
    	if(this.contactrate==0.0)
        	return Double.POSITIVE_INFINITY;
        
    	//no probability of contact
        if(betahigh==0.0) 
        	return Double.POSITIVE_INFINITY;
      
       
        //new variable which is worth to avoid NaN errors by multiplying zero and infinity
        double succes_low = 
        	this.contactrate*betalow;
        
        if(betalow == 0.0)
        	succes_low = 0.0;
        if(this.contactrate == 0.0)
        	succes_low = 0.0;
        
        //the slope of the function (unintegrated) is: 
        double s = 
        	this.contactrate*(betahigh-betalow)/(this.incubation_period);
      
        if(s==0)//either the contact rate is zero, betahigh equals beta low or the incubation time is infinite
        {
        	if(betalow == 0.0)
        		return Double.POSITIVE_INFINITY;
        	//in all options the effective contact rate is always beta low
        	//it does not increase from it thus the inverse integration is:
        	 return this.latency_period+y/(succes_low);
        
        }
        //return latency time when y is zero
        if(y <= 0)
        {
            return this.latency_period;
        }//when the value of y is between the value at the end of the latency period and the end of the incubation time
        else if(y <= getIntegrated(this.incubation_period+this.latency_period))
        {
        	if(Double.isInfinite(s))
        		return this.latency_period;
            return ((1/s)*(this.latency_period*s-succes_low+Math.sqrt(Math.pow(succes_low,2)+2*y*s)));
        }else 
        	return this.incubation_period+ this.latency_period + (y-getIntegrated(this.incubation_period+this.latency_period))/(betahigh*this.contactrate);
    }
    
	/**
	 * @see simcolep.lepra.function.Kernel#next(double, double, java.util.Random)
	 */
	public double next(double t, double c, Random rand) {
		if(!set)
			throw new SimulationException("Kernel is not set");
		
		//set the contact rate
		this.contactrate = c;
		
		  //next time depends on the rates but also on current time
        //with infinite contact rates this can go wrong because all events should have been 
        //done but in the case of movement this can cause a problem
        double next = t;
       
        if(!Double.isInfinite(this.contactrate))
        	next =
        		CinlarsNonStatPoisson.next(t, rand.nextDouble(), this);  
        else if(betalow+betahigh == 0)
        	next = 
        		Double.POSITIVE_INFINITY;
       
        
        //check whether this time is in the future
        if(next < t)
            	throw new SimulationException(
        			"next infection in the past!");
    
        if(Double.isNaN(next))
        	throw new SimulationException("next is NaN");
        
         //reset contact rate to 1
        this.contactrate =1.0;
        
        //return the next infection time
        return next;
	}
	/**
	 * @see simcolep.lepra.function.Kernel#set(simcolep.lepra.Morbus)
	 */
	public void set(Morbus m) {
		//latency period
		this.latency_period = m.getLatencyPeriod();
		// incubation period
		this.incubation_period = m.getIncMBPeriod();
		//set
		this.set = true;
	}
	/**
	 * 
	 */
	public Kernel copy()
	{
		MBKernel copy = new MBKernel();
		copy.betahigh = this.betahigh;
		copy.betalow = this.betalow;
		return copy;
	}

	/**
	 * @see java.lang.Object#toString()
	 */
	public String toString()
	{
		String s = "MBKernel:"+PN.TAB+
		"beta high:"+PN.TAB+this.betahigh +PN.TAB+
		"beta low:"+PN.TAB+this.betalow +PN.TAB;
		
		return s; 
	}
}
