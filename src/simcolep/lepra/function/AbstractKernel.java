/*
 * This file is part of SIMCOLEP, a simulation model developed by the Department of Public Health, 
 * Erasmus MC, University Medical Center Rotterdam, Rotterdam, The Netherlands.
 *
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/ 
 * or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA
 *
 * You are free to:
 * Share � copy and redistribute the material in any medium or format
 * Under the following terms:
 * Attribution � You must give appropriate credit, provide a link to the license, and indicate if changes were 
 * made. You may do so in any reasonable manner, but not in any way that suggests the licensor endorses
 * you or your use.
 * NonCommercial � You may not use the material for commercial purposes.
 * NoDerivatives � If you remix, transform, or build upon the material, you may not distribute the modified material.
 */

/**
 * created: 11-Apr-2007
 */
package simcolep.lepra.function;

import simcolep.exceptions.SimulationException;
import simcolep.fun2.Function;

/**
 * @author ir. E.A.J. Fischer
 * @version 1.1
 */
public abstract class AbstractKernel 
							extends	Function 
								implements Kernel {


	/***/
	private static final String MORE_THAN_ONE_ANSWER = "Many x-values for y = "; //$NON-NLS-1$

	/**
	 * @see simcolep.fun2.Function#changeParameter(java.lang.String, double)
	 */
	@Override
	public void changeParameter(String name, double value) {
		return;
	}

	/**
	 * @see simcolep.fun2.Function#getX(double)
	 */
	@Override
	public double getX(double y) {
		throw new SimulationException(MORE_THAN_ONE_ANSWER + y);
	}

	/**
	 * @see simcolep.fun2.Function#getDerivative(double)
	 */
	@Override
	public double getDerivative(double x) {
		return 0;
	}

	
	/**
	 * @see simcolep.fun2.Function#clone()
	 */
	@Override
	public Function clone() {
		return null;
	}

	/**
	 * @see simcolep.fun2.Function#equalsFunction(simcolep.fun2.Function)
	 */
	@Override
	public boolean equalsFunction(Function f) {
		return false;
	}



	/**
	 * @see java.lang.Object#toString()
	 */
	public String toString()
	{
		return this.getName();
	}

	/**
	 * @see simcolep.fun2.Function#inputGroup()
	 */
	public int inputGroup()
	{
		return Function.CONTINUOUS_FUNCTION_INPUT;
	}
	
	

}
