/*
 * This file is part of SIMCOLEP, a simulation model developed by the Department of Public Health, 
 * Erasmus MC, University Medical Center Rotterdam, Rotterdam, The Netherlands.
 *
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/ 
 * or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA
 *
 * You are free to:
 * Share � copy and redistribute the material in any medium or format
 * Under the following terms:
 * Attribution � You must give appropriate credit, provide a link to the license, and indicate if changes were 
 * made. You may do so in any reasonable manner, but not in any way that suggests the licensor endorses
 * you or your use.
 * NonCommercial � You may not use the material for commercial purposes.
 * NoDerivatives � If you remix, transform, or build upon the material, you may not distribute the modified material.
 */
package simcolep.lepra.function;



import java.util.Map;
import java.util.Random;

import simcolep.fun2.Function;
import simcolep.fun2.Distributionable;

import simcolep.lepra.Morbus;
import simcolep.lepra.State;

/**
 * 
 * @author ir. E.A.J. Fischer
 * @version 1.3
 */
public final class Asymptomatic 
		extends AbstractDiseaseHistoryFunction {
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 6351588046856093906L;

	/**states of type 1: asymptomatic*/ 
	private static int[] TYPE1_STATES =
								{State.SUSCEPTIBLE_STATE,
								State.LATENT_STATE, 
								State.ASYMPTOMATIC_LOWCONTAGIOUS_STATE};

	


	/**
	 *  
	 * @param funcs
	 */
	public Asymptomatic(Map<String, Function> funcs) {
		super();
		
		this.set(funcs);
		}
	/**
	 * 
	 *
	 */
	public Asymptomatic()
	{
		super();
	}
	

	/**
	 * 
	 * @see simcolep.lepra.function.AbstractDiseaseHistoryFunction#detectionProb(simcolep.lepra.Morbus, double)
	 */
	public double detectionProb(Morbus m, double time) {
				return 0;
	}

	/**
	 * 
	 * @see simcolep.lepra.function.DiseaseHistory#getLatency(Random, double)
	 */
	public double getLatency(Random rand, double age) {
		return this.lat.drawX(new double[]{rand.nextDouble(),age});
	}

	/**
	 * 
	 * @see simcolep.lepra.function.DiseaseHistory#getIncubationPB(Random, double)
	 */
	public double getIncubationPB(Random rand, double age) {
		
		return Double.POSITIVE_INFINITY;
	}

	/**
	 * 
	 * @see simcolep.lepra.function.DiseaseHistory#getIncubationMB(Random, double)
	 */
	public double getIncubationMB(Random rand, double age) {
		
		return Double.POSITIVE_INFINITY;
	}

	/**
	 * 
	 * @see simcolep.lepra.function.DiseaseHistory#getSelfHealing(Random, double)
	 */
	public double getSelfHealing(Random rand, double age) {
			return this.sh.drawX(new double[]{rand.nextDouble(),age});
	}

	/**
	 * Asymptomatic infections are never detected
	 * @return <code> Double.<i>POSITIVE_INFINITY</i></code>
	 * @see simcolep.lepra.function.DiseaseHistory#getPasDetection(Morbidable, Random)
	 */
	public double getPasDetection(Morbus m, Random rand) {
		
		return Double.POSITIVE_INFINITY;
	}
	/**
	 * @see simcolep.lepra.function.DiseaseHistory#set(java.util.Map)
	 */
	public AbstractDiseaseHistoryFunction set(Map<String, Function> funcs) {
//		duration of latency 
		this.lat  = (Distributionable)funcs.get(LAT);
		//duration of selfhealing
		this.sh = (Distributionable)funcs.get(SH);
		//infectivity kernel
		this.kernel = (Kernel)funcs.get(KERNEL);
		
		return this;
	}
	@Override
	public int[] getStates() {
		return TYPE1_STATES;
	}



}
