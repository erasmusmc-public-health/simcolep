/*
 * This file is part of SIMCOLEP, a simulation model developed by the Department of Public Health, 
 * Erasmus MC, University Medical Center Rotterdam, Rotterdam, The Netherlands.
 *
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/ 
 * or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA
 *
 * You are free to:
 * Share � copy and redistribute the material in any medium or format
 * Under the following terms:
 * Attribution � You must give appropriate credit, provide a link to the license, and indicate if changes were 
 * made. You may do so in any reasonable manner, but not in any way that suggests the licensor endorses
 * you or your use.
 * NonCommercial � You may not use the material for commercial purposes.
 * NoDerivatives � If you remix, transform, or build upon the material, you may not distribute the modified material.
 */
package simcolep.lepra.function;

import java.util.Map;
import java.util.Random;

import simcolep.fun2.Function;
import simcolep.lepra.Morbus;
import simcolep.lepra.State;

/**
 * Function for non susceptibles (not very interesting..)
 * @author ir. E.A.J. Fischer
 * @version 1.0
 */
public final class Immune extends AbstractDiseaseHistoryFunction {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2100903812874862226L;

	 
	/**states of immune type*/
	private static int[] IMMUNE_STATES =
								{State.NEVER_SUSCEPTIBLE_STATE};
	

	/**
	 * 
	 * @param funcs
	 */
	public Immune(Map <String, Function> funcs) {
		super();
		
	}
	
	/**
	 * 
	 */
	public Immune()
	{
		super();		
	}


	/**
	 * @return 0 
	 * @see simcolep.lepra.function.DiseaseHistory#getLatency(simcolep.lepra.Morbidable, double)
	 */
	public double getLatency(Random rand, double age) {
		return 0;
	}

	/**
	 * @return 0
	 * @see simcolep.lepra.function.DiseaseHistory#getIncubationPB(simcolep.lepra.Morbidable, double)
	 */
	public double getIncubationPB(Random rand, double age) {
			return 0;
	}

	/**
	 * @return 0
	 * @see simcolep.lepra.function.DiseaseHistory#getIncubationMB(simcolep.lepra.Morbidable, double)
	 */
	public double getIncubationMB(Random rand, double age) {
		return 0;
	}

	/**
	 * @return 0
	 * @see simcolep.lepra.function.DiseaseHistory#getSelfHealing(simcolep.lepra.Morbidable, double)
	 */
	public double getSelfHealing(Random rand, double age) {
		return 0;
	}

	/**
	 * @return Positive infinity
	 * @see simcolep.lepra.function.DiseaseHistory#getPasDetection(simcolep.lepra.Morbidable, Random)
	 */
	public double getPasDetection(Morbus m, Random rand) {
		return Double.POSITIVE_INFINITY;
	}



	/**
	 * Immune does not contain any parameters
	 * @see simcolep.fun2.Function#changeParameter(java.lang.String, double)
	 */
	public void changeParameter(String name, double value) {
		return;
		
	}
	/**
	 * @see simcolep.lepra.function.DiseaseHistory#set(java.util.Map)
	 */
	public AbstractDiseaseHistoryFunction set(Map<String, Function> funcs) {
		return this;
	}

	@Override
	public int[] getStates() {
		return IMMUNE_STATES;
	}

}
