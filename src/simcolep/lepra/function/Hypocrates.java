/*
 * This file is part of SIMCOLEP, a simulation model developed by the Department of Public Health, 
 * Erasmus MC, University Medical Center Rotterdam, Rotterdam, The Netherlands.
 *
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/ 
 * or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA
 *
 * You are free to:
 * Share � copy and redistribute the material in any medium or format
 * Under the following terms:
 * Attribution � You must give appropriate credit, provide a link to the license, and indicate if changes were 
 * made. You may do so in any reasonable manner, but not in any way that suggests the licensor endorses
 * you or your use.
 * NonCommercial � You may not use the material for commercial purposes.
 * NoDerivatives � If you remix, transform, or build upon the material, you may not distribute the modified material.
 */

/**
 * created: 23-Aug-2006
 */
package simcolep.lepra.function;

import simcolep.fun2.Function;



/**
 * Just a list of the leprosy functions
 * @author ir. E.A.J. Fischer
 * @version 1.0
 */
public enum Hypocrates{
	
	Immune(new Immune()), 
	Asymptomatic(new Asymptomatic()),
	shPB(new shPB()),
	dgPB(new dgPB()),
	MB(new MB()),
	MBKernel(new MBKernel()),
	PBKernel(new PBKernel());
	 
	 
	
	 /**
	  * Function
	  */
	private Function f = null;
	/**
	 * 
	 * @param f
	 */
	private Hypocrates(Function f)
	 {
	         this.f = f;
	 }
	
	 /**
	  * 
	  * @return new instance of the function
	 * @throws IllegalAccessException 
	 * @throws InstantiationException 
	  */
	 public Function getInstance() throws InstantiationException,
	 										IllegalAccessException
	 {
		     return 
		        this.f.getClass().newInstance();
	 }
}