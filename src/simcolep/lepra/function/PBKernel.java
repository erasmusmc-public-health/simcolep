/*
 * This file is part of SIMCOLEP, a simulation model developed by the Department of Public Health, 
 * Erasmus MC, University Medical Center Rotterdam, Rotterdam, The Netherlands.
 *
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/ 
 * or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA
 *
 * You are free to:
 * Share � copy and redistribute the material in any medium or format
 * Under the following terms:
 * Attribution � You must give appropriate credit, provide a link to the license, and indicate if changes were 
 * made. You may do so in any reasonable manner, but not in any way that suggests the licensor endorses
 * you or your use.
 * NonCommercial � You may not use the material for commercial purposes.
 * NoDerivatives � If you remix, transform, or build upon the material, you may not distribute the modified material.
 */

/**
 * created: 11-Apr-2007
 */
package simcolep.lepra.function;

import java.util.Random;


import simcolep.exceptions.SimulationException;
import simcolep.lepra.Morbus;
import simcolep.tools.PN;

/**
 * @author ir. E.A.J. Fischer
 * @version 1.1
 */
public final class PBKernel extends 
					AbstractKernel{



	/** serialVersionUID*/
	private static final long serialVersionUID = -7630011944239933646L;

	/**beta*/
	protected double beta = 0.0;
	
	/**length of latency state*/
	private double latency_period = 0.0;
	
	/**length of asymptomatic state*/
	private double asymp_length = 0.0;
	
	/**length of symptomatic state*/
	private double symp_length = 0.0;
	
	/**check whether everything has been set*/
	private boolean set = false;
	/**
	 * 
	 */
	public PBKernel() {
		super();
		}

	/**
	 * @see simcolep.fun2.Function#changeParameter(java.lang.String, double)
	 */
	@Override
	public void changeParameter(String name, double value) {
		if(name.equalsIgnoreCase(PN.BETA))
			this.beta = value;

	}

	/**
	 * In kernels this method can be used to get the rate at time t
	 * @see simcolep.fun2.Function#getY(double)
	 */
	@Override
	public double getY(double t) {
		if(t > this.latency_period)
			if(t<= this.latency_period+this.asymp_length+this.symp_length)
				return this.beta;
			else 
				return 0;
		return 0;
	}

	/**
	 * @see simcolep.fun2.Function#getDerivative(double)
	 */
	@Override
	public double getDerivative(double x) {
			return 0;
	}

	/**
	 * @see simcolep.fun2.Function#getIntegrated(double)
	 */
	@Override
	public double getIntegrated(double x) {
			return 0.5*this.getY(x)*(x-this.latency_period);
	}

	/**
	 * @see simcolep.fun2.Function#getInvIntegrated(double)
	 */
	@Override
	public double getInvIntegrated(double y) {
		if(y <= 0.0)
			return this.latency_period;
		else if(y <= this.getIntegrated(this.latency_period+this.asymp_length+this.symp_length))
			return (y/this.beta)-this.latency_period; 
		return this.latency_period+this.asymp_length+this.symp_length;
	}

	

	/**
	 * @see simcolep.lepra.function.Kernel#next(double, double, java.util.Random)
	 */
	public double next(double t, double c, Random rand) {
		if(!set)
			throw new SimulationException(SimulationException.MORBUSNOTPRESENT);
		//if the transmission rate = 0 return infinity (separate check the avoid infinity times 0)
		//beta most important
        if(this.beta == 0.0) 
        	return Double.POSITIVE_INFINITY;
        if(c == 0.0)
        	return Double.POSITIVE_INFINITY;
        if(Double.isInfinite(this.beta*c))
        	return t;
        //determine whether still in latent wait until latency period ahs passed
        if(t < this.latency_period)
        	t = this.latency_period;
        
         //draw exponential IID     
        double next = 
        	t - (1/(beta*c))*Math.log(rand.nextDouble()); 
                
        //check whether this time is in the future
        if(next < t)
        	throw new SimulationException(
        			SimulationException.INTHEPAST);
       
        //return the next infection time
        return next;
	}

	/**
	 * @see simcolep.lepra.function.Kernel#set(simcolep.lepra.Morbus)
	 */
	public void set(Morbus m) {
		//set the values
		//latency
		this.latency_period = 
			m.getLatencyPeriod();
		//asymptomatic
		this.asymp_length = 
			Math.min(m.getIncPBPeriod(), m.getSelfhealingPeriod());
		//length of symptomatic phase
		this.symp_length = 
			Math.max(0.0,m.getSelfhealingPeriod()-m.getIncPBPeriod());
		
		this.set = true;
	}

	/**
	 * @see java.lang.Object#toString()
	 */
	public String toString()
	{
		String s = "PBKernel:"+PN.TAB+PN.BETA+PN.TAB+this.beta;
	
		return s; 
	}
	
	/**
	 * 
	 */
	public Kernel copy()
	{
		PBKernel copy = new PBKernel();
		copy.beta = this.beta;
		return copy;
	}
}
