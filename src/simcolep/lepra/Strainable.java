/*
 * This file is part of SIMCOLEP, a simulation model developed by the Department of Public Health, 
 * Erasmus MC, University Medical Center Rotterdam, Rotterdam, The Netherlands.
 *
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/ 
 * or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA
 *
 * You are free to:
 * Share � copy and redistribute the material in any medium or format
 * Under the following terms:
 * Attribution � You must give appropriate credit, provide a link to the license, and indicate if changes were 
 * made. You may do so in any reasonable manner, but not in any way that suggests the licensor endorses
 * you or your use.
 * NonCommercial � You may not use the material for commercial purposes.
 * NoDerivatives � If you remix, transform, or build upon the material, you may not distribute the modified material.
 */

/**
 * created: 06-Apr-2007
 */
package simcolep.lepra;

import java.util.List;



/**
 * Strainable classes implement methods that can identify or follow a specific strain of the pathogen. 
 * A path is kept which contains the id's of the morbus-objects through which this strain is passed.
 * 
 * @author ir. E.A.J. Fischer
 * @version 1.1
 */
public interface Strainable {

	/**
	 * get a list with the next strainables
	 * @return next
	 */
	public List<Strainable> getNext();
	/**
	 * Get the path through which this strain has gone so far
	 * @return path
	 */
	@SuppressWarnings("unchecked")
	public List getPath();
	
	/**
	 * Get the name of the Strainable
	 * 
	 * @return name
	 */
	public String getName();
	
	/**
	 * Return the type of arc between this strainable and the previous
	 * 
	 * @return arc
	 */
	public int getArctype();
	
	/**
	 * Get the previous node
	 * 
	 * @return previous
	 */
	public Strainable getPrevious();
	
	/**
	 * Get a certain parameter of this strain e.g. virulence, infectivity, mutation rate
	 * @param param
	 * @return p
	 */
	public Number getStrainParameter(String param);

	/**
	 * Add a next strain to this strain
	 * 
	 * @param strain
	 */
	public void add(Strainable strain);

	/**
	 * Get the origin of this strain 
	 * 
	 * @return origin
	 */
	public String getOrigin();
	
	
	
	
	
}
