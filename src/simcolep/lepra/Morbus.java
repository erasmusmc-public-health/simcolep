/*
 * This file is part of SIMCOLEP, a simulation model developed by the Department of Public Health,
 * Erasmus MC, University Medical Center Rotterdam, Rotterdam, The Netherlands.
 *
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/
 * or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA
 *
 * You are free to:
 * Share � copy and redistribute the material in any medium or format
 * Under the following terms:
 * Attribution � You must give appropriate credit, provide a link to the license, and indicate if changes were
 * made. You may do so in any reasonable manner, but not in any way that suggests the licensor endorses
 * you or your use.
 * NonCommercial � You may not use the material for commercial purposes.
 * NoDerivatives � If you remix, transform, or build upon the material, you may not distribute the modified material.
 */

/*
 * Created on 04-May-2005
 */
package simcolep.lepra;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;

import nl.mgz.simulation.discreteevent.Event;
import nl.mgz.simulation.discreteevent.Schedulable;
import nl.mgz.simulation.discreteevent.Target;
import simcolep.asgard.Idunn;
import simcolep.asgard.Odin;
import simcolep.demo.Inhabitant;
import simcolep.demo.Person;
import simcolep.exceptions.SimulationException;
import simcolep.fun2.Archimedes;
import simcolep.fun2.Pythagoras;
import simcolep.lepra.event.SelfHealing;
import simcolep.lepra.event.StateChange;
import simcolep.lepra.event.Transmission;
import simcolep.lepra.event.TransmissionRM;
import simcolep.lepra.event.intervention.FollowUp;
import simcolep.lepra.event.intervention.SelfReport;
import simcolep.lepra.event.intervention.TreatmentEnd;
import simcolep.lepra.function.Kernel;
import simcolep.lepra.mendel.Genomable;
import simcolep.sim.Herbarium;
import simcolep.tools.History;
import simcolep.tools.PN;
import simcolep.tools.message.Message;
import simcolep.tools.message.Ratatosk;

/**
 * Morbus is the disease object. It has the individual states of the Person that
 * is its owner.
 *
 *
 *
 * @author ir. E.A.J. Fischer
 * @version 1.2 -- Last Modified: 11/04/2007
 *
 */
public final class Morbus extends Target implements Serializable {

	/** serialVersionUID */
	private static final long serialVersionUID = 574931875039822174L;

	/** a type to which other typse relapse */
	public static int RELAPSETYPE = -1;

	/** state for immune */
	public static final int IMMUNE_TYPE = 0;

	/** number of disease types */
	public static int DISEASE_TYPES = 5;

	/** Diagnosis state of a healthy person */
	public static final int HEALTHY = 0;

	/** whether to save disease history for debugging */
	public static boolean saveDiseaseHistory = false;

	/**
	 * counter counting number of transmission events scheduled
	 */
	public static int transscheduled = 0;

	/** disease phenotype */
	private int phenotype = -1;

	/**
	 * original phenotype (due to BCG a shift can occur and after relapse as
	 * well)
	 */
	private Genomable genome = null;

	/* disease state variables */
	/** disease state index */
	private int state_index = 0;

	/** current disease state */
	private State state = null;

	/** infectivity kernel */
	private Kernel kernel = null;

	/** time of infection */
	private double inftime = Double.POSITIVE_INFINITY;//

	/** latency period */
	private double lat = Double.POSITIVE_INFINITY;// latency period

	/** duration of incubation until PB since end latency period */
	private double incPB = Double.POSITIVE_INFINITY;//

	/** duration of incubation until MB since end incubation period to PB */
	private double incMB = Double.POSITIVE_INFINITY;//

	/** duration of self healing since end latency period */
	private double sh = Double.POSITIVE_INFINITY;//

	/**
	 * -1 = not examined, >=0 examined and given a diagnosis (or found healthy)
	 */
	private int diagnosis = -1; //

	/**
	 * Detected, if the morbus has been detected no other cure, bcg or what so
	 * ever can be given any more but proper treatment
	 */
	private boolean detected = false;

	/**
	 * If detected the method of detection
	 */
	private String detectionMethod = null;

	/** predicted detection by passive detection */
	private double detperiod = Double.NEGATIVE_INFINITY;//

	/** time of detection relative to the infection time */
	private double relative_det_time = Double.POSITIVE_INFINITY;// time of
																// detection

	/** time between treatment and relapse */
	private double relapse_period = Double.NEGATIVE_INFINITY;// time to relapse
																// since
																// treatment

	/** time of vaccination */
	private double vaccination_time = Double.NEGATIVE_INFINITY; // time of
																// vaccination

	/** duration of treatment */
	private double treat_duration = Double.POSITIVE_INFINITY;

	/** time of the transmission random mixing of a copied morbus */
	private double transRM = Double.POSITIVE_INFINITY;

	/** Map containing parameters of the disease strain */
	private transient Strainable strain = null;

	/** Person that 'owns' this set of disease parameters */
	private Person owner = null;

	/**
	 * events for which this morbus is the source of infection within it's own
	 * level
	 */
	private transient LinkedHashSet<Schedulable> levelTransmissionEvents = null;

	/** events for which this morbus is the source out side its levels */
	private transient TransmissionRM gpTransmissionEvent = null;

	/** disease history */
	private transient History diseasehistory = null;

	/**
	 * time and template id's of within level transmission for activation
	 * purposes
	 */
	private Map<Integer, Double> transLevel = null;

	/** owner is dead */
	public boolean isDead = false;

	/** layer of this morbus */
	private int layer = 0;

	private boolean vaccinated;

	private boolean undertreatment = false;

	private int fu = 0;

	/**
	 * 
	 * @param layer
	 * @param disease
	 */
	public Morbus(Genomable genes) {

		// genome
		this.genome = genes;

		// phenotype
		this.phenotype = this.genome.getPhenotype();

		// start susceptible
		this.state = State.state(State.SUSCEPTIBLE_STATE);
		// unless your phenotype is immune
		if (this.phenotype == Morbus.IMMUNE_TYPE)
			this.state = State.state(State.NEVER_SUSCEPTIBLE_STATE);

	}

	/**
	 * Activates the morbus when the population is read from file
	 * 
	 * @param timedif
	 *            difference between the time this morbus was saved and the
	 *            current simulation time
	 * 
	 */
	@SuppressWarnings("unchecked")
	public void activate(double timedif) {

		// this morbus is not infected no further handling
		if (this.state.getState() == State.SUSCEPTIBLE_STATE)
			return;
		// get current time
		double current_time = Idunn.getCurrentTime();
		// set the new infection time
		this.inftime = this.inftime + timedif;

		// continue if the infection time is in the past else throw Exception
		if (this.inftime > current_time)
			throw new SimulationException("infection takes place in the future!");

		// if this person is not infected any more (SUSCEPTIBLE OR RECOVERED
		// STATE) return
		if (this.getState() >= State.RECOVERED_STATE) {
			// first check whether this person has to be released from treatment
			this.scheduleRFT();

			return;
		}

		// self healing
		if (this.getSelfhealingTime() < current_time) {
			this.selfheal();
			// notify activation
			Ratatosk.notify(Message.formulate(Message.MORBUSACTIVATION), this, this);
			//
			return;
		} else {
			double shTime = this.getSelfhealingTime();
			if (shTime < this.getDetectionTime()) {
				// schedule selfhealing
				SelfHealing sh = new SelfHealing(Message.ACTIVATE, this, shTime);
				Idunn.getEventManager().schedule(sh);
			}
		}

		// if this morbus is not yet detected
		if (this.getDetectionTime() >= current_time) {

			// within the general population
			// if not infinite
			if (!Double.isInfinite(this.transRM)) {
				// if not in past
				if (this.transRM + timedif >= current_time) {
					if (transRM + timedif < this.getOwner().getDeathTime()) {
						this.gpTransmissionEvent = new TransmissionRM(this, this, this.transRM + timedif);
						Idunn.getEventManager().schedule(this.gpTransmissionEvent);

					}

				}
			}

			// transmission within level
			if (transLevel != null) {
				if (!transLevel.isEmpty()) {
					if (this.getOwner().getLevel().getDFactor(this.layer)) {
						// iterate the victims
						for (Inhabitant i : this.getOwner().getLevel().getInhabitants()) {
							// cast
							Person vic = (Person) i;
							// template ID
							Integer templateID = new Integer(vic.templateID);

							// if not in the map return
							if (this.transLevel.containsKey(templateID)) {
								// schedule transmission at this fellow/gal
								double tTime = this.transLevel.get(templateID).doubleValue();
								tTime += timedif;

								// check for 'precision' otherwise set to the
								// current time
								if (Math.abs(current_time - tTime) < Pythagoras.getDefaultPrecision())
									tTime = current_time;

								// if the new time is larger than the current
								// time
								if (tTime >= current_time) {
									// schedule event
									Idunn.getIdunn().scheduleDwellingTransmission(this, vic.getMorbus(this.getLayer()),
											tTime);

								} else
									throw new SimulationException("transmission within level time < current time!");
							}

						}
					}

				}
			}
			// delete the transmission within level stored here
			this.transLevel = null;

			// state change
			if (this.state_index < Idunn.getStateSequence(this.phenotype).length - 1)
				this.scheduleStateChange();

			// passive detection
			this.schedulePassiveDetection(this.getDetectionTime());

		} else {
			// detected is true
			this.setDetected(true, Clinic.DETECTIONMETHODS[1]);

			// add to the history that this one has been detected before the
			// start
			this.addDiseaseHistory(current_time, "detected before start");

			// if treatment time is also already passed
			if (this.getRFTtime() <= current_time) {
				// add to disease history
				this.addDiseaseHistory(current_time, "treatment finished before start");

				// check
				this.changeState(this.state.getState(), State.CURE_STATE);

				// return no need to report infection and or passed detection
				return;
			}

			// and notify activation of morbus
			Ratatosk.notify(Message.formulate(Message.MORBUSACTIVATION), this, this);

			return;
		}

		// and notify activation of morbus
		Ratatosk.notify(Message.formulate(Message.MORBUSACTIVATION), this, this);

	}

	/**
	 * The patient is actively detected. A passive detection event (if present)
	 * is removed. The patient is notified as detected.
	 */
	public void activeDetection(String method) {
		// if the disease is already detected this is nothing new
		if (this.detected)
			return;
		// remove a detection event
		for (Schedulable det : super.getTargetedEvents(SelfReport.class.getName())) {
			if (det != null) {
				Idunn.getEventManager().cancel(det);
			}
		}

		// remove follow up events that were scheduled before this one was
		// detected
		Iterator<Schedulable> fu = super.getTargetedEvents(FollowUp.class.getName()).iterator();

		while (fu.hasNext()) {
			Idunn.getEventManager().cancel(fu.next());
		}

		// set detected
		this.setDetected(true, method);

		// add the active detection to the history
		this.addDiseaseHistory(Idunn.getCurrentTime(), "active detection");

	}

	/**
	 * @param time
	 * @param event
	 * @see simcolep.lepra.Morbus#addDiseaseHistory(double, java.lang.Object)
	 */
	public void addDiseaseHistory(double time, Object event) {
		// setting saving it or not
		if (!Morbus.saveDiseaseHistory)
			return;
		// new history book if needed
		if (this.diseasehistory == null)
			this.diseasehistory = new History();

		// add to the history
		this.diseasehistory.add(time, event);

	}

	/**
	 * Add a event of which this morbus is the source to the list of source d
	 * events
	 * 
	 * @param event
	 */
	public void addSourcedEvent(Transmission event) {
		if (event == null)
			throw new SimulationException("Adding null-event");

		// random mixing
		if (event.getTransmissionType() == Transmission.RANDOM_MIXING)
			if (this.gpTransmissionEvent != null)
				throw new IllegalArgumentException("Morbus already source of random mixing event");
			else
				this.gpTransmissionEvent = (TransmissionRM) event;

		// non random mixing
		if (event.getTransmissionType() == Transmission.LEVEL) {
			if (!this.levelTransmissionEvents.add(event))
				transscheduled++;
		}

	}

	/**
	 * 
	 * @see nl.mgz.simulation.discreteevent.Targetable#addTargeted(nl.mgz.simulation.discreteevent.Schedulable)
	 */
	@Override
	public void addTargeted(Schedulable event) {
		super.addTargeted(event);
	}

	/**
	 * Absolutely very very dead after this method.
	 *
	 */
	public void braindead() {

		this.getOwner().setMorbus(null, this.layer);

		this.owner = null;

	}

	/**
	 * Cancel all events targeted at this and all for which this is the source
	 * 
	 */
	public void cancelAll() {

		// cancel events within the level
		this.cancelTransmission();

		// cancel targeted events
		for (Schedulable s : new ArrayList<Schedulable>(this.getTargetedEvents())) {
			// also remove at the source
			if (s.getClass().getSuperclass().equals(Transmission.class))
				((Morbus) s.getSource()).removeSourcedEvent((Transmission) s);

			// cancel the whole event
			Idunn.getEventManager().cancel(s);
		}

		// check
		if (Odin.inSafeMode())
			if (!this.getTargetedEvents().isEmpty())
				throw new SimulationException("not evertyhing canceled");

	}

	/**
	 * Cancels transmission events targeted at level-members
	 * 
	 * @see simcolep.lepra.Morbidable#cancelLevel()
	 */
	@SuppressWarnings("unchecked")
	public void cancelLevel() {
		// no events return
		if (this.levelTransmissionEvents == null)
			return;

		if (this.levelTransmissionEvents.isEmpty())
			return;

		// in the sourced event we find the events targeted at 'level' members
		for (Schedulable s : (Set<Schedulable>) this.levelTransmissionEvents.clone()) {
			// iterate the source events
			if (s.isScheduled())
				// remove those targeted at the
				Idunn.getEventManager().cancel(s);

		}

		// clean set
		this.levelTransmissionEvents = new LinkedHashSet<Schedulable>();
	}

	/**
	 * Change a non-infected to another type Other wise this method returns
	 * without doing anything
	 * 
	 * @param toType
	 */
	public void changeType(int toType) {
		// check if it is a change
		if (this.phenotype == toType)
			return;
		// do not interfere with spectrumshift by vaccination
		if (vaccinated)
			if (Idunn.getIdunn().getClinic().BCGshift)
				return;

		if (this.getState() == State.SUSCEPTIBLE_STATE || this.getState() == State.NEVER_SUSCEPTIBLE_STATE) {
			// report change to the Clinic
			Idunn.getIdunn().getClinic().getRegister().changeType(this, this.phenotype, toType);

			// phenotype
			this.phenotype = toType;

			// state index
			this.state_index = 0;

			if (this.phenotype == Morbus.IMMUNE_TYPE)
				this.changeState(this.getState(), State.NEVER_SUSCEPTIBLE_STATE);
			else
				this.changeState(this.getState(), State.SUSCEPTIBLE_STATE);

		} else
			return;
	}

	/**
	 * Administer chemoprophylactic treatment
	 * 
	 */
	public void chemoprophylatics() {
		// when detected a person will not get chemoprophylactic treatment
		if (detected)
			return;

		// add to history
		this.addDiseaseHistory(Idunn.getCurrentTime(), "received chemoprophylactics");

		// return if immune
		if (this.getType() == Morbus.IMMUNE_TYPE)
			return;

		// return if susceptible
		if (this.state.getState() == State.SUSCEPTIBLE_STATE)
			return;

		// return if recovered
		if (this.state.getState() >= State.RECOVERED_STATE)
			return;
		// obtain random number generator
		Random rand = Herbarium.rand(Herbarium.DIS);

		// duration of infection
		double t = Idunn.getCurrentTime() - this.getInfectionTime();

		// fraction gets cured
		if (rand.nextDouble() < Archimedes.ask(PN.CHEMOCURE + this.getState()).getY(t)) {
			this.cure();
			return;
		} else {
			double delay = Archimedes.ask(PN.CHEMODELAY + getState()).getY(t);
			this.delay(delay);
			return;
		}

	}

	/**
	 * cure cancel all events and set state to cure state
	 *
	 */
	public void cure() {
		// cancel events
		this.cancelTransmission();

		// cancel next state change
		for (Schedulable s : this.getTargetedEvents(StateChange.class.getName()))
			Idunn.getEventManager().cancel(s);

		// add this to the history
		this.addDiseaseHistory(Idunn.getCurrentTime(), "cure");

		// set cured
		this.changeState(this.getState(), State.CURE_STATE);

		this.state_index = 0;

		// cancel state changes
		List<Schedulable> list = this.getTargetedEvents(StateChange.class.getName());

		for (Schedulable s : list)
			Idunn.getEventManager().cancel(s);

	}

	/**
	 * Cancel all events and set dead
	 * 
	 * @see simcolep.lepra.Morbidable#die()
	 */
	public void die() {
		this.isDead = true;

		// add to history
		this.addDiseaseHistory(Idunn.getCurrentTime(), "died");

		// cancel all events of this morbus
		this.cancelAll();

	}

	/**
	 * This method differs from the method getDetectionTime in that way that
	 * getDetectionTime returns the 'predicted' time of detection. If no active
	 * detection is done this will finally be the actual detection time. <br>
	 * This method returns after detection the time that this occured.
	 * 
	 * @return
	 */
	public double getActualDetectionTime() {

		return this.relative_det_time + this.getInfectionTime();
	}

	/**
	 * 
	 * @return detectionMethod
	 */
	public String getDetectionMethod() {
		return detectionMethod;
	}

	/**
	 * @return
	 * @see simcolep.lepra.Morbus#getDetectionPeriod()
	 */
	public double getDetectionPeriod() {
		return this.detperiod;
	}

	/**
	 * @return time of incubation plus detection
	 * @see simcolep.lepra.Morbus#getDetectionTime()
	 */
	public double getDetectionTime() {

		if (Double.isInfinite(this.getIncMBTime()))
			return this.getIncPBTime() + this.getDetectionPeriod();
		else
			return this.getIncMBTime() + this.getDetectionPeriod();

	}

	/**
	 * @param t
	 * 
	 * @return probability of detection at current time t
	 */
	public double getDetProb(double t, int fu) {
		// return the detection probability of the current state
		return this.state.detProb(t, fu);
	}

	/**
	 * Returns an int for which stated diagnosis
	 * <li>-1 = not diagnosed
	 * <li>0 = healthy
	 * <li>1 = state 3
	 * <li>2 = state 5
	 * <li>3 = state 6
	 * <li>4 = asympatomatic states
	 * 
	 * @return diagnosis
	 */
	public int getDiagnosis() {
		return this.diagnosis;
	}

	/**
	 * @return diseasehistory
	 * 
	 * @see simcolep.lepra.Morbus#getDiseaseHistory()
	 */
	@SuppressWarnings("unchecked")
	public List getDiseaseHistory() {

		return this.diseasehistory;
	}

	/**
	 * Get the number of follow up
	 * 
	 * @return
	 */
	public int getFollowUp() {

		return this.fu;
	}

	/**
	 * Get the original phenotype of the morbus
	 * 
	 * @return genome
	 */
	public Genomable getGenome() {
		return this.genome;
	}

	/**
	 * 
	 * @return id
	 */
	public String getID() {
		if (this.owner == null)
			throw new SimulationException("do not ask the id of a morbus " + "without owner");

		return Integer.toString(this.getOwner().getID());
	}

	/**
	 * @return
	 * @see simcolep.lepra.Morbus#getIncMBPeriod()
	 */
	public double getIncMBPeriod() {
		return this.incMB;
	}

	/**
	 * @return Time of first MB symptoms
	 */
	public double getIncMBTime() {

		return this.getIncPBTime() + this.getIncMBPeriod();
	}

	/**
	 * @return
	 * @see simcolep.lepra.Morbus#getIncPBPeriod()
	 */
	public double getIncPBPeriod() {

		return this.incPB;
	}

	/**
	 * @return Time of first PB symptoms
	 */
	public double getIncPBTime() {

		return this.getLatencyTime() + this.getIncPBPeriod();
	}

	/**
	 * return the time at which this morbus was infected
	 * 
	 * @see simcolep.lepra.Morbidable#getInfectionTime()
	 */
	public double getInfectionTime() {
		// infectio time
		return this.inftime;
	}

	/**
	 * @return the time at which the latency period ends
	 */
	public double getLatencyPeriod() {
		return this.lat;

	}

	/**
	 * @return time of end of the latency period
	 * 
	 * @see simcolep.lepra.Morbus#getLatencyPeriod()
	 */
	public double getLatencyTime() {

		return this.getInfectionTime() + this.getLatencyPeriod();
	}

	/**
	 *
	 * @return layer
	 */
	public int getLayer() {
		return this.layer;
	}

	/**
	 * Get the Person owner object of this disease object
	 * 
	 * @see simcolep.lepra.Morbidable#getOwner()
	 */
	public Person getOwner() {
		if (this.owner == null)
			throw new SimulationException("owner is not set");

		return this.owner;
	}

	/**
	 *
	 * @param m
	 * @return
	 */
	public int getRelationship(Morbus m) {
		return this.owner.getRelationship(m.getOwner());
	}

	/**
	 * Return the time it took be released from treatment starting from the
	 * detection time
	 * 
	 * @see simcolep.lepra.Morbidable#getRFTtime()
	 */
	public double getRFTtime() {
		return this.treat_duration + this.getActualDetectionTime();
	}

	/**
	 * @return this.sh
	 * 
	 * @see simcolep.lepra.Morbus#getSelfhealingPeriod()
	 */
	public double getSelfhealingPeriod() {
		return this.sh;
	}

	/**
	 * 
	 * @return the time at which self healing occurs
	 */
	public double getSelfhealingTime() {
		return this.sh + this.getLatencyTime();

	}

	/**
	 * update the infection state <br>
	 * <br>
	 * Infection states are:<br>
	 * <br>
	 * 0: Susceptible<br>
	 * 1: Latent<br>
	 * 2: low infectious undetectable<br>
	 * 3: low infectious detectable<br>
	 * 4: building up infection undetectable<br>
	 * 5: building up infection detectable<br>
	 * 6: higly infectious detectable <br>
	 * 9: Recoverd with impairment<br>
	 * 10: Recovered and immune<br>
	 * 11: Recovered and immune due to treatment<br>
	 *
	 * 
	 */
	public int getState() {

		// return state
		return this.state.getState();
	}

	/**
	 * 
	 * @return strain information
	 * 
	 */
	public Strainable getStrain() {
		return this.strain;
	}

	/**
	 * @return phenotype
	 */
	public int getType() {

		return this.phenotype;
	}

	/**
	 *
	 * @return vaccination_time
	 */
	public double getVacinationTime() {
		return this.vaccination_time;
	}

	/**
	 * @param currentTime
	 * @return
	 * @see simcolep.lepra.Morbus#getVacProtect(double)
	 */
	public double getVacProtect(double currentTime) {

		// BCG does not protect
		if (!Idunn.getIdunn().getClinic().BCGprotect)
			return 0.0;

		// BCG vaccination took place at an infinite time
		if (Double.isInfinite(this.vaccination_time))
			return 0.0;

		// return the value of BCG protection time after vaccination
		// which is based upon the type of genetic leprosy
		return (Archimedes.ask(PN.BCGPROTECT + this.getType()).getY(Idunn.getCurrentTime() - this.vaccination_time));

	}

	/**
	 * @param event
	 * @return
	 * @see simcolep.lepra.Morbus#hasSourcedAbstractLepraEvent(simcolep.lepra.event.AbstractLepraEvent)
	 */
	public boolean hasSourcedAbstractLepraEvent(Event event) {
		return this.levelTransmissionEvents.contains(event);
	}

	/**
	 * 
	 * @param source
	 * @param time
	 * @param infect_at_level
	 * @return true if infection was succesfull
	 */
	@SuppressWarnings("unchecked")
	public boolean infect(Morbus source, double time, int infect_at_level) {
		// obtain random number generator
		Random rand = Herbarium.rand(Herbarium.DIS);

		// check if the owner shouldn't be dead
		if (this.getOwner().getDeathTime() < time)
			throw new SimulationException(SimulationException.SHOULDBEDEAD);

		// not susceptible than this morbus cannot be infected again
		if (this.state.getState() != State.SUSCEPTIBLE_STATE)
			return false;

		// protected against vaccination
		if (rand.nextDouble() < this.getVacProtect(time))
			return false;

		// the time that is passed is infinity return this morbus is not
		// infected
		if (Double.isInfinite(time))
			return false;

		// immune -> return
		if (getType() == IMMUNE_TYPE) {
			// add this event to the disease history
			this.addDiseaseHistory(time, "target of UNSUCCESFUL transmission by " + source.getOwner().getID());
			source.addDiseaseHistory(time, "source of UNSUCCESFUL transmission to " + this.getOwner().getID());
			return false;
		}
		// add to the disease history
		this.addDiseaseHistory(time, "target of SUCCESFUL transmission by " + source.getOwner().getID());
		source.addDiseaseHistory(time, "source of SUCCESFUL transmission to " + this.getOwner().getID());

		// create level transmission event map
		this.levelTransmissionEvents = new LinkedHashSet<Schedulable>();

		// infection time is:
		this.inftime = time;

		// Infection is 'transmitted' and the next strain node is added
		this.strain = new Strain(source.getStrain(), this, infect_at_level, time);

		// duration latency period
		this.lat = Idunn.getLatency(this);
		// duration of incubation to pb
		this.incPB = Idunn.getIncPB(this);
		// duration of incubation to mb
		this.incMB = Idunn.getIncMB(this);
		// selfhealing and detection is arranged somewhere else
		this.sh = Idunn.getSelfhealing(this);

		// next state from susceptible is latent
		this.nextState();

		//
		double shTime = this.getSelfhealingTime();
		double deathTime = this.getOwner().getDeathTime();

		if (shTime > Idunn.getCurrentTime()) {
			if (shTime < deathTime) {
				SelfHealing sh = new SelfHealing(source, this, shTime);
				Idunn.getEventManager().schedule(sh);
			}
		} else {
			this.selfheal();
			return true;
		}

		// set a infectivity kernel
		this.kernel = Idunn.getKernel(this);

		this.kernel.set(this);

		// schedule transmission
		// within level
		Idunn.getIdunn().newInfectionInDwelling(this);
		// to the general population
		Idunn.getIdunn().scheduleToGeneralPopulation(this);

		// return true because got infected
		return true;
	}

	/**
	 * This method creates a new infection for which the strain is given by
	 * string Use it to initialise the infection
	 *
	 * @param origin
	 *            - origin of this infection so the path transmission, import,
	 *            generatio spontanea or some thing like that
	 * @param currentTime
	 *            - time of infection
	 * @param infect_at_level
	 *            - level at which the infection occurs
	 * @return true if succesfull infection occured
	 *
	 */
	public boolean infect(String origin, double currentTime, int infect_at_level) {
		// only uninfected can be infected
		if (this.state.getState() != State.SUSCEPTIBLE_STATE)
			return false;

		// put the strain
		this.strain = new Strain(origin, this, currentTime);

		// infect with this same morbus as source
		return infect(this, currentTime, infect_at_level);

	}

	/**
	 * 
	 * @return this.detected
	 */
	public boolean isDetected() {

		return this.detected;
	}

	/**
	 * True if now or ever to become infectious
	 * 
	 * @return b
	 */
	public boolean isInfectious() {
		return this.state.isInfectious();
	}

	/**
	 *
	 * @return vaccinated
	 */
	public boolean isVaccinated() {
		return this.vaccinated;
	}

	/**
	 * Calculate/determine the next time of transmission
	 *
	 * @param contact
	 * @param currentTime
	 * @param rand
	 * @return t
	 */
	public double next(double currentTime, double contact, Random rand) {

		double t = this.kernel.next(currentTime - this.inftime, contact, rand);

		return t + this.inftime;
	}

	/**
	 * Changes the state of the morbus to the next state of normal disease
	 * progress
	 * 
	 * @param state
	 */
	public void nextState() {
		// return if the current state is recovered
		if (this.getState() >= State.RECOVERED_STATE)
			return;

		if (this.getInfectionTime() > Idunn.getCurrentTime())
			throw new SimulationException("Not infected!");

		// get current state
		int c_state = this.state.getState();

		// increase the state_index with one
		this.state_index += 1;

		// change the state
		this.changeState(c_state, Idunn.getStateSequence(this.phenotype)[state_index]);

		// schedule next state change
		if (this.state_index < Idunn.getStateSequence(this.phenotype).length - 1)
			this.scheduleStateChange();
	}

	/**
	 * Return true if the relapse period is not infinite (either positive or
	 * negative)
	 * 
	 * @return b
	 * @see simcolep.lepra.Morbus#relapsing()
	 */
	public boolean relapsing() {
		boolean b = !Double.isInfinite(this.relapse_period);

		return b;
	}

	/**
	 * @param s
	 * 
	 * @return true if removed
	 * 
	 */
	public boolean removeSourcedEvent(Transmission s) {

		// remove within level transmission
		if (s.getTransmissionType() == Transmission.LEVEL)
			return this.levelTransmissionEvents.remove(s);

		// or remove random mixing event
		else if (s.getTransmissionType() == Transmission.RANDOM_MIXING)
			if (s.equals(this.gpTransmissionEvent)) {
				this.gpTransmissionEvent = null;
				return true;
			} else
				return false;
		else
			return false;
	}

	/**
	 * Release from treatment
	 */
	public void rft() {
		this.undertreatment = false;
		this.setDetected(false, null);

	}

	/**
	 * Schedules a detection event. If there are any old detection events these
	 * are canceled.
	 * 
	 * @param t
	 *            - time of detection
	 */

	@SuppressWarnings("unchecked")
	public void schedulePassiveDetection(double t) {
		// if detection occurs before the start of treatment deal with that
		if (Idunn.getCurrentTime() < Idunn.getIdunn().getClinic().getStartTreatment()) {
			Idunn.getIdunn().scheduleDelayedSelfReport(this);
			return;
		}
		// if detection occurs after death ignore
		if (this.getOwner().getDeathTime() < t)
			return;

		// if detection occurs after self healing ignore
		if (this.getSelfhealingTime() < t)
			return;

		// cancel other detection events
		if (this.isTargetOf(SelfReport.class.getName())) {
			List<Schedulable> o = this.getTargetedEvents(SelfReport.class.getName());
			for (Schedulable s : o) {
				Idunn.getEventManager().cancel(s);
			}

		}
		// schedule the new detection event
		if (Math.abs(t - Idunn.getCurrentTime()) < Pythagoras.getDefaultPrecision())
			t = Idunn.getCurrentTime() + Pythagoras.getDefaultPrecision();

		// schedule
		SelfReport de = new SelfReport(this, this, t);
		Idunn.getEventManager().schedule(de);

	}

	/**
	 * Self healing of the disease for any state to self-heal state
	 *
	 */
	public void selfheal() {
		// no need for further action
		if (this.state.getState() == State.SELFHEAL_STATE)
			return;

		// or if susceptible no further action
		if (this.state.getState() == State.SUSCEPTIBLE_STATE)
			return;

		// addto history
		this.addDiseaseHistory(Idunn.getCurrentTime(), "self-healed");

		// set state
		this.changeState(this.getState(), State.SELFHEAL_STATE);

		// set the infection time to Positive infinity
		if (this.state.getState() == State.SUSCEPTIBLE_STATE) {
			this.inftime = Double.POSITIVE_INFINITY;

			this.sh = Double.POSITIVE_INFINITY;
		}

		// remove all events
		this.cancelAll();

		// set state index
		this.state_index = 0;

	}

	/**
	 * @param b
	 * @see simcolep.lepra.Morbus#setDetected(boolean)
	 */
	public void setDetected(boolean b, String method) {
		if (b) {
			// set the time relative to the infection time
			this.relative_det_time = Idunn.getCurrentTime() - this.getInfectionTime();
			// if already detected
			if (this.detected)
				throw new SimulationException("was already detected!");
			if (this.state.getState() == State.SUSCEPTIBLE_STATE)
				throw new SimulationException("cannot be detected!");

			// add to the disease history
			this.addDiseaseHistory(Idunn.getCurrentTime(), "detected: " + b);
		}
		// set detected
		this.detected = b;
		this.detectionMethod = method;

	}

	/**
	 * @param time
	 * @param diag
	 * @see simcolep.lepra.Morbus#setDiagnosis(double, java.lang.String)
	 */
	public void setDiagnosis(double time, int diag) {
		// do diagnose!
		if (diag < 0)
			throw new SimulationException(PN.TAB + diag);

		// add to disease history
		this.addDiseaseHistory(time, "diagnosis = " + diag);

		// set diagnosis
		this.diagnosis = diag;

	}

	/**
	 * Set follow up number
	 * 
	 * @param newfu
	 */
	public void setFollowUp(int newfu) {
		this.fu = newfu;

	}

	/**
	 *
	 * @param index
	 */
	public void setLayer(int index) {
		this.layer = index;
	}

	/**
	 * Sets the owner of this morbus
	 * 
	 * @see simcolep.lepra.Morbidable#setOwner(simcolep.demo.Person)
	 */
	@SuppressWarnings("unchecked")
	public void setOwner(Person owner) {
		// set the owner
		this.owner = owner;

	}

	/**
	 * Return a string with the data without descriptions
	 * 
	 * @return morbus
	 */

	public String toData() {
		// id
		StringBuffer morbus = new StringBuffer(256);
		morbus.append(this.owner.getID() + PN.TAB);
		// phenotype
		morbus.append(this.getType() + PN.TAB);
		// original phenotype before vaccination
		morbus.append(this.genome + PN.TAB);
		// state
		morbus.append(this.getState() + PN.TAB);
		// level number
		morbus.append("level:" + PN.TAB + (this.getOwner()).getLevel().getID() + "\t"
				+ (this.getOwner()).getLevel().getHabitationSize());// return
		return morbus.toString();
	}

	/**
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {

		String morbus = "Morbus:\t" + this.owner.getID() + PN.TAB + "Layer: " + PN.TAB + this.layer + PN.LINESEPARATOR
				+ "state:\t" + this.state + PN.LINESEPARATOR + "phenotype:\t" + this.phenotype + PN.LINESEPARATOR
				+ "original: \t" + this.genome + PN.LINESEPARATOR + "infection time\t" + this.inftime;
		return morbus;
	}

	/**
	 * 'Proper'treament of a detected case as opposed to prophylactic treatment.
	 *
	 */
	public void treat() {
		// obtain random number generator
		Random rand = Herbarium.rand(Herbarium.DIS);
		// add to the disease history
		this.addDiseaseHistory(Idunn.getCurrentTime(), "treatment");
		// treat
		this.undertreatment = true;
		// determine treatment duration
		Clinic clinic = Idunn.getIdunn().getClinic();
		double treat_duration = clinic.getTreatTime(this);

		this.setTreatmentTime(treat_duration);

		// check whether a relapse is experienced based upon the simulation time
		double prob_relapse = Archimedes.ask(PN.RELAPSECHANCE + getState()).getY(Idunn.getCurrentTime());
		if (rand.nextDouble() < prob_relapse) {

			// schedule the events occuring due to relapse
			this.scheduleRelapse();

		} else {

			// no relapse thus proper treatment occurences
			this.treatwithoutrelapse();
		}

	}

	/**
	 * Set the time of vaccination and:<br>
	 * if method BCGshift == <i>"on"</i>:<br>
	 * Vaccination can lead to a shift in the spectrum:<br>
	 * <li>Types 3 and 4 can go to types 2,1 and 0
	 * <li>Type 2 to 1 and 0
	 * <li>Type 1 to 0 <br>
	 * <br>
	 * Vaccination given to already infected people and giving a shift will lead
	 * to resetting:
	 * <li>latency period
	 * <li>incubation period
	 * <li>self healing time (if applicable) <br>
	 * when the infection time + a period &lt current time, this person will
	 * immediately go in to the state
	 * 
	 * @param time
	 */
	public void vaccinate(double time) {

		if (this.detected)
			return;

		// if revaccination has no effect
		if (!Idunn.getIdunn().getClinic().revaccinate)
			// and this morbus is vaccinated
			if (this.vaccinated)
				return;

		// random number
		Random rand = Herbarium.rand(Herbarium.DIS);

		// disease history
		this.addDiseaseHistory(time, "received BCG");

		// set vaccination time
		this.vaccination_time = time;
		// set vaccinated
		this.vaccinated = true;

		if (Idunn.getIdunn().getClinic().BCGshift) {

			if (this.getState() != State.SUSCEPTIBLE_STATE) {
				// already infected just a fraction will shift
				boolean shift = rand.nextDouble() < Archimedes.askDouble(PN.INFBCGSHIFT + this.getState());

				// if false return
				if (!shift)
					return;
			}

			// will shift to phenotype
			int toType = phenotype - 1;
			while (toType >= 0) {
				if (rand.nextDouble() < Archimedes.askDouble(PN.BCG + phenotype + PN.TO + toType)) {
					// change of phenotype because of vaccination
					this.spectrumShift(toType);

					this.addDiseaseHistory(time, "BCG shifted phenotype to " + this.getType());
					;

					return;
				}
				toType = toType - 1;
			}

			// no shift due to vaccination
			return;
		}
		return;
	}

	/**
	 * Cancel all transmission events.
	 *
	 */
	protected void cancelTransmission() {
		// cancel towards the dwelling
		this.cancelLevel();

		// cancel generalpopulation
		// cancel sourced random mixing event if necessary
		if (this.gpTransmissionEvent != null)
			if (this.gpTransmissionEvent.isScheduled())
				Idunn.getEventManager().cancel(this.gpTransmissionEvent);
		this.gpTransmissionEvent = null;
	}

	/**
	 * Sets the infections state to the value i<br>
	 *
	 * <br>
	 * Infection states are:<br>
	 * <br>
	 * 0: Susceptible<br>
	 * 1: Latent<br>
	 * 2: low infectious undetectable<br>
	 * 3: low infectious detectable<br>
	 * 4: building up infection undetectable<br>
	 * 5: building up infection detectable<br>
	 * 6: higly infectious detectable <br>
	 * 7: never susceptible<b> 8: Recoverd 1 9: Recovered 2<br>
	 *
	 *
	 * @param to
	 * @param from
	 *
	 */
	private void changeState(int from, int to) {
		if (to != this.state.getState())
			// add to history
			this.addDiseaseHistory(Idunn.getCurrentTime(), "changed from " + from + "  to state: " + to);

		// set the state
		this.state = State.state(to);

		// if changeing to this state means symptoms schedule
		// passive detection
		if (this.state.isSymptomatic()) {
			// when under treatment
			if (this.undertreatment) {
				this.schedulePassiveDetection(Idunn.getCurrentTime() + Pythagoras.getDefaultPrecision());

			} else {
				// else
				if (this.detperiod < Idunn.getCurrentTime() || Double.isInfinite(this.detperiod))
					this.detperiod = Idunn.getDetection(this);
				// schedule detection
				double detectionTime = this.getDetectionTime();
				// schedule detection
				if (detectionTime >= Idunn.getCurrentTime()) {
					// set detection false
					this.setDetected(false, null);
					// schedule (new) detection event
					this.schedulePassiveDetection(detectionTime);

				} else
					throw new SimulationException("detection less than no time");

			}

		}

		// notify
		Object[] info = { new Integer(from), new Integer(to) };
		Ratatosk.notify(Message.formulate(Message.CHANGESTATE, info), this, this);
	}

	/**
	 * @param delay
	 */
	private void delay(double delay) {
		if (delay <= Pythagoras.getDefaultPrecision())
			return;
		// cancel the events in the targeted and sourced events
		cancelAll();
		// set the delay of the infection time
		this.inftime = this.inftime + delay;
		// to avoid problems introduce a latent period of such length that the
		// infection time is never in the future
		// even when latency periods are supposed not to exist!
		if (this.inftime > Idunn.getCurrentTime()) {
			// calculate the extra latency period
			double extra_latency = this.inftime - Idunn.getCurrentTime();

			// set infection time to the current time
			this.setInfectionTime(Idunn.getCurrentTime());

			// set the latency period
			this.lat += extra_latency;

		}
		// **this person is susceptible again*/
		if (this.inftime >= this.owner.getDeathTime())
			return;

		// schedule new transmission
		// within the same level
		Idunn.getIdunn().newInfectionInDwelling(this);

		// to the general population
		Idunn.getIdunn().scheduleToGeneralPopulation(this);

		// new detection event
		this.detperiod = Idunn.getDetection(this);

		// direct complications of delay causing event
		if (this.getDetectionTime() < Idunn.getCurrentTime())
			this.detperiod += Idunn.getCurrentTime() - this.getDetectionTime();

		// cannot detect before infection
		if (this.getDetectionTime() < this.inftime)
			throw new SimulationException("cannot detect before infect");

		// schedule detection
		this.schedulePassiveDetection(this.getDetectionTime());

		// add to history
		this.addDiseaseHistory(Idunn.getCurrentTime(), "delayed disease development" + "new infection time:\r\n"
				+ this.inftime + "\t it was:" + (this.inftime - delay));

		// set latent - always latent because infection time

		this.changeState(this.state.getState(), State.LATENT_STATE);

		// set the state index to the index of the latent state
		this.state_index = 1;

		// schedule progression
		this.scheduleStateChange();

	}

	/**
	 * Unschedules all events and then schedules events that are caused by the
	 * relapsing of this morbus
	 *
	 */
	private void scheduleRelapse() {
		// obtain random number generator
		Random rand = Herbarium.rand(Herbarium.DIS);

		// cancel all things that were going to happen to this morbus
		this.cancelAll();

		// determine the time between now and the relapse occuring (i.e. end of
		// asymptomatic time)
		this.relapse_period = Archimedes.askDistribution((PN.RELAPSEPERIOD + this.getState()))
				.drawX(new double[] { rand.nextDouble(), this.getOwner().getAge(Idunn.getCurrentTime()) });

		// if relapse is longer than the life time
		if (this.getOwner().getDeathTime() <= this.relapse_period + Idunn.getCurrentTime()) { // there
																								// will
																								// be
																								// no
																								// relapse
																								// thus
																								// this
																								// morbus
																								// is
																								// cured
			this.treatwithoutrelapse();
			// return
			return;
		}

		// if the relapse time is longer than release from treatment schedule
		// rft
		if (this.relapse_period + Idunn.getCurrentTime() > this.getRFTtime())
			this.scheduleRFT();

		this.undertreatment = true;

		// fraction of all downgrading PB relapsing to MB
		if (this.getType() != Morbus.RELAPSETYPE) {
			if (rand.nextDouble() < Archimedes.askDouble(PN.UPGRADE_AT_RELAPSE)) {
				// report at the clinic (to possibly make shifts in the
				// registers)
				Idunn.getIdunn().getClinic().getRegister().changeType(this, this.phenotype, Morbus.RELAPSETYPE);

				// change the phenotype
				this.phenotype = Morbus.RELAPSETYPE;
			}
		}

		// determine new durations for this diseased object
		// get the durations of the disease peroids
		this.lat = Idunn.getLatency(this);
		this.incPB = Idunn.getIncPB(this);
		this.incMB = Idunn.getIncMB(this);
		this.sh = Idunn.getSelfhealing(this);

		double total_period = this.lat;
		if (!Double.isInfinite(this.incPB))
			total_period += this.incPB;
		if (!Double.isInfinite(this.incMB))
			total_period += this.incMB;
		// the model assumes that within the relapse period all states are
		// re-done i.e. the treatment always renders a person uninfectious,
		// asymptomatic
		// reset the durations to durations adding up to the relapse period
		double time_since_infection = Idunn.getCurrentTime() - this.getInfectionTime();
		// Latency period
		this.lat = time_since_infection + (this.lat / total_period) * relapse_period;
		// incubation to PB
		this.incPB = (this.incPB / total_period) * relapse_period;
		// incubation to MB
		this.incMB = (this.incMB / total_period) * relapse_period;
		// set state index
		this.state_index = 1;

		// current state
		int current_state = this.state.getState();

		this.changeState(current_state, Idunn.getStateSequence(this.phenotype)[state_index]);

		// schedule state change
		this.scheduleStateChange();

		// within level
		Idunn.getIdunn().newInfectionInDwelling(this);
		// to the general population
		Idunn.getIdunn().scheduleToGeneralPopulation(this);
		// add relapse to history
		this.addDiseaseHistory(Idunn.getCurrentTime(),
				"will relapse at: " + (relapse_period + Idunn.getCurrentTime()) + " to type " + this.getType());

		// detection period
		this.setDetected(false, null);

	}

	/**
	 * Schedules release from treatment of if the time until release from
	 * treatment is larger than the current time
	 *
	 */
	private void scheduleRFT() {
		if (Idunn.getCurrentTime() > this.getRFTtime())
			return;

		TreatmentEnd te = new TreatmentEnd(this, this, this.getRFTtime());
		Idunn.getEventManager().schedule(te);
	}

	/**
	 * Creates a StateChange event and schedules it at the next time that this
	 * morbus will change state in normal progress of disease
	 *
	 */
	private void scheduleStateChange() {
		// determine next state
		int next_state = Idunn.getStateSequence(this.phenotype)[this.state_index + 1];
		// time of next state
		double time = -1;
		// detemine the time
		// end of latency period
		if (next_state == State.ASYMPTOMATIC_LOWCONTAGIOUS_STATE || next_state == State.ASYMPTOMATIC_BUILDINGUP_STATE)
			time = this.getLatencyTime();
		// end of incubation to PB
		else if (next_state == State.SYMPTOMATIC_LOWCONTAGIOUS_STATE
				|| next_state == State.SYMPTOMATIC_BUILDINGUP_STATE)
			time = this.getIncPBTime();
		// end of incubation to MB
		else if (next_state == State.SYMPTOMATIC_HIGHCONTAGIOUS_STATE)
			time = this.getIncMBTime();
		else
			throw new SimulationException("Unknown next state: " + next_state);

		// create and schedule event
		StateChange sc = new StateChange(this, this, time);
		// if now or in the past execute otherwise schedule
		if ((time - Idunn.getCurrentTime()) <= Pythagoras.getDefaultPrecision()) {
			sc.execute();
		} else // otherwise
		{
			Idunn.getEventManager().schedule(sc);
		}

	}

	/**
	 * Sets the time of infection to infectiontime. No transmission, incubation
	 * or any other thing is changed. Use method activate to do so.
	 * 
	 * @param infectiontime
	 */
	private void setInfectionTime(double infectiontime) {
		this.inftime = infectiontime;

	}

	/**
	 * 
	 * @param duration_of_treatment
	 */
	private void setTreatmentTime(double duration_of_treatment) {
		this.treat_duration = duration_of_treatment;

	}

	/**
	 * 
	 * Shifts the disease phenotype to toType.<br>
	 * Assumed is that a person that shifts in the spectrum is first put back
	 * into the first state <br>
	 * Includes:
	 * <li>redetermination of periods
	 * <li>new transmission times (if applicable)
	 * <li>new detection time (if applicable) <br>
	 * 
	 * @param toType
	 */
	private void spectrumShift(int toType) {

		// nothing happens if there is no shift (current type = toType)
		if (this.phenotype == toType)
			return;

		// report shift to the Clinic
		Idunn.getIdunn().getClinic().getRegister().changeType(this, this.phenotype, toType);

		// Remove all transmission to the general population and detection
		// events
		this.cancelAll();

		// set phenotype
		this.phenotype = toType;

		// if the toType = immune_type
		// if the new phenotype is immune change the state
		if (this.phenotype == IMMUNE_TYPE) {
			// cancel events
			this.cancelTransmission();

			// cancel next state change
			for (Schedulable s : this.getTargetedEvents(StateChange.class.getName()))
				Idunn.getEventManager().cancel(s);

			// add this to the history
			this.addDiseaseHistory(Idunn.getCurrentTime(), "cured by spectrum shift");

			// set cured
			this.changeState(this.getState(), State.NEVER_SUSCEPTIBLE_STATE);

			this.state_index = 0;

			// cancel state changes
			List<Schedulable> list = this.getTargetedEvents(StateChange.class.getName());

			for (Schedulable s : list)
				Idunn.getEventManager().cancel(s);
			// add to history
			this.addDiseaseHistory(Idunn.getCurrentTime(), "Shifted to immune phenotype");
			// return
			return;
		}

		// finished if not infected
		if (this.getState() == State.SUSCEPTIBLE_STATE)
			return;

		// shift
		this.state_index = 1;

		// determine latency,
		this.lat = Idunn.getLatency(this);
		// incubation,
		this.incPB = Idunn.getIncPB(this);
		this.incMB = Idunn.getIncMB(this);
		// selfhealing
		this.sh = Idunn.getSelfhealing(this);

		// schedule state changes
		this.scheduleStateChange();

		// and detection delay
		this.detperiod = Idunn.getDetection(this);
		double dettime = this.getDetectionTime();
		// detection can now be much earlier than before
		// this means that the patient gets directly complications after event
		// causing shift
		if (dettime <= Idunn.getCurrentTime())
			dettime = Idunn.getCurrentTime();

		this.setDetected(false, null);

		// delete detection event
		this.schedulePassiveDetection(dettime);

		// reschedule transmission events
		// to the general population
		Idunn.getIdunn().scheduleToGeneralPopulation(this);

		// within the own level
		Idunn.getIdunn().newInfectionInDwelling(this);

		return;

	}

	/**
	 * TreatmentEnd is given, rft is scheduled and cure
	 *
	 */
	private void treatwithoutrelapse() {

		// schedule a treatment ending event
		this.scheduleRFT();

		// cure
		this.cure();
	}

}
