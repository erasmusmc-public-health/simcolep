/*
 * This file is part of SIMCOLEP, a simulation model developed by the Department of Public Health, 
 * Erasmus MC, University Medical Center Rotterdam, Rotterdam, The Netherlands.
 *
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/ 
 * or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA
 *
 * You are free to:
 * Share � copy and redistribute the material in any medium or format
 * Under the following terms:
 * Attribution � You must give appropriate credit, provide a link to the license, and indicate if changes were 
 * made. You may do so in any reasonable manner, but not in any way that suggests the licensor endorses
 * you or your use.
 * NonCommercial � You may not use the material for commercial purposes.
 * NoDerivatives � If you remix, transform, or build upon the material, you may not distribute the modified material.
 */

/**
 * created: 06-Apr-2007
 */
package simcolep.lepra;

import java.util.List;
import java.util.ArrayList;
import java.util.Map;

import java.io.FileWriter;
import java.io.IOException;
import java.io.Serializable;

import simcolep.fun2.Archimedes;
import simcolep.tools.PN;


/**
 * @author ir. E.A.J. Fischer
 * @version 1.1
 */
public final class Strain implements Strainable, Serializable {

	private static final String L = " l ";

	private static final String ARCS = "*Arcs";

	private static final String VERTICES = "*Vertices ";

	/**serialVersionUID*/
	private static final long serialVersionUID = 
		-6317990614788147853L;
	
	/**Colour codes as used in Pajek*/
	private static final String[] PAJEKCOLOURS = {
		"Cyan",	"Yellow",	"Red",	"Blue",	"OliveGreen",
		"Purple","Orange", "CadetBlue","TealBlue",
		"LimeGreen","Gray",	"White","Black","Maroon",
		"MidnightBlue",	"Magenta","Dandelion",
		"WildStrawberry","ForestGreen",	"Salmon",
		"LSkyBlue",	"GreenYellow","Lavender",
		"LFadedGreen","LightPurple","CornflowerBlue",
		"LightOrange","Tan","LightCyan","LightGreen",
		"LightYellow","Pink","Gray20","Gray60",
		"Gray40","Gray75",	"Gray10","Gray85","Gray30",
		"Gray70"
	};

	/**representation of an start or import for the arc*/
	public static final int IMPORTARC = 999;

	
	/**Data structure containing start of all Strains*/
	private static List<Strainable> strains = null;

	/**if true save all connection for link*/
	private static boolean link = false;
	
	/**
	 *Initialize a new list of strains
	 */
	public static void initialize()
	{
		strains =  
			new ArrayList<Strainable>();
		
	}
	
	/**add the start node to the list of origin new ArrayList<Strainable>();inal strains*/
	private static void addOrigin(Strainable origin)
	{
		if(link)
			strains.add(origin);
	}
	
	/**return all nodes/vertices and arcs
	 * @return nodesAndArcs
	 */
	@SuppressWarnings("unchecked")
	public static List[] getNodesAndArcs()
	{
		//names of the nodes
		List<Strainable> nodes = 
			new ArrayList<Strainable>();
		
		//arc information
		List<String> arcs = 
			new ArrayList<String>();
	
		for(Strainable original : strains)
		{
			
			//get the list of the next nodes
			List<Strainable> next_strain_nodes = 
				original.getNext();
			
			while(!next_strain_nodes.isEmpty())
			{
				next_strain_nodes =
					Strain.addNodesAndArcs(next_strain_nodes,
						nodes, 
						arcs);
			}
			
		}
		//return all the link
		List[]nodesAndArcs = new List[2];
		nodesAndArcs[0] = nodes;
		nodesAndArcs[1] = arcs;
		return nodesAndArcs;
	}
	/**
	 * 
	 * @param strain_nodes
	 * @param nodes
	 * @param arcs
	 * @return next_nodes
	 */
	private static List<Strainable>addNodesAndArcs(
			List<Strainable>strain_nodes,
			List<Strainable> nodes,
			List<String> arcs)
	{
		List<Strainable> next_nodes = 
			new ArrayList<Strainable>();
		
		for(Strainable s : strain_nodes)
		{
			if(s.getArctype() != Strain.IMPORTARC)
			{
				//add node name to the list
				nodes.add(s);
				//add arc info to the list
				//previous vertex
				StringBuffer arc = new StringBuffer(s.getPrevious().getName());
				//this vertex
				arc.append(PN.SPACE+ s.getName());
				//type of arc
				arc.append(PN.SPACE+Integer.toString(s.getArctype()));
				//label of arc with time between creation of this vertex and previous
				String label = 
					Double.toString(Math.rint(100*(((Strain)s).time-((Strain)s.getPrevious()).time))/100.0);
				arc.append(PN.SPACE +label);
				
				arcs.add(arc.toString());
			}else
			{

				//add node name to the list
				nodes.add(s);
			}
			//add the next nodes to the next_nodes list
			next_nodes.addAll(s.getNext());
			
		}
		//return
		return next_nodes;
	}
	
	/**
	 * Create the link for Pajek as a String
	 * 
	 * @return out
	 */
	@SuppressWarnings("unchecked")
	public static String outputForPajek()
	{
		List[] nodesAndArcs = 
			Strain.getNodesAndArcs();
		
		//as the identity number are often too large we rename the nodes
		
		
		StringBuffer out =new StringBuffer( VERTICES+nodesAndArcs[0].size()+PN.LINESEPARATOR);
		
		int vertex_index = 1;
		
		Map<String, Integer> vertex_indices = 
			new java.util.HashMap<String, Integer>();
		
		for(Object node : nodesAndArcs[0])
		{
			//cast
			Strain strain = (Strain) node;
			
			vertex_indices.put(strain.getName(),
					new Integer(vertex_index));
			
			
			//index name and other information
			out.append(vertex_index +PN.SPACE + strain.toPajekVertex()+PN.LINESEPARATOR);
			
			vertex_index++;
		}
		
		out.append(ARCS + PN.LINESEPARATOR);
		for(Object arc : nodesAndArcs[1])
		{
			//cut the arc string into an array
			String[] arc_array =
				((String)arc).split(PN.SPACE);
			
			//link
			if(!arc_array[0].equals(arc_array[1]))
			{
				//add start vertex
				out.append(vertex_indices.get(arc_array[0])+PN.SPACE);
				//to vertex
				out.append(vertex_indices.get(arc_array[1])+PN.SPACE);
				//add one as 0 would lead to no arc in pajek
				out.append((Integer.parseInt(arc_array[2])+1)+PN.SPACE);
				//	lable
				out.append(L + (arc_array[3]+PN.LINESEPARATOR));
			}
		}
		//return 
		return out.toString();
	}
	
	/**
	 * Write the link for Pajek to the specified file
	 * 
	 * @param writer
	 * @throws IOException 
	 */
	@SuppressWarnings("unchecked")
	public static void outputForPajek(FileWriter writer) throws IOException
	{
		List[] nodesAndArcs = 
			Strain.getNodesAndArcs();
		
		//as the identity number are often too large we rename the nodes
		writer.append(VERTICES+nodesAndArcs[0].size()+PN.LINESEPARATOR);
		
		int vertex_index = 1;
		
		Map<String, Integer> vertex_indices = 
			new java.util.HashMap<String, Integer>();
		
		for(Object node : nodesAndArcs[0])
		{
			//cast
			Strain strain = (Strain) node;
			
			vertex_indices.put(strain.getName(),
					new Integer(vertex_index));
			
			
			//index name and other information
			writer.append(vertex_index +PN.SPACE + strain.toPajekVertex()+PN.LINESEPARATOR);
			
			vertex_index++;
		}
		
		writer.flush();
		
		writer.append(ARCS + PN.LINESEPARATOR);
		for(Object arc : nodesAndArcs[1])
		{
			//cut the arc string into an array
			String[] arc_array =
				((String)arc).split(PN.SPACE);
			
			//link
			if(!arc_array[0].equals(arc_array[1]))
			{
				//add start vertex
				writer.append(vertex_indices.get(arc_array[0])+PN.SPACE);
				//to vertex
				writer.append(vertex_indices.get(arc_array[1])+PN.SPACE);
				//add one as 0 would lead to no arc in pajek
				writer.append((Integer.parseInt(arc_array[2])+1)+PN.SPACE);
				//lable
				writer.append(L + arc_array[3]+PN.LINESEPARATOR);
				
			}
		}
		writer.flush();
	}
	
	/**path of the pathogen */
	private List<Strainable> next = null;

	/**name of this strain node*/
	private String name = null;

	/**arc type*/
	private int arc = -1;

	/**previous node*/
	private Strainable previous = null;
	
	/**type of morbus*/
	private int type = -1;

	private double time = 0;

	private int layer = 0;

	private String origin = null;
	
	/**
	 * 
	 */
	public Strain(String origin, Morbus m, double t) {
		super();
		
		this.next = 
			new ArrayList<Strainable>();
		
		this.name = m.getID();
		
		this.layer = m.getLayer();
		
		this.type = m.getType();
		
		this.time = t;
		
		this.arc = Strain.IMPORTARC;
		
		this.origin = origin;
		
		Strain.addOrigin(this);
	}
	

	/**
	 * 
	 *
	 */
	public Strain(Strainable s, 
			Morbus m, 
			int arc_type, 
			double t)
	{
		super();
		this.origin = s.getOrigin();
		
		this.next = 
			new ArrayList<Strainable>();
		
		this.addtoPath(s, m,arc_type, t);
	}
	
	/**
	 * Add next strain-node to the list of nodes originating from this node 
	 * 
	 */
	public void add(Strainable next)
	{
			this.next.add(next);
	}
	/**
	 * @see simcolep.lepra.Strainable#addtoPath(Strainable, simcolep.lepra.Morbus, int, double)
	 */
	private void addtoPath(Strainable s, Morbus m, int arc_type, double t) {
		//set the name of this node		
		this.name = 
			m.getID();
		
		this.layer = 
			m.getLayer();
		
		//set the arc type how this node is connected to the previous
		this.arc = arc_type; 
		
		//type of morbus
		this.type = m.getType();
		
		//double linked list to make easier in the future
		if(Strain.link)
			this.previous = s;
		
		//set time of event causing this
		this.time = t;
		
		//add this strain to previous for link
		if(Strain.link)
			s.add(this);
	
	}

	/**
	 * @see simcolep.lepra.Strainable#getPath()
	 */
	@SuppressWarnings("unchecked")
	public List getPath() {
		ArrayList<String> path 
				= new ArrayList<String>();
		
		Strain s = this; 
		
		while(s.previous != null)
		{
			path.add("[" + s.arc+PN.SPACE+s.name+"]");
			s = (Strain)s.previous;
		}
		
		return path;
	}

	/**
	 * @see simcolep.lepra.Strainable#getArctype()
	 */
	public int getArctype() {
		return this.arc;
	}

	/**
	 * @see simcolep.lepra.Strainable#getName()
	 */
	public String getName() {
		return this.name + "_" + this.layer;
	}
	/**
	 * 
	 * @return
	 */
	public String getOrigin()
	{
		return this.origin;
	}
	/**
	 * 
	 * @return type
	 */
	public int getType()
	{
		return this.type;
	}
	/**
	 * @see simcolep.lepra.Strainable#getNext()
	 */
	public List<Strainable> getNext() {
		
		return this.next;
	}

	/**
	 * @see simcolep.lepra.Strainable#getPrevious()
	 */
	public Strainable getPrevious()
	{
		return this.previous;
	}
	
	/**
	 * @see simcolep.lepra.Strainable#getStrainParameter(java.lang.String)
	 */
	public Number getStrainParameter(String param) {
			return null;
	}
	
	/**
	 * @see java.lang.Object#toString()
	 */
	public String toString()
	{
		String s = "";
		for(Object id : this.getPath())
			s+=id+" <- ";
		
		return s;
	}
	
	/**
	 * Create a string representing this strain node as a Pajek Vertex 
	 * @return
	 */
	public String toPajekVertex()
	{
		//name of the vertex
		String vertex = this.getName()+ PN.SPACE;
		//x, y and z coordinate where x is time/runtime , y = type /number of types and z = 0.5 
		//x
		vertex += (Math.round(1000*this.time)/Archimedes.askDouble(PN.RUNTIME))/1000 +PN.SPACE;
		//y
		vertex += this.type/Archimedes.askDouble(PN.DISEASETYPES) + PN.SPACE;
		//z
		vertex += Math.min(Math.abs((float)this.arc)/(Archimedes.askDouble(PN.LEVELDEPTH)+1), 1.0) + PN.SPACE;
		
//		determine vertex colour based upon type
	 	int ctype = this.getType();
		
		if(ctype < 0)
			ctype = PAJEKCOLOURS.length-1;
		if(this.arc == Strain.IMPORTARC)
			ctype = 10+ctype;
		
		//add colour coding		
		vertex += "ic "+ PAJEKCOLOURS[ctype] + " bc "+ PAJEKCOLOURS[ctype];
		
		return vertex;
	}
	
	/**
	 * Time of infection
	 * @return time
	 */
	public double getTime() {
		return this.time;
	}

	/**
	 * Turns link possibility on, otherwise Strain will not collect initial strains and will not link strain-elements 
	 * 
	 * @param b
	 */
	public static void keepLink(boolean b) {
		Strain.link  = b;
	}
	

}
