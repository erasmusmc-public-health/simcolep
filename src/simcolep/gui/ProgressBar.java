/*
 * This file is part of SIMCOLEP, a simulation model developed by the Department of Public Health, 
 * Erasmus MC, University Medical Center Rotterdam, Rotterdam, The Netherlands.
 *
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/ 
 * or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA
 *
 * You are free to:
 * Share � copy and redistribute the material in any medium or format
 * Under the following terms:
 * Attribution � You must give appropriate credit, provide a link to the license, and indicate if changes were 
 * made. You may do so in any reasonable manner, but not in any way that suggests the licensor endorses
 * you or your use.
 * NonCommercial � You may not use the material for commercial purposes.
 * NoDerivatives � If you remix, transform, or build upon the material, you may not distribute the modified material.
 */
package simcolep.gui;

import java.awt.Dimension;

import javax.swing.BoxLayout;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JLabel;

import nl.mgz.simulation.discreteevent.CalendarQueueEventManager;

import simcolep.tools.message.Eavesdropper;
import simcolep.tools.message.Message;
import simcolep.tools.message.Ratatosk;

/**
 * @author ir E.A.J. Fischer
 *
 */
public final class ProgressBar 
				extends JPanel 
				implements Eavesdropper{

	private static final String SPACE = " ";

	/**
	 * 
	 */
	private static final long serialVersionUID = 698853074091958381L;
	
	/**update bar after x messages*/
	private static final int STEP = 1000;

	private static final String LINESEPARATOR = 
		System.getProperty("line.separator");

	private static final String RUNNING = "Running    : ";
	private static final String STARTED = "Started at    : ";

	private static final String SAVING = "Saving : ";	
	/**maximum of the progressbar*/
	private int max = 0;
	
	/**number of messages past */
	private int counter = 0;
	
	
	
	/**progress bar*/
	private JProgressBar jpb = null;

	/**saving label*/
	private JLabel save1 = null;
	private JLabel save2 = null;

	/**Currently running input*/
	private String input = null;

	private String[] run_info = null;

	/**default dimensions*/
	private static Dimension DEFAULT_SIZE = new Dimension(200,60);
	

	

	/**
	 * 
	 */ 
	public ProgressBar(int max, String input) {
		super();
		this.input  = input.trim();
		this.max = max;
		this.create();
		
	}
/**
 * 
 *
 */
	private void create() {
		///preferred size
		this.setPreferredSize(DEFAULT_SIZE);
		//set 
		this.setVerifyInputWhenFocusTarget(true);
		//set BoxLayout
		this.setLayout(new BoxLayout(this,BoxLayout.Y_AXIS));
		//add the bar
		this.addBar();
		//the label
		this.save1 = new JLabel("INITIALIZING");
		this.add(this.save1);
		this.save2 = new JLabel(SPACE);
		this.add(this.save2);
		//register
		Ratatosk.register(this);
		
	}

	/**
	 * Adds a progress bar to the panel
	 *  
	 */
	private void addBar() {
	
		//create bar
		this.jpb = new JProgressBar();
		//set maximum
		jpb.setMaximum(this.max);
		//set determinated
		jpb.setIndeterminate(false);
		//show the current value
		jpb.setStringPainted(true);
		//make visible
		jpb.setVisible(true);
		
		//add to this pane;
		this.add(jpb);
		
					
	}
	
	
	
	/**
	 * @see simcolep.tools.message.Eavesdropper#listenIn(simcolep.tools.message.Message)
	 */
	public void listenIn(Message message) {
		//SET THE PROGRESS BAR TO THE RIGHT LENGTH OF THE RUN
		if(message.getMessage().contains(Message.LAUNCHINFO))
		{
			String s = message.getMessage();
											
			//first statement between { and } is the length of the run
			s = s.substring(s.indexOf("{")+1,s.indexOf("}"));
			
			//change the text
			this.run_info = (message.getMessage().split(LINESEPARATOR));
			
			this.save1.setText(RUNNING +this.input+" : " + run_info[2].substring(1+run_info[2].indexOf("{"),run_info[2].indexOf("}")));
			this.save2.setText(STARTED +run_info[3].substring(1+run_info[3].indexOf("{"),run_info[3].indexOf("}")));
			
			//set the maximum
			this.max = ((int)Double.parseDouble(s));
			
			//add the bar
			this.reset();
			
			return;
		}else if (message.getMessage().contains(Message.STOP))
		{
			
			StringBuffer strbf = new StringBuffer(SAVING +this.input);
			if(this.run_info!=null)
				strbf.append(" : " + run_info[2].substring(1+run_info[2].indexOf("{"),run_info[2].indexOf("}")));
			//change the text
			this.save1.setText(strbf.toString());
			this.save2.setText(SPACE);
						
			this.save1.repaint();
			this.save2.repaint();
			
			return;
		}
		
		
		//count the number of events passed by
		counter++;
		
		if(counter%STEP==0) 
			{
				int time = (int)CalendarQueueEventManager.getInstance().getTime();
				jpb.setValue(time);
				return;
			}
		
	}

	/**
	 * Reset the progress bar
	 *
	 */
	private void reset() {
		this.jpb.setMaximum(this.max);
		this.jpb.setValue(0);
		this.jpb.setVisible(true);
		
	}
}
