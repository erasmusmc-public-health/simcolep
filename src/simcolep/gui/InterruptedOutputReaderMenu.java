/*
 * This file is part of SIMCOLEP, a simulation model developed by the Department of Public Health, 
 * Erasmus MC, University Medical Center Rotterdam, Rotterdam, The Netherlands.
 *
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/ 
 * or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA
 *
 * You are free to:
 * Share � copy and redistribute the material in any medium or format
 * Under the following terms:
 * Attribution � You must give appropriate credit, provide a link to the license, and indicate if changes were 
 * made. You may do so in any reasonable manner, but not in any way that suggests the licensor endorses
 * you or your use.
 * NonCommercial � You may not use the material for commercial purposes.
 * NoDerivatives � If you remix, transform, or build upon the material, you may not distribute the modified material.
 */

/**
 * created: 11-Dec-2006
 */
package simcolep.gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;

import javax.swing.JMenu;
import javax.swing.JMenuItem;

/**
 * @author ir. E.A.J. Fischer
 * @version 1.0
 */
public class InterruptedOutputReaderMenu {

	/**Only one simulation menu	 */
	private static InterruptedOutputReaderMenu instance 
					= 	null;
	
	/**
	 * read menu item prompt a file chooser by means of InterruptedOutputReader
	 * Select and read files that were not necesarrily readable at for hand
	 * @author ir. E.A.J. Fischer
	 * @version 1.0
	 */
	protected class READ 
			extends JMenuItem 
				implements ActionListener
	{
		/**	 */
		private static final long serialVersionUID = 1998660564070558282L;

		/**
		 * 
		 *
		 */
		protected READ()
		{
			super("READ");
			this.addActionListener(this);
			this.setMnemonic(KeyEvent.VK_R);
		}

		/**
		 * 
		 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
		 */
		public void actionPerformed(ActionEvent e) {
			//run the simulation
			this.setEnabled(false);
			//run in separate thread
			Runnable r = new Runnable() {
                public void run() {
                	try{
                	    	new simcolep.tools.io.InterruptedOutputReader().gui();
                	}catch(Exception e) 
                	{
                		MainFrame.errorMessage(e);
                	}
                }
               };
			
               //run
               r.run();
                              
               //enable button
               this.setEnabled(true);
        	
		}
		
		
	}
	
	
	/**
	 * Singleton
	 * @return
	 */
	private static InterruptedOutputReaderMenu getInstance()
	{
		if(instance == null) 
			instance = new InterruptedOutputReaderMenu();
		
		return instance;
	}
	
	/**
	 * 
	 * @return menu
	 */
	public static JMenu create()
	{
		JMenu menu = new JMenu("Output reader");
			
		//short key
		menu.setMnemonic(KeyEvent.VK_O);
		
		//add run 'button'
		menu.add(getInstance().new READ());
			
		//return menu
		return menu;
	}
	
	/**
	 * 
	 *
	 */	
	private InterruptedOutputReaderMenu()
	{
		
	}
}
