/*
 * This file is part of SIMCOLEP, a simulation model developed by the Department of Public Health, 
 * Erasmus MC, University Medical Center Rotterdam, Rotterdam, The Netherlands.
 *
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/ 
 * or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA
 *
 * You are free to:
 * Share � copy and redistribute the material in any medium or format
 * Under the following terms:
 * Attribution � You must give appropriate credit, provide a link to the license, and indicate if changes were 
 * made. You may do so in any reasonable manner, but not in any way that suggests the licensor endorses
 * you or your use.
 * NonCommercial � You may not use the material for commercial purposes.
 * NoDerivatives � If you remix, transform, or build upon the material, you may not distribute the modified material.
 */

/**
 * created: 26-Jul-2006
 */
package simcolep.gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;

import javax.swing.*;

/**
 * @author ir. E.A.J. Fischer
 * @version 1.0
 */
public class SimulationMenu {

	/**Only one simulation menu	 */
	private static SimulationMenu instance 
					= 	null;
	
	/**
	 * 
	 * @author ir. E.A.J. Fischer
	 * @version 1.0
	 */
	protected class RUN 
			extends JMenuItem 
				implements ActionListener
	{
		/**	 */
		private static final long serialVersionUID = 1998660564070558282L;

		/**
		 * 
		 *
		 */
		protected RUN()
		{
			super("RUN");
			this.addActionListener(this);
			this.setMnemonic(KeyEvent.VK_R);
		}

		/**
		 * 
		 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
		 */
		public void actionPerformed(ActionEvent e) {
			//run the simulation
			this.setEnabled(false);
			//run in separate thread
			Runnable r = new Runnable() {
                public void run() {
                	MainFrame.main(new String[0]);
                	
                }
               };
			
               //run
               r.run();
               
               //enable button
               this.setEnabled(true);
        	
		}
		
		
	}
	/**
	 * 
	 * @author ir. E.A.J. Fischer
	 * @version 1.0
	 */
	protected class ABOUT extends JMenuItem implements ActionListener
	{
		/**
		 * 
		 */
		private static final long serialVersionUID = -1383212443075886678L;

		/** message that will be displayed */
		private static final String MESSAGE = "Author: E.A.J. Fischer \r\n" +
				"e-mail: e.a.j.fischer@erasmusmc.nl\r\n" +
				"www:  http://mgzlx4.erasmusmc.nl/pwp/?eajfischer " ;
		/**
		 * 
		 *
		 */
		public ABOUT()
		{
			super("About...");
			this.addActionListener(this);
		}

		/**
		 * 
		 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
		 */
		public void actionPerformed(ActionEvent e) {
			JOptionPane.showMessageDialog(new JDialog(),
					MESSAGE, 
					"About SIMCOLEP...",
					JOptionPane.INFORMATION_MESSAGE);
			
		}
		
		
	}
	/**
	 * 
	 */
	private static final long serialVersionUID = 3124960567473299047L;

	/**
	 * Singleton
	 * @return
	 */
	private static SimulationMenu getInstance()
	{
		if(instance == null) 
			instance = new SimulationMenu();
		
		return instance;
	}
	
	/**
	 * 
	 * @return menu
	 */
	public static JMenu create()
	{
		JMenu menu = new JMenu("Simulation");
			
		//short key
		menu.setMnemonic(KeyEvent.VK_S);
		
		//add run 'button'
		menu.add(getInstance().new RUN());
		
		//add about 
		menu.add(getInstance().new ABOUT());
		
		//return menu
		return menu;
	}
	
	/**
	 * 
	 *
	 */	
	private SimulationMenu()
	{
		
	}

	

}
