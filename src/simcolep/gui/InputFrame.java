/*
 * This file is part of SIMCOLEP, a simulation model developed by the Department of Public Health, 
 * Erasmus MC, University Medical Center Rotterdam, Rotterdam, The Netherlands.
 *
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/ 
 * or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA
 *
 * You are free to:
 * Share � copy and redistribute the material in any medium or format
 * Under the following terms:
 * Attribution � You must give appropriate credit, provide a link to the license, and indicate if changes were 
 * made. You may do so in any reasonable manner, but not in any way that suggests the licensor endorses
 * you or your use.
 * NonCommercial � You may not use the material for commercial purposes.
 * NoDerivatives � If you remix, transform, or build upon the material, you may not distribute the modified material.
 */

package simcolep.gui;


import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;


import simcolep.sim.Main;
import simcolep.tools.xmlparser.XMLInput;

/**
 * Creates a frame with tabs for each input file a separate tab. Input can be changed and saved.
 * 
 * @author ir. E.A.J. Fischer
 * @version 0.2 -- Last Modified: 16-feb-2006
 */
public class InputFrame 
		extends JFrame 
			implements ActionListener{

	private static final String EXIT_TEXT = "exit"; //$NON-NLS-1$

	private static final String SAVE_TEXT = "save"; //$NON-NLS-1$

	/**
	 * 
	 */
	private static final long serialVersionUID = -5247931300311985165L;
	
	//the pane
	private JTabbedPane tpane = null;

	/**
	 * 
	 * @param input
	 * @throws Exception 
	 * @throws Exception 
	 */
	public InputFrame(List<File>input) throws Exception 
	{
		super("SIMCOLEP ~ INPUT");
		this.createTabs(input);
	}
	
	/**
	 * Create the inputframe and show it.
	 *
	 */
	public void createAndShow()
	{
		
	}
	/**
	 * 
	 * @param input
	 * @throws Exception 
	 */
	private void createTabs(List<File>input) throws Exception
	{
		this.tpane = new JTabbedPane();
		
		for(File file : input)
		{
			if(file.getName().contains("xml"))
			{
				XMLInput xml = new XMLInput(file);
				Map<String,Double> param = xml.getParameters();
				
				JPanel panel = new JPanel();
				panel.setAlignmentX(JFrame.CENTER_ALIGNMENT);
				panel.setAlignmentY(JFrame.CENTER_ALIGNMENT);
				for(String s : param.keySet())
				{
					
					panel.add(this.createSlider(s,param.get(s)));
				}
				
				tpane.addTab(file.getName(),panel);
			}
		}
		this.add(tpane);
	}
	
	/**
	 * Will not create a slider but a label
	 * @param s
	 * @param current
	 * 
	 * @return
	 */
	private JComponent createSlider(String s, Double current)
	{
		JLabel label = new JLabel(s);
		
		return label;
	}
	
	/**
	 * 
	 * @param args
	 * @throws Exception 
	 * @throws Exception 
	 */
	public static void main(String[] args) throws Exception 
	{
		//get the files
		XMLInput xml = new XMLInput(Main.DEFAULTINPUTFILE);
		
		List<File> list = new LinkedList<File>(xml.getFiles().values());
		
		InputFrame i = new InputFrame(list);
		
		i.setMaximumSize(new Dimension(1000,800));
		
		i.setSize(600,400);
		
		i.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		i.setVisible(true);
	}
	

	/*
	 *  (non-Javadoc)
	 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
	 */
	public void actionPerformed(ActionEvent event) {
		String actioncommand =event.getActionCommand(); 
		if(actioncommand.equalsIgnoreCase(SAVE_TEXT))
		{
			this.saveAs();
		}else if(actioncommand.equalsIgnoreCase(EXIT_TEXT))
		{
			this.exit();
		}
				
		
	}

	/**
	 * Discard everything and set invisible
	 *
	 */
	private void exit() {
		//set no longer visible
		this.setVisible(false);
		//remove the pane
		this.tpane = null;
		
	}

	/**
	 * not yet implemented
	 *
	 * Saving input as... something else 
	 */
	private void saveAs() {
		throw new IllegalArgumentException("not yet implemented");		
	}
}
