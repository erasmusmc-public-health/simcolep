/*
 * This file is part of SIMCOLEP, a simulation model developed by the Department of Public Health, 
 * Erasmus MC, University Medical Center Rotterdam, Rotterdam, The Netherlands.
 *
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/ 
 * or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA
 *
 * You are free to:
 * Share � copy and redistribute the material in any medium or format
 * Under the following terms:
 * Attribution � You must give appropriate credit, provide a link to the license, and indicate if changes were 
 * made. You may do so in any reasonable manner, but not in any way that suggests the licensor endorses
 * you or your use.
 * NonCommercial � You may not use the material for commercial purposes.
 * NoDerivatives � If you remix, transform, or build upon the material, you may not distribute the modified material.
 */
package simcolep.gui;

//imports


import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import java.io.File;
import java.io.FileNotFoundException;

import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;


import simcolep.exceptions.SimulationException;
import simcolep.sim.Main;

import simcolep.tools.PN;
import simcolep.tools.io.SimulationLog;
import simcolep.tools.xmlparser.XMLInput;


/**
 * Main graphical user interface for the simcolep simulation program
 * 
 * @author ir E.A.J. Fischer
 * @version 1.3
 */
public final class MainFrame extends JFrame {

	private static final String RETURN_TEXT = "RETURN"; //$NON-NLS-1$



	private static final String EXIT_TEXT = "EXIT RUN"; //$NON-NLS-1$



	private static final String RUNMENU_TEXT = "RUN"; //$NON-NLS-1$

	/**
	 * Exiot menu item
	 * @author ir. E.A.J. Fischer
	 * @version 1.1
	 */
	private class Exit 	
			extends JMenuItem 
			implements ActionListener {

	
		/***/
		private static final String EXIT = EXIT_TEXT; //$NON-NLS-1$
		/**
		 * 
		 */
		private static final long serialVersionUID = -225435840268827644L;
		/**
		 * 
		 */
		public Exit() {
			super(EXIT);
			super.addActionListener(this);
			
		}

		/**
		 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
		 */
		public void actionPerformed(ActionEvent arg0){
			//disable further pushing of this button
			this.setEnabled(false);
						
			//let main stop
			Main.stop();
			
			//close
			MainFrame.close();
		
			
		}
	}	
		
			
		
	
	
	
	
	/**
	 * 
	 * @author ir. E.A.J. Fischer
	 * @version 1.0
	 */
	private class RunMain extends Thread
	{
		//arguments
		private transient String[] args = null;
		
		 
		/**
		 * 
		 * @see java.lang.Runnable#run()
		 */
		public void run() {
			try{
							
		        //start 
		        XMLInput start = null;
		              
			        //determine the start of simulation input file
			        String filename = null;
			        String path = null;
			        if(args.length==0) 
			        	filename = Main.DEFAULTINPUTFILE;
			        if(args.length==1) 
			        	filename = args[0];
			        if(args.length >=2)
			        	{
			        	 filename =args[1];
			        	 path = args[0];
			        	}
			        try{
			        	if(path == null)
			        		start = new XMLInput(filename);
			        	else 
			        		start = new XMLInput(path, filename);
		        }catch (FileNotFoundException e)
		        {
		        	String searches  = null;
		        	if(path!= null)
		        		searches = path + File.separator+filename;
		        	else 
		        		searches = XMLInput.inpath + File.separator+filename;
		       
		        	SimulationException se = 
		        		new SimulationException( 
		        			SimulationException.SIMIN_FILE_NOT_FOUND_MESSAGE+
		        			PN.LINESEPARATOR
		        			+searches+PN.LINESEPARATOR+e.getMessage());
		     
		        	
		        	//return
		        	MainFrame.errorMessage(se);
		        }
		        
		        //check if a gui is wanted 
		        GUI 
		        	= start.getBoolean(PN.GUI);
		        
		        //set dialog handling
		        dialog 
		        	= start.getBoolean(PN.DIALOG);
		        
		        //record time 
		        TIME 
		        	= start.getBoolean(PN.MEASURE_TIME);
		        
		        //gui options
		        if(GUI)
		        {
		        	MainFrame.inputfilename = filename;
		        	//Schedule a job for the event-dispatching thread:
		            //creating and showing this application's GUI.
		            javax.swing.SwingUtilities.invokeLater(new Runnable() {
		                public void run() {
		                	try{
		                		//Mainframe created and shown.
		        	        MainFrame.createAndShow();
		        	        
		        	        }
		        	        catch (Exception e)
		        			{
		        	        	errorMessage(e);
		        	        }
		                }
		            });
		          
		        }
		        
		        
		    	//the actual simulation program is accessed
		        try{
		        	if(!GUI)
		        		System.out.println(PN.VERSION_NAME+"\n");
				        //initialise settings
				        Main.main(args);
				       				       
				         //crush
				        Main.crushMain();
			      
				        
		        } catch(Exception e)
		        {
		        	errorMessage(e);
		        }	      
		       
		        
		        //close the window
		        MainFrame.close();
		        
			}catch (Throwable e)//all exceptions are to be cought 
			{
				errorMessage(e);
				}
			
		}
		
		/**
		 * @param args
		 */
		public void setArguments(String[] args0)
		{
			this.args = args0;
		}
	
	}
	/**serial version UID*/
	private static final long serialVersionUID = 3689427227191332935L;

	
	
	/**dimensions of the frame when opened*/
	private static final Dimension FRAME_DIMENSION 
						= new Dimension(425,110);

	/**progress bar dimension*/
	private static final Dimension BAR_DIMENSION 
						= new Dimension(400,250);
	
	/**main frame*/
	private static MainFrame frame = null;
	
	/**menu*/
	private static JMenuBar menubar = null;
	
	/**GUI on or off*/
	protected static boolean GUI = false;
	
	/**GUI dialog message yes/no*/
	protected static boolean dialog = false;
	/**time measurement*/
	protected static boolean TIME = false;
	/***/
	private static boolean end = false;



	private static String inputfilename = null;
	/**
	 * @param string 
	 * 
	 */
	private MainFrame(String string) {
		super(string);
		
	}
	
	/**
	 * Creates a frame and shows it
	 * @param args 
	 * @throws Exception
	 *
	 */
	private static void createAndShow() throws Exception
	{
		//create the frame
		if(frame == null)
			frame = new MainFrame(PN.VERSION_NAME);
		//always ontop of other programs
		frame.setAlwaysOnTop(false);
		
		//dimensions
		frame.setSize(MainFrame.FRAME_DIMENSION);
		//frame.setMinimumSize(minimized);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		frame.setLocation(600,600);
		
		
		//create a progressbar
		ProgressBar pb = new ProgressBar(10, MainFrame.inputfilename );
		pb.setVisible(true);
		pb.setSize(MainFrame.BAR_DIMENSION);
		
		//add to the frame
		//frame.rootPane.setPreferredSize(pb.getSize());
		frame.add(pb);
		
		//add the program menu to the frame
		menubar = createMenuBar();
		frame.setJMenuBar(menubar);
		
		frame.setVisible(true);
		
	}

	/**
	 * 
	 * @return
	 */
	private static JMenuBar createMenuBar() {
		//bar
		JMenuBar bar = new JMenuBar();
		
		//file menu
		JMenu file = new JMenu(RUNMENU_TEXT);
		//exit button
		Exit exit = frame.new Exit(); 
		//add the exit button
		file.add(exit);
		//add menu
		bar.add(file);
		bar.setVisible(true);
		//return menu
		return bar;
	}

	/**
	 * 
	 * @param args 
	 * 
	 */
	public static void main(String[] args) {
		
		//frame
		if(frame == null)
			frame = new MainFrame(PN.VERSION_NAME);
		
		//running from this main thread means it ends after this
		MainFrame.end = true;
		
		//create thread
		RunMain runmain = 
			new MainFrame(new String()).new RunMain();
				
		//set arguments
		runmain.setArguments(args);
		
		//run
		runmain.start();
			
		//return
		return;
			
	}
		

	
	/**
	 * Produce an Message frame that reports the dialog and after confirmation end the program
	 * 
	 * 
	 * @param e
	 */
	protected static void errorMessage(Throwable e) {
		
		//create dialog message
		String message = e.toString()+PN.LINESEPARATOR;
			
		//Get the trace
		StackTraceElement[] ste = e.getStackTrace();
		//keep size some what within bounds
		int x = 0;
		
		for(StackTraceElement el : ste)
		{
			x++;
			message+=PN.LINESEPARATOR+el.toString();
			if (x > 15)
			{
				message+=PN.LINESEPARATOR+"........"+PN.LINESEPARATOR;
				message+=ste[ste.length-2]+PN.LINESEPARATOR;
				message+=ste[ste.length-1];
				break;
			}
				
		}
		
		//only show when dialogs are requested
		if(dialog)
		{
				//the parent component 
			JDialog dialog = new JDialog();
			dialog.setAlwaysOnTop(true);
			//show dialog message
			String[] options = {EXIT_TEXT,RETURN_TEXT};
			int n = JOptionPane.showOptionDialog(dialog, 
					message,
					"An Error occured while running SIMCOLEP",
					JOptionPane.YES_NO_OPTION,
					JOptionPane.ERROR_MESSAGE,
					null,
					options,
					options[0]);
		 
			 //EXIT close program
			if(n == JOptionPane.YES_OPTION)
			{
				//create log
				SimulationLog.speciallog(SimulationLog.ERRORLOG,
								message, 
								false,
								true, true);
				//close
				SimulationLog.closeSpecial(true);
				
				//and exit the program
				System.exit(-1);
			}
			else
				return;
		}
		else
		{
			
			//create log
			SimulationLog.speciallog(SimulationLog.ERRORLOG,
					message, 
					true,
					true, 
					true);
			SimulationLog.closeSpecial(true);
			//short messages to the screen
			if(message.length()> 500)
				message = new String("");
			
			//system.out
			System.out.println("Error: log created" 
					+ PN.LINESEPARATOR 
					+ SimulationLog.ERRORLOG 
					+PN.LINESEPARATOR
					+PN.LINESEPARATOR 
					+ message);
			//and exit the program
			System.exit(-1);
		}
		
	}

	/**
	 * close the frame
	 *
	 */
	private static void close() {
		
		//no frame return
		if(frame==null)
			return;
		
		//close the frame
		frame.dispose();
		
		//if ending in mainframe
		if(MainFrame.end)
			System.exit(0);
		else
			return;
		
	}
	
	/**
	 * 
	 *
	 */
	public static void repaintMain()
	{
		
		frame.repaint();
		frame.setVisible(true);
	}

	
}
