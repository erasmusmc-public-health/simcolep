/*
 * This file is part of SIMCOLEP, a simulation model developed by the Department of Public Health, 
 * Erasmus MC, University Medical Center Rotterdam, Rotterdam, The Netherlands.
 *
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/ 
 * or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA
 *
 * You are free to:
 * Share � copy and redistribute the material in any medium or format
 * Under the following terms:
 * Attribution � You must give appropriate credit, provide a link to the license, and indicate if changes were 
 * made. You may do so in any reasonable manner, but not in any way that suggests the licensor endorses
 * you or your use.
 * NonCommercial � You may not use the material for commercial purposes.
 * NoDerivatives � If you remix, transform, or build upon the material, you may not distribute the modified material.
 */
package simcolep.gui;

import java.awt.Dimension;

import javax.swing.*;
/**
 * 
 * @author ir. E.A.J. Fischer
 * @version 1.0
 */
public class TabbedPaneProb {

	/**
	 * 
	 *
	 */
	public TabbedPaneProb() {
		super();
		
	}

	/**
	 * create and set visible the tabbed pane prob
	 *
	 */
	public static void createAndShow()
	{
		JTabbedPane tpane = new JTabbedPane();
		tpane.addTab("first", new JPanel().add(new JLabel("1st tab")));
		
		tpane.addTab("second", new JPanel().add(new JLabel("2nd tab")));
		tpane.addTab("third", new JPanel().add(new JLabel("3rd tab")));
		tpane.setAlignmentX(JFrame.CENTER_ALIGNMENT);
		tpane.setVisible(true);
		
		JFrame frame = new JFrame("test");
		frame.setSize(new Dimension(300,100));
		frame.add(tpane);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
		
		
	}
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		TabbedPaneProb.createAndShow();
	}

}
