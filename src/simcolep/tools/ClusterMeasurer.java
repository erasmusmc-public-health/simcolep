/*
 * This file is part of SIMCOLEP, a simulation model developed by the Department of Public Health, 
 * Erasmus MC, University Medical Center Rotterdam, Rotterdam, The Netherlands.
 *
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/ 
 * or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA
 *
 * You are free to:
 * Share � copy and redistribute the material in any medium or format
 * Under the following terms:
 * Attribution � You must give appropriate credit, provide a link to the license, and indicate if changes were 
 * made. You may do so in any reasonable manner, but not in any way that suggests the licensor endorses
 * you or your use.
 * NonCommercial � You may not use the material for commercial purposes.
 * NoDerivatives � If you remix, transform, or build upon the material, you may not distribute the modified material.
 */
package simcolep.tools;


import java.util.Set;

import simcolep.demo.Inhabitant;
import simcolep.demo.Level;
import simcolep.demo.ILevelRegister;
import simcolep.demo.Person;
import simcolep.exceptions.SimulationException;
import simcolep.fun2.Archimedes;
import simcolep.lepra.Morbus;
import simcolep.lepra.State;
import simcolep.tools.io.SimulationLog;
import simcolep.tools.io.Table;

/**
 * Measures clustering of disease and disease types in households and creates output
 *
 */
public class ClusterMeasurer {
	/**table size*/
	private static final int ts = 25;
	
	private ILevelRegister<Level> reg = null;
	
	private Table sus = null;

	private Table infection = null;

	private Table detection = null;

	private Table type = null;
	/**
	 * 
	 * @param lr
	 */
	public ClusterMeasurer(ILevelRegister<Level> lr)
	{
		this.setRegister(lr);
	}
	
	/**
	 * Set register
	 * @param lr
	 */
	private void setRegister(ILevelRegister<Level> lr) {
		this.reg = lr;
	}
	/**
	 * Measure if necessary and return a table with clustering of susceptibles
	 * @return sus
	 */
	public Table susceptibility()
	{
		if(sus == null)
			measure();
		
		return sus;
	}
	
	 /**
	 * Measure if necessary and return a table with clustering of infection
	 * @return infection
	 */
	public Table infection()
	{
		if(infection == null)
			measure();
		
		return infection;
	}
	
	/**
	 * Measure if necessary and return a table with clustering of detected
	 * @return detection
	 */
	public Table detection()
	{
		if(detection == null)
			measure();
		
		return detection;
	}
	/**
	 * Measure if necessary and return a table with clustering of type
	 * @return
	 */
	public Table type()
	{
		if(type  == null)
			measure();
		
		return type;
	}
	
	/**
	 * 
	 * @return
	 */
	@SuppressWarnings("unchecked")
	private void measure()
	{
		if(reg == null)
			throw new SimulationException("no register set");
		
		//lists to gather information cutt off at household size ts
		double[][] sus_array = new double[ts][ts]; 
				
		double[][] inf_array = new double[ts][ts]; 
		
		double [][] det_array = new double [ts][ts];
		//type table is larger 
		int rows = (int)((ts+3)/2)*ts;
		double [][] type_array = new double[rows][ts];
		
		//iterate the register determining the fraction of susceptibles per household per household size
		Iterable<Set<Level>> it = reg.getRegister();
		
		for(Set<Level> set : it)
		{
			//determine size
			for(Level l : set)
			{
				int size = l.getHabitationSize();
				if(size>0)
				{
						if(size > sus_array.length-1)
						{
							size = sus_array.length-1;
							SimulationLog.log("Maximum household size of cluster measurer reached, set to maximum = " + ts);
						}
					
						//determine the number of non-susceptibles for each layer
						int[] nonsus = new int [((Person)l.getInhabitant(0)).getMorbi().length];
						int[] noninf = new int [((Person)l.getInhabitant(0)).getMorbi().length];
						int[] nondet = new int [((Person)l.getInhabitant(0)).getMorbi().length];
						int[][] types = new int[((Person)l.getInhabitant(0)).getMorbi().length][Archimedes.askInteger(PN.DISEASETYPES)];
						for(Inhabitant inhab : l.getInhabitants())
						{
							for(int i =0; i < nonsus.length;i++)
							{ 
								Morbus m = ((Person)inhab).getMorbus(i);
								int state = m.getState();
								int type = m.getType();
								types[i][type]+=1;
								if(type==Morbus.IMMUNE_TYPE)
								{
									nonsus[i]++;
									noninf[i]++;
									nondet[i]++;
								}else if(state==State.SUSCEPTIBLE_STATE||state==State.SELFHEAL_STATE||state == State.CURE_STATE)
								{
									noninf[i]++;
									if(!m.isDetected())
										nondet[i]++;
								}
							}
						}
			
				//determine start index of type
				int type_index = 0;
				for(int k =1; k <size;k++)
				{
					type_index += (k+1);
				}
				//put the values in the list
				for(int i =0; i<nonsus.length; i++)
					{
						int sus = Math.max(size-nonsus[i], 0);
							
						sus_array[size][sus]++;
						
						int inf = Math.max(size-noninf[i], 0);
						
						inf_array[size][inf]++;
						
						int det= Math.max(size-nondet[i], 0);
						
						det_array[size][det]++;
						
						int index = type_index + types[i][1];
						
						type_array[index][types[i][2]]++;
					}
				}
			}
		}
		
		//write to output
		sus = new Table(sus_array);
		infection = new Table(inf_array);
		detection  = new Table(det_array);
		type = new Table(type_array);
	}
}
