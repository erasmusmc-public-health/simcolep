/*
 * This file is part of SIMCOLEP, a simulation model developed by the Department of Public Health, 
 * Erasmus MC, University Medical Center Rotterdam, Rotterdam, The Netherlands.
 *
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/ 
 * or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA
 *
 * You are free to:
 * Share � copy and redistribute the material in any medium or format
 * Under the following terms:
 * Attribution � You must give appropriate credit, provide a link to the license, and indicate if changes were 
 * made. You may do so in any reasonable manner, but not in any way that suggests the licensor endorses
 * you or your use.
 * NonCommercial � You may not use the material for commercial purposes.
 * NoDerivatives � If you remix, transform, or build upon the material, you may not distribute the modified material.
 */
package simcolep.tools;

/**
 * This class contains the String representations of all parameters in the program
 * 
 * 
 * @author Dr. ir. E.A.J. Fischer
 * @version 1.3.6 
 */
public final class PN {

	//version information
	/**name of the main frame*/
	public static final String VERSION_NAME = 
		"SIMCOLEP version 1.4.12";
	
	//	suppress warning 
	public static final String UNCHECKED = "unchecked";
	
	/*text additions*/
	public static final String LINESEPARATOR = 
		System.getProperty("line.separator");
		
	public static final String TAB = "\t";
	public static final String SPACE = " ";
	public static final String RIGHT_SQUARE_BRACKET = "[";
	public static final String LEFT_SQUARE_BRACKET = "]";
	public static final String SEMICOLON = ";";
	public static final String THREEDOTS = "...";
	
	
	/*input - output*/
	public static final String OUTPUT_DIR = "output"; 
	
	public static final String DB_DIR = "database";

	public static final String INPUT_DIR = "input";
	
	public static final String OUTPUT_TO_ONE_FILE = "one_file";
	
	public static final String OVERWRITE_ALL = "overwrite_all";
	
	//	System parameters
	/**name of bucketwidth parameter*/
	public static final String BUCKETWIDTH = 
		"bucketWidth";
	public  static final String BUCKETSIZE = 
		"bucketSize";
	/**checks certain alogrithms, but slows computation*/
	public static final String SAFEMODE = "safemode";
	/**give a user interface*/
	public static final String GUI = "GUI";
	/**in case of error give dialog*/
	public static final String DIALOG = "dialog";
	/**History saving is a debugging option*/
	public static final String SAVEHISTORY = "savehist"; //$NON-NLS-1$
	/**create seeds by clock*/
	public static final String SEED = "clockseed";
	/**change seeds*/
	public static final String SEEDCHANGE = "cseed";
	/**change seed at this time */
	public static final String SEEDCHANGETIME = "cseedtime";
	/**measure computation time*/
	public static final String MEASURE_TIME = "timemeasurer";
	
	
	//Model settings
	/**number of layers*/
	public static final String L = "layers";
	/**step size*/
	public static final String STEP = "step";
	/**Width of the age classes*/
	public static final String AGECLASSWIDTH = "ageclasswidth";
	/**initial maximum age*/
	public static final String AGECLASSMAX = "ageclassmax";
	/**number of levels of contact*/
	public static final String LEVELDEPTH = "level_depth";
	/**level rank for dwelling*/
	public static final String RANKCONTACT = "rank_contact";
	
	//Run parameters
	/**string parameter name of RUNTIME*/
	public static final String RUNTIME = 
		"RunTime";
		/**string parameter name of RECORDINTERVAL*/
	public static final String RECORDINTERVAL = 
		"recordInterval";
		/**string of parameter RECORDSTART*/
	public static final String RECORDSTART = 
		"recordStart";
	/**time to open databases*/
	public static final String OPENDATABASE = "databaseStart";
	
	/**number of repeats*/
	public static final String REPEATS = "repeats"; //$NON-NLS-1$
	
	/*Seeds*/
   	/**name of seed for population random number generator*/
	public static final String POPSEED = "pop";
	/**name of seed for disease random number generator*/
	public static final String DISSEED = "dis";
	/**name of seed for disease random number generator*/
	public static final String INTROSEED = "intro";
	/*secondary seeds*/
	/**name of seed for population random number generator*/
	public static final String POPSEED2 = "pop2";
	/**name of seed for disease random number generator*/
	public static final String DISSEED2 = "dis2";
	
	//Demography 
	/**initial age distribution of females*/ 
	public static final String INITAGEDIST_FEMALE = "initagedist_female";
	/**initial age distribution of males*/
	public static final String INITAGEDIST_MALE = "initagedist_male";
	/**initial size distribution of households*/
	public static final String INITLEVELSIZEDIST = "initlevelsize";
	
	//Demographic parameters and functions
	/**population size at time t*/
	public static final String POPSIZE = "popsize"; //$NON-NLS-1$

	/**survival of females*/
    public static final String FEMALE_SURVIVAL = "femalesurvival"; //$NON-NLS-1$
    /**survival of males*/
	public static final String MALE_SURVIVAL = "malesurvival"; //$NON-NLS-1$

	/**weighing function for birhts*/
	public static final String BIRTH_WEIGTH = "w_birth"; //$NON-NLS-1$
	
	/**sex ratio of newly born*/
	public static final String SEXRATIO = "sexratio"; //$NON-NLS-1$
	
	/**fraction married males*/
	public static final String MARRIED_MALES = "MarriedMales"; //$NON-NLS-1$
		/**fraction married females*/
	public static final String MARRIED_FEMALES = "MarriedFemales"; //$NON-NLS-1$
	
	/**weighting of levels with random movement*/
	public static final String LEVELWEIGHT = "levelweight";
	
	/**allowing movement after marriage*/
	public static final String MOVEAFTERMARRIAGE = "RMafterM";
	
	/**movement rate of females*/
	public static final String MOVEFEMALE = "move_female";
	/**movement rate of males*/
	public static final String MOVEMALE = "move_male";
	
	/**timing until hh split*/
	public static final String SPLIT = "split";
	
	/*movement parameters*/
	/**density of movement of a widow*/
	public static final String DENSMOVEWID = "densityMoveWidow";
	public static final String MOVEWIDOWER = "fractMoveWidower";
	public static final String MOVEWIDOW = "fractMoveWidow";
	public static final String MOVETOMALE = "movetomale";
	public static final String MOVETOFEMALE = "movetofemale";
	public static final String MOVESINGLE = "movesingle";
		
	//Disease functions and parameters
	/**Functions describing natural history of an infection*/
	public static final String DISEASE_HISTORY = "DiseaseHistory"; //$NON-NLS-1$
	/**number of disease types*/
	public static final String DISEASETYPES = "distypes";
	
    /**string presentation of this self heal state parameter*/
	public static final String SELFHEAL = "selfheal_state";
	/**string presentation of this cure state parameter*/
	public static final String CURE = "cure_state";
	
	/**Name of parameter determining the model */
	public static final String IMODELNAME = "iModel";
	/**code for genes*/
	public static final String GENE = "g";
	/**number of genes in a gene inheritance model*/
	public static final String NUMOFGENES = "genes";
	/**number of alleles (followed in input by gene number)*/
	public static final String NUMOFALLELES = "alleles";
	/**Name param probability inheritance from mother (for type or susceptibility model)*/
	public static final String PROBDISMOM = "dis_mom";

	/** generic transmission rate name*/
	public static final String BETA = "beta"; //$NON-NLS-1$
	
	/**Name of param for type of random mixing
	 * <br>i.e.<br>
	 * <li> 777 members same dwelling excluded
	 * <li> 888 members same group of levels excluded
	 * <li> 999 everyone included
	 * */
	public static final String RANDOMMIXINGCODE = "random_mixing_code"; //$NON-NLS-1$
	public static final String CONTACT_GP = "c_pop";
	public static final String DWELLINGCONTACT = "c_dwel";
	
	/*import of infection*/
	public static final String IMPMETHOD = "import_method";
	public static final String IMPTYPE = "import_type";
	public static final String IMPHHSIZE = "import_hhsize";
	public static final String IMP_PER_EVENT = "imports_per_event";
	public static final String TIMEFIRSTIMP = "time_of_firstImport";
	public static final String TIMING_IMP = "TimeOfImport";
	public static final String IMPEVENTS = "import_events";
	
	/**generic detection rate */
	public static final String DETECTION = "det";
	/**active detection*/
	public static final String ACTIVEDETECTIONPROBABILITY = "Detection";

	/*vaccination*/
	public static final String BCGCOVERAGE = "BCGcoverage";
	public static final String BCGCAMPAIGN = "BCGcampaign";
	public static final String BCGCAMP_TYPE = "BCGcampaigntype";
	public static final String BCGSHIFT = "BCGshift";
	public static final String INFBCGSHIFT = "infBCGshift";
	public static final String BCGPROTECT = "BCGprotection";
	public static final String BCG = "BCG";
	public static final String TO = "to";
	/**effect of revaccination*/
	public static final String BCGREVAC = "BCGrevac";
	
	/*bcg campaign*/
	public static final String STARTBCGCAMPAIGN = "BCGcampaign_start";
	public static final String ENDBCGCAMPAIGN = "BCGcampaign_end";
	public static final String BCGCAMPLEVEL = "bcgLevel";
    public static final String BCGCAMPFREQ = "bcgFreq";

    /*interventions on/off */
	public static final String CONTACTTRACING = "contacttracing";
	public static final String SURVEY = "survey";
	public static final String PROPHYLACTIC = "prophylactic";
	public static final String BLANKET = "blanket";
	public static final String FOLLOWUP = "followup";
	public static final String CONTACTVACCINATION = "contactvaccination";
	
	/*treatment*/
	public static final String TSTART = "tStart";
	
	/*contact tracing*/
	public static final String CCLEVEL = "ccLevel";
	public static final String CCINDIVIDUAL = "ccIndividual";
	public static final String CSTART = "cStart";
	public static final String CCRANK = "ccRank";
	
	/*prophylactics*/
	public static final String PCLEVEL = "pcLevel";
	public static final String PCINDIVIDUAL = "pcIndividual";
	public static final String PSTART = "pStart";
	public static final String PCRANK = "pcRank";
	
	/*survey*/
	public static final String SCLEVEL = "scLevel";
	public static final String SCINDIVIDUAL = "scIndividual";
	public static final String SSTART = "sStart";
	public static final String SEND = "sEnd";
	public static final String SFREQ = "sFreq";
	
	/*blanket*/
	public static final String BCLEVEL = "bcLevel";
	public static final String BCINDIVIDUAL = "ccIndividual";
	public static final String BSTART = "bStart";
	public static final String BEND = "bEnd";
	public static final String BFREQ = "bFreq";
	
	/*follow up of contacts*/
	public static final String FUEND = "fuEnd";
	public static final String FUFREQ = "fuFreq";
	public static final String FCINDIVIDUAL = "cfIndividual";
	
	
	/*vaccination of contacts*/
	public static final String VCLEVEL = "vcLevel";
	public static final String VCINDIVIDUAL = "vcIndividual";
	public static final String VSTART = "vStart";
	public static final String VCRANK = "vcRank";

	/**treatment duration*/
	public static final String TREATMENTDURATION = "treatmentduration";

	/**relapse*/
	public static final String UPGRADE_AT_RELAPSE = "relapseUpgraded";
	public static final String RELAPSETYPE = "relapseType";
	public static final String RELAPSEPERIOD = "relapse_period";
	public static final String RELAPSECHANCE = "relapse";

	/*reaction to chemoprophylatica*/
	public static final String CHEMOCURE = "chemocure";
	public static final String CHEMODELAY = "chemodelay";
	
	/*Function attributes*/
	/**Cumulative discrete functions*/
	public static final String CUMULATIVE = "cum";
	/**Interpolate between entries of discrete function*/
	public static final String INTERPOLATE = "interpolate";

	/**only levels with this factor contain susceptibles*/
	public static final String DFACTOR = "dFactor";

	public static final String HOMOZYGOUS = "homozygous";

	public static final String PREVENTINBREEDING = "preventinbreeding";

	public static final String TWOSUSMECH = "twosusmechanisms";

	public static final String TSMDIST = "tsmDistribution";

	

	



	
	
	

	



	
}
