/*
 * This file is part of SIMCOLEP, a simulation model developed by the Department of Public Health, 
 * Erasmus MC, University Medical Center Rotterdam, Rotterdam, The Netherlands.
 *
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/ 
 * or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA
 *
 * You are free to:
 * Share � copy and redistribute the material in any medium or format
 * Under the following terms:
 * Attribution � You must give appropriate credit, provide a link to the license, and indicate if changes were 
 * made. You may do so in any reasonable manner, but not in any way that suggests the licensor endorses
 * you or your use.
 * NonCommercial � You may not use the material for commercial purposes.
 * NoDerivatives � If you remix, transform, or build upon the material, you may not distribute the modified material.
 */
/*
 * Created on 7-jul-2005
 *
 *
 */
package simcolep.tools.xmlparser;

import java.io.File;
import java.io.IOException;

import org.jdom.Document;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;



/**
 * @author ir. E.A.J. Fischer
 *@version 0.1.1 -- Last Modified: 7-jul-2005
 */
public  class XMLSimpleInput {
    /**name of the file */
	protected String filename = null;
	
	/**path to the file*/
    protected String path = null;
   
    /**document in xml-format*/
	protected org.jdom.Document domDocument;
	
	/**actual file*/
    protected File file;
    


	/**
	 * 
	 * @param fn
	 */
	
	protected XMLSimpleInput(String fn)
				throws IOException,
				JDOMException
	{
		//path
	    this.path = 
	    	XMLInput.inpath.toString();
		
	    //filename
	     if(fn.endsWith("xml"))
	    	this.filename = new String(fn);
	    else
	    	this.filename = new String(fn+".xml");
	    
	    //create the file
	    this.file = new File(this.filename);
	    
	    //read the xml-file in
	    getXMLfile();
	}
	/**
	 * @param path
	 * @param fn
	 */
	
	public XMLSimpleInput(String path,String fn) throws IOException, JDOMException
	{
	    this.path = path;
	    
	    //filename
	     if(fn.endsWith("xml"))
	    	this.filename = new String(fn);
	    else
	    	this.filename = new String(fn+".xml");
		
	     //create the file
	    this.file = new File(this.path+File.separator+this.filename);
	    
	    //read the xml-file in
		getXMLfile();
	}
    /**
     * 
     * @param file
     * @throws JDOMException
     * @throws IOException
     */
    public XMLSimpleInput(File file) 
    			throws JDOMException, 
    			IOException
    {
    	this.file = file;
    	this.filename  = file.getName();
    	this.path = file.getPath();
    	//remove filename
    	this.path = this.path.substring(this.path.lastIndexOf(File.separator));
    	//get the file
    	 getXMLfile(file);
    }
	
    /**
	 * 
	 *
	 */
	protected XMLSimpleInput()
	{
	}
	/**
	 * 
	 * 
	 * @return org.jdom.Document domDocument
	 */
	public org.jdom.Document getXMLfile()throws IOException, JDOMException
	{   	
		if(this.domDocument!=null) 
			return this.domDocument;
		//else build from what we already know
	
		//create a file
		if(this.file==null)
			this.file = new File(this.path,this.filename);
		
		//and get the XML-file
	   this.domDocument = getXMLfile(this.file);
	   
	   //return it
	   return this.domDocument;
		
	}
    
    /**
     * 
     * @param file
     * @return this.domDocument
     * @throws JDOMException
     * @throws IOException
     */
    private org.jdom.Document getXMLfile(File file) throws JDOMException, IOException {
        //sax builder
    	SAXBuilder d = new SAXBuilder();
    	//build the document
        this.domDocument = d.build(file);
         //return the document
        return this.domDocument;
        
    }
    
   /**
    * 
    * @return Document
    */
    public Document getdomDocument()
    {
        return this.domDocument;
    }
    
    /**
     * Returns the file that was used to read from
     * 
     * @return file
     */
    public File getInternalFile()
    {
    	return this.file;
    }
}
