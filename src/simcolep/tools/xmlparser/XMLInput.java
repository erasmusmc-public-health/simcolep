/*
 * This file is part of SIMCOLEP, a simulation model developed by the Department of Public Health,
 * Erasmus MC, University Medical Center Rotterdam, Rotterdam, The Netherlands.
 *
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/
 * or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA
 *
 * You are free to:
 * Share � copy and redistribute the material in any medium or format
 * Under the following terms:
 * Attribution � You must give appropriate credit, provide a link to the license, and indicate if changes were
 * made. You may do so in any reasonable manner, but not in any way that suggests the licensor endorses
 * you or your use.
 * NonCommercial � You may not use the material for commercial purposes.
 * NoDerivatives � If you remix, transform, or build upon the material, you may not distribute the modified material.
 */
/*
 * Created on 17-Nov-2005
 *
 */
package simcolep.tools.xmlparser;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;

import org.jdom.Attribute;
import org.jdom.DataConversionException;
import org.jdom.Element;
import org.jdom.JDOMException;

import simcolep.exceptions.SimulationException;
import simcolep.fun2.Archimedes;
import simcolep.fun2.Archimedes.Library;
import simcolep.fun2.DiscreteFunctionInput;
import simcolep.fun2.DiscreteFunctionInputElement;
import simcolep.fun2.Function;
import simcolep.fun2.IDistribution;
import simcolep.fun2.QDiscreteFunction;
import simcolep.fun2.TimeDistribution;
import simcolep.fun2.cFunction;
import simcolep.lepra.function.AbstractDiseaseHistoryFunction;
import simcolep.lepra.function.Hypocrates;
import simcolep.tools.PN;
import simcolep.tools.io.FileRegister;
import simcolep.tools.io.OutputList;

/**
 * Input of functions and parameters
 *
 *
 * @author ir E.A.J. Fischer
 * @version 0.1.2 -- Last Modified 17-Nov-2005
 */
public class XMLInput extends XMLSimpleInput {

	/* Names of direct children in the input files */
	private static final String TITLE = "Title"; //$NON-NLS-1$
	private static final String OUTPUT = "Output"; //$NON-NLS-1$
	private static final String METHODS = "Methods"; //$NON-NLS-1$
	private static final String FUNCTIONS = "Functions"; //$NON-NLS-1$
	private static final String PARAMETERS = "Parameters"; //$NON-NLS-1$
	private static final String SEEDS = "Seeds"; //$NON-NLS-1$
	private static final String PATH = "Path";
	private static final String FILES = "Files";
	/* children of path */
	private static final String IN = "in";
	private static final String OUT = "out";

	/* attribute names */
	/** For the paths */
	private static final String ABSOLUTE_PATH = "absPath"; //$NON-NLS-1$
	/** output at certain time point */
	private static final String TIME = "time"; //$NON-NLS-1$
	/** attribute called value for parameters */
	private static final String VALUE = "value";
	/** attribute determining the function type */
	private static final String TYPE = "type"; //$NON-NLS-1$

	/* possible attribute values with special action */
	private static final String ON = "on"; //$NON-NLS-1$
	private static final String TRUE = "true"; //$NON-NLS-1$
	private static final String OFF = "off"; //$NON-NLS-1$
	private static final String FALSE = "false"; //$NON-NLS-1$
	private static final Object TIMEDISTRIBUTION = "TimeDistribution";
	private static final String IDISTRIBUTION = "IDistribution"; //$NON-NLS-1$
	private static final String INTERVAL_STARTS = "interval_starts"; //$NON-NLS-1$
	private static final String MIN = "min"; //$NON-NLS-1$

	/* mathematical operators */
	/** Multiplier */
	private static final String MULTIPLIER = "x"; //$NON-NLS-1$
	private static final String POWER = "^"; //$NON-NLS-1$
	private static final String DIVIDER = "/"; //$NON-NLS-1$
	/** separators of values in a string */
	private static final String[] SEPARATORS = { " ", "\t", ",", ";" };

	/** map containing instances of functions by type of function */
	private static LinkedHashMap<String, Function> functionmap = new LinkedHashMap<String, Function>();

	/**
	 * input path (input files are extended with this path) <br>
	 * default: System.getProperty("user.dir")/input/
	 */
	public static File inpath = new File(FileRegister.IN_PATH);

	/**
	 * output path (output is saved in this directory)<br>
	 * default: System.getProperty("user.dir")/output/
	 */
	public static File outpath = new File(FileRegister.OUT_PATH);

	/**
	 * Add custom function f for the input that is not in Archimedes.Library or
	 * Hypocrates
	 *
	 * @param f
	 * @throws IllegalAccessException
	 * @throws InstantiationException
	 */
	public static void addFunction(Function f) throws InstantiationException, IllegalAccessException {
		// check if the map is not empyt
		if (functionmap.isEmpty())
			init();

		// add function
		functionmap.put(f.getClass().getSimpleName(), f);
	}

	/**
	 * Create a function from an element of the xml-file
	 *
	 * @param el
	 * @return f
	 *
	 * @throws DataConversionException
	 * @throws IllegalAccessException
	 * @throws InstantiationException
	 */

	@SuppressWarnings("unchecked")
	public static Function createFunction(Element el)
			throws DataConversionException, InstantiationException, IllegalAccessException {
		// null will result in null
		if (el == null)
			return null;

		// get the type of function from the map with functions
		String type = el.getAttributeValue(TYPE);

		if (!functionmap.containsKey(type))
			throw new SimulationException(SimulationException.UNKNOWNFUNCTION_MESSAGE + type);
		// get instance of this function type
		Function f = functionmap.get(type).getClass().newInstance();

		// if it is a combined function do it differently
		if (f.inputGroup() == Function.COMBINED_FUNCTION_INPUT)
			return create_cFunction(el);

		if (f.inputGroup() == Function.DISEASE_FUNCTION_INPUT)
			return create_diseaseFunction(el);
		// continuous function
		if (f.inputGroup() == Function.CONTINUOUS_FUNCTION_INPUT) {
			for (Attribute a : (List<Attribute>) el.getAttributes()) {

				if (!a.getName().equalsIgnoreCase(TYPE)) {

					double double_value = readParameter(a);

					f.changeParameter(a.getName(), double_value);

				}
			}
		}
		// discrete functions
		else if (f.inputGroup() == Function.DISCRETE_FUNCTION_INPUT) {
			String string_interpolate = el.getAttributeValue(PN.INTERPOLATE);

			boolean interpolate = true;

			if (string_interpolate != null)
				if (string_interpolate.equalsIgnoreCase(OFF) || string_interpolate.equalsIgnoreCase(FALSE))
					interpolate = false;

			((QDiscreteFunction) f).interpolate(interpolate);

			((QDiscreteFunction) f).setFunction(getDiscreteFunctionInput(el));
		}

		return f;
	}

	/**
	 * Set the default in- and output paths
	 *
	 * @param IN
	 * @param OUT
	 *
	 * @throws DataConversionException
	 */
	public static void setPaths(String IN, String OUT) throws DataConversionException {

		// input
		if (IN != null) {
			File in = new File(IN);
			inpath = in;
			if (!in.exists())
				in.mkdirs();
		}
		if (OUT != null) {
			// output
			File out = new File(OUT);
			outpath = out;
			if (!out.exists())
				out.mkdirs();
		}
		// set the output lists path
		OutputList.setPath(outpath.getPath());

	}

	/**
	 * Clean the string from spaces, tabs, carriage returns and enters
	 *
	 * @param s
	 * @return s
	 */
	private static String cleanString(String s) {
		// first trim
		s = s.trim();
		// replace all /t
		s = s.replaceAll(PN.TAB, "");
		// and /r
		s = s.replaceAll(PN.LINESEPARATOR, "");

		// return
		return s.trim();
	}

	/**
	 * @param el
	 * @throws IllegalAccessException
	 * @throws InstantiationException
	 * @throws DataConversionException
	 */

	@SuppressWarnings("unchecked")
	private static Function create_cFunction(Element el)
			throws DataConversionException, InstantiationException, IllegalAccessException {

		// a cFunction element has elements of each function
		List<Element> functions = el.getChildren();
		// if
		if (el.getChild(INTERVAL_STARTS) == null)
			throw new SimulationException("combined function: " + el.getName() + " needs element:\r\n interval_starts");
		// and attributes that determine from which time point they start
		List<Attribute> starts = el.getChild(INTERVAL_STARTS).getAttributes();

		functions.remove(el.getChild(INTERVAL_STARTS));

		if (starts.size() != functions.size())
			throw new SimulationException("Functions and starts should be" + " the same length");

		// create array of functions and starts
		Function[] funcs = new Function[functions.size()];
		double[] i_starts = new double[starts.size()];

		for (int index = 0; index < funcs.length; index++) {
			funcs[index] = XMLInput.createFunction(functions.get(index));

			i_starts[index] = starts.get(index).getDoubleValue();
		}

		// create the function
		if (el.getAttributeValue(TYPE).equals(TIMEDISTRIBUTION))
			return new TimeDistribution(i_starts, funcs);
		else if (el.getAttributeValue(TYPE).equals(IDISTRIBUTION))
			return new IDistribution(i_starts, funcs);
		else
			return new cFunction(i_starts, funcs);
	}

	/**
	 * @param el
	 * @throws IllegalAccessException
	 * @throws InstantiationException
	 * @throws DataConversionException
	 */
	@SuppressWarnings("unchecked")
	private static Function create_diseaseFunction(Element el)
			throws DataConversionException, InstantiationException, IllegalAccessException {

		// a cFunction element has elements of each function
		List<Element> functions = el.getChildren();

		// create array of functions and starts
		Map<String, Function> funcs = new LinkedHashMap<String, Function>();

		for (Element e : functions) {
			funcs.put(e.getName(), XMLInput.createFunction(e));
		}

		// create the function
		return // get instance of this function type
		((AbstractDiseaseHistoryFunction) functionmap.get(el.getAttribute(TYPE).getValue()).getClass().newInstance())
				.set(funcs);
	}

	/**
	 * Read and return discrete function input
	 *
	 * @param el
	 * @return dfi
	 * @throws DataConversionException
	 */
	@SuppressWarnings("unchecked")
	private static DiscreteFunctionInput getDiscreteFunctionInput(Element el) throws DataConversionException {

		DiscreteFunctionInput dfi = new DiscreteFunctionInput(el.getAttributeValue(PN.CUMULATIVE).equals(TRUE));

		List<Element> elist = el.getChildren();
		for (Element e : elist) {
			dfi.add(new DiscreteFunctionInputElement(readParameter(e.getAttribute(MIN)),
					readParameter(e.getAttribute(VALUE))));
		}

		return dfi;
	}

	/**
	 * Initialises Functions sothat a map containing instances by name can be
	 * used<br>
	 * Archimedes.Library contains all standard functions<br>
	 * Hypocrates is a enum-type containing diseasefunctions
	 *
	 * @throws IllegalAccessException
	 * @throws InstantiationException
	 */
	private static void init() throws InstantiationException, IllegalAccessException {
		// get Archi's library
		Library[] c = Archimedes.Library.values();
		// iterate them
		for (Library l : c) {
			// get instance
			Function f = l.getInstance();
			// put in the map
			functionmap.put(f.getClass().getSimpleName(), f);

		}
		// get the library of disease functions
		for (Hypocrates h : Hypocrates.values()) {
			// get instance
			Function f = h.getInstance();
			// put in the map
			functionmap.put(f.getClass().getSimpleName(), f);
		}

	}

	/**
	 * Read parameters that can be of the following formats
	 *
	 * <li>a single double value => returns this value
	 * <li>a fraction (e.g. 1/6) => returns numerator divided by denominator
	 * <li>a multiplication (e.g. 2x3) => returns multiplication of 2 times 3
	 * <li>a power (e.g. 2^2) => returns solution of that equations
	 * <li>a exponent of E (e.g. e^2)
	 *
	 * @param e
	 * @return value
	 */
	private static double readParameter(Attribute a) {
		// check if the attribute is not null
		if (a == null)
			throw new NullPointerException("Attribute containing value == null");

		// get the value as String and clean it (remove spaces returns tabs
		// etc.)
		String s_value = cleanString(a.getValue());
		double value = 0;
		if (s_value.contains(DIVIDER)) {

			// split the string along the operator
			String[] nom_over_denom = s_value.split(DIVIDER);
			// determine both parts

			// calculate the value
			value = Double.parseDouble(nom_over_denom[0]) / Double.parseDouble(nom_over_denom[1]);

		} else if (s_value.contains(POWER)) {

			// split the string along the operator
			String[] base_power = s_value.split(POWER);
			// determine both parts
			double base = 0;
			if (base_power[0].toUpperCase().contains("E"))
				base = Math.E;
			else
				base = Double.parseDouble(base_power[0]);

			double power = Double.parseDouble(base_power[1]);
			// calculate the value
			value = Math.pow(base, power);

		} else if (s_value.contains(MULTIPLIER)) {
			// split the string along the operator
			String[] nom_over_denom = s_value.split(MULTIPLIER);
			// determine both parts

			// calculate the value
			value = Double.parseDouble(nom_over_denom[0]) * Double.parseDouble(nom_over_denom[1]);

		} else
			value = Double.parseDouble(s_value);

		// return the value
		return value;

	}

	/**
	 * @throws IllegalAccessException
	 * @throws InstantiationException
	 *
	 */
	public XMLInput() throws InstantiationException, IllegalAccessException {
		super();
		// initialize map
		if (functionmap.isEmpty())
			init();
	}

	/**
	 * @param file
	 * @throws IOException
	 * @throws JDOMException
	 * @throws IllegalAccessException
	 * @throws InstantiationException
	 *
	 */
	public XMLInput(File file) throws JDOMException, IOException, InstantiationException, IllegalAccessException {
		super(file);
		// initialize map
		if (functionmap.isEmpty())
			init();
	}

	/**
	 * @param filename
	 * @throws IOException
	 * @throws JDOMException
	 * @throws IllegalAccessException
	 * @throws InstantiationException
	 */
	public XMLInput(String filename) throws IOException, JDOMException, InstantiationException, IllegalAccessException {
		// super constructor
		super(XMLInput.inpath.toString(), filename);

		// initialize map
		if (functionmap.isEmpty())
			init();
	}

	/**
	 * @param path
	 * @param filename
	 * @throws IOException
	 * @throws JDOMException
	 * @throws IllegalAccessException
	 * @throws InstantiationException
	 */
	public XMLInput(String path, String filename)
			throws IOException, JDOMException, InstantiationException, IllegalAccessException {
		// super constructor
		super(path, filename);

		// initialize map
		if (functionmap.isEmpty())
			init();

	}

	/**
	 * From the inputfile read and return setting (on/off or true/false)
	 *
	 * @param s
	 * @return boolean value of this string
	 */
	@SuppressWarnings("unchecked")
	public boolean getBoolean(String s) {
		// get the methods
		for (Element e_list : (List<Element>) this.domDocument.getRootElement().getChildren(METHODS)) {
			// get the element of this name
			Element el = e_list.getChild(s);
			if (el != null) {
				// return the value true if value is "on"
				if (el.getAttributeValue(VALUE).equalsIgnoreCase(ON))
					return true;
				// return false if value is "off"
				if (el.getAttributeValue(VALUE).equalsIgnoreCase(OFF))
					return false;

				// else return the parsed value
				return Boolean.valueOf(el.getAttributeValue(VALUE));
			}

		}
		// method was not found
		throw new NullPointerException("File: " + this.filename + "\r\ndoes not contain method: " + s);
	}

	/**
	 * Reads the parameters from the domDocument and return a Map containing
	 * these values with the name as key
	 *
	 * @return params
	 */
	@SuppressWarnings("unchecked")
	public Map<String, Boolean> getBooleans() {
		// create a map
		Map<String, Boolean> booleans = new LinkedHashMap<String, Boolean>();

		// elements containing methods
		List<Element> e_list = this.domDocument.getRootElement().getChildren(METHODS);
		// return if the element Methods is not available
		if (e_list == null)
			return booleans;
		// iterate
		for (Element el : e_list) {
			// iterate the separate methods elements
			for (Element e : (List<Element>) el.getChildren()) {
				if (e.getAttributeValue(VALUE).equalsIgnoreCase(ON))
					booleans.put(e.getName(), true);
				else
					booleans.put(e.getName(), Boolean.valueOf(e.getAttributeValue(VALUE)));
			}
		}
		// return
		return booleans;
	}

	/**
	 * Create a file from the information in the element and with the path
	 * starting in start_path
	 *
	 * @param el
	 * @param start_path
	 * @return f
	 * @throws IOException
	 */
	@SuppressWarnings("unchecked")
	public File getFile(Element el, File start_path) throws IOException {
		// if the element is null well return null
		if (el == null)
			return null;

		// attributes of the element
		List<Attribute> alist = el.getAttributes();

		// start of the path
		String totalPath = start_path.getPath();

		// add the attributes to the total path
		for (Attribute a : alist) {
			// no need for two file separators is there
			if (totalPath.endsWith(File.separator))
				totalPath += a.getValue();
			else
				totalPath += File.separator + a.getValue();
		}

		// create file
		File f = new File(totalPath.trim());
		// if the file does not exist it should be created
		if (!f.exists()) {
			// create the path
			String path = f.getPath().substring(0, f.getPath().lastIndexOf(File.separator));
			new File(path).mkdirs();
			// create the file
			// f.createNewFile();
		}
		return f;

	}

	/**
	 * Returns a file at the place specified by in/out path and the name in the
	 * insim.xml file
	 *
	 * @param s
	 *            - name of the file
	 * @param io
	 *            - in/out put file
	 * @return File - null if this file was not specified in the inputfile
	 * @throws IOExcept
	 *             ion
	 */
	@SuppressWarnings("unchecked")
	public File getFile(String io, String s) throws IOException {
		// create the start path
		String start_path = null;

		if (io.equalsIgnoreCase(IN))
			start_path = inpath.getPath();
		else if (io.equalsIgnoreCase(OUT))
			start_path = outpath.getPath();
		else
			start_path = System.getProperty(FileRegister.USERDIR);

		// get the list of file specifications
		List<Element> e_list = this.domDocument.getRootElement().getChildren("Files");
		Element el = null;
		for (Element e : e_list) {
			// if it contains the io specified files
			if (e.getChild(io) != null)
				el = e.getChild(io).getChild(s);

		}
		// if it was not found return null
		if (el == null)
			return null;

		// create an return
		return this.getFile(el, new File(start_path));
	}

	/**
	 * Reads out the filename information from this element and returns this as
	 * a path
	 *
	 * @param el
	 *
	 * @return totalPath
	 */
	@SuppressWarnings("unchecked")
	public String getFileName(Element el) {
		// if the element is null well return null
		if (el == null)
			return null;

		// attributes of the element
		List<Attribute> alist = el.getAttributes();

		// start of the path
		String totalPath = new String("");

		// add the attributes to the total path
		for (Attribute a : alist) {
			// no need for two file separators is there
			if (totalPath.endsWith(File.separator))
				totalPath += a.getValue();
			else
				totalPath += File.separator + a.getValue();
		}

		if (totalPath.startsWith(File.separator))
			totalPath = totalPath.substring(1);

		return totalPath.trim();
	}

	/**
	 * Get the filename specified by the io parameter and filename strings
	 *
	 * @param io
	 * @param file
	 *
	 */
	@SuppressWarnings("unchecked")
	public String getFileName(String io, String file) {

		// get the list of Files elements
		List<Element> e_list = this.domDocument.getRootElement().getChildren("Files");

		// get the element
		Element el = null;
		for (Element e : e_list) {
			// get the child
			if (e.getChild(io) != null)
				el = e.getChild(io).getChild(file);
		}
		if (el == null)
			return null;
		// create an return
		return this.getFileName(el);
	}

	/**
	 *
	 * @return Map containing the names of the files
	 */
	@SuppressWarnings("unchecked")
	public Map[] getFileNames() {

		// get list of file name elements
		List<Element> e_list = this.domDocument.getRootElement().getChildren(FILES);

		// the maps
		SortedMap[] filenames = new TreeMap[2];
		filenames[0] = new TreeMap();
		filenames[1] = new TreeMap();

		// if the document does not contain files return empty maps
		if (e_list.isEmpty()) {
			filenames[0] = new TreeMap();
			filenames[1] = new TreeMap();
			return filenames;
		}
		// string for both in and out
		String[] inout = { IN, OUT };

		// iterate the elements

		for (Element element : e_list) {
			// position of the array of treemaps
			int x = 0;
			for (String io : inout) {
				SortedMap<String, String> map = new TreeMap<String, String>();

				// elements
				Element io_element = element.getChild(io);
				List<Element> l = null;
				if (io_element == null)
					l = new LinkedList();
				else
					l = io_element.getChildren();

				for (Element e : l) {
					String s = this.getFileName(e);

					map.put(e.getName(), s);
				}

				filenames[x++].putAll(map);

			}
		}
		return filenames;
	}

	/**
	 * get all in- and output files
	 *
	 * @return Map containing files
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	@SuppressWarnings("unchecked")
	public Map<String, File> getFiles() throws FileNotFoundException, IOException {
		Map<String, File> map = new LinkedHashMap<String, File>();

		String[] inout = { IN, OUT };
		for (String io : inout) {
			map.putAll(this.getFiles(io));
		}
		return map;
	}

	/**
	 *
	 * @return Map containing files
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	@SuppressWarnings("unchecked")
	public Map<String, File> getFiles(String io) throws FileNotFoundException, IOException {
		Map<String, File> map = new LinkedHashMap<String, File>();

		// start path
		String start_path = null;

		if (io.equalsIgnoreCase(IN))
			start_path = inpath.getPath();
		else if (io.equalsIgnoreCase(OUT))
			start_path = outpath.getPath();
		else
			start_path = FileRegister.IN_PATH;
		// elements
		List<Element> e_list = this.domDocument.getRootElement().getChildren(FILES);
		for (Element el : e_list) {
			if (el.getChild(io) != null) {
				List<Element> l = el.getChild(io).getChildren();
				for (Element e : l) {
					File f = this.getFile(e, new File(start_path));
					if (f != null)
						map.put(e.getName(), f);
				}
			}
		}

		return map;
	}

	/**
	 * Reads function from the domDocument
	 *
	 * @param s
	 * @return f
	 * @throws DataConversionException
	 * @throws IllegalAccessException
	 * @throws InstantiationException
	 */
	@SuppressWarnings("unchecked")
	public Function getFunction(String s)
			throws DataConversionException, InstantiationException, IllegalAccessException, NullPointerException {
		// iterate the elements that are called Functions
		for (Element e : (List<Element>) this.domDocument.getRootElement().getChildren(FUNCTIONS)) {
			// return the first Function found by this name (=> never use names
			// twice!)
			if (e.getChild(s) != null)
				return createFunction(this.domDocument.getRootElement().getChild(FUNCTIONS).getChild(s));
		}
		return null;
	}

	/**
	 * read all functions that are in this file
	 *
	 * @return map containing the functions in the XMLfile
	 * @throws IllegalAccessException
	 * @throws InstantiationException
	 * @throws DataConversionException
	 */
	@SuppressWarnings("unchecked")
	public Map<String, Function> getFunctions()
			throws DataConversionException, InstantiationException, IllegalAccessException {
		// create a map to store the functions
		// use LinkedHashMap so that the same order of functions as in the file
		// is preserved
		Map<String, Function> fmap = new LinkedHashMap<String, Function>();

		// if there are no functions
		if (this.domDocument.getRootElement().getChildren(FUNCTIONS) == null)
			return fmap;

		// iterate all the Functions - elements
		for (Element f_e : (List<Element>) this.domDocument.getRootElement().getChildren(FUNCTIONS)) {
			// iterate the list of functions contained by each Functions element
			for (Element e : (List<Element>) f_e.getChildren()) {
				fmap.put(e.getName(), createFunction(e));
			}
		}
		// return the map
		return fmap;
	}

	/**
	 * Get the specifications of a certain output
	 *
	 * @param s
	 * @return times
	 * @throws DataConversionException
	 */
	@SuppressWarnings("unchecked")
	public List<Double> getOutput(String s) throws DataConversionException {
		// list output elements
		List<Element> e_list = this.getdomDocument().getRootElement().getChildren(OUTPUT);
		// list to return
		List<Double> times = new LinkedList<Double>();

		// iterate
		for (Element e : e_list) {
			// if e != null
			if (e != null) {
				// iterate attributes
				for (Attribute a : (List<Attribute>) e.getAttributes()) {
					if (a.getName().contains(TIME)) {
						times.add(new Double(a.getDoubleValue()));
					}
				}
			}
		}
		return times;
	}

	/**
	 * Read form this file element (Output) a map containing codes in strings
	 * with a list of times when to collect these data If it does not contain
	 * this element return empty map
	 *
	 * @return map
	 * @throws DataConversionException
	 *
	 */
	@SuppressWarnings("unchecked")
	public Map<String, List<Double>> getOutputs() throws DataConversionException {
		// Map to return
		Map<String, List<Double>> map = new LinkedHashMap<String, List<Double>>();

		// get the list of output elements
		List<Element> e_list = this.domDocument.getRootElement().getChildren(OUTPUT);

		for (Element oe : e_list)
			// if not there return
			if (oe != null) {
				// list of outputs
				List<Element> output = oe.getChildren();

				// put in the map
				for (Element e : output) {
					List<Double> times = new LinkedList<Double>();

					for (Attribute a : (List<Attribute>) e.getAttributes()) {
						if (a.getName().contains(TIME)) {

							times.add(new Double(a.getDoubleValue()));
						}
					}

					map.put(e.getName(), times);
				}
			}

		return map;
	}

	/**
	 * Get the value of a parameter specified by the string
	 *
	 * @param s
	 * @return value of parameter with name string
	 */
	@SuppressWarnings("unchecked")
	public double getParameter(String s) {
		// iterate the elements that are called Functions
		for (Element e : (List<Element>) this.domDocument.getRootElement().getChildren(PARAMETERS)) {
			// return the first Function found by this name (=> never use names
			// twice!)
			if (e.getChild(s) != null)
				return readParameter(e.getChild(s).getAttribute(VALUE));
		}
		throw new NullPointerException("no such parameter: " + s);
	}

	/**
	 * Reads the parameters from the domDocument and return a Map containing
	 * these values with the name as key
	 *
	 * @return params
	 */
	@SuppressWarnings("unchecked")
	public Map<String, Double> getParameters() {
		List<Element> e_list = this.domDocument.getRootElement().getChildren(PARAMETERS);
		// create map
		LinkedHashMap<String, Double> params = new LinkedHashMap<String, Double>();

		// if the element does not exist return empty map
		if (e_list.isEmpty())
			return params;
		for (Element el : e_list) {
			// other wise fill
			for (Element e : (List<Element>) el.getChildren()) {
				params.put(e.getName(), readParameter(e.getAttribute(VALUE)));
			}
		}
		return params;
	}

	/**
	 * Returns main in and output path if the xml-file contains them If not it
	 * will throw a IllegalArgumentException
	 *
	 * @param IO
	 *            - in or out -> else exception
	 * @return path
	 */
	@SuppressWarnings("unchecked")
	public String getPath(String IO) {

		List<Element> e_list = this.domDocument.getRootElement().getChildren(PATH);

		for (Element el : e_list) {
			// specify element
			Element path_element = el.getChild(IO);

			if (path_element == null)
				throw new IllegalArgumentException(this.filename + "\r\n does not contain " + IO + "-element");
			// create path
			String path = FileRegister.USERDIR;
			// determine whether it is a relative or absolute path
			if (Boolean.parseBoolean(path_element.getAttributeValue(ABSOLUTE_PATH))) {
				path = "";
			}

			// get content
			path += path_element.getTextNormalize();

			// replace / for File.separator symbols
			path.replaceAll(DIVIDER, File.separator);
		}
		if (path == null)
			throw new IllegalArgumentException(this.filename + "\r\n does not contain " + IO + "-element");
		// return the path
		return path;

	}

	/**
	 * Gets the directories specified by s
	 *
	 * @param s
	 * @return File with path
	 * @throws DataConversionException
	 */
	public File getPath(String s, String io) throws DataConversionException {
		// return
		return new File(this.getPathName(s, io));

	}

	/**
	 *
	 * @param s
	 * @return path
	 * @throws DataConversionException
	 */
	@SuppressWarnings("unchecked")
	public String getPathName(String s, String io) throws DataConversionException {
		// get the element with this path
		List<Element> e_list = this.domDocument.getRootElement().getChildren(PATH);

		String epath = null;

		for (Element io_e : e_list) {

			// get the io part
			Element e = io_e.getChild(io);

			if (e != null) {
				e = e.getChild(s);

				if (e != null) {

					// if this element is an absolute path
					epath = e.getText();
					if (epath.contains(PN.TAB) || epath.contains(PN.LINESEPARATOR))
						epath = cleanString(path);

					// check if the pathe ends with a file separator character
					// otherwise add
					if (!(epath.endsWith(DIVIDER) || epath.endsWith(File.separator)))
						epath += File.separator;

					// if it is an absolute path
					if (e.getAttribute(ABSOLUTE_PATH).getBooleanValue())
						return epath;

					// otherwise add the current directory to the path
					epath = System.getProperty(FileRegister.USERDIR) + File.separator + epath;
				}
			}
		}
		return epath;
	}

	/**
	 *
	 * @return an array with two maps containing in and output paths
	 * @throws DataConversionException
	 */
	@SuppressWarnings("unchecked")
	public Map[] getPathNames() throws DataConversionException {
		// in- output strings
		String[] IO = { IN, OUT };

		// map
		Map[] map = new LinkedHashMap[2];
		map[0] = new LinkedHashMap();
		map[1] = new LinkedHashMap();

		// leist of path name elements
		List<Element> e_list = this.domDocument.getRootElement().getChildren(PATH);

		// if the document does not contain files return empty maps
		if (e_list.isEmpty()) {
			map[0] = new LinkedHashMap();
			map[1] = new LinkedHashMap();
			return map;
		}
		// iterate
		for (Element el : e_list) {
			for (int x = 0; x < IO.length; x++) {
				// get the list
				// elements
				Element io_element = el.getChild(IO[x]);
				List<Element> l = null;
				if (io_element == null)
					l = new LinkedList();
				else
					l = io_element.getChildren();

				Map<String, String> paths = new LinkedHashMap<String, String>();

				for (Element e : l) {
					paths.put(e.getName(), this.getPathName(e.getName(), IO[x]));
				}

				map[x].putAll(paths);
			}
		}
		return map;
	}

	/**
	 *
	 * @return map containing paths and their names
	 * @throws DataConversionException
	 */
	@SuppressWarnings("unchecked")
	public Map<String, File> getPaths() throws DataConversionException {
		// names
		Map[] names = getPathNames();

		// map to return
		Map<String, File> paths = new LinkedHashMap<String, File>();
		for (Map m : names) {
			for (Object o : m.keySet()) {
				paths.put(o.toString(), new File(m.get(o).toString()));
			}
		}

		return paths;

	}

	/**
	 * Reads the seeds from the domDocument and return a Map containing these
	 * values with the name as key
	 *
	 * @return seeds
	 */
	@SuppressWarnings("unchecked")
	public Map<String, Long[]> getSeeds() {
		List<Element> e_list = this.domDocument.getRootElement().getChildren(SEEDS);
		// create map
		LinkedHashMap<String, Long[]> seeds = new LinkedHashMap<String, Long[]>();

		// if the element does not exist return empty map
		if (e_list.isEmpty())
			return seeds;

		// other wise fill
		for (Element el : e_list) {
			for (Element e : (List<Element>) el.getChildren()) {
				String content = e.getValue().trim();
				// get the content
				String[] values = this.cuttValue(content);

				// values as long
				Long[] vals = new Long[values.length];

				for (int index = 0; index < vals.length; index++) {
					vals[index] = Long.valueOf(values[index].trim());
				}

				seeds.put(e.getName(), vals);
			}
		}
		return seeds;
	}

	/**
	 * @return title
	 */
	public String getTitle() {
		Element title_el = this.domDocument.getRootElement().getChild(TITLE);
		if (title_el == null)
			return null;
		else
			return title_el.getText();

	}

	/**
	 *
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return this.filename;
	}

	/**
	 * Cutts a certain value into pieces separated by separation chars
	 *
	 * @param content
	 * @return
	 */
	private String[] cuttValue(String content) {
		List<String> cutted = new ArrayList<String>();
		cutted.add(content);
		for (String separator : XMLInput.SEPARATORS) {
			List<String> smallercutts = new ArrayList<String>();
			for (String smaller : cutted) {
				String[] cut = smaller.split(separator);
				for (String piece : cut) {
					if (piece.trim().length() > 0)
						smallercutts.add(piece.trim());
				}
			}
			cutted = smallercutts;
		}
		// all separators have cutt the string
		String[] values = new String[cutted.size()];
		for (int i = 0; i < values.length; i++)
			values[i] = cutted.get(i);

		return values;
	}

}
