/*
 * This file is part of SIMCOLEP, a simulation model developed by the Department of Public Health, 
 * Erasmus MC, University Medical Center Rotterdam, Rotterdam, The Netherlands.
 *
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/ 
 * or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA
 *
 * You are free to:
 * Share � copy and redistribute the material in any medium or format
 * Under the following terms:
 * Attribution � You must give appropriate credit, provide a link to the license, and indicate if changes were 
 * made. You may do so in any reasonable manner, but not in any way that suggests the licensor endorses
 * you or your use.
 * NonCommercial � You may not use the material for commercial purposes.
 * NoDerivatives � If you remix, transform, or build upon the material, you may not distribute the modified material.
 */
/*
 * Created on 23-Dec-2004
 *
 */
package simcolep.tools.xmlparser;


import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Collection;

import org.jdom.*;
import org.jdom.output.Format;
import org.jdom.output.XMLOutputter;



/**
 * @author ir E.A.J. Fischer
 *@version 0.1.1-- Last Modified 08-02-05
 */
public class XMLOutput {
	/**name of the file*/
	private String filename = null;
	
	/**output stream*/
	private FileOutputStream out = null;
	
	/**internal docu,ent*/
	private org.jdom.Document internalDoc = null;
	
	/**
	 * Constructor will create outputstream to a file with this name
	 * @param filename
	 * @throws FileNotFoundException 
	 */
	public XMLOutput(String filename) 
				throws FileNotFoundException 
	{
		//file name
		this.filename = filename;
		//output stream
		this.out = new FileOutputStream(System.getProperty("user.dir")+File.separator+"output"+File.separator+this.filename+".xml");
		
		
		
	}
	
	/**
	 * Constructor will creat outputstream to a file with this name and in this path
	 * @param path
	 * @param filename
	 * @throws FileNotFoundException 
	 */
	public XMLOutput(String path,String filename) 
			throws FileNotFoundException {
//		file name
		this.filename = filename;
		//output stream
		
		this.out = new FileOutputStream(this.filename+".xml");
		
		
	}
    
    /**
     * Constructor creates a file stream to this file
     * @param file
     * @throws FileNotFoundException 
     */
	public XMLOutput(File file) 
			throws FileNotFoundException {
       this.filename = file.getName();
       
       this.out = new FileOutputStream(file);
    }
	
	/**
	 * Constructor uses the give ouput stream
	 * @param fos
	 */
	public XMLOutput(FileOutputStream fos) 
	{
		this.out = fos;
	}

    /**
	 * Writes an jdom-Document to an XML-file
	 * 
	 * @param doc
     * @throws IOException 
	 */
	public void OutputXML (Document doc) 
			throws IOException {
			
			XMLOutputter outputter = new XMLOutputter();
			//set 'pretty'format
			outputter.setFormat(Format.getPrettyFormat());
			//output
			outputter.output(doc, out);
			//flush
			this.out.flush();
			
		
		}
	/**
	 * Writes an element to the internal document
	 * @param element
	 */
	public void OutputXML(org.jdom.Element element)
	{
		//if the internaldocument was not set yet 
		if(this.internalDoc == null)
				this.internalDoc = new Document(new org.jdom.Element("output"));
		
		//add this element to the content
		this.internalDoc.getRootElement().addContent(element);
		
	}
	/**
	 * 
	 * @param string
	 * @param element
	 */
	public void OutputXML(String string, Element element) {
//		if the internaldocument was not set yet 
		if(this.internalDoc == null)
				this.internalDoc = new Document(new org.jdom.Element("output"));
		//an element of this type?
		if(this.internalDoc.getRootElement().getChild(string)==null)
			this.internalDoc.getRootElement().addContent(new Element(string));
		
		//add the element
		this.internalDoc.getRootElement().getChild(string).addContent(element);
			
		
	}

	/**
	 * Writes a list of elements to the internal documents
	 * @param children
	 */
	public void OutputXML(java.util.List<?> children)
	{
		//if the internaldocument was not set yet
		if(this.internalDoc == null)
				this.internalDoc = new Document(new Element("output"));
		//add this element to the content
		this.internalDoc.addContent(children);
		
	}
	
	/**
	 * Add the children to the element with the specific string as name
	 * @param string
	 * @param children
	 */
	public void OutputXML(String string, Collection<?> children) {
		//if the internaldocument was not set yet 
		if(this.internalDoc == null)
				this.internalDoc = new Document(new org.jdom.Element("output"));
		//an element of this type?
		if(this.internalDoc.getRootElement().getChild(string)==null)
			this.internalDoc.getRootElement().addContent(new Element(string));
		
		//add the element
		this.internalDoc.getRootElement().getChild(string).addContent(children);
	}
	
	/**
	 * Sets the rootelement to an element with specified name
	 * @param name
	 */
	public void setRootElement(String name)
	{
		//create internal document
		if(this.internalDoc==null)
			this.internalDoc = new Document(new Element(name));
		else
		{
			//detach old root
			Element root = this.internalDoc.detachRootElement();
			Content content = root.detach();
			
			//creat new root
			Element newRoot = new Element(name);
			//add old content
			newRoot.addContent(content);
			//add new root to the document
			this.internalDoc.setRootElement(newRoot);
		}
	}
	
	/**
	 * Sets the newRoot as the rootelement 
	 * 
	 * @param newRoot
	 */
	public void setRootElement(Element newRoot)
	{
		//create internal document
		if(this.internalDoc==null)
			this.internalDoc = new Document(newRoot);
		else
		{
			//detach old root
			Element root = this.internalDoc.detachRootElement();
			Content content = root.detach();
			
			//add old content
			newRoot.addContent(content);
			//add new root to the document
			this.internalDoc.setRootElement(newRoot);
		}
	}
	
	
	/**
	 * Writes the internal document to file and then closes the outputstream
	 * @throws IOException 
	 *  
	 *
	 */
	public void end() 
		throws IOException
	{
		//write
		this.OutputXML(this.internalDoc);
	    
		//close
		this.close();
		
	}
	
	/**
	 * Closes the outputstream 
	 * <br>Does <b>not</b> write the internal document to file first than use method end() 
	 * 
	 * @throws IOException
	 */
	public void close() 
			throws IOException
	{
		out.flush();
		out.close();
	}
}