/*
 * This file is part of SIMCOLEP, a simulation model developed by the Department of Public Health, 
 * Erasmus MC, University Medical Center Rotterdam, Rotterdam, The Netherlands.
 *
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/ 
 * or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA
 *
 * You are free to:
 * Share � copy and redistribute the material in any medium or format
 * Under the following terms:
 * Attribution � You must give appropriate credit, provide a link to the license, and indicate if changes were 
 * made. You may do so in any reasonable manner, but not in any way that suggests the licensor endorses
 * you or your use.
 * NonCommercial � You may not use the material for commercial purposes.
 * NoDerivatives � If you remix, transform, or build upon the material, you may not distribute the modified material.
 */
/**
 * created: 14-Aug-2006
 */
package simcolep.tools.io;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;



/**
 * Used to format a collection of output of single numbers
 * 
 * @author ir. E.A.J. Fischer
 * @version 1.0
 */
@SuppressWarnings("unchecked")
public final class NumberFormat implements DataFormat {

	/**Can be used in text documents as header for the columns*/
	public static final String HEADER =
		"Average\tMinimum\tMaximum\tValues....";	
	/***/
	public static final String MEAN = "average";
	/***/
	public static final String MIN = "minimum";
	/***/
	public static final String MAX = "maximum";
	/***/
	public static final String VALUES = "values" ;
	/**
	 * 
	 */
	public NumberFormat() {
		super();
		
	}

	/**
	 * @see simcolep.tools.io.DataFormat#combine(java.util.Collection)
	 */
	@SuppressWarnings("unchecked")
	public Serializable combine(Collection output) {
		String res = HEADER+"\r\n";
		
		//combine a map
		for(Object o : output)
		{
			//able to use
			Class c = o.getClass();
			boolean map = c.equals(Map.class)||c.equals(LinkedHashMap.class)||c.equals(TreeMap.class);
			//map contain averages and other from number format
			if(map)
			{
				//average
				res+= ((Map)o).get("average")+"\t";
				
				//minimum
				res+= ((Map)o).get("minimum")+"\t";
				
				//maximum
				res+= ((Map)o).get("maximum")+"\t";
				
				//values
				for(Number n : (List<Number>)((Map)o).get("values"))
						{
					res+=n.toString()+"\t";
						}
							
				//next line
				res+="\r\n";
			}else
					res += o.toString()+"\r\n";
		}
		
		return res;
	}

	
	/**
	 * Although unexpected the average will return a map containing:
	 * <br> respectively: KEY - ENTRY
	 * <li> mean - mean of the outputs
	 * <li> min - minimum value
	 * <li> max - maximum value
	 * <li> values - list containing all values of the outputs
	 * 
	 * @see simcolep.tools.io.DataFormat#average(java.util.Collection)
	 */
	public Object average(Collection output) {
		//average
		double av = 0.0;
		//minimum
		double min = Double.POSITIVE_INFINITY;
		//maximum
		double max = Double.NEGATIVE_INFINITY;
		//values
		List<Number> values = new ArrayList<Number>();
		
		//iterate the outputs
		for(Object o : output)
		{
			//cast to number
			Number n = (Number)o;
			
			//add to average
			av+=n.doubleValue();
			
			//set min
			min = Math.min(n.doubleValue(),min);
			//set max
			max = Math.max(n.doubleValue(),max);
			
			//set value
			values.add(n);
			
		}
		//create that map that will be returned
		Map<String, Object> map 
			= new LinkedHashMap<String, Object>();
		//add the output
		//average
		map.put(NumberFormat.MEAN,new Double(av/output.size()));
		//minimum
		map.put(NumberFormat.MIN,new Double(min));
		//maximum
		map.put(NumberFormat.MAX,new Double(max));
		//the list with all entries
		map.put(NumberFormat.VALUES, values);
		
		// return the map
		return map;
	}

	/**
	 * @see simcolep.tools.io.DataFormat#averageString(java.util.Collection)
	 */
	@SuppressWarnings("unchecked")
	public Serializable averageString(Collection output) {
		//get the data averaged
		Map<String,Object> map = (Map<String,Object>)this.average(output);
		
		//create a single line of text stating the values 
		//average
		StringBuffer res = new StringBuffer(map.get(MEAN)+DataFormat.TAB);
		//min
		res.append(map.get(MIN)+DataFormat.TAB);
		//max
		res.append(map.get(MAX)+DataFormat.TAB);
		//values
		for(Number n : (List<Number>) map.get(VALUES))
		{
			res.append(n.toString()+DataFormat.TAB);
		}
		//return the result
		return res.toString();
	}

	/**
	 * @see simcolep.tools.io.DataFormat#nodatamessage()
	 */
	public String nodatamessage() {
		
		return DataFormat.DEFAULTNODATAMESSAGE;
			
	}

	@Override
	public Serializable sum(Collection output) {
		//average
		double sum = 0.0;
		//minimum
		double min = Double.POSITIVE_INFINITY;
		//maximum
		double max = Double.NEGATIVE_INFINITY;
		//values
		List<Number> values = new ArrayList<Number>();
		
		//iterate the outputs
		for(Object o : output)
		{
			//cast to number
			Number n = (Number)o;
			
			//add to average
			sum+=n.doubleValue();
			
			//set min
			min = Math.min(n.doubleValue(),min);
			//set max
			max = Math.max(n.doubleValue(),max);
			
			//set value
			values.add(n);
			
		}
		
		
		//create a single line of text stating the values 
		//average
		StringBuffer res = new StringBuffer(sum+DataFormat.TAB);
		//min
		res.append(min+DataFormat.TAB);
		//max
		res.append(max+DataFormat.TAB);
		//values
		for(Number n : values)
		{
			res.append(n.toString()+DataFormat.TAB);
		}
		//return the result
		return res.toString();
	}

	@Override
	public Serializable standard() {
			return new Long(0);
	}

}
