/*
 * This file is part of SIMCOLEP, a simulation model developed by the Department of Public Health, 
 * Erasmus MC, University Medical Center Rotterdam, Rotterdam, The Netherlands.
 *
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/ 
 * or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA
 *
 * You are free to:
 * Share � copy and redistribute the material in any medium or format
 * Under the following terms:
 * Attribution � You must give appropriate credit, provide a link to the license, and indicate if changes were 
 * made. You may do so in any reasonable manner, but not in any way that suggests the licensor endorses
 * you or your use.
 * NonCommercial � You may not use the material for commercial purposes.
 * NoDerivatives � If you remix, transform, or build upon the material, you may not distribute the modified material.
 */
/**
 * created: 05-Apr-2006
 */
package simcolep.tools.io;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;



/**
 * String format can format string for output handlers
 * 
 * 
 * @author ir. E.A.J. Fischer
 * @version 1.0
 */
public class StringFormat implements DataFormat<String> {
	private boolean sidebyside = false;
	
	private static final String NODATACHAR ="-";
	
	/**
	 * 
	 */
	public StringFormat(boolean trans) {
		super();
		
		this.sidebyside = trans;
		
	}

	/**
	 * @see simcolep.tools.io.DataFormat#combine(java.util.Collection)
	 */
	@SuppressWarnings("unchecked")
	public Serializable combine(Collection<String> output) {
		if(this.sidebyside)
			return sidebyside(output);
		
		StringBuffer s = new StringBuffer();
		for(Object obj : output)
		{
			s.append(obj.toString()+DataFormat.LINESEPARATOR);
		}
		return s;
	}

	/**
	 * Return the combined string but now with each line of each string on the same line of the output string separated by a tab 
	 * <br>For instance:<br>
	 * output contains two strings:<br>
	 * hello world <br> how are you?<br>
	 * and <br>
	 * out there<br>
	 * I'm fine<br>
	 * Would return as: <br>
	 * hello world <code>Tab</code> out there<br>
	 * how are you<code> Tab</code> I'm fine<br>
	 * @param output
	 * @return
	 */
	@SuppressWarnings("unchecked")
	private Serializable sidebyside(Collection output) {
		ArrayList<String[]> cutDownStrings = new ArrayList<String[]>();
		
		int maximumLines = 0;
		
		for(Object obj : output)
		{
			String[] cutDown =
				((String)obj).split(DataFormat.LINESEPARATOR);
			maximumLines = Math.max(maximumLines, cutDown.length);
			
			cutDownStrings.add(cutDown);
		}
		
		StringBuffer strbuf = new StringBuffer();
		
		for(int i = 0; i < maximumLines; i++)
		{
			//add line number
			strbuf.append(i + DataFormat.TAB);
			for(String[] s : cutDownStrings)
			{
				if(s.length > i)
					strbuf.append(s[i]+DataFormat.TAB);
				else
					strbuf.append(NODATACHAR);
			}
			strbuf.append(DataFormat.LINESEPARATOR);
		}
		
		return strbuf;
	}

	/**
	 * @see simcolep.tools.io.DataFormat#average(java.util.Collection)
	 */
	public String average(Collection<String> output) {
		
		return null;
	}

	/**
	 * @see simcolep.tools.io.DataFormat#averageString(java.util.Collection)
	 */
	@SuppressWarnings("unchecked")
	public Serializable averageString(Collection<String> output) {
		
		return null;
	}

	/**
	 * 
	 * @see simcolep.tools.io.DataFormat#nodatamessage()
	 */
	public String nodatamessage() {
		
		return OutputHandler.NODATA_COLLECTED_MESSAGE;
	}

	@Override
	public Serializable sum(Collection<String> output) {
	
		return nodatamessage();
	}

	@Override
	public Serializable standard() {
		return NODATACHAR;
	}

}
