/*
 * This file is part of SIMCOLEP, a simulation model developed by the Department of Public Health, 
 * Erasmus MC, University Medical Center Rotterdam, Rotterdam, The Netherlands.
 *
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/ 
 * or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA
 *
 * You are free to:
 * Share � copy and redistribute the material in any medium or format
 * Under the following terms:
 * Attribution � You must give appropriate credit, provide a link to the license, and indicate if changes were 
 * made. You may do so in any reasonable manner, but not in any way that suggests the licensor endorses
 * you or your use.
 * NonCommercial � You may not use the material for commercial purposes.
 * NoDerivatives � If you remix, transform, or build upon the material, you may not distribute the modified material.
 */
package simcolep.tools.io;

import java.io.Serializable;

import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.TreeMap;



/**
 * Determines the outputformat for outputlists
 * 
 * @author ir. E.A.J. Fischer
 * @version 1.0
 */
public class OutputListFormat 
			implements DataFormat<OutputList<?>> {

	/**
	 * How to handle null 
	 * true do not allow null values for aggregation, false is ignore value
	 */
	public boolean handleNull = false;
	
	/**
	 * When combining which symbol to use to depict a null or non existing value
	 */
	public final static String nullSymbol = "0.0"; 
	
	/**
	 * Excel cannot contain more than 256 columns (which is incredibly stupid for any spread sheet program not to be flexible in this).
	 * But any if this parameter is true the program will return the results in rows instead of columns
	 */
	public boolean compatible_with_excel = false;
	
	/**
	 * 
	 *
	 */
	public OutputListFormat()
	{
		super();
	}
	
	/**
	 * 
	 * 
	 * @param excell - whether or not concern about excell compatability
	 */
	public OutputListFormat(boolean excell) {
		super();
		this.compatible_with_excel = excell;
		}

	
	/**
	 * The data is stored in separate lists for each repeat or run. 
	 * 
	 * @see simcolep.tools.io.DataFormat#combine(Collection)
	 */
	public Serializable combine(Collection<OutputList<?>> output) {
		if(this.compatible_with_excel)
			return this.combine_rows(output);
		else
			return this.combine_columns(output);
	}
	
	/**
	 * The data is returned with each list as a separate column and 1 x or time value
	 * @param output
	 * @return string of the output
	 * 
	 */
	@SuppressWarnings("unchecked")
	protected Serializable combine_rows(Collection<OutputList<?>> output) {
		//first line will contain the time line 
		StringBuffer first_line = new StringBuffer("--"+DataFormat.TAB);
		int length_first_line = 0;
		//futher text
		StringBuffer result_lines = new StringBuffer();
		
		//combine output
		for(OutputList<?> ol : output)
		{		
			//add the name of the list 
			result_lines.append(ol.getName()+DataFormat.TAB);
			//iterate the outputlist
			for(OutputElement o : ol)
			{
				if(o==null)
					result_lines.append(nullSymbol);
				else
				{
					if(length_first_line <= ol.indexOf(o))
						{
						//add time
						first_line.append(o.getTime()+DataFormat.TAB);
						//add one to the length
						length_first_line++;
						}
					
					result_lines.append(o.getValue().toString()+DataFormat.TAB);
				}
					
			}
			
			result_lines.append(DataFormat.LINESEPARATOR);
		}
			
		first_line.append(DataFormat.LINESEPARATOR);
		first_line.append(result_lines);
		//return the results
		return first_line.toString();
	}

	/**
	 * The data is returned with each list as a separate column and 1 x or time value
	 * @param output
	 * @return s
	 * 
	 */
	protected Serializable combine_columns(Collection<OutputList<?>> output)
	{
			
		//get the sorted maop
		Map<Double,Number[]> lines = this.toLines(output);
		
		//create the output string
		StringBuffer s = new StringBuffer();
		Iterator<Double> it =lines.keySet().iterator();
		while(it.hasNext())
		{
			//key is time
			Double key = it.next();
			s.append(key+DataFormat.TAB);
			//array
			for(Object obj : lines.get(key))
			{
				if(obj == null) 
					s.append(nullSymbol+DataFormat.TAB);
				else 
					s.append(obj.toString()+DataFormat.TAB);
			}
			s.append(DataFormat.LINESEPARATOR);
		}
		
		return s.toString();
	}

	/**
	 * Calculates the averages of all outputlist  
	 * 
	 * @see simcolep.tools.io.DataFormat#aggregate(Collection)
	 * @throws IllegalArgumentException - handling of null values is true and null values are present
	 * @throws ClassCastException - if the output is not a Number
	 */
	public OutputList<?> average(Collection<OutputList<?>> output) {
		//get the treemap with the values
		Map<Double,Number[]> map = this.toLines(output);
		
		//Outputlist 
		OutputList<Double> out = new OutputList<Double>("");
		
		//iterate the lines 
		for(Double d : map.keySet())
		{
			//get the average of the line
			Number[] array = (Number[]) map.get(d);
			double total = 0;
			double count = 0;
			for(Number db : array)
			{
				
				if(db == null)
					{
					 if(handleNull)
						throw new IllegalArgumentException(
								"entry == null " +
								"and handling is set to throw exception");
					}else
						{
							total+=db.doubleValue();
							count+=1.0;
						}				
			 }
			
			//time 
			double time = d.doubleValue();
			//value
			Double value = new Double(total/count);
			
			//add the value to a new OutputElement
			OutputElement<Double> el = new OutputElement<Double>(time,value);
			
			//add to outputlist
			out.addLast(el);
		}
		
		//return the outputlist
		return out;
	}

	/**
	 * @see simcolep.tools.io.DataFormat#averageString(Collection)
	 */
	public Serializable averageString(Collection<OutputList<?>> output) {
		StringBuffer s = new StringBuffer();
		//averages
		OutputList<?> lines = this.average(output);
		//iterate
		for(OutputElement<?> el :lines)
		{
			s.append(el.getTime()+DataFormat.TAB+el.getValue()+DataFormat.LINESEPARATOR);
		}
		
		return s.toString();
	}

	/**
	 * A treemap is created containing arrays of the length of the collection of outputs
	 * for each time untill the list with longrest time 
	 * 
	 * @param output
	 * 
	 * @return 
	 */
	protected TreeMap<Double, Number[]> toLines(Collection<OutputList<?>> output)
	{
		//create a treemap containing arrays length output collection 
		//that is indexed by the Double time of output 
		TreeMap<Double, Number[]>lines = new TreeMap<Double, Number[]>(); 
		
		//size of the array
		int size = output.size();
		
		//iterate collection of output
		int index = 0;
		for(OutputList<?> list : output)
		{
			//iterate outputlist
			for(OutputElement<?> element: list)
			{
				//time
				Double time = new Double(element.getTime());
				//add the value for the rigth time at the right place
				if(lines.containsKey(time))
						lines.get(time)[index] = (Number)element.getValue();
				else{ 
					Number[] line = new Number[size];
					lines.put(time,line);
					lines.get(time)[index] = (Number) element.getValue();
				}
			}
			index++;
		}
		
		return lines;
	}

	/**
	 * 
	 * @see simcolep.tools.io.DataFormat#nodatamessage()
	 */
	@SuppressWarnings("unchecked")
	public OutputList<?> nodatamessage() {
		return new OutputList(OutputHandler.NODATA_COLLECTED_MESSAGE);
			}

	@Override
	public Serializable sum(Collection<OutputList<?>> output) {
		//get the treemap with the values
			Map<Double,Number[]> map = this.toLines(output);
			
			//Outputlist 
			OutputList<Double> out = new OutputList<Double>("");
			
			//iterate the lines 
			for(Double d : map.keySet())
			{
				//get the average of the line
				Number[] array = (Number[]) map.get(d);
				double total = 0;
			
				for(Number db : array)
				{
					
					if(db == null)
						{
						 if(handleNull)
							throw new IllegalArgumentException(
									"entry == null " +
									"and handling is set to throw exception");
						}else
							{
								total+=db.doubleValue();
								
							}				
				 }
				
				//time 
				double time = d.doubleValue();
				//value
				Double value = new Double(total);
				
				//add the value to a new OutputElement
				OutputElement<Double> el = new OutputElement<Double>(time,value);
				
				//add to outputlist
				out.addLast(el);
			}
			
			//return the outputlist as string
			return out.toString();
		
	}

	@Override
	public Serializable standard() {
			return new OutputList<Object>();
	}

	
}
