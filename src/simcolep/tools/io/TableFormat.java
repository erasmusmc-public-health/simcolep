/*
 * This file is part of SIMCOLEP, a simulation model developed by the Department of Public Health, 
 * Erasmus MC, University Medical Center Rotterdam, Rotterdam, The Netherlands.
 *
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/ 
 * or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA
 *
 * You are free to:
 * Share � copy and redistribute the material in any medium or format
 * Under the following terms:
 * Attribution � You must give appropriate credit, provide a link to the license, and indicate if changes were 
 * made. You may do so in any reasonable manner, but not in any way that suggests the licensor endorses
 * you or your use.
 * NonCommercial � You may not use the material for commercial purposes.
 * NoDerivatives � If you remix, transform, or build upon the material, you may not distribute the modified material.
 */
/**
 * created: 04-Apr-2006
 */
package simcolep.tools.io;

import java.io.Serializable;
import java.util.Collection;

/**
 * Formats a collection of Tables
 * 
 * @author ir. E.A.J. Fischer
 * @version 1.0
 * 
 */
public class TableFormat implements DataFormat<Table> {

	/**When averaging or summing make all tables fit to the first 
	 * <br> In the case of too many columns or rows add the values in the extra columns to the last
	 * <br> in the case of too less columns or rows expend to extra cells with value 0
	 *  If false only Tables of equal size are summed or averaged 
	 * */
	public boolean fittofirst = true;
	/**
	 * 
	 * @author ir. E.A.J. Fischer
	 * @version 1.0
	 */
	public enum Format{
		HORIZONTAL,
		VERTICAL,
		CELLbyCELL;
		
	}
		
	/**
	 * Format of the tables when the collection is combined
	 * default HORIZONTAL
	 */
	public Format cAlignment = Format.HORIZONTAL;
	
	/**
	 * 
	 */
	public TableFormat() {
		super();
		
	}
	
	
	/**
	 * @see simcolep.tools.io.DataFormat#combine(java.util.Collection)
	 */
	public Serializable combine(Collection<Table> output) {
		
		StringBuffer res = new StringBuffer();
		
		if(cAlignment.equals(Format.VERTICAL))
		{
			for(Table t : output)
			{
				res.append(t.toString()+DataFormat.LINESEPARATOR);
			}
			return res.toString();
		}else if(cAlignment.equals(Format.HORIZONTAL))
		{
			int num = output.size(); 
			//write the column names
			for(Table t : output)
			{
				res.append(DataFormat.TAB);
				for(String cname : t.getColumnNames())
				{
					res.append(cname+DataFormat.TAB);
				}
				res.append(DataFormat.TAB);
			}
			res.append(DataFormat.LINESEPARATOR);
			int row_index = 0;
			//write rowname followed by the values in the row
			while(num>0)
			{
				
				for(Table t : output)
				{
					//if the table has this row
					if(row_index < t.getSize()[0])
					{
						res.append(t.getRowName(row_index)+DataFormat.TAB);
						
						for(double d : t.getRow(row_index))
						{
							res.append(d+DataFormat.TAB);
						}
					}else 
					{
						num--;
						for(int y = 0; y<t.getSize()[1]+1;y++)
						{
							res.append(" "+DataFormat.TAB);
						}
					}
					res.append(DataFormat.TAB);
				}
				row_index++;
				res.append(DataFormat.LINESEPARATOR);
			}
			
			return res.toString();
		}else if(cAlignment.equals(Format.CELLbyCELL))
		{
			throw new IllegalArgumentException("not implemented yet");
		}
		
		return null;
	}

	/**
	 * @see simcolep.tools.io.DataFormat#average(java.util.Collection)
	 */
	public Table average(Collection<Table> output) {
		//table with results
		Table res = null;
		
		//get all the tables added up
		for(Table t : output)
		{
			if(res==null){//new Table 
				res = new Table(t.getSize());
				//set column names
				res.setColumnNames(t.getColumnNames());
				//set row names
				res.setRowNames(t.getRowNames());
			}
			
			//if the size of the table does not equal that of the results table 
			if(!res.equalDimensions(t))
				if(this.fittofirst)
					t = this.fit(t,res);
				else
					throw new IllegalArgumentException(
							"all tables should have " +
							"the same dimensions for averaging");
			
			//add the values to the cells
			for(int x = 0; x<t.getSize()[0];x++)
			{
				for(int y = 0; y<t.getSize()[1]; y++)
				{
					//add the value
					res.addtoCell(x,y,t.getCell(x,y));
				}
			}
			
		}
		
		//divide all cells by the number of tables
		for(int x = 0; x < res.getSize()[0]; x++)
		{
			for(int y = 0; y < res.getSize()[1]; y++)
			{
				res.divideCell(x,y,output.size());
			}
		}
		//return the result
		return res;
	}

	/**
	 * @see simcolep.tools.io.DataFormat#averageString(java.util.Collection)
	 */
	public Serializable averageString(Collection<Table> output) {
		
		return this.average(output).toString();
		
	}

	/**
	 * 
	 * @see simcolep.tools.io.DataFormat#nodatamessage()
	 */
	public Table nodatamessage() {
		Table t = new Table(1,1);
		t.setCell(0,0,Double.NaN);
		t.setColumnName(0,OutputHandler.NODATA_COLLECTED_MESSAGE);
		t.setRowName(0,OutputHandler.NODATA_COLLECTED_MESSAGE);
		
		return t;
	}


	@Override
	public Table sum(Collection<Table> output) {
		//table with results
		Table res = null;
		
		//get all the tables added up
		for(Table t : output)
		{
			if(res==null){//new Table 
				res = new Table(t.getSize());
				//set column names
				res.setColumnNames(t.getColumnNames());
				// set row names
				res.setRowNames(t.getRowNames());
			}
			
			//if the size of the table does not equal that of the results table 
			if(!res.equalDimensions(t))
				if(this.fittofirst)
					t = this.fit(t,res);
				else
					throw new IllegalArgumentException(
							"all tables should have " +
							"the same dimensions for averaging");
			
			//add the values to the cells
			for(int x = 0; x<t.getSize()[0];x++)
			{
				for(int y = 0; y<t.getSize()[1]; y++)
				{
					//add the value
					res.addtoCell(x,y,t.getCell(x,y));
				}
			}
			
		}
		
		//return the result
		return res;
	}

	/**
	 * 
	 * @param t
	 * @param res
	 * @return
	 */
	private Table fit(Table t, Table res) {
		
	
		double[][] t_table = t.table;
		/*columns*/
		/*too many columns*/
		if(t.c()> res.c())
		{
			for(int r = 0; r < t_table.length; r++)
			{
				//for each row add the columns that are too much to the last okay one
				double value = t_table[r][res.c()-1];
				for(int c = res.c(); c <t.c();c++)
				{
					value += t_table[r][c];
					t_table[r][c] = 0;
				}
				t_table[r][res.c()-1] = value;
			}
		}/* too few*/
		else if (t.c()< res.c())
		{
			double[][] dummy_t = new double [t.r()][res.c()];
			for(int r =0; r < t.r(); r++)
			{
				//add zeros for those that are not in t 
				for(int c = 0; c <res.c();c++)
				{
					if(c < t.c())
					{
						dummy_t[r][c] = t_table[r][c];
					}else
						dummy_t[r][c] = 0;
				}
			}
			t_table = dummy_t;
		}
		
		/*rows*/
		/*too many rows*/
		if(t.r()> res.r())
		{
			for(int c = 0; c < t_table[0].length; c++)
			{
				//for each row add the columns that are too much to the last okay one
				double value = t_table[res.r()-1][c];
				for(int r = res.r(); r <t.r();r++)
				{
					value += t_table[r][c];
					t_table[r][c] = 0;
				}
				t_table[res.r()-1][c] = value;
			}
		}/* too few*/
		else if (t.r()< res.r())
		{
			double[][] dummy_t = new double [res.r()][res.c()];
			for(int c =0; c < res.c(); c++)
			{
				//add zeros for those that are not in t 
				for(int r = 0; r <res.r();r++)
				{
					if(r < t.r())
					{
						dummy_t[r][c] = t_table[r][c];
					}else
						dummy_t[r][c] = 0;
				}
			}
			t_table = dummy_t;
		}
		
		//write the correct dimensions to a table
		double[][] new_table = new double[res.r()][res.c()]; 
		for(int r =0; r < res.r(); r++)
		{
			for(int c = 0; c < res.c(); c++)
			{
				new_table[r][c] = t_table[r][c];
			}
		}
		
		t = new Table(new_table);
		return t;
	}


	@Override
	public Serializable standard() {
		return new Table(0,0);
	}

}
