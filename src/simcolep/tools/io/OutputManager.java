/*
 * This file is part of SIMCOLEP, a simulation model developed by the Department of Public Health, 
 * Erasmus MC, University Medical Center Rotterdam, Rotterdam, The Netherlands.
 *
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/ 
 * or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA
 *
 * You are free to:
 * Share � copy and redistribute the material in any medium or format
 * Under the following terms:
 * Attribution � You must give appropriate credit, provide a link to the license, and indicate if changes were 
 * made. You may do so in any reasonable manner, but not in any way that suggests the licensor endorses
 * you or your use.
 * NonCommercial � You may not use the material for commercial purposes.
 * NoDerivatives � If you remix, transform, or build upon the material, you may not distribute the modified material.
 */
/**
 * created: 04-Apr-2006
 */
package simcolep.tools.io;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;

import java.text.DateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.LinkedHashMap;


import simcolep.exceptions.SimulationException;
import simcolep.fun2.Archimedes;

import simcolep.sim.Gylfi;
import simcolep.sim.Herbarium;
import simcolep.tools.PN;



/**
 * Manager for output
 * 
 * @author ir. E.A.J. Fischer
 * @version 1.0
 */
public class OutputManager {

	 /**Default extension of output files
	 * This is not added by this manager to file names but filenames can be extended with it by other methods or classes
	 * */
	public static final String DEFAULT_EXTENSION = ".txt";
	/***/
	public static final String PAJEKNETWORKEXTENSION = ".net";
	/***/
	private static final String RUN_AT_TEXT = "Model ran at: ";
	/***/
	private static final String NO_HANDLER ="No output handler of the name:  ";
	
	
	/**the single instance of outputmanager*/
	private static OutputManager manager = null;
	
	

	/**
	 * Default output path
	 */
	public static String DEFAULT_OUTPUT_PATH = 
		System.getProperty("user.dir")+File.separator;
	
	
	
	/**
	 * 
	 * @return manager
	 */
	public static OutputManager getManager()
	{
		if(manager == null) 
				manager = new OutputManager();
		
		return manager; 
	}
	
	
	

	
	/**the managers' outputhandler map*/
	private Map<Object, OutputHandler> handlers = null;
	

	/**filewriter if only one file is written for all outputs*/
	private FileWriter out = null;
	
	private HashMap<String,Writer> databasewriters = null;
	
	
	
	/**
	 * 
	 */
	private OutputManager() {
		super();
		this.initialize();
	}
	
	/**
	 * 
	 *Initializa the map of output handlers
	 */
	public void initialize()
	{
		this.handlers = 
			new LinkedHashMap<Object,OutputHandler>();
	}
	
	/**
	 * Creates and adds a new handler to the manager
	 * 
	 * @param name
	 * @param path
	 * @param file
	 * @param outputmode
	 * @param df
	 */
	public void newHandler(Object name, String path, String file, int outputmode, DataFormat<?> df, int repeats)
	{
		//output handler
		OutputHandler oh = new OutputHandler();
		//set up handler
		oh.setup(path, file,outputmode,df, repeats);
		//add to list
		this.handlers.put(name,oh);
		
		
	}
	
	/**
	 * Add a handler to the manager
	 * 
	 * @param name
	 * @param oh
	 */
	public void addHandler(Object name, OutputHandler oh)
	{
		this.handlers.put(name,oh);
	
	}
	/**
	 * 
	 * @throws IOException
	 */
	public void addDatabasewriter(String code) throws IOException
	{//if the file writer is not yet initialized
		if(this.databasewriters==null)
			this.databasewriters= new HashMap<String, Writer>();
		
		if(!this.databasewriters.containsKey(code))
		{
			//out-file
			String output_file 
					= FileRegister.getFile_fullpath(
							PN.DB_DIR+code, 
							FileRegister.OUT);
			
			this.databasewriters.put(code, new FileWriter(output_file));
			//clean up any stuff
			this.databasewriters.get(code).flush();
		}
	}
	/**
	 * Remove all temporary files created for use of outputhandlers
	 *
	 */
	public void removeHandlerTemporaries()
	{
		for(Object o: this.handlers.keySet())
		{
			this.handlers.get(o).delete_temp_files();
		}
	}
	
	/**
	 * get the handler
	 * @param name
	 * @return handler
	 */
	public OutputHandler getHandler(String name)
	{
		if(!this.handlers.containsKey(name))
			throw new SimulationException(
					NO_HANDLER+name);
		
		return this.handlers.get(name);		 
	}
	
	
	
	/**
	 * Remove the handler
	 * @param name
	 * @return handler
	 */
	public OutputHandler removeHandler(String name)
	{
		return this.handlers.remove(name);
	}
	
	/**
	 * Simple output of a collection
	 * 
	 * @param file
	 * @param output
	 * @throws IOException 
	 */
	public void output(File file, Collection<?> output) 
				throws IOException
	{
		//create file writer
		FileWriter f = new FileWriter(file);
		
		//iterate the collection
		for(Object o : output)
		{
			f.write(o.toString()+DataFormat.LINESEPARATOR);
		}
		
		//close and flush writer
		f.flush();
		f.close();
		
	}
	/**
	 * Simple output of any object
	 * @param file
	 * @param output
	 * @throws IOException 
	 */
	public void output(File file, Object output) 
			throws IOException
	{
		//create file writer
		FileWriter f = new FileWriter(file);
		
		f.write(output.toString()+DataFormat.LINESEPARATOR);
				
		//close and flush writer
		f.flush();
		f.close();
	}
	
	
	/**
	 * Finish this particular handler
	 * 
	 * @param s
	 * @throws IOException 
	 * @throws ClassNotFoundException
	 */
	public void finish(String s) 
			throws IOException, ClassNotFoundException
	{
			this.handlers.get(s).finish();
		
	}
	
	/**
	 * Finalize all handlers in this manager
	 * @param one_file Write all to one file 
	 * 
	 * @throws ClassNotFoundException 
	 * @throws IOException 
	 *
	 */
	public void finishAll(boolean one_file) 
		throws IOException, ClassNotFoundException
	{
		//if the file writer is not yet initialized
			if(this.out==null)
			{
				//out-file
				String output_file 
						= FileRegister.getFile_fullpath(
								PN.OUTPUT_DIR, 
								FileRegister.OUT);
				
				this.out = new FileWriter(output_file);
				//clean up any stuff
				this.out.flush();
			}
		
			if(this.databasewriters != null)
				{
					for(String key : this.databasewriters.keySet())
					{
						this.databasewriters.get(key).flush();
						this.databasewriters.get(key).close();
					}
				}
			ClassCastException handlersCCException = null;
			IOException handlersIOException = null;
			//normal handlers
			for(Object obj : this.handlers.keySet())
			{
				if(one_file)
				{
					//finish the handler and append to the file
					this.out.append(DataFormat.LINESEPARATOR);
					this.out.append(obj.toString()+DataFormat.LINESEPARATOR);
					try{
						try{
							this.handlers.get(obj).finishToFile(this.out);
						} catch (IOException e)
						{
							this.out.append("Handler "+obj.toString()+" threw exception: " + e.getClass().getName()+PN.LINESEPARATOR);
							this.out.flush();
							this.handlers.get(obj).delete_temp_files();
							handlersIOException = e;
						}
					}catch(ClassCastException c)
					{
						this.out.append("Handler "+obj.toString()+" threw exception: " +c.getClass().getName()+PN.LINESEPARATOR);
						this.out.flush();
						this.handlers.get(obj).delete_temp_files();
						handlersCCException = c;
					}
				}else{
					//finish the normal handler
					this.handlers.get(obj).finish();
				}
			}
			
		
			//write all input to the main output file
				//version
				this.out.append(DataFormat.LINESEPARATOR + DataFormat.LINESEPARATOR + PN.VERSION_NAME+DataFormat.LINESEPARATOR);
				//date and time
				Date date = new Date(System.currentTimeMillis());
				DateFormat df = DateFormat.getDateTimeInstance(DateFormat.LONG,
                        DateFormat.SHORT,
                        Locale.getDefault());
				this.out.append(RUN_AT_TEXT +DataFormat.TAB+ df.format(date)+DataFormat.LINESEPARATOR);
				//add the input data
				this.out.append(DataFormat.LINESEPARATOR);
				this.out.append(Archimedes.BraintoString()+DataFormat.LINESEPARATOR);
				this.out.append(DataFormat.LINESEPARATOR+DataFormat.LINESEPARATOR);
				this.out.append(DataFormat.LINESEPARATOR);
				this.out.append(Gylfi.BraintoString()+DataFormat.LINESEPARATOR);
				this.out.append(DataFormat.LINESEPARATOR);
				//seeds
				this.out.append(Herbarium.SeedBanktoString());
				//flush and close
				this.out .flush();
				this.out.close();
				this.out = null;
				
				//if handlers caused an exception throw it now
				if(handlersCCException != null)
					throw handlersCCException;
				if(handlersIOException != null)
					throw handlersIOException;
			
	}
	
	/**
	 * 
	 * @see java.lang.Object#toString()
	 */
	public String toString(){
		StringBuffer s =new StringBuffer("Outputmanager"+DataFormat.LINESEPARATOR+"for ");
		//handlers
			if(!this.handlers.isEmpty())
				s.append("handlers: ");
		for(Object o :this.handlers.keySet())
		{
			s.append(o.toString()+", ");
		}
		 
		s.append(DataFormat.LINESEPARATOR);
			
		return s.toString();
	}

	/**
	 * 
	 * @param code 
	 * @return databasewriter
	 */
	public Writer getDatabaseWriter(String code) {
		return databasewriters.get(code);
	}

}
