/*
 * This file is part of SIMCOLEP, a simulation model developed by the Department of Public Health, 
 * Erasmus MC, University Medical Center Rotterdam, Rotterdam, The Netherlands.
 *
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/ 
 * or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA
 *
 * You are free to:
 * Share � copy and redistribute the material in any medium or format
 * Under the following terms:
 * Attribution � You must give appropriate credit, provide a link to the license, and indicate if changes were 
 * made. You may do so in any reasonable manner, but not in any way that suggests the licensor endorses
 * you or your use.
 * NonCommercial � You may not use the material for commercial purposes.
 * NoDerivatives � If you remix, transform, or build upon the material, you may not distribute the modified material.
 */
package simcolep.tools.io;

import java.io.Serializable;

import java.util.Collection;



/**
 * Specifies the format that the data are in. Format specify the way the output of different repeats is combined or averaged
 * 
 * @author ir. E.A.J. Fischer
 * @version 1.0
 * @param C - the type of data in the collection
 */
public interface DataFormat<C> {

	/**Combine format*/
	public final static int COMBINE =0;
	
	/**average format*/
	public final static int AVERAGE =1;
	
	/**sum format*/
	public final static int SUM =2;
	/**default message when no data is added*/
	public final static String DEFAULTNODATAMESSAGE = "### NO DATA ADDED###";
	
	/**next line*/
	public final static String LINESEPARATOR = System.getProperty("line.separator");

	/**tab*/
	public final static String TAB = "\t";

	/**space*/
	public static final String SPACE = " ";
	
	/**
	 * Combine the outputs without performing any calculations
	 * 
	 * @param output
	 * @return s - output as String
	 */
	public Serializable combine(Collection<C> output);
	
	/**
	 * Average over outputs of repeats
	 * 
	 * @param output
	 * @return c - output in format of the parameter
	 */
	public Object average(Collection<C> output);
	
	/**
	 * Average over outputs of repeats put in a String
	 * 
	 * @param output
	 * @return s - String containing averages 
	 * @see simcolep.tools.io.DataFormat#average(Collection)
	 */
	public Serializable averageString(Collection<C> output);
	
	/**
	 * 
	 * @return an Object that can indicates that no data is found and can be handled by the formatting class
	 */
	public C nodatamessage();

	/**
	 * Sum output entries
	 * 
	 * @param output
	 * @return
	 */
	public Serializable sum(Collection<C> output);

	/**
	 * Return a standard that can always be used in this data format
	 * @return
	 */
	public Serializable standard();
}
