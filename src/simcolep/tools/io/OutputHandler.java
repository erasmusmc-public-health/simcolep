/*
 * This file is part of SIMCOLEP, a simulation model developed by the Department of Public Health, 
 * Erasmus MC, University Medical Center Rotterdam, Rotterdam, The Netherlands.
 *
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/ 
 * or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA
 *
 * You are free to:
 * Share � copy and redistribute the material in any medium or format
 * Under the following terms:
 * Attribution � You must give appropriate credit, provide a link to the license, and indicate if changes were 
 * made. You may do so in any reasonable manner, but not in any way that suggests the licensor endorses
 * you or your use.
 * NonCommercial � You may not use the material for commercial purposes.
 * NoDerivatives � If you remix, transform, or build upon the material, you may not distribute the modified material.
 */
package simcolep.tools.io;


import java.io.EOFException;
import java.io.File;
import java.io.Reader;
import java.io.FileReader;
import java.io.BufferedReader;
import java.io.FileWriter;
import java.io.Serializable;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.IOException;

import java.util.ArrayList;
import java.util.List;




/**
 * This OutputHandler gathers the information and at finalize writes them to file.
 * 
 * @author ir. E.A.J. Fischer
 * @version 1.0
 * 
 *
 */
public final class OutputHandler  {

	private static final String UNCHECKED = "unchecked";

	private static final String TEMP_EXTENSION = ".tmp";

	private static final String TEMP_ADDITION = "temp";

	/**string if the handler did not obtain any information before finish method was called*/
	public static final String NODATA_COLLECTED_MESSAGE =
		"###NO DATA COLLECTED### by OutputHandler"; 
	/**overall temporary folder*/
	private static final String TEMP ="SIMCOLEP_tmp";
	/**
	 * When a outputhandler is initialized with a file that already exists delete it
	 * 
	 */
	protected static boolean overwriteAll = true;
	
	/**
	 * Data format 
	 */
	@SuppressWarnings(UNCHECKED)
	protected DataFormat dataFormat = null;
	
	/**
	 * finalize by combining (0 = default), averaging (1), summing (2) data 
	 */
	protected int outputmode = 0;
	
	/**
	 * Finalize a collected file (true) by reading all lines
	 */
	private boolean fileCollected = false;
	/**
	 * Temporary directory in which information is stored until finalize
	 * 
	 */
	protected File temp_dir = null;
	
	/**
	 * Temporary files
	 */
	protected List<File> temp_files = null;
	
	/**
	 * The outputfile
	 */
	protected File outputFile = null;

	private int repeats = 0;

	private Serializable dataObject = null; 
	
	/**
	 * Set overwriting of existing files on or off (files are appended)
	 * @param b
	 */
	public void setOverwriteall(boolean b)
	{
		overwriteAll = b;
	}
	
	/**
	 * Set up handler 
	 * 
	 * @param file
	 * @param outputmode
	 * @param df
	 */
	@SuppressWarnings(UNCHECKED)
	public void setup(String path, String filename, 
			int outmode, 
			DataFormat df,
			int repeats)
	{
		//finalization 
		this.outputmode = outmode;
		
		//data format
		this.dataFormat = df;
		
		//create file
		File file = new File(path+filename);
		
		//overwriting of file
		if(overwriteAll)
			if(file.exists()) 
				file.delete();
		
		//the output file
		this.outputFile = file;
		
		//number of repeats
		this.repeats = repeats;
		
		if(repeats > 0)
		{
			//create a directory for temporary storage of output
			this.temp_dir = 
				new File(path+File.separator+TEMP+File.separator+filename+
						TEMP_ADDITION+File.separator);
			
			this.temp_dir.mkdirs();
			
			//set to delete on exit (they are temporary anyway)
			this.temp_dir.deleteOnExit();
			
			//delete overall directory on exit
			new File(path+File.separator+TEMP+File.separator).deleteOnExit();
					
		}
	}
	
	/**
	 * Collect output to a temporary file
	 * 
	 * @param output 
	 * @throws IOException 
	 */
	public void collect(Serializable output) throws IOException
	{
	//more than one repeat save data in temp-files	
	if(this.repeats > 0)
		{
		//collect data in temporary files
		if(temp_files == null) 
			this.temp_files = new ArrayList<File>();
		
		//temporary file
		File temp = new File(temp_dir+
				File.separator+
				TEMP_ADDITION+
				this.temp_files.size()+
				TEMP_EXTENSION);
		
		//delete file when exiting the program
		temp.deleteOnExit();
		
		//add to the list of temporary files
		this.temp_files.add(temp);
		
		//output-streams
		FileOutputStream f = 
			new FileOutputStream(temp);
		ObjectOutputStream oos = 
			new ObjectOutputStream(f);
		
		//write
		oos.writeObject(output);
		
		//flush and close
		oos.flush();
		oos.close();
		}else
		{
			//other wise save the data object until finish has been called
			this.dataObject = output;
		}
	
	}
	
	/**
	 * Write all gathered information to a file without adding any information 
	 * 
	 * @throws ClassNotFoundException 
	 * @throws IOException 
	 * @see OutputHandler#finish(String)
	 */
	public void finish() throws IOException, ClassNotFoundException
	{
		//it is a quick method
		this.finish(new String());
	}
	/**
	 * Write all gathered information to a file
	 * @throws IOException 
	 * @throws ClassNotFoundException 
	 *
	 */
	@SuppressWarnings({UNCHECKED,UNCHECKED})
	public void finish(String text) 
		throws IOException, ClassNotFoundException
	{
//		Collection of the output
		List<Object> list = 
			new ArrayList<Object>();
		//if the number of repeats was 1 a data object was stored
		if(this.repeats == 1)
			list.add(dataObject);
		else{
		//if there is nothing to finish
		if(this.temp_files==null) 
			return;
						
		//iterate the files
		for(File f : this.temp_files)
			{
			if(f.exists()&& f.canRead())
				{
					//input stream
					FileInputStream fis = 
						new FileInputStream(f);
					ObjectInputStream ois = 
						new ObjectInputStream(fis);
					
					//read Object
					Object obj = 
						ois.readObject();
					list.add(obj);
					
					//close streams
					fis.close();
					ois.close();
				}
			}
		}
		String output = new String();
		
	
		//either outputmode of combining data
		if(this.outputmode==DataFormat.COMBINE)
			output = this.dataFormat.combine(list).toString();
		else if(this.outputmode==DataFormat.AVERAGE)
			output = this.dataFormat.averageString(list).toString();
		else if(this.outputmode == DataFormat.SUM)
			output = this.dataFormat.sum(list).toString();
					
		
		//write to file
		FileWriter writer = 
			new FileWriter(this.outputFile);
		writer.write(text+output);
		writer.flush();
		writer.close();
		
		//delete temporary files
		this.delete_temp_files();
	}
	
	/**
	 * Finishes the handler but instead of writing the content to file it will return a serializable object (String in current implementation) that represents the value normally written to file.
	 * 
	 * @return 
	 * @throws IOException 
	 * @throws ClassNotFoundException 
	 */
	@SuppressWarnings(UNCHECKED)
	public void finishToFile(FileWriter fw) 
		throws IOException, ClassNotFoundException
	{
		//either outputmode of combining data
		if(this.fileCollected)
			this.readFiles(fw);
		else
		{	//either outputmode of combining data
			if(this.outputmode==DataFormat.COMBINE)
				fw.append(this.string());
			else if(this.outputmode==DataFormat.AVERAGE)
				fw.append(this.average().toString());
			else if(this.outputmode == DataFormat.SUM)
				fw.append(this.sum().toString());
			
		}
		
		//delete temporary files
		this.delete_temp_files();
		
		fw.flush();
	}
	
	/**
	 * Does the same as finish 
	 * but returns an Object as given by the outputmode method in the DataFormat
	 * 
	 * @throws ClassNotFoundException 
	 * @throws IOException 
	 *
	 */
	@SuppressWarnings(UNCHECKED)
	public Serializable sum() throws IOException, ClassNotFoundException
	{
		
		//Collection of the output
		List<Object> list = 
			new ArrayList<Object>();
		
		//no data collected
		if(this.temp_files == null) {
			return (Serializable) dataFormat.nodatamessage();
		}
		
		//iterate the files
		for(File f : this.temp_files)
			{

			if(f.exists()&& f.canRead())
			{
				//input stream
				FileInputStream fis = 
					new FileInputStream(f);
				ObjectInputStream ois = 
					new ObjectInputStream(fis);
				
				//read Object
				Object obj =null;
				try{
				obj = ois.readObject();
				}catch (EOFException e)
				{
					obj = this.dataFormat.standard();
				}
				list.add(obj);
				
				//close streams
				fis.close();
				ois.close();
			}
		}
		
		
		return (Serializable) this.dataFormat.sum(list);
			
		
	}

	/**
	 * Does the same as finish 
	 * but returns an Object as given by the outputmode method in the DataFormat
	 * 
	 * @throws ClassNotFoundException 
	 * @throws IOException 
	 *
	 */
	@SuppressWarnings(UNCHECKED)
	public Serializable average() throws IOException, ClassNotFoundException
	{
		//Collection of the output
		List<Object> list = 
			new ArrayList<Object>();
		
		//no data collected
		if(this.temp_files == null)
			return (Serializable) dataFormat.nodatamessage();
		
		//iterate the files
		for(File f : this.temp_files)
			{
			if(f.exists()&& f.canRead())
			{
			//input stream
				FileInputStream fis = 
					new FileInputStream(f);
				ObjectInputStream ois = 
					new ObjectInputStream(fis);
				
				//read Object
				Object obj =null;
				try{
				obj = ois.readObject();
				}catch (EOFException e)
				{
					obj = this.dataFormat.standard();
				}
				list.add(obj);
				
				//close streams
				fis.close();
				ois.close();
			}
		}
		
		
		return (Serializable) this.dataFormat.average(list);
			
		
	}

	/**
	 * 
	 * @return string containing output handled by this OutputHandler
	 * @throws IOException 
	 * @throws ClassNotFoundException 
	 */
	@SuppressWarnings(UNCHECKED)
	public String string() throws IOException, ClassNotFoundException
	{
		//Collection of the output
		List<Object> list = 
			new ArrayList<Object>();
			
		//when no data was collected for this specific output
		if(this.temp_files==null)
			if(this.dataObject == null)
				return OutputHandler.NODATA_COLLECTED_MESSAGE;
			else 
			{
				list.add(this.dataObject);
				return this.dataFormat.combine(list).toString();
			}
		
		//iterate the files
		for(File f : this.temp_files)
			{
			if(f.exists()&& f.canRead())
			{
				//input stream
				FileInputStream fis = 
					new FileInputStream(f);
				ObjectInputStream ois = 
					new ObjectInputStream(fis);
				
				//read Object
				Object obj =null;
				try{
				obj = ois.readObject();
				}catch (EOFException e)
				{
					obj = this.dataFormat.standard();
				}
				
				list.add(obj);
				
				//close streams
				fis.close();
				ois.close();
			}
		}
		
		return this.dataFormat.combine(list).toString();
	}
	
	/**
	 * @param fw 
	 * 
	 * @return string containing output handled by this OutputHandler
	 * @throws IOException 
	 * 
	 */
	@SuppressWarnings(UNCHECKED)
	public String readFiles(FileWriter fw) 
				throws IOException 
	{			
		//when no data was collected for this specific output
		if(this.temp_files==null)
			return OutputHandler.NODATA_COLLECTED_MESSAGE;
		
		String output = new String();
		
		//iterate the files
		for(File f : this.temp_files)
			{
				//input stream
				Reader fis = 
					new FileReader(f);
				BufferedReader ois = 
					new BufferedReader(fis);
				
				//read Objects
				Object obj = ois.readLine();
				if(obj!=null)
					{
					if(fw== null)
						output+= obj.toString()+DataFormat.LINESEPARATOR;
					else
						fw.append(obj.toString()+DataFormat.LINESEPARATOR);
					}
				while(obj!= null)
				{
					obj = ois.readLine();
					if(obj!=null)
						output+= obj.toString()+DataFormat.LINESEPARATOR;
				}
				
				//close streams
				fis.close();
				ois.close();
				
				//add to list
				output+=DataFormat.LINESEPARATOR;
			}
		
		
		return output;
	}
	/**
	 * Deletes all the temporary files made for this handler. 
	 * The temporary files are already deleted on exit. This method can be used to delete during a program run.
	 * 
	 */
	public void delete_temp_files()
	{
		//if not temporary files present
		if(this.temp_files== null)
						return;
		
		
		//delete the files
		for(File f : this.temp_files)
		{
			f.delete();
		}
		
		//files = null 
			this.temp_files = null;
	
	}
	
	/**
	 * Deleta the temporary directory used by this output handler
	 *
	 */
	public void delete_temp_directory()
	{
		//if already is null return
		if(this.temp_dir == null)
			return;
		
		//delete from harddisk
		this.temp_dir.delete();
		
		//delete as field
		this.temp_dir = null;
	}

	/**
	 * 
	 * @return temp_dir
	 */
	public File getTempDir() {
				return this.temp_dir;
	}
}
