/*
 * This file is part of SIMCOLEP, a simulation model developed by the Department of Public Health, 
 * Erasmus MC, University Medical Center Rotterdam, Rotterdam, The Netherlands.
 *
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/ 
 * or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA
 *
 * You are free to:
 * Share � copy and redistribute the material in any medium or format
 * Under the following terms:
 * Attribution � You must give appropriate credit, provide a link to the license, and indicate if changes were 
 * made. You may do so in any reasonable manner, but not in any way that suggests the licensor endorses
 * you or your use.
 * NonCommercial � You may not use the material for commercial purposes.
 * NoDerivatives � If you remix, transform, or build upon the material, you may not distribute the modified material.
 */
/**
 * created: 04-Apr-2006
 */
package simcolep.tools.io;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Serializable;
import java.io.Writer;

import java.util.Iterator;
import java.util.List;

/**
 * 
 * A Table of size R times C. The data is in doubles
 * The table is not resizeable.
 * 
 * @author ir. E.A.J. Fischer
 * @version 1.0
 */
@SuppressWarnings("unchecked")
public class Table 
	implements Iterable,
				Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -4380440059415382222L;

	/**names of the rows*/
	protected String[] rowNames = null;
	
	/**names of the columns*/ 
	protected String[] columnNames = null;
	
	/**the actual table*/
	protected double[][] table = null;
	
	/**number of columns*/
	private int columns = 0;
	
	/**number of rows*/
	private int rows = 0;
	
	/**
	 * Construct a table of rows by columns
	 * 
	 * @param r rows
	 * @param c columns
	 */
	public Table(int r, int c) {
		super();
		this.rowNames = new String[r];
		//default names
		for(int i = 0; i <r;i++)
			{
				this.rowNames[i]=Integer.toString(i);
			}
		this.columnNames = new String[c];
		//default names
		for(int i = 0; i <c;i++)
			{
				this.columnNames[i]=Integer.toString(i);
			}
		
		
		table = new double[r][c];
		this.rows = r;
		this.columns = c;
	}
	
	/**
	 * 
	 * @param size
	 */
	public Table(int[] size) {
		super();
		
		int r = size[0];
		int c = size[1];
		
		this.rowNames = new String[r];
		//default names
		for(int i = 0; i <r;i++)
			{
				this.rowNames[i]=Integer.toString(i);
			}
		this.columnNames = new String[c];
		//default names
		for(int i = 0; i <c;i++)
			{
				this.columnNames[i]=Integer.toString(i);
			}
		
		table = new double[r][c];
		this.rows = r;
		this.columns = c;
		
	}
	
	/**
	 * 
	 * @param size
	 */
	public Table(double[][] table_array) {
		super();
		
		int r = table_array.length;
		int c = table_array[0].length;
		
		this.rowNames = new String[r];
		//default names
		for(int i = 0; i <r;i++)
			{
				this.rowNames[i]=Integer.toString(i);
			}
		this.columnNames = new String[c];
		//default names
		for(int i = 0; i <c;i++)
			{
				this.columnNames[i]=Integer.toString(i);
			}
		
		table = table_array;
		this.rows = r;
		this.columns = c;
		
	}
	
	/**
	 * Set the row name
	 * 
	 * @param row
	 * @param name
	 */
	public void setRowName(int row, String name)
	{
		this.rowNames[row] = name;
	}
	/**
	 * Sets this array as rownames if it is too long or too short it will cut or not fill all names
	 * @param names
	 */
	public void setRowNames(String[] names)
	{
		this.rowNames = this.setNames(this.rowNames,names);			
	}
	
	/**
	 * 
	 * @param our_names
	 * @param names
	 */
	private String[] setNames(String[] our_names, String[] names)
	{
		if(names.length==our_names.length)
			our_names = names;
		
		else
		{
			int max = Math.min(names.length,our_names.length);
			
			for(int index = 0;index < max; index++)
			{
				our_names[index] = names[index];
				
				if(names[index]==null)
					our_names[index] = Integer.toString(index);
			}
		}
		
		return our_names;
	}
	/**
	 * 
	 * @param row
	 * @return row if no name set the name oterwise
	 */
	public String getRowName(int row)
	{
		if(this.rowNames[row]==null)
			return Integer.toString(row);
		else 
			return this.rowNames[row];
	}
	
	/**
	 * 
	 * @return rowNames
	 */
	public String[] getRowNames()
	{
		return this.rowNames;
	}
	
	/**
	 * The size of this table {rows, columns}
	 * 
	 * @return res
	 */
	public int[] getSize()
	{
		int[] res = {this.rows,this.columns};
		return res;
	}
	
	/**
	 * The number columns of this table
	 * @return columns
	 */
	public int c()
	{
		return this.columns;
	}
	/**
	 * The number rows of this table
	 * @return rows
	 */
	public int r()
	{
		return this.rows;
	}
	/**
	 * 
	 * @param column
	 * @param name
	 */
	public void setColumnName(int column, String name)
	{
		this.columnNames[column] = name;
		
		if(name == null)
			this.columnNames[column] = Integer.toString(column);
	}
	/**
	 * Sets this array as column names if it is too long or too short 
	 * it will cut or not fill in all names
	 * @param names
	 */
	public void setColumnNames(String[] names)
	{
		this.columnNames = this.setNames(this.columnNames, names);
	}

	/**
	 * 
	 * @param column
	 * @return column name
	 */
	public String getColumnName(int column)
	{
		if(this.columnNames[column]==null)
			return Integer.toString(column);
		else 
			return this.columnNames[column];
	}
	/**
	 * Get a array containing the column names
	 * @return columnNames
	 */
	public String[] getColumnNames()
	{
			return this.columnNames;
	}
	/**
	 * Set the value of the cell int he table
	 * 
	 * @param row
	 * @param column
	 * @param value
	 */
	public void setCell(int row, int column, double value)
	{
		this.table[row][column] = value;
	}
	
	/**
	 * Get the value of a cell in the table
	 * 
	 * @param row
	 * @param column
	 * @return the value of the cell
	 */
	public double getCell(int row,int column)
	{
		return this.table[row][column];
	}
	
	/**
	 * Set a complete column to this table
	 * @param column
	 * @param list
	 */
	public void setColumn(int column, List<Number> list)
	{
		//check the size of this column
		if(list.size()>columns)
			throw new IndexOutOfBoundsException("too large list for this column ");
		
		//put its values in the table
		for(int i =0; i < list.size(); i++)
		{
			this.table[i][column] = list.get(i).doubleValue();
		}
		
	}
	
	/**
	 * Set a complete column to this table
	 * @param column
	 * @param list
	 */
	public void setColumn(int column, OutputList<Double> list)
	{
		//check the size of this column
		if(list.size()>this.rows)
			throw new IndexOutOfBoundsException(
					"too large list for this column:\r\n " +
					"table size = " +this.columns+" x "+ this.rows);
		
		//put its values in the table
		for(int i =0; i < list.size(); i++)
		{
			this.table[i][column] = list.get(i).getValue().doubleValue();
		}
		
	}
	/**
	 * 
	 * @param column
	 * @return
	 */
	public double[] getColumn(int column)
	{
		double[] res = new double[rows];
		for(int y = 0; y<this.rows;y++)
		{
			res[y] = this.table[y][column];
		}
		return res;
	}
	
	/**
	 * 
	 * @param row
	 * @return
	 */
	public double[] getRow(int row)
	{
		return this.table[row];
	}
	
	/**
	 * The sum of the values in this row
	 * @param row
	 * @return res
	 */
	public double rowTotal(int row)
	{
		double res = 0;
		//get the row
		double[] rowValues = this.table[row];
		
		//iterate and sum
		for(double value : rowValues)
		{
			res+= value;
		}
		
		//return
		return res;
				
	}
	
	/**
	 * The sum of the values in this column
	 * @param column
	 * @return res
	 */
	public double columnTotal(int column)
	{
		double res = 0;
				
		//iterate and sum
		for(int x = 0; x < this.rows;x++)
		{
			res+= this.table[x][column];
		}
		
		//return
		return res;
	}
	
	/**
	 * Set this array in the table starting with the first value of the array (0,0) at row and column
	 * 
	 * @param array
	 * @param row
	 * @param column
	 * 
	 *  @throws IndexOutOfBoundException - when the array, row, column are extend the size of the table 
	 */
	public void setSubtable(double[][] array, int row, int column)
	{
		if(row+array.length>this.rows) 
			throw new IndexOutOfBoundsException(row+"+"+array.length+">"+this.rows);
		if(row+array[0].length>this.columns) 
			throw new IndexOutOfBoundsException(row+"+"+array[0].length+">"+this.columns);
		
		for(double[] rowArray : array)
		{
			for(double cell : rowArray)
			{
				this.table[row][column]= cell;
				column++;
			}
			row++;
		}
	}
	/**
	 * 
	 * @param rowStart
	 * @param rowEnd
	 * @param columnStart
	 * @param columnEnd
	 * @return res
	 */
	public double[][] getSubtable(int rowStart, int rowEnd, int columnStart,int columnEnd)
	{
		//result 
		double[][] res = new double[rowEnd-rowStart][columnEnd-columnStart];
		
		//iterate the table 
		for(int x = rowStart; x < rowEnd; x++)
		{
			for(int y = columnStart;y <columnEnd; y++)
			{
				res[x-rowStart][y-columnStart] = this.table[x][y];
			}
		}
		
		return res;
	}
	
	/**
	 * Determine if the dimensions (rows and columns) equal that of this table
	 * @param table
	 * @return true if the tables have the same size
	 */
	public boolean equalDimensions(Table table)
	{
		//same number of rows
		if(table.getSize()[0]-this.rows!=0) 
			return false;
		
		//same number of columns
		if(table.getSize()[1]-this.columns!=0)
			return false;
		
		//else return true
		return true;
	}
	
	/**
	 * 
	 * @see java.lang.Object#toString()
	 */
	public String toString()
	{
		//estimated string size
		//tabs and carriage returns
		int StrSize = this.rows*4 + this.columns * 12 +this.rows*this.columns*5;
		
		//tab
		StringBuffer s = new StringBuffer(StrSize);
		
		s.append(DataFormat.SPACE+DataFormat.TAB);
		
		//set the column names
		for(int y = 0; y<this.columns;y++)
		{
			s.append(getColumnName(y)+DataFormat.TAB);
		}
		//carriage return
		s.append(DataFormat.LINESEPARATOR);
		//set the values
		for(int x = 0; x<this.rows; x++)
		{
			s.append(this.getRowName(x)+DataFormat.TAB);
			for(int y =0; y<this.columns;y++)
			{
				s.append(this.table[x][y]+DataFormat.TAB);
			}
			s.append(DataFormat.LINESEPARATOR);
		}
		return s.toString();
		
	}

	/**
	 * Add the value d to the current value of the table cell
	 * 
	 * @param r
	 * @param c
	 * @param d
	 */
	public void addtoCell(int r, int c, double d) {
		//check the dimensions of the table
		if(r>this.rows)
			throw new IndexOutOfBoundsException(
					"rows: " + r +  " > " +this.rows);
		 if(c >this.columns)
			 throw new IndexOutOfBoundsException(
					"columns: " + c + " > " + this.columns);
		 
		this.table[r][c]+=d;
		
	}
	
	/**
	 * Divide cell by d
	 * 
	 * @param r
	 * @param c
	 * @param d
	 */
	public void divideCell(int r, int c, double d)
	{
		this.table[r][c]/=d;
	}

	/**
	 * 
	 * @see java.lang.Iterable#iterator()
	 */
	public Iterator iterator() {
		return new TableIterator(this);
	}

	/**
	 * 
	 * @param table2
	 * @param filename_population
	 * @param path
	 * @throws IOException 
	 */
	public static void toFile(Table table, String filename, String pathname) throws IOException {
		
    	if(pathname == null)
    		pathname = OutputList.path;
    	
        if(filename.contains(File.separator))
        {   
        	//last occurence of the seperator sign
        	int lastSeparator = filename.lastIndexOf(File.separator)+1;
        	//get the piece of the path
        	String extendPathwith = 
        		filename.substring(0,lastSeparator);
        	//cut the first part of the filename
        	filename = filename.substring(lastSeparator);
        	
        	//extend pathname
        	pathname += File.separator+extendPathwith;
        	new File(pathname).mkdirs();
        }else  
        	new File(pathname).mkdir();
        
        File output_file = new File(pathname,
        		       		filename+OutputList.EXTENSION);
        
        FileWriter f = new FileWriter(output_file);
        //write
        toFile(table,f);
		}
	
	/**
     * 
     * @param data
     * @param f
     * @throws IOException
     */
    @SuppressWarnings("unchecked")
    public static void toFile(Table table,Writer f) throws IOException
    {
    	
        f.write("time"+"\t");
        //write labels
        
            f.write(table.toString());
       
        
        f.flush();
        f.close();
    }
}
