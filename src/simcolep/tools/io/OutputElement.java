/*
 * This file is part of SIMCOLEP, a simulation model developed by the Department of Public Health, 
 * Erasmus MC, University Medical Center Rotterdam, Rotterdam, The Netherlands.
 *
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/ 
 * or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA
 *
 * You are free to:
 * Share � copy and redistribute the material in any medium or format
 * Under the following terms:
 * Attribution � You must give appropriate credit, provide a link to the license, and indicate if changes were 
 * made. You may do so in any reasonable manner, but not in any way that suggests the licensor endorses
 * you or your use.
 * NonCommercial � You may not use the material for commercial purposes.
 * NoDerivatives � If you remix, transform, or build upon the material, you may not distribute the modified material.
 */
/*
 * Created on 23-Dec-2004
 *
 * 
 */
package simcolep.tools.io;

import java.io.Serializable;


/**
 * Contains output at a certain time 
 * 
 * @author ir E.A.J. Fischer
 * @version 1.0 -- Last modified 31-3-2006
 */
public final class OutputElement<O> implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 4873525076021327277L;
	
	/**time of output*/
	private double time = -1.0;
	
	
	/**output value*/
	private O value = null;//value of the object
	
	
    /**
	 * 
	 *
	 */
    public OutputElement()
    {
    	super();
    }
 
	
    /**
     * 
     * @param time
     * @param value
     */
    public OutputElement(double time, O value)
    {
        this.time = time;
        this.value = value;
    }
	
	
	/**
	 * 
	 * @return
	 */
	public String getType()
	{
	    String type = 
	    	this.value.getClass().getName();
		return type.substring(type.lastIndexOf('.')+1);
		
	}
	/**
	 * 
	 * @return
	 */
	public double getTime()
	{
		return this.time;
	}
	/**
	 * 
	 * @return this.value
	 */
	public O getValue()
	{
		return this.value;
	}
	
	
	/**
	 * 
	 * @see java.lang.Object#toString()
	 */
	public String toString()
	{
	   return this.time +DataFormat.TAB
	   				+this.value;
	}
}
