/*
 * This file is part of SIMCOLEP, a simulation model developed by the Department of Public Health, 
 * Erasmus MC, University Medical Center Rotterdam, Rotterdam, The Netherlands.
 *
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/ 
 * or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA
 *
 * You are free to:
 * Share � copy and redistribute the material in any medium or format
 * Under the following terms:
 * Attribution � You must give appropriate credit, provide a link to the license, and indicate if changes were 
 * made. You may do so in any reasonable manner, but not in any way that suggests the licensor endorses
 * you or your use.
 * NonCommercial � You may not use the material for commercial purposes.
 * NoDerivatives � If you remix, transform, or build upon the material, you may not distribute the modified material.
 */
package simcolep.tools.io;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Serializable;
import java.io.Writer;

import java.util.Iterator;
import java.util.List;
import java.util.LinkedList;

import simcolep.exceptions.SimulationException;


/**
 * 
 * Outputlist extends the use of linkedlist for the output of the simulation
 * 
 * @author ir E.A.J. Fischer
 *
 * @version 0.1.1 Last Modified 23 dec 2004
 * @param <O>
 */
public class OutputList<O> 
			extends LinkedList<OutputElement<O>> 
				implements Serializable{

	
	private static final String TIME_STRING = "time"; //$NON-NLS-1$

	/**	 serial version UID	 */
	private static final long serialVersionUID = 8332891262669061672L;

	/**
	 * 
	 */
	static final String EXTENSION = ".outputlist.txt";
	
	/**path where the output is placed<br><br> <b> default:</b> <code>System.getProperty("user.dir") + File.separator + "output"</code>*/ 
	static String path = System.getProperty("user.dir") +
									File.separator +
									"output";
	
	/**generic file writer (writes outputlists to this file)*/
	private static FileWriter generic = null;
	
	/**the name of the generic output file*/
	private static final String genericoutputfilename = "genericoutputfile.scl";
	
	/**blocks writing any of the lists to file*/
	private static boolean lockToFile = false;
	
	/**
	 * Set the path where output for this list is set
	 * @param repath
	 */
	public static void setPath(String repath)
	{
		path = repath;
	}

	/**Name of the container*/
    private String name;
    
    /**Type of values in the elements that can be put in this container*/
	private String type;
	
	/**
	 * 
	 */
	public OutputList() {
		super();
		
	}
	/**
	 * 
	 * @param name
	 * @param type
	 */
	public OutputList(String name)
	{
		super();
		this.name = name;
		
	}
	
	/**
	 * 
	 * @param name
	 * @param array
	 */
	@SuppressWarnings("unchecked")
	public OutputList(String name, double[][]array)
	{
		super();
		if(array[0].length>2)
			if(array.length>2)
				throw new IllegalArgumentException("can only handle 2 x n arrays");
		
		this.name = name;
		if(array.length != 2)
		{
		for(double[] d : array)
		{
			OutputElement el = new OutputElement(d[0],new Double(d[1]));
			this.addLast(el);
		}
		}else
		{
			for(int x =0; x<array[0].length;x++)
			{
				OutputElement<Double> el = new OutputElement<Double>(array[0][x],new Double(array[1][x]));
				this.addLast((OutputElement<O>) el);
			}
		}
	}
	
	/**
	 * 
	 * @param name
	 * @param array
	 */
	@SuppressWarnings("unchecked")
	public OutputList(String name, double[] time, int[]array)
	{
		super();
	
		this.name = name;
		if(time.length!= array.length)
			throw new IllegalArgumentException(time.length +" != " +array.length);
		{
			for(int x =0; x<array.length;x++)
			{
				OutputElement<Integer> el = 
					new OutputElement<Integer>(time[x],new Integer(array[x]));
				this.addLast((OutputElement<O>) el);
			}
		}
	}
	/**
	 * 
	 * @param name
	 * @param time
	 * @param array
	 */
	@SuppressWarnings("unchecked")
	public OutputList(String name, Double[] time, int[] array) {
		super();
		
		this.name = name;
		if(time.length!= array.length)
			throw new IllegalArgumentException(time.length +" != " +array.length);
		{
			for(int x =0; x<array.length;x++)
			{
				OutputElement<Integer> el = 
					new OutputElement<Integer>(time[x],new Integer(array[x]));
				this.addLast((OutputElement<O>) el);
			}
		}
	}
	/**
	 * 
	 * @param index
	 * @param element
	 */
	public void add(int index, OutputElement<O> element)
	{
		if(this.type==null)this.type = element.getType();
		
	    if (!element.getType().equals(this.type))throw new SimulationException(SimulationException.TYPESDONOTMATCH);
			
		super.add(index, element);
	}
	/**
	 * 
	 * @param element
	 */
	public void addFirst(OutputElement<O> element)
	{
	    if(this.type==null)this.type = element.getType();
		if (!element.getType().equals(this.type))throw new IllegalArgumentException("types do not match");
		
		super.addFirst(element);
	}
	/**
	 * 
	 * @param element
	 */
	public void addLast(OutputElement<O> element)
	{
	    if(this.type==null)this.type = element.getType();
		if (!element.getType().equals(this.type))throw new SimulationException(SimulationException.TYPESDONOTMATCH+this.type +" != " + element.getType());
		super.addLast(element);
	}
    
    @Override
    public OutputElement<O> get(int x)
    {
        return super.get(x);
    }
	
    /**
     * Returns the iterator of LinkedList but as this list contains OutputElements it will return an iterator that gives OutputElements
     * @see java.lang.Iterable#iterator()
     */
    public Iterator<OutputElement<O>> iterator()
    {
    	return super.iterator();
    }
    /**
     * 
     * @param name
     */
    public void setName(String name)
    {
    	this.name = name;
    }
    
	/**
	 * 
	 * @return name of the list
	 */
	public String getName()
	{
		return this.name;
	}
	
	/**
	 * @see java.util.AbstractCollection#toString()
	 */
	public String toString()
	{
		StringBuffer string = new StringBuffer(name + DataFormat.LINESEPARATOR);
		for(int x = 0; x< this.size();x++)
			{
				string.append(this.get(x)+DataFormat.LINESEPARATOR);
			}
		return string.toString();
	}
	/**
     * writes to file this outputlist in file with the name of the list
     * @throws IOException 
     */
    public void toFile() throws IOException
    {
    	if(lockToFile) 
    		return;
    	FileWriter f = new FileWriter(new File(System.getProperty("user.dir")+File.separator+"output"+File.separator+name+OutputList.EXTENSION));
        this.toFile(f);
        f.flush();
        f.close();
    }
    
    /**
     * writes to file this outputlist in file with the name of the list
     * @param dir
     * @throws IOException 
     */
    public void toFile(String dir) throws IOException
    {
    	if(lockToFile) return;
        
        if(dir.contains(File.separator))
        {   new File(path+dir).mkdirs();
        }else  new File(path+dir).mkdir();
        
        FileWriter f = new FileWriter(new File(path+dir+File.separator+name+OutputList.EXTENSION));
        this.toFile(f);
        
    }
    
    /**
     * 
     * @param f
     * @throws IOException
     */
    public void toFile(Writer f)throws IOException
    {
    	if(lockToFile) return;
    	
        for(OutputElement<O> e : this)
        {
            f.write(e.getTime()+DataFormat.TAB+e.getValue()+DataFormat.LINESEPARATOR);
        }
        f.flush();
        f.close();
    }
    
    /**
     * 
     * @param data
     * @param filename
     * @throws IOException
     */
    @SuppressWarnings("unchecked")
	public static void toFile(List<OutputList> data, String filename, String pathname) throws IOException
    {
    	if(lockToFile) 
    		return;
    	
    	if(pathname == null)
    		pathname = path;
    	
        if(filename.contains(File.separator))
        {   
        	//last occurence of the seperator sign
        	int lastSeparator = filename.lastIndexOf(File.separator)+1;
        	//get the piece of the path
        	pathname = File.separator+ 
        		filename.substring(0,lastSeparator);
        	//cut the first part of the filename
        	filename = filename.substring(lastSeparator);
        	
        	//extend pathname
        	new File(pathname).mkdirs();
        }else  
        	new File(pathname).mkdir();
        
        File output_file = new File(pathname,
        		       		filename+EXTENSION);
        
        FileWriter f = new FileWriter(output_file);
        //write
        toFile(data,f);
    }
    /**
     * 
     * @param data
     * @param s
     * @throws IOException
     */
    @SuppressWarnings("unchecked")
	public static void toFile(List<OutputList> data, String s) throws IOException
    {
    	toFile(data, s, null);
    }
    
 
    
    /**
     * 
     * @param data
     * @param f
     * @throws IOException
     */
    @SuppressWarnings("unchecked")
    public static void toFile(List<OutputList> data,Writer f) throws IOException
    {
    	if(lockToFile) 
    		return;
    	
        f.write(TIME_STRING+DataFormat.TAB);
        //write labels
        for(OutputList o : data)
        {
            f.write(o.getName()+DataFormat.TAB);
        }
        f.write(DataFormat.LINESEPARATOR);
        
        //write data
        for(int x =0; x<data.get(0).size();x++)
        {
            for(int y = 0; y<data.size();y++)
            {
                //from first write the time (notice that the data lists should have the same length and same time points)
                if(y==0)
                	f.write(data.get(y).get(x).getTime()+DataFormat.TAB);
                
                f.write(data.get(y).get(x).getValue()+DataFormat.TAB);
            }
            f.write(DataFormat.LINESEPARATOR);
        }
        
        f.flush();
        f.close();
    }
    
    /**
     * Write this list to the generic file
     * @param list
     * @throws IOException 
     */
    @SuppressWarnings("unchecked")
	public static void toFile(List<OutputList> list) throws IOException
    {
    	if(lockToFile) 
    		return;
    	
    	//if the generic file is null create a file writer 
    	if(generic== null)
    	{
    		//create the folder
    		File dirs = new File(path);
    		dirs.mkdirs();
    		//create the file
    		File f = new File(path+File.separator+genericoutputfilename+OutputList.EXTENSION);
    		//if the file already exist delete
    		if(f.exists())f.delete();
    		//create the fileWriter
    		generic = new FileWriter(f);
    	}
    	
    	//write the list to the generic file
    	toFile(list,generic);
    }
    
    /**
     * flushes and closes the outputstream that was the generic file to write business to.
     * When closed the filewriter will be removed.
     * @throws IOException 
     *
     */
    public static void flushAndcloseGenericFile() throws IOException
    {
    	if(lockToFile) return;
    	
    	//flush
    	generic.flush();
    	
    	//close
    	generic.close();
    	
    	//delete
    	generic = null;
    
    }
    
    /**
     * Create a list containing array of x and y values
     * 
     * @return data
     * @throws ClassCastException
     */
 
    public List<double[]> toList() throws ClassCastException
    {
    	//create list
    	List<double[]> data = new LinkedList<double[]>();
    	//enter all elements in one dimensionale arrays
        for(OutputElement<?> e : this)
        {
            double[]datapoint = {e.getTime(),(Double)e.getValue()};
            data.add(datapoint);
        }
        
        //return list
        return data;
    }
    
    /**
     * Create and returns an OutputList with entries 0,0 so that the size of the list equals i
     * @param i
     * @return list
     */
    public static OutputList<Double> newOutputList(int i,String s) {
        OutputList<Double> list = new OutputList<Double>(s);
        for(int x =0; x<i;x++)
        {
            list.add(new OutputElement<Double>(0.0,0.0));
        }
        return list;
    }
	
}
