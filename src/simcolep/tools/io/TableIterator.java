/*
 * This file is part of SIMCOLEP, a simulation model developed by the Department of Public Health, 
 * Erasmus MC, University Medical Center Rotterdam, Rotterdam, The Netherlands.
 *
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/ 
 * or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA
 *
 * You are free to:
 * Share � copy and redistribute the material in any medium or format
 * Under the following terms:
 * Attribution � You must give appropriate credit, provide a link to the license, and indicate if changes were 
 * made. You may do so in any reasonable manner, but not in any way that suggests the licensor endorses
 * you or your use.
 * NonCommercial � You may not use the material for commercial purposes.
 * NoDerivatives � If you remix, transform, or build upon the material, you may not distribute the modified material.
 */
/**
 * created: 04-Apr-2006
 */
package simcolep.tools.io;

import java.util.Iterator;

/**
 * Iterates a table 
 * 
 * 
 * @author ir. E.A.J. Fischer
 * @version 1.0
 */
public class TableIterator implements Iterator<Object> {

	/**
	 * Order in which this iterator will produce the next cell
	 * 
	 * 
	 * 
	 */
	public enum Order
	{
		ROW,/**Iterate all colums and than the next row*/
		COLUMN;/**Iterate all rows  and than the next column*/
	}
	/**
	 * 
	 */
	public Order order = Order.ROW;
	
	/**
	 * 
	 */
	private Table table = null;
	
	/**
	 * 
	 */
	private int[]position = {0,0};
	
	/**
	 * 
	 */
	public TableIterator(Table table) {
		super();
		this.table = table;
	}

	/**
	 * @see java.util.Iterator#hasNext()
	 */
	public boolean hasNext() {
		if(this.table.getSize()[0]>position[0])
			return true;
		if(this.table.getSize()[1]>position[1])
			return true;
		
		return false;
	}

	/**
	 * @see java.util.Iterator#next()
	 */
	public Object next() {
		int x = 0;
		int y = 1;
		if(order.equals(Order.COLUMN))
		{
		  x = 1;
		  y =0;
		}
		
		//next position
			if(this.position[x]<table.getSize()[x])
				{
					this.position[x]++;
				}
			else{
				this.position[x]=0;
				this.position[y]++;
			}
		
			//return the value of the cell
		return new Double(table.getCell(position[0],position[1]));
	}

	/**
	 * @see java.util.Iterator#remove()
	 */
	public void remove() {
		return;

	}

}
