/*
 * This file is part of SIMCOLEP, a simulation model developed by the Department of Public Health, 
 * Erasmus MC, University Medical Center Rotterdam, Rotterdam, The Netherlands.
 *
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/ 
 * or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA
 *
 * You are free to:
 * Share � copy and redistribute the material in any medium or format
 * Under the following terms:
 * Attribution � You must give appropriate credit, provide a link to the license, and indicate if changes were 
 * made. You may do so in any reasonable manner, but not in any way that suggests the licensor endorses
 * you or your use.
 * NonCommercial � You may not use the material for commercial purposes.
 * NoDerivatives � If you remix, transform, or build upon the material, you may not distribute the modified material.
 */
/**
 * created: 23-Aug-2006
 */
package simcolep.tools.io;

import java.io.File;

import java.util.Map;
import java.util.LinkedHashMap;


/**
 * Registers files 
 * @author ir. E.A.J. Fischer
 * @version 1.0
 */
public final class FileRegister {
	private static final String WINDOWS_SEPARATOR = "/"; //$NON-NLS-1$

	/**user directory*/
	public static final String USERDIR = "user.dir";
	
	/**default path is current directory*/
	private final static String DEFAULT_PATH =
			System.getProperty(USERDIR)+File.separator;
	
	
	/**integer representation of the FILE map*/
	public static final int IN = 0;
	
	/**integer representation of the PATH map*/
	public static final int OUT = 1;
	
	/**integer representation of the FILE map*/
	public static final int FILE = 0;
	
	/**integer representation of the PATH map*/
	public static final int PATH = 1;
	
	/**main path*/
	public static String MAIN_PATH = DEFAULT_PATH;
	
	/**main in path*/
	public static String IN_PATH = DEFAULT_PATH;
	
	/**main out path*/
	public static String OUT_PATH = DEFAULT_PATH;
	
	
	
	
	/**register*/
	private static Map<String, String>[][] 
				Cabinet	= null;
	
	/**
	 * 
	 * @param path
	 */
	public static void setMainPath(String path)
	{
		if(path == null)
			path = DEFAULT_PATH;
		//make platform independent	
		if(path.contains(WINDOWS_SEPARATOR))
			path = path.replaceAll(WINDOWS_SEPARATOR,File.separator);
		
		if(path.endsWith(File.separator))
				MAIN_PATH = path;
		else 
			MAIN_PATH = path+File.separator;
		
		//create output directories if not present
		if(!new File(MAIN_PATH).exists())
			new File(MAIN_PATH).mkdirs();
	}
			
	
	/**
	 * 
	 * @param path
	 */
	public static void setMainPath(String path, int io)
	{
		if(path == null)
			path = FileRegister.DEFAULT_PATH;
			
		//make platform independent	
		if(path.contains(WINDOWS_SEPARATOR))
			path = path.replaceAll(WINDOWS_SEPARATOR,File.separator);
		if(!path.endsWith(File.separator))
			path += File.separator;
		
		if(io == IN)
			IN_PATH = path;
		else if(io == OUT)
			OUT_PATH = path;
		
		//create output directories if not present
		if(!new File(DEFAULT_PATH).exists())
			new File(DEFAULT_PATH).mkdirs();
		
		if(!new File(OUT_PATH).exists())
			new File(OUT_PATH).mkdirs();
		
	}
	/**
	 * 
	 * @param file
	 * @param filename
	 */
	public static void putFile(String file, String filename, int io)
	{
		FileRegister.put(file, filename, io, FILE);
	}
	
	/**
	 * put a path 
	 * @param path
	 * @param pathname
	 * @param io
	 */
	public static void putPath(String path, String pathname, int io)
	{
		FileRegister.put(path, pathname, io,PATH);
	}
	/**
	 * 
	 * @param key name of the object
	 * @param entry the object
	 * @param io input or output
	 * @param type file or path
	 */
	private static void put(String key, String entry, int io ,int type)
	{
		if(Cabinet == null)
			emptyCabinet(); 
		
		Cabinet[io][type].put(key,entry);
	}

	/**
	 * 
	 * @param file
	 * @return filename
	 */
	public static String getFile(String file, int io)
	{
		return FileRegister.get(file,io,FILE);
	}
	
	/**
	 * 
	 * @param path path-name
	 * @param io in or output
	 * @return get(path,io,PATH)
	 */
	public static String getPath(String path, int io)
	{
		if(Cabinet == null)
			return FileRegister.MAIN_PATH;
		
		return FileRegister.get(path,io,PATH);
			}
	/**
	 * 
	 * @param io
	 * @return main in or out path
	 */
	public static String getPath(int io)
	{
		if(io== OUT) 
			return OUT_PATH;
		else if (io == IN)
			return IN_PATH;
		
		return null;
	}
	/**
	 * Return a file that has a complete path to it. The path added infront is either:
	 * <li> a path with the same name
	 * <li> the default path 
	 * 
	 * @param file
	 * @param io
	 * @return path + filename
	 */
	public static String getFile_fullpath(String file, int io)
	{
		//get the path specified for this file
		String path = 
			FileRegister.getPath(file,io);
		if(path == null)
			{
		 //if it is for input
			if(io == IN)
				path+=IN_PATH;
			//or output
			else if (io == OUT)
				path= OUT_PATH;
			else
				//default path
				path = MAIN_PATH;
			}
		
		if(!path.endsWith(File.separator))
			path+=File.separator;
		
		return path + FileRegister.getFile(file,io);
		
	}
	/**
	 * Get the function from the map
	 * @param name
	 * @param io in or output
	 * @param type
	 * @return filename
	 */
	private static String get(String name , int io,int type)
	{
		if(Cabinet == null)
			return null;
		
		return Cabinet[io][type].get(name);
	}
	
	/**
	 * Create a new and empty File cabinet
	 *
	 */
	
	
	@SuppressWarnings("unchecked")
	public static void emptyCabinet()
	{
		//file cabinet
		Cabinet = 
			new Map[2][2];
		
		//input
		Cabinet[IN] = 
			new Map[2];
		
		//files
		Cabinet[IN][FILE] =
			new LinkedHashMap<String,String>();
		//paths
		Cabinet[IN][PATH] =
			new LinkedHashMap<String,String>();
		
		//output
		Cabinet[OUT] =
			new Map[2];
		     
//		files
		Cabinet[OUT][FILE] =
			new LinkedHashMap<String,String>();
		//paths
		Cabinet[OUT][PATH] =
			new LinkedHashMap<String,String>();
		
	}
	
	/**
	 * Get the files either the input or output  
	 * 
	 * @param io
	 * @return Cabinet[FILE][io]
	 */
	public static Map<String, String> getFiles(int io) {
		
		return Cabinet[FILE][io];
	}
	

}
