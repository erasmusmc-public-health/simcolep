/*
 * This file is part of SIMCOLEP, a simulation model developed by the Department of Public Health, 
 * Erasmus MC, University Medical Center Rotterdam, Rotterdam, The Netherlands.
 *
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/ 
 * or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA
 *
 * You are free to:
 * Share � copy and redistribute the material in any medium or format
 * Under the following terms:
 * Attribution � You must give appropriate credit, provide a link to the license, and indicate if changes were 
 * made. You may do so in any reasonable manner, but not in any way that suggests the licensor endorses
 * you or your use.
 * NonCommercial � You may not use the material for commercial purposes.
 * NoDerivatives � If you remix, transform, or build upon the material, you may not distribute the modified material.
 */
/*
 * Created on 08-Feb-2005
 *
 *
 */
package simcolep.tools.io;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Reader;
import java.io.Writer;

import java.util.Date;
import java.util.Iterator;
import java.util.LinkedHashMap;

import javax.swing.JDialog;

import simcolep.exceptions.SimulationException;
import simcolep.tools.PN;

/**
 * @author ir. E.A.J. Fischer
 * @version 0.1.1
 * 
 * Simulation log keeps a log of interesting things in the simulation
 *
 */
public final class SimulationLog {


	private static final String DEFAULTLOGNAME = "LOG.txt";

	private static final String TEXTFILE_EXTENSION = ".txt"; //$NON-NLS-1$

	/**writer for the generic log file*/
	private static FileWriter logWriter = null;
	
	/**writers for specific purposes*/
	private static LinkedHashMap<String, Writer> speciallogs 
		= new LinkedHashMap<String,Writer>();
	
	/**default path*/
	private static String path = null;
	
	/**generic log name*/
	private static String filename = "LOG";

	/**generic error log name*/
	public static final String ERRORLOG = "RUNERROR.LOG";

	/**Default extension*/
	private static final String DEFAULTEXTENSION = ".TXT";

	/**Message that a log-file was written**/
	private static final String LOGWRITTEN = PN.LINESEPARATOR+"A simulation-log was written for this run: ";
	
   

    
    /**text with which the log is started*/
    private static String headerText = null;
    
   
    
    /**create a log*/
    public static boolean log = true;
	/**
	 * 
	 * @param message
	 * @throws IOException 
	 */
	public static void log(String message)
	{
		//log is off
        if(!log)
        	return;
        //path is not set 
        if(path == null)
        	path =  FileRegister.OUT_PATH;
        /***/
	    if(logWriter==null)
	    	filename = filename+TEXTFILE_EXTENSION;
	    
	    JDialog.setDefaultLookAndFeelDecorated(true);
		try{
			logWriter = 
				new FileWriter(path+filename,true);
			try{
				
				if(headerText == null)
					logWriter.append(millisecondsToString(System.currentTimeMillis())+PN.LINESEPARATOR+message+PN.LINESEPARATOR);
				else
					logWriter.append(headerText+PN.LINESEPARATOR+millisecondsToString(System.currentTimeMillis())+PN.LINESEPARATOR+message+PN.LINESEPARATOR);
			
			}catch (NullPointerException e)
			{
			    logWriter.append(message+PN.LINESEPARATOR);
			}
			logWriter.flush();
		}catch(IOException ioe)
		{
			throw new SimulationException(ioe);
		}
			
		 
	}
	
	/**
	 * 
	 * @param nameOfFile
	 * @param message
	 * @param overwrite
	 * @param with_time
	 * @param with_header
	 * @throws IOException 
	 */
	public static void speciallog(String nameOfFile, 
				String message, 
				boolean overwrite,
				boolean with_time, 
				boolean with_header)
	{
		SimulationLog.speciallog(nameOfFile,
				SimulationLog.DEFAULTEXTENSION,
				message, 
				overwrite,
				with_time, 
				with_header);
	}

	
	/**
	 * Create or append a text file with a message. Overwrite existing files from previous runs
	 * 
	 * @param nameOfFile - name of log file
	 * @param extension - extension of log file
	 * @param message - current message
	 * @param overwrite  - overwrite existing files of this name
	 * @param with_time - write the time of logging
	 * @param with_header - add a header to the log
	 * @throws IOException 
	 * @throws IOException 
	 * 
	 */
	public static void speciallog(String nameOfFile, 
									String extension,
									String message, 
										boolean overwrite,
										boolean with_time, 
										boolean with_header)
	{
   
		//if path is not set
        if(path == null)
        	path =  FileRegister.OUT_PATH;
        
	    //to perserve all saved logs we add the time to the filename
	    String speciallogName = nameOfFile.toUpperCase();
        
        	
        	StringBuffer text = new StringBuffer();
        	if(headerText != null && with_header)
        		text.append(headerText+PN.LINESEPARATOR);
        	
        	if(with_time)
        		text.append(millisecondsToString(System.currentTimeMillis())+PN.LINESEPARATOR+ message);
        	else 
        		text.append(message);
        try{	
	     if(!speciallogs.containsKey(nameOfFile))
		    {
		     
		    	 //check if a file with this name exist if this should be deleted
		    	
		    	 if(overwrite)
		    	 {
		    		 File old_file = new File(path+speciallogName+extension);
		    		 if(old_file.exists())
		    		 	old_file.delete();
		    	 }
		    			 //create this log 
			       speciallogs.put(nameOfFile,
			    		   new FileWriter(path+speciallogName+extension,true).append(text));
			       //flush first appended message
			       speciallogs.get(nameOfFile).flush();
		    }else
            {
		            speciallogs.get(nameOfFile).append(PN.LINESEPARATOR+PN.LINESEPARATOR+text);
		            //flush
		            speciallogs.get(nameOfFile).flush();
		           
		     }
        }catch (IOException ioe)
        {
        	throw new SimulationException(ioe);
        }
		
	}
	/**
	 * @param string
	 * @param d
	 * @param with_time
	 * @throws IOException 
	 * @throws IOException 
	 */
	public static void speciallog(String nameOfFile, double d, boolean with_time)  {
	    
	    speciallog(nameOfFile,Double.toString(d), true, with_time, true);
	    
	}
	/**
	 * @param string
	 * @param d
	 * @throws IOException 
	 * @throws IOException 
	 */
	public static void speciallog(String nameOfFile, int d, boolean with_time)  {
	     
	    
	    speciallog(nameOfFile,Integer.toString(d), true, with_time, true);
	    
	}
	

	/**
	 * Create or append a text file with a message. Overwrite existing files from previous runs
	 * 
	 * @param nameOfFile - name of log file
	 * @param extension - extension of log file
	 * @param message - current message
	 * @param overwrite  - overwrite existing files of this name
	 * @param with_time - write the time of logging
	 * @param with_header - add a header to the log
	 * @throws IOException 
	 * @throws IOException 
	 * 
	 */
	public static void speciallog(String nameOfFile, 
									String extension,
									File file, 
										boolean overwrite,
										boolean with_time, 
										boolean with_header) 
	{
	
//		if path is not set
        if(path == null)
        	path =  FileRegister.OUT_PATH;
        
	    //to perserve all saved logs we add the time to the filename
	    String speciallogName = nameOfFile.toUpperCase();
     
	     
		     if(!speciallogs.containsKey(nameOfFile))
			    {
			     
			    	 //check if a file with this name exist if this should be deleted
			    	
			    	 if(overwrite)
			    	 {
			    		 File old_file = new File(path+speciallogName+extension);
			    		 if(old_file.exists())
			    		 	old_file.delete();
			    	 }
			    	try{ 
			    	 //create writer
			    	 	FileWriter fw= new FileWriter(path+speciallogName+extension,true);
			    	
			    	 	//add headertext
			    	 	if(headerText!=null)
			    	 		fw.append(headerText);
			    	 	
			    	 	//copy line by line
			    	 	//read the file
			    		if(file.canRead())
			    		{ 
			    			Reader fis = 
			    				 new FileReader(file);
			    			BufferedReader d
			    	          = new BufferedReader(fis);
			    			
			    	    
			    	      String nextLine = d.readLine();
			    	    	  while(nextLine !=null)
			    	    	  {
			    	    		  fw.append(nextLine +PN.LINESEPARATOR);
			    	    		  
			    	    		  nextLine = d.readLine();
			    	    	  }
			    	      
			    			
			    	    	  fis.close();
			            
			    		}
			    	
			    	 	//create this log 
				       speciallogs.put(nameOfFile,fw);
				       //flush first appended message
				       speciallogs.get(nameOfFile).flush();
			    	}catch(IOException ioe)
			    	{
			    		throw new SimulationException(ioe);
			    	}
		    }
        
		
       
	
			
	}
	/**
	 * @param outPath
	 * @param instance
	 * @throws IOException 
	 * 
	 *
	 */
	public static void init(String outpath) throws IOException
	{
	    filename = DEFAULTLOGNAME;
	    
	    path = outpath;
	    
        logWriter = new FileWriter(path+filename,true);
			
		
		
	}
	/**
	 * @throws IOException 
	 * 
	 *
	 */
	public final static void close() throws IOException
	{
//		path is not set
        if(path == null)
        	path =  FileRegister.OUT_PATH;
        
        if(logWriter==null) 
        	return;
 
        logWriter.flush();
		   
		logWriter.close();
		
		System.out.println(LOGWRITTEN+PN.LINESEPARATOR + path+filename);
          
	}
	/**
	 * Close special log i.e. other than the generic log file
	 * @throws IOException 
	 * 
	 *
	 */
	public static void closeSpecial() 
	{
//		path is not set
        if(path == null)
        	path =  FileRegister.OUT_PATH;
        
	    if(!speciallogs.isEmpty())
	    {
			  Iterator<String> it =
				  speciallogs.keySet().iterator();
			   while(it.hasNext())
			    {
			        String speciallogname = 
			        	(String)it.next();
			        FileWriter speciallogWriter = 
			        	(FileWriter)speciallogs.get(speciallogname);
			    if(speciallogWriter!= null)
			    {
			    
			    	try{
				    /* ask whether to save the log or not*/
					speciallogWriter.flush();
				    speciallogWriter.close();
			    	}catch (IOException ioe)
			    	{
			    		throw new SimulationException(ioe);
			    	}
				
			    }
			    speciallogs = new LinkedHashMap<String,Writer>();
		    }
	    }
     
	}
	/**
	 * 
	 * @param speciallog
	 * @throws IOException 
	 */
	public static void closeSpecial(String speciallog)
	{
		Writer sfw = 
			speciallogs.remove(speciallog);
		
		if(sfw == null)
			return;
		try{
			sfw.flush();
			sfw.close();
		}catch(IOException ioe)
		{
			throw new SimulationException(ioe);
		}
		
		
	}
	
	/**
	 * Close special log i.e. other than the generic log file
	 * @param no_discarding
	 * @throws IOException 
	 *
	 */
	public static void closeSpecial(boolean no_discarding) 	{
		//path is not set
        if(path == null)
        	path =  FileRegister.OUT_PATH;
        
	    if(!speciallogs.isEmpty())
	    {
		    Iterator<String> it = speciallogs.keySet().iterator();
		    while(it.hasNext())
		    {
		        String speciallogname = (String)it.next();
		        FileWriter speciallogWriter = (FileWriter)speciallogs.get(speciallogname);
		    if(speciallogWriter!= null)
		    {
		    
			try{
			    /* ask whether to save the log or not*/
				speciallogWriter.flush();
			    speciallogWriter.close();
			}catch(IOException ioe)
			{
				throw new SimulationException(ioe);
			}
			    
			    if(no_discarding)
			    	return;
			    else	    {
			        String name = 
			        	path+speciallogname+DEFAULTEXTENSION;
			        deleteLog(name);
			        
			    	}			    
				
				 
		    	}
		    }
		    speciallogs = 
		    	new LinkedHashMap<String,Writer>();
	    }
	}
	
	/**
	 * 
	 *
	 */
    public static void deleteLog(String filename) {
     
            File file = new File(filename);
            file.delete();
          
            return;
        
    }
   
    /**
     * Converts time in milliseconds to a <code>String</code> in the format HH:mm:ss.SSS.
     * @param time the time in milliseconds.
     * @return a <code>String</code> representing the time in the format HH:mm:ss.SSS.
     */
    public static String millisecondsToString(long time)
    {
        Date d = new Date();
        d.setTime(time);
        return d.toString();
        
    }
	
    
    /**
     * Reset the default path
     * @param new_path
     */
    public static void setDefaultpath(String new_path)
    {
    	path = new_path;
    }
    
    /**
     * Set the text with which is added to each logging 
     * @param header
     */
	public static void setHeaderText(String header) {
		// set text
		SimulationLog.headerText = header;
	}

	
}
