/*
 * This file is part of SIMCOLEP, a simulation model developed by the Department of Public Health,
 * Erasmus MC, University Medical Center Rotterdam, Rotterdam, The Netherlands.
 *
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/
 * or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA
 *
 * You are free to:
 * Share � copy and redistribute the material in any medium or format
 * Under the following terms:
 * Attribution � You must give appropriate credit, provide a link to the license, and indicate if changes were
 * made. You may do so in any reasonable manner, but not in any way that suggests the licensor endorses
 * you or your use.
 * NonCommercial � You may not use the material for commercial purposes.
 * NoDerivatives � If you remix, transform, or build upon the material, you may not distribute the modified material.
 */
/**
 * created: 04-Apr-2006
 */
package simcolep.tools.io;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * @author ir. E.A.J. Fischer
 * @version 1.0
 */
public final class CollectionFormat implements DataFormat<Collection<?>> {

	/**
	 *
	 */
	public CollectionFormat() {
		super();
	}

	/**
	 * @see simcolep.tools.io.DataFormat#average(java.util.Collection)
	 */
	@Override
	public Collection<?> average(Collection<Collection<?>> output) {
		return null;
	}

	/**
	 * @see simcolep.tools.io.DataFormat#averageString(java.util.Collection)
	 */
	@Override
	public Serializable averageString(Collection<Collection<?>> output) {
		return null;
	}

	/**
	 * @see simcolep.tools.io.DataFormat#combine(java.util.Collection)
	 */
	@Override
	public Serializable combine(Collection<Collection<?>> output) {
		StringBuffer s = new StringBuffer();

		for (Collection<?> c : output) {
			for (Object obj : c) {
				s.append(obj + DataFormat.LINESEPARATOR);
			}
			s.append(DataFormat.LINESEPARATOR);
		}
		return s.toString();
	}

	/**
	 *
	 * @see simcolep.tools.io.DataFormat#nodatamessage()
	 */
	@Override
	public Collection<?> nodatamessage() {

		List<String> c = new ArrayList<String>();
		c.add(OutputHandler.NODATA_COLLECTED_MESSAGE);
		return c;
	}

	@Override
	public Serializable standard() {
		return new ArrayList<Object>();
	}

	@Override
	public Serializable sum(Collection<Collection<?>> output) {
		return null;
	}

}
