/*
 * This file is part of SIMCOLEP, a simulation model developed by the Department of Public Health, 
 * Erasmus MC, University Medical Center Rotterdam, Rotterdam, The Netherlands.
 *
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/ 
 * or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA
 *
 * You are free to:
 * Share � copy and redistribute the material in any medium or format
 * Under the following terms:
 * Attribution � You must give appropriate credit, provide a link to the license, and indicate if changes were 
 * made. You may do so in any reasonable manner, but not in any way that suggests the licensor endorses
 * you or your use.
 * NonCommercial � You may not use the material for commercial purposes.
 * NoDerivatives � If you remix, transform, or build upon the material, you may not distribute the modified material.
 */
/*
 * Created on 07-Nov-2005
 *
 */
package simcolep.tools.message;

import java.util.Map;
import java.util.LinkedHashMap;


/**
 * Messages that are passed through by Ratatosk
 * This class contains a list of constants that can be used for messaging. These constants start with an M 
 * @author ir. E.A.J. Fischer
 * @version 1.3
 */
public final class Message {
	
	/*pre-imlemented messages all constant names start with an M*/
	/**birth*/
	public static final String BIRTH = "b";
	/**death*/
	public static final String DEATH = "d";
	/**change age category*/
	public static final String AGE = "a";
	/**wedding*/
	public static final String WEDDING = "w";
	/**spouse dies*/
	public static final String SPOUSEDEATH = "sd";
	/**activation*/
	public static final String ACTIVATE = "act";
	/**splitting up of a level/household*/
	public static final String SPLIT = "sl";
	/**add an inhabitant to a level*/
	public static final String ADDINHABITANT = "ai";
	/**remove an inhabitant from a level*/
	public static final String REMOVEINHABITANT = "ri";

	/**Infected*/
	public static final String INFECTED = "i";
	/**change infection state*/
	public static final String CHANGESTATE = "cs";
	/**import infection*/
	public static final String IMPORTINFECTION = "imp";
		
	/**Blanket treatment*/
	public static final String BLANKET = "bl";
	/**contact tracing*/
	public static final String CONTACTTRACING = "ct";
	/**Follow up*/
	public static final String FOLLOWUP = "fu";
	/**Passive detection or self reporting*/
	public static final String PASDETECTION = "pasdet";
	/**Survey */
	public static final String SURVEY = "sur";
	/**End of treatment*/
	public static final String TREATMENTEND = "te";
	/**cure*/
	public static final String CURE = "cure";
	/**activation of a morbus*/
	public static final String MORBUSACTIVATION = "mact";
	/**stop simulation*/
	public static final String STOP = "stop";
	/**Infection went extinct*/
	public static final String EXTINCT = "ext";
	/**contractions start*/
	public static final String WOE = "woe";
	/**schedule births*/
	public static final String CONCEPTIONS = "conc";
	/**launch information*/
	public static final String LAUNCHINFO = "LaunchInfo";
	/**bcg campaign*/
	public static final String BCGCAMPAIGN = "bcg";
	
	
	

	
	
	
	/**contains already formulated messages*/
    private static Map<String, Message> messages =
    	null;
    	
    /**contains additional information with a messages*/
    private static Map<Message, Object[]> info_messages =
    	null;
    
	/**message*/
	private String message = null;
	
	/**
     * 
     * @param string
     */
    private Message(String string) {
        this.message = string;
    }
    
   
    /**
     * Formulate the specific message
     * @param string
     * @return message for popnum and with string as message
     */
    public static Message formulate(String string)
    {
        //determine if it was formulated before 
        if (messages==null) 
        	messages = 
        		new LinkedHashMap<String,Message>();
        if(!messages.containsKey(string))
        	messages.put(string,new Message(string));
        //return the message
        return messages.get(string);
    }
    
    /**
     * Formulate the specific message
     * @param string
     * @param info
     * @return message for popnum and with string as message
     */
     public static Message formulate(String string, Object[] info)
    {
    
    	 //formulate the message
    	  Message m = Message.formulate(string);
    	  
    	  //add additional information
    	  if(info_messages == null)
    		  info_messages = new LinkedHashMap<Message, Object[]>();
    	  info_messages.put(m,info);
    	      
       //return
       return m;
    }
    /**
     * Compare string to the message 
     * @param s
     * 
     */
    public boolean equalsIgnoreCase(String s)
    {
        return s.equalsIgnoreCase(message);
    }
    
    /**
     * 
     * @param s
     * 
     */
    public boolean equals(String s)
    {
        return s.equals(message);
    }
    /**
     * 
     * @param m
     * @return true if message and population number equal
     */
    public boolean equals(Message m)
    {
        return (this.message.equals(m.message));
    }
    
      
    /**
     * 
     * @return message
     */
    public String getMessage()
    {
        return this.message;
    }
    
    /**
     * Get the additional information
     * @return info
     */
    public Object[] getInfo()
    {
    	return info_messages.get(this);
    }
    
    /**
     * Set the information
     * @param obj
     */
    public void setInfo(Object[] obj)
    {
    	//remove old information
    	info_messages.remove(this);
    	//add new information
    	info_messages.put(this,obj);
    }
    
    /**
     *
     * @see java.lang.Object#toString()
     */
    public String toString()
    {
       String s = 
    	   this.message; 
       
       if(info_messages.get(this)!= null)
    	   for(Object o : info_messages.get(this))
    		   s+=o.toString();
       
       return s;
    }
   

}
