/*
 * This file is part of SIMCOLEP, a simulation model developed by the Department of Public Health, 
 * Erasmus MC, University Medical Center Rotterdam, Rotterdam, The Netherlands.
 *
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/ 
 * or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA
 *
 * You are free to:
 * Share � copy and redistribute the material in any medium or format
 * Under the following terms:
 * Attribution � You must give appropriate credit, provide a link to the license, and indicate if changes were 
 * made. You may do so in any reasonable manner, but not in any way that suggests the licensor endorses
 * you or your use.
 * NonCommercial � You may not use the material for commercial purposes.
 * NoDerivatives � If you remix, transform, or build upon the material, you may not distribute the modified material.
 */
/*
 * Created on 7-jul-2005
 *
 *
 */
package simcolep.tools.message;

import java.io.IOException;

import java.util.Set;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;

import simcolep.exceptions.SimulationException;
import simcolep.sim.Main;



/**
 * 
 * <b> Ratatosk</b> is a squirrel that runs over the stem of Yggdrasil bringing gossip from the wise Eagle in the top of Yggdrasil to Nidhogg and back again.
 * It is also the bringer of snow and rain. <br><br>
 * In SIM<i>CO</i>LEP it is the messenger between the different Gods and Objects of the Interface Notifyable.
 * @author ir. E.A.J. Fischer
 *@version 0.1.1 -- Last Modified: 7-jul-2005
 */
public class Ratatosk{
	/***/
	public static final String UNKNOWNMESSAGE = "message is unknown what to do for this class";
	
	/**Let Ratatosk shut up*/
	private static boolean MUTE = false;

	/**map containing objects listening to*/
    private static LinkedHashMap<Message,Set<Notifyable>> messageTo 
    		= new LinkedHashMap<Message,Set<Notifyable>>();
   
    /**list with eavesdropper*/
    private static Set<Eavesdropper> eavesDroppers 	
    	= new LinkedHashSet<Eavesdropper>();
    
    /**
     * register a certain Object to be notified by certian messages
     * @param message
     * @param obj
     * @return true
     * 
     */
    @SuppressWarnings("unchecked")
    public static boolean register(Message message, Notifyable obj)
    {
    	if(messageTo == null)
    		initialize();
                      
        if(messageTo.containsKey(message))
        {
            Set set = ((Set)messageTo.get(message));
            if(set.contains(obj))
            	throw new IllegalArgumentException("Can only register once per Object and message " + obj);
            if(!set.add(obj))
            	throw new IllegalArgumentException("not registered");
            return true;
               
        }else
        {
            LinkedHashSet<Notifyable> set = new LinkedHashSet<Notifyable>();
            set.add(obj);
            messageTo.put(message,set);
            return true;
            
        }
    }
    /**
     * 
     * @param eavesdropper
     * @return true if added
     */
    public static boolean register(Eavesdropper eavesdropper)
    {
    	if(eavesDroppers == null)
    		initialize();
    		
    	return eavesDroppers.add(eavesdropper);
    }
    /**
     * Notify those that have registered to listen to this typical message
     * @param message
     * @param subject
     * @param source
     * @throws IOException 
     * 
     */
    public static void notify(Message message, Object subject, Object source) 
    		
    {
    	if(MUTE)
    		return;
    	//first inform the eavesDroppers
    	for(Eavesdropper ed : eavesDroppers)
    	{
    		ed.listenIn(message);
    	}
    	if(message.getMessage().contains(Message.LAUNCHINFO))
			return;
    	
    	try{
    		
	    	//notify those that are registered
	        for(Notifyable next : ((Set<Notifyable>)messageTo.get(message)))
		     {
	           		next.notify(message,subject,source);
	        }
	  
    	}catch(Exception e){
    		
    		if(!messageTo.containsKey(message))
    			Main.handleException(new SimulationException("Nobody registered to listen to: "+ message));
    		Main.handleException(e);
    	}
    		
    }
    
    /**
     * 
     *
     */
    public static void initialize()
    {
    	//creaet message to map
    	if(messageTo == null)
    		messageTo = new LinkedHashMap<Message,Set<Notifyable>>();
    	
    	//eavesDroppers set
    	if(eavesDroppers == null)
    		eavesDroppers = new LinkedHashSet<Eavesdropper>();
    	
    	MUTE = false;
    }
    
    /**
     * 
     * resets Ratatosk (messageTo = new HashMap<String,LinkedHashSet>())
     * 
     */
    public static void reset()
    {
        messageTo = new LinkedHashMap<Message,Set<Notifyable>>();
        MUTE = false;
    }
    /**
     * Kill Ratatosk and all its tasks with it!
     *
     */
    public static void kill()
    {
    	
    	messageTo = null;
    	    	
    	eavesDroppers = null;
    }
    /**
     * Mute Ratatosk
     */
    public static void silence()
    {
    	MUTE = true;
    }
    /**
     * @param message
     * @param obj
     */
    @SuppressWarnings("unchecked")
	public static void unregister(Message message, Object obj) {
       ((Set)messageTo.get(message)).remove(obj);
        
    }
    /**
     * 
     * @param eavesdropper
     * @return true if removed
     */
    public static boolean unregister(Eavesdropper eavesdropper)
    {
    	return eavesDroppers.remove(eavesdropper);
    }
    
    
    /**
     * 
     * @return messageTo.isEmpty();
     */
	public static boolean isEmpty() {
			return messageTo.isEmpty();
	}
}
