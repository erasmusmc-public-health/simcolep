/*
 * This file is part of SIMCOLEP, a simulation model developed by the Department of Public Health, 
 * Erasmus MC, University Medical Center Rotterdam, Rotterdam, The Netherlands.
 *
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/ 
 * or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA
 *
 * You are free to:
 * Share � copy and redistribute the material in any medium or format
 * Under the following terms:
 * Attribution � You must give appropriate credit, provide a link to the license, and indicate if changes were 
 * made. You may do so in any reasonable manner, but not in any way that suggests the licensor endorses
 * you or your use.
 * NonCommercial � You may not use the material for commercial purposes.
 * NoDerivatives � If you remix, transform, or build upon the material, you may not distribute the modified material.
 */
/**
 * created: 31-Oct-2006
 */
package simcolep.tools;

import java.io.Serializable;
import java.util.LinkedList;

/**
 * @author ir. E.A.J. Fischer
 * @version 1.0
 */
public class History 
			extends LinkedList<History.HistoricalObject> 
				implements Serializable {

	/**
	 * Object contains a event desciption for a certain time moment
	 * @author ir. E.A.J. Fischer
	 * @version 1.0
	 */
	class HistoricalObject implements Serializable
	{
		private static final String TAB = " \t"; //$NON-NLS-1$

		/**
		 * serialk version uid
		 */
		private static final long serialVersionUID = 5407630109362130245L;

		/**time of event*/
		private double time = 0.0;
		
		/**event*/
		private transient Object event = null;
		/**
		 * 
		 * @param t
		 * @param e
		 */
		private HistoricalObject(double t,Object e)
		{
			this.time = t;
			this.event = e;
		}
		/**
		 * 
		 * @see java.lang.Object#toString()
		 */
		public String toString()
		{
			return this.time +TAB+
					this.event.toString();
		}
	}
	/**
	 * 
	 */
	private static final long serialVersionUID = 6947319481171180352L;

	/**
	 * Add history to the list
	 * @param d - time
	 * @param obj - event description
	 */
	public void add(double d, Object obj)
	{
		super.add(new HistoricalObject(d,obj));
	}
	/**
	 * 
	 * @see java.lang.Object#toString()
	 */
	public String toString()
	{
		//a string
		String s = new String();
		
		//iterate this list
		for(HistoricalObject e : this)
		{
			s+=e.toString()+ System.getProperty("line.separator");
		}
		
		//return string
		return s;
	}
}
