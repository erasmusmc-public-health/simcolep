/*
 * This file is part of SIMCOLEP, a simulation model developed by the Department of Public Health, 
 * Erasmus MC, University Medical Center Rotterdam, Rotterdam, The Netherlands.
 *
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/ 
 * or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA
 *
 * You are free to:
 * Share � copy and redistribute the material in any medium or format
 * Under the following terms:
 * Attribution � You must give appropriate credit, provide a link to the license, and indicate if changes were 
 * made. You may do so in any reasonable manner, but not in any way that suggests the licensor endorses
 * you or your use.
 * NonCommercial � You may not use the material for commercial purposes.
 * NoDerivatives � If you remix, transform, or build upon the material, you may not distribute the modified material.
 */
/*
 * Created on 05-Jan-2005
 *
 *
 */
package simcolep.tools;


import java.util.HashMap;
import java.util.Vector;
import java.util.LinkedList;


import simcolep.tools.io.SimulationLog;

/**
 * Time measurer is a tool to measure times from start to end and in between returns in hours with millisecond precision
 * @author ir. E.A.J. Fischer
 * @version 0.1.1 -- last modified -- 26-07-05
 *
 */
public class TimeMeasurer {

	private static final String TIMEMEASUREMENT_TEXT = "timemeasurement";

	private static final String STARTTOEND_TEXT = "Start to end in: ";

	private static final String TOTALTIME_TEXT = "Total time: ";

	private static final String TAB = "\t";

	private static String LINESEPARATOR = System.getProperty("line.separator");

	/***/
	private static Vector<TimeMeasurer> measurers 
					= null;
	

	/**time at which the timer was started*/
	private  long starttime = 0;
	

	
    /**recorded times*/
	private LinkedList<String> times = 
		new LinkedList<String>();
	
	/**previous time*/
	private long previous = 0;

	/**Title*/
	private static String title = "";

	/** false when timer is off*/
	private static boolean timer = true;
	
	/**names for measurments*/
	private HashMap<Integer,String> specialnames = 
			new HashMap<Integer,String>();
	
	
	/**
	 * 
	 */
	public TimeMeasurer() {
		super();
		
	}
    /**
     * 
     * @return t
     */
    public static TimeMeasurer startNew()
    {
    	if(!timer)
    		return new TimeMeasurer();
    	//create a new time measurer
        TimeMeasurer t = 
        	new TimeMeasurer();
        //remember
        if(measurers == null)
        	measurers = new Vector<TimeMeasurer>();
        measurers.add(t);
        //start
        t.start();
        //return
        return t;
    }
    
    /**
     * 
     * @param title
     * @return t
     */
    public static TimeMeasurer startNew(String title)
    {
    	TimeMeasurer.title = title;
    	
    	return TimeMeasurer.startNew();
    }
    /**
     * Record and return a lap
     * @return last
     */
    public static String lap()
    {
    	if(!timer)
    		return null;
    	//if null 
    	if(measurers == null)
    		return null;
    	//get the last entered 
    	return measurers.get(measurers.size()-1).relapsedTime();
    }
    
    /**
     * Record and return a lap
     * @return last
     */
    public static String lap(String name)
    {
    	if(!timer)
    		return null;
    	//if null 
    	if(measurers == null)
    		return null;
    	//set the name
    	measurers.get(measurers.size()-1).specialnames.put(new Integer(measurers.size()-2),name);
    	//get the last entered 
    	return measurers.get(measurers.size()-1).relapsedTime();
    }

	/**
	 * The parameter gui determines whether the output is written to file or given in a JDialog at the end
	 * 
	 *
	 */
	public void start()
	{ 	
		if(!timer)
    		return;
		
		starttime = 
			System.currentTimeMillis();
 
	}
	/**
	 * 
	 * @return time from previous call to this method to this point
	 */
	public String relapsedTime()
	{ 	
		if(!timer)
    		return null;
		//if previous was not set yet set the start time
	    if(this.previous <=0)
	    	this.previous = this.starttime;
	    //get the current time
	    long time = System.currentTimeMillis();
	    //create a string with the time since the previous call or start
	    String t = millisecondsToString(time-this.previous); 
	    //add to the list of times
	    this.times.add(t);
	    //set previous to current
	    this.previous =time;
	    //return the time passed
	    return t;
	}
	
	
	/**
	 * 
	 * @return String
	 *
	 */
	private String end()
	{
		if(!timer)
    		return null;
		
		return millisecondsToString(
				System.currentTimeMillis()-starttime);
	}
	
	/**
	 * 
	 *
	 */
	public void endOut()
	{
		
	    System.out.println(end());
	}
	/**
	 *Ends this timemeasurer by writing to a log-file 
	 *
	 */
	public void endLog()
	{
		if(!timer)
    		return;
	
       SimulationLog.setHeaderText(null);
       SimulationLog.speciallog(TIMEMEASUREMENT_TEXT,
    		   STARTTOEND_TEXT+end()+LINESEPARATOR, 
    		   false, 
    		   false,
    		   true);
    
	}
	/**
	 * End with full information on the laps and times passsed
	 * 
	 * @param overwrite existing logs
	 * 
	 *
	 */
	public void endWithReport(boolean overwrite)
	{
		if(!timer)
    		return;
		
	    String end = end();
	    String Alltime = TimeMeasurer.title;
	   
	   for(int x = 0;x<this.times.size();x++)
	    {
	       if(this.specialnames.get(new Integer(x))!=null)
	    		   Alltime += LINESEPARATOR+this.specialnames.get(x) +TAB+this.times.get(x);
	       else
	    	   Alltime += LINESEPARATOR+"Measurement: "+ x +TAB+this.times.get(x);
	    }
	   Alltime += LINESEPARATOR+TOTALTIME_TEXT + end +LINESEPARATOR;
	    
	   SimulationLog.setHeaderText(null);
	   SimulationLog.speciallog(TIMEMEASUREMENT_TEXT,
    			   Alltime, 
    			   overwrite,
    			   false, 
    			   true);
       
	}
	/**
	 * Converts time in milliseconds to a <code>String</code> in the format HH:mm:ss.SSS.
	 * @param time the time in milliseconds.
	 * @return a <code>String</code> representing the time in the format HH:mm:ss.SSS.
	 */
	public static String millisecondsToString(long time)
	{
	    long milliseconds = (time % 1000);
	    long seconds = ((time/1000) % 60);
	    long minutes = ((time/60000) % 60);
	    long hours = ((time/3600000) % 24);
	    String millisecondsStr = (milliseconds<10 ? "00" : (milliseconds<100 ? "0" : ""))+milliseconds;
	    String secondsStr = (seconds<10 ? "0" : "")+seconds;
	    String minutesStr = (minutes<10 ? "0" : "")+minutes;
	    String hoursStr = (hours<10 ? "0" : "")+hours;
	    return new String(hoursStr+":"+minutesStr+":"+secondsStr+"."+millisecondsStr);
	}
	
	/**
	 * Turn gui and timer on or off
	 * @param gui
	 * @param timer
	 */
	public static void turnoff(boolean timer) {

		timer = false;
		
	}


}
