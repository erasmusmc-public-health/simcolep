/*
 * This file is part of SIMCOLEP, a simulation model developed by the Department of Public Health,
 * Erasmus MC, University Medical Center Rotterdam, Rotterdam, The Netherlands.
 *
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/
 * or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA
 *
 * You are free to:
 * Share � copy and redistribute the material in any medium or format
 * Under the following terms:
 * Attribution � You must give appropriate credit, provide a link to the license, and indicate if changes were
 * made. You may do so in any reasonable manner, but not in any way that suggests the licensor endorses
 * you or your use.
 * NonCommercial � You may not use the material for commercial purposes.
 * NoDerivatives � If you remix, transform, or build upon the material, you may not distribute the modified material.
 */

package simcolep.asgard;

/**
 * <b>Ase</b> those who dwell in Asgard. The higher gods of the nordic patheon.
 *
 * @author ir. E.A.J. Fischer
 * @version 1.0
 */
public interface Ase {

	/**
	 * The end of the god
	 */
	public void ragnarok();

	/**
	 * Start saving those things for which the Ase in responsible
	 * 
	 * @param b
	 */
	public void setSave(boolean b);
}
