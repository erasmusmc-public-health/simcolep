/*
 * This file is part of SIMCOLEP, a simulation model developed by the Department of Public Health,
 * Erasmus MC, University Medical Center Rotterdam, Rotterdam, The Netherlands.
 *
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/
 * or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA
 *
 * You are free to:
 * Share � copy and redistribute the material in any medium or format
 * Under the following terms:
 * Attribution � You must give appropriate credit, provide a link to the license, and indicate if changes were
 * made. You may do so in any reasonable manner, but not in any way that suggests the licensor endorses
 * you or your use.
 * NonCommercial � You may not use the material for commercial purposes.
 * NoDerivatives � If you remix, transform, or build upon the material, you may not distribute the modified material.
 */

/*
 * Created on 04-Nov-2005
 *
 */
package simcolep.asgard;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;

import nl.mgz.simulation.discreteevent.EventManager;
import nl.mgz.simulation.discreteevent.Target;
import simcolep.demo.Female;
import simcolep.demo.Inhabitant;
import simcolep.demo.Level;
import simcolep.demo.Person;
import simcolep.exceptions.SimulationException;
import simcolep.fun2.Archimedes;
import simcolep.fun2.Distributionable;
import simcolep.lepra.Clinic;
import simcolep.lepra.Morbus;
import simcolep.lepra.State;
import simcolep.lepra.Strain;
import simcolep.lepra.event.TransmissionDwelling;
import simcolep.lepra.event.TransmissionImport;
import simcolep.lepra.event.TransmissionRM;
import simcolep.lepra.event.intervention.DelayedSelfReport;
import simcolep.lepra.function.DiseaseHistory;
import simcolep.lepra.function.Kernel;
import simcolep.lepra.mendel.GeneModel;
import simcolep.lepra.mendel.Genomable;
import simcolep.lepra.mendel.InheritanceModel;
import simcolep.lepra.mendel.iModel;
import simcolep.sim.DiseaseRun;
import simcolep.sim.Gylfi;
import simcolep.sim.Herbarium;
import simcolep.sim.Main;
import simcolep.tools.PN;
import simcolep.tools.io.SimulationLog;
import simcolep.tools.message.Message;
import simcolep.tools.message.Notifyable;
import simcolep.tools.message.Ratatosk;

/**
 * <i>Idunn</i> godess of health, wife of Thor
 *
 * <code>Idunn</code> governs all disease related procedures *
 *
 * @author ir. E.A.J. Fischer
 * @version 1.1
 */
public final class Idunn extends Target implements Notifyable, Ase {

	private static final String IMPORT_MARK = "import"; //$NON-NLS-1$

	/** serialVersionUID */
	private static final long serialVersionUID = 8977315189008285977L;

	/**
	 * listens to the following messages of Ratatosk
	 */
	protected static final String[] listensTo = { Message.ACTIVATE, // activate
			Message.BIRTH, // birth
			Message.ADDINHABITANT, // add inhabitant
			Message.REMOVEINHABITANT, // remove inhabitant
			Message.IMPORTINFECTION, // import infection
			Message.EXTINCT // infection goes extinct
	};

	/* import coding */
	/** random */
	private static final int RANDOMIMPORT = 0;
	/** susceptibles */
	private static final int SUSCEPTIBLEIMPORT = 1;
	/** type */
	private static final int TYPEIMPORT = 2;
	/** dwelling size */
	private static final int DWELLIMPORT = 3;

	private static final String AFTERROUNDMESSAGE = " after: ";

	private static final String NOIMPORTMESSAGE = " No import possible ";

	/* Parameter names */

	/** time of the first import of infection */
	private static double time_of_firstImport = Double.POSITIVE_INFINITY;

	/** number of imports per import event */
	protected static double imports_per_event = 0;

	/**
	 * how to import infection<br>
	 * 0: Random <br>
	 * 1: Only susceptibles<br>
	 * 2: Only certain type<br>
	 * 3: Random within a certain household size<br>
	 */
	private static int import_method = 0;

	/** method 2: type to import to */
	private static int import_type = 4;

	/** method 3: household size to import in */
	private static int import_hhsize = 2;

	/**
	 * Code to determine random mixing:
	 * <li>code 888 : Members of the own level (and sublevel) are not included
	 * in the mixing
	 * <li>code 999 : Nobody excluded
	 */
	protected static int random_mixing_code = 0;
	/** instances of Idunn */
	private static Idunn idunn = null;

	/** event manager */
	private static EventManager em = null;

	private static double[] curTime = new double[1];

	/** disease functions */
	protected static DiseaseHistory[] disfunc = null;

	/** inheritance model of disease types */
	public static InheritanceModel i_Model = null;

	/** number of morbi per individual */
	public static int LAYERS = 0;

	/**
	 * two mechanisms by which people are susceptible are possible thus and
	 * genes and/or household
	 */
	private static boolean twosusmechanisms = false;

	public static double MAXFU;

	/**
	 * Collect data of all registers of clinics
	 *
	 * @param time
	 */
	public static void collectData(double time) {
		idunn.clinic.getRegister().collectData(time);
	}

	/**
	 *
	 * @return time of the simulation
	 */
	public static double getCurrentTime() {
		if (em == null)
			return Double.NEGATIVE_INFINITY;
		return em.getTime();
	}

	/**
	 *
	 * @return time of the simulation in an array
	 */
	public static double[] getCurrentTimeArray() {
		if (em == null)
			curTime[0] = Double.NEGATIVE_INFINITY;
		else
			curTime[0] = em.getTime();

		return curTime;
	}

	/**
	 * Get the period between first symptoms and detection (and treatment)
	 *
	 * @param m
	 * @return period betweeb symptoms and passive detection
	 */
	public static double getDetection(Morbus m) {
		return disfunc[m.getType()].getPasDetection(m, Herbarium.rand(Herbarium.DIS));
	}

	/**
	 *
	 * @return the event manager of the simulation
	 */
	public static EventManager getEventManager() {

		return em;
	}

	/**
	 * Getter for the instance of Idunnn for this population with popnum as
	 * identification
	 *
	 * @return idunn[popnum]
	 */
	public static Idunn getIdunn() {
		return idunn;
	}

	/**
	 * The incubation until multibaccilary
	 *
	 * @param m
	 * @return incubation period to MB
	 */
	public static double getIncMB(Morbus m) {
		return disfunc[m.getType()].getIncubationMB(Herbarium.rand(Herbarium.DIS),
				m.getOwner().getAge(Idunn.getCurrentTime()));
	}

	/**
	 * The incubation until paucibacilary
	 *
	 * @param m
	 * @return incubation period to PB
	 */
	public static double getIncPB(Morbus m) {
		return disfunc[m.getType()].getIncubationPB(Herbarium.rand(Herbarium.DIS),
				m.getOwner().getAge(Idunn.getCurrentTime()));
	}

	/**
	 *
	 * @param morbus
	 * @return kernel
	 */
	public static Kernel getKernel(Morbus morbus) {
		return Idunn.disfunc[morbus.getType()].getKernel();
	}

	/**
	 * Duration until latency
	 *
	 * @param m
	 *
	 */
	public static double getLatency(Morbus m) {
		return disfunc[m.getType()].getLatency(Herbarium.rand(Herbarium.DIS),
				m.getOwner().getAge(Idunn.getCurrentTime()));
	}

	/**
	 * Duration between end latency period and selfhealing
	 *
	 * @param m
	 * @return time until self healing
	 */
	public static double getSelfhealing(Morbus m) {
		return disfunc[m.getType()].getSelfHealing(Herbarium.rand(Herbarium.DIS),
				m.getOwner().getAge(Idunn.getCurrentTime()));
	}

	/**
	 * The sequence of types in natural progression
	 *
	 * @param type
	 * @return disfunc[type].getStates();
	 */
	public static int[] getStateSequence(int type) {
		return disfunc[type].getStates();
	}

	/**
	 * Initialize Idunn
	 *
	 */
	public static void init() {
		// for debugging
		Morbus.saveDiseaseHistory = Gylfi.ask(PN.SAVEHISTORY);

		// number of specified disease types
		Morbus.DISEASE_TYPES = Archimedes.askInteger(PN.DISEASETYPES);

		Morbus.RELAPSETYPE = Archimedes.askInteger(PN.RELAPSETYPE);

		Idunn.LAYERS = Archimedes.askInteger(PN.L);

		// get the disease function
		disfunc = new DiseaseHistory[Morbus.DISEASE_TYPES];

		for (int x = 0; x < Morbus.DISEASE_TYPES; x++)
			disfunc[x] = (DiseaseHistory) Archimedes.ask(PN.DISEASE_HISTORY + x);

		Idunn.twosusmechanisms = Gylfi.ask(PN.TWOSUSMECH);

		if (Idunn.twosusmechanisms)
			System.out.println("\r\nTAKE NOTICE: Two mechanisms for susceptibility working alongside");

		// determine which inheritance model is used
		// 0: Random
		// 1: Inheritance of type
		// 2: Inheritance of susceptibility
		int inheritance_model = Archimedes.askInteger(PN.IMODELNAME);
		// inheritance model parameters
		double[] i_model_parameters = new double[0];
		if (inheritance_model == 0) {
			Idunn.i_Model = iModel.RandomModel.getModel();

			// parameters
			i_model_parameters = new double[0];
		}

		else if (inheritance_model == 1) {
			Idunn.i_Model = iModel.TypeModel.getModel();

			i_model_parameters = new double[1];
			i_model_parameters[0] = Archimedes.askDouble(PN.PROBDISMOM);
		}

		else if (inheritance_model == 2) {
			Idunn.i_Model = iModel.SusceptibleModel.getModel();

			i_model_parameters = new double[1];
			i_model_parameters[0] = Archimedes.askDouble(PN.PROBDISMOM);
		}

		else if (inheritance_model == 3) {
			Idunn.i_Model = iModel.GeneModel.getModel();
			// number of genes
			i_model_parameters = new double[1];
			i_model_parameters[0] = Archimedes.askDouble(PN.NUMOFGENES);

			GeneModel.starthomozygous = Gylfi.ask(PN.HOMOZYGOUS);
		}

		else
			throw new SimulationException("unknown inheritance model: " + inheritance_model);
		// set the model

		Idunn.i_Model.setModel(i_model_parameters);

		// contact rates
		random_mixing_code = Archimedes.askInteger(PN.RANDOMMIXINGCODE);

		// initialize Strain
		Strain.initialize();

		// instantiate idunn array
		idunn = new Idunn();

		// add a clinic to the Idunn
		idunn.clinic = new Clinic();

		// set the interventions
		idunn.clinic.setInterventions(Gylfi.ask(PN.CONTACTTRACING), Gylfi.ask(PN.PROPHYLACTIC), Gylfi.ask(PN.SURVEY),
				Gylfi.ask(PN.BLANKET), Gylfi.ask(PN.FOLLOWUP), Gylfi.ask(PN.BCGREVAC), Gylfi.ask(PN.BCGPROTECT),
				Gylfi.ask(PN.BCGSHIFT), Gylfi.ask(PN.CONTACTVACCINATION), Gylfi.ask(PN.BCGCAMPAIGN));
	}

	/**
	 * Initialize the events for the import of disease
	 *
	 *
	 */
	public static void initImport() {
		// how to import
		import_method = Archimedes.askInteger(PN.IMPMETHOD);

		if (import_method == 2)
			import_type = Archimedes.askInteger(PN.IMPTYPE);
		else if (import_method == 3)
			import_type = Archimedes.askInteger(PN.IMPHHSIZE);

		// and parameters
		imports_per_event = Archimedes.askDouble(PN.IMP_PER_EVENT);
		// first import at...
		time_of_firstImport = Archimedes.askDouble(PN.TIMEFIRSTIMP);

		// number of imports
		int imp_events = Archimedes.askInteger(PN.IMPEVENTS);

		// set parameters and schedule first import for each idunn
		// set the number
		idunn.import_events = imp_events;

		// schedule the first
		idunn.scheduleImport();

	}

	/**
	 */
	public static boolean inSafeMode() {
		return DiseaseRun.inSafeMode();
	}

	/**
	 * Set the EventManager
	 *
	 * @param em
	 */
	public static void setEventManager(EventManager em) {
		Idunn.em = em;

	}

	/**
	 * Set maximum number of follow ups
	 *
	 * @param maxfu
	 */
	public static void setMaxFU(double maxfu) {
		Idunn.MAXFU = maxfu;

	}

	/**
	 * Set the code for random mixing
	 * <li>777: All but the own dwelling
	 * <li>888: All but the own neighbourhood and thus needs specification of
	 * the neighbourhood
	 * <li>999: Every person
	 *
	 * @param rm
	 */
	public static void setRandommixing(int rm) {
		random_mixing_code = rm;
	}

	private int importcounter = 0;

	/**
	 * total number of import events
	 */
	protected int import_events = 0;

	/**
	 * function to time imports of infection
	 */
	protected Distributionable TimeOfImport = null;

	/**
	 * clinic for the population
	 */
	private Clinic clinic = null;

	/**
	 * List of morbi ready for self reporting before start of treatment. These
	 * are scheduled for real passive detection at start of treatment
	 */
	private List<Morbus> delayedSelfreporters = null;

	/**
	 * Constructor for Idunn
	 */
	private Idunn() {
		super();

		// register with Ratatosk
		for (String s : listensTo) {
			Ratatosk.register(Message.formulate(s), this);
		}

		Idunn.i_Model.reset();
	}

	/**
	 * Schedule transmission to the other inhabitants of the level to which the
	 * person was added
	 *
	 * @param person
	 */
	@SuppressWarnings(PN.UNCHECKED)
	public void addedToLevel(Person person) {
		// get the level
		Level l = person.getLevel();

		if (Idunn.twosusmechanisms)
			for (int i = 0; i < Idunn.LAYERS; i++) {
				if (l.getDFactor(i)) {
					Morbus m = person.getMorbus(i);
					m = setTSMPhenotype(m, true);

				}
			}
		// iterate the inhabitants of the level
		// so that we know if some one is infected
		// yes or no
		for (Inhabitant inhabitant : l.getInhabitants()) {
			// iterate the morbi
			for (int index = 0; index < Idunn.LAYERS; index++) {
				Morbus inhabitant_morbus = ((Person) inhabitant).getMorbus(index);

				// check whether one of the two is infectious
				if (inhabitant_morbus.isInfectious() || person.getMorbus(index).isInfectious()) {
					if (inhabitant_morbus.getState() == State.SUSCEPTIBLE_STATE)
						this.scheduleDwellingTransmission(person.getMorbus(index), inhabitant_morbus);
					else if (person.getMorbus(index).getState() == State.SUSCEPTIBLE_STATE)
						this.scheduleDwellingTransmission(inhabitant_morbus, person.getMorbus(index));
				}
			}
		}

	}

	/**
	 * Set error to this time
	 *
	 * @param time
	 * @param error
	 */
	public void collectError(double time, double error) {
		// collect the register error stuff
		this.clinic.getRegister().collectDataAtTimeAs(time, error);

	}

	/**
	 * Imports external infections. <br>
	 * Import occur in one of 4 methods:<br>
	 * <br>
	 * 0: random selection<br>
	 * 1: Only susceptibles<br>
	 * 2: Only certain type<br>
	 * 3: Only certain household size<br>
	 * <br>
	 *
	 * <br>
	 * The parameters imports_per_event works in the following way: if
	 * imports_per_event is smaller than 1 this means a number proportional to
	 * the population size is imported otherwise the number is imported
	 * specified by this parameter <br>
	 * e.g. imports_per_event = 0.01 and population size = 100 than 1 infection
	 * is imported if imports_per_event = 2 than 2 will be imported
	 *
	 *
	 * @throws Exception
	 */

	public void external() {
		// obtain random number generator
		Random rand = Herbarium.rand(Herbarium.INTRO);

		// Determine the number of imports (if imports_per_event is smaller than
		// 1 this means
		// a number proportional to the population size is imported
		// otherwise we import the number specified by this parameter
		int imps = 0;

		if (Idunn.imports_per_event < 1.0)
			imps = (int) (Idunn.LAYERS
					* Math.round(Odin.getOdin().getPopulationRegister().getSize() * Idunn.imports_per_event));
		else
			imps = (int) Idunn.imports_per_event;

		// prevent rounding resulting in no imports
		if (imps < 1 && Idunn.imports_per_event > 0)
			imps = 1;

		if (import_method == RANDOMIMPORT)
			this.externalRandom(imps, rand);
		// susceptible
		else if (import_method == SUSCEPTIBLEIMPORT)
			this.externalSusceptible(imps, rand);
		// certain type
		else if (import_method == TYPEIMPORT)
			this.externalType(imps, rand);
		// level size
		else if (import_method == DWELLIMPORT)
			this.externalDwelling(imps, rand);

		return;

	}

	/**
	 *
	 * @return clinic
	 */
	public Clinic getClinic() {

		return this.clinic;
	}

	/**
	 * Schedule transmission events to all members of the new Level
	 *
	 * @param source
	 */
	public void newInfectionInDwelling(Morbus source) {

		// Schedule transmission to all non-infected level-inhabitants
		// get the level inhabitants
		Set<Inhabitant> l = source.getOwner().getLevel().getInhabitants();

		// iterate the inhabitants
		for (Inhabitant p : l) {
			// get the morbus of the right layer
			Morbus vic = ((Person) p).getMorbus(source.getLayer());

			if (vic.getState() == State.SUSCEPTIBLE_STATE) {
				if (vic.getType() != Morbus.IMMUNE_TYPE)// not immune
				{
					this.scheduleDwellingTransmission(source, vic);
				}
			}
		}

	}

	/**
	 * @see simcolep.tools.message.Notifyable#notify(simcolep.tools.message.Message,
	 *      java.lang.Object, java.lang.Object)
	 */
	@Override
	@SuppressWarnings(PN.UNCHECKED)
	public void notify(Message message, Object subject, Object source) throws Exception {

		// birth
		if (message.equalsIgnoreCase(Message.BIRTH)) {
			// subject is null
			if (subject == null)
				return;

			// vaccination
			this.clinic.vaccination_at_birth((Person) subject);

		}
		// a person is added to a level
		else if (message.equalsIgnoreCase(Message.ADDINHABITANT)) {
			addedToLevel((Person) subject);
		}
		// a person is removed from a level
		else if (message.equalsIgnoreCase(Message.REMOVEINHABITANT)) {
			removedFromLevel((Person) subject, (Level) source);
		} else if (message.equals(Message.IMPORTINFECTION))// import
		{
			// import external infection
			this.external();
			// schedule a new import
			scheduleImport();
		} else if (message.equals(Message.EXTINCT)) {
			this.extinction();
		} else
			return;

	}

	/**
	 * Open a database in the clinic-register (this is different from starting
	 * recording stuff). In this way it is possible to store detailed data for a
	 * shorter period than aggregated data
	 */
	public void openDatabase() {
		try {
			this.clinic.getRegister().open();
		} catch (java.io.IOException e) {
			Main.handleException(e);
		}
	}

	/**
	 * @see simcolep.asgard.Ase#ragnarok()
	 */
	@Override
	public void ragnarok() {
		this.clinic = null;

	}

	/**
	 * Idunn keeps a list of delayed self reporters
	 *
	 * @param morbus
	 */
	public void scheduleDelayedSelfReport(Morbus morbus) {
		if (this.delayedSelfreporters == null) {
			this.delayedSelfreporters = new ArrayList<Morbus>();

			DelayedSelfReport dsf = new DelayedSelfReport(this, this, this.clinic.getStartTreatment());
			// schedule event
			Idunn.getEventManager().schedule(dsf);

		}
		/*
		 * if eligible for scheduling of self reporting aftter introduction of
		 * treatment add to list
		 */
		if (morbus.getOwner().getDeathTime() > this.clinic.getStartTreatment())
			if (morbus.getSelfhealingTime() > this.clinic.getStartTreatment())
				this.delayedSelfreporters.add(morbus);

	}

	/**
	 * Schedule passive detection for all self reporters, that were symptomatic
	 * before introduction of treatment.
	 *
	 */
	public void scheduleDelayedSelfreporters() {
		for (Morbus m : this.delayedSelfreporters) {
			m.schedulePassiveDetection(this.getClinic().getStartTreatment() + Idunn.getDetection(m));
		}

	}

	/**
	 * Schedules a transmission event at time t
	 *
	 * @param source
	 * @param victim
	 * @param t
	 */
	public void scheduleDwellingTransmission(Morbus source, Morbus victim, double t) {
		// if the victim is not susceptible return
		if (victim.getType() == Morbus.IMMUNE_TYPE)
			return;

		// if the household does not have the d-factor
		if (!victim.getOwner().getLevel().getDFactor(victim.getLayer()))
			if (!Idunn.twosusmechanisms)
				return;

		// time t is smaller than death of source
		if (t < source.getOwner().getDeathTime()) {
			// and t is smaller than the self healing time
			if (t < source.getSelfhealingTime()) {
				// and time t is smaller than death of victim
				if (t < victim.getOwner().getDeathTime()) {
					// create event
					TransmissionDwelling tl = new TransmissionDwelling(source, victim, t);

					// schedule event
					Idunn.getEventManager().schedule(tl);

					// add to the sourced events of the source
					// (sothat they can be canceled at treatment movement death)
					source.addSourcedEvent(tl);
				}
			}
		}
	}

	/**
	 * Schedules an event of transmission to the general population
	 *
	 * @param source
	 */
	@SuppressWarnings(PN.UNCHECKED)
	public void scheduleToGeneralPopulation(Morbus source) {
		// obtain random number generator
		Random rand = Herbarium.rand(Herbarium.DIS);
		// contact rate to general population gp
		double c = Archimedes.askDouble(PN.CONTACT_GP);

		// if contact rate is 0 return
		if (c <= 0.000000)
			return;

		// get the transmission time
		double t = source.next(Idunn.getCurrentTime(), c, rand);
		// if the time is larger than the selfhealing time return
		if (t > source.getSelfhealingTime())
			return;
		// if the person is dead
		if (t > source.getOwner().getDeathTime())
			return;

		// schedule an event
		// create event
		TransmissionRM trm = new TransmissionRM(source, source, t);
		// schedule
		Idunn.em.schedule(trm);
		// add to the source
		source.addSourcedEvent(trm);
	}

	/**
	 * Set the morbus object to this person
	 *
	 * @param person
	 *            for whom the morbus is going to be set
	 * @param mother
	 *            The mother
	 *
	 */
	@SuppressWarnings(PN.UNCHECKED)
	public void setMorbus(Person person, Female mother) {
		// obtain random number generator
		Random rand = Herbarium.rand(Herbarium.DIS);
		// the type variable
		Genomable genome = null;

		// set for each layer
		for (int i = 0; i < Idunn.LAYERS; i++) {
			genome = i_Model.getGenotype(mother.getMorbus(i).getGenome(), mother.getSpouseGenotype(i), rand);

			// create morbus
			Morbus m = new Morbus(genome);

			m.setLayer(i);

			// add to person
			person.setMorbus(m, i);

			// add to the clinic
			this.getClinic().getRegister().addMorbus(m);
		}
	}

	/**
	 * @see simcolep.asgard.Ase#setSave(boolean)
	 */
	@Override
	public void setSave(boolean b) {
		// set saving
		this.clinic.getRegister().storeData(b);
	}

	/**
	 * Spread the disease to a random person in the population
	 *
	 * Code to determine random mixing:
	 * <li>code 777 : Inhabitants of the own dwelling (lowest level) are not
	 * included in the mixing
	 * <li>code 888 : Members of the own level (and sublevel) are not included
	 * in the mixing
	 * <li>code 999 : Nobody excluded (PREFERRED!!)
	 *
	 * The size (or denominator) to select a person from already simulated
	 * levels is given by the virtual size of a level. If the selected level is
	 * however the highest ranking level we assume this to be the TOP level
	 * which is the population. Than the current population size is the
	 * denominator.
	 *
	 * @param source
	 * @throws Exception
	 *
	 */
	public void spread(Morbus source) throws Exception {
		// obtain random number generator
		Random rand = Herbarium.rand(Herbarium.DIS);

		// determine rank
		int rank = this.spread_rank();

		Morbus victim = null;
		// split up between methods of selection

		// if rank is top rank select through population register rather than
		// through levels
		if (rank == Odin.getOdin().getLevelRegister().getToplevel().getRank()) {
			victim = this.getVictimFromGeneralPopulation(rand, source);

		} else {
			victim = this.getVictimFromNeighbourhood(rand, source, rank);
		}

		// check the household for having the 'd-factor'
		if (victim.getOwner().getLevel().getDFactor(victim.getLayer()) || Idunn.twosusmechanisms) {
			// transmit the infection
			if (this.transmit(victim, source, rank)) {
				this.getClinic().getRegister().registerTransmission(rank);
			}
		}

	}

	/**
	 * Transmit infection from source to vic If the vic and source are dwelling
	 * members and this transmission was not succesful a new within dwelling
	 * transmission is scheduled
	 *
	 * @param vic
	 * @param source
	 */
	public boolean transmit(Morbus vic, Morbus source, int infect_at_level) {

		// if either the source is not infectious or the victim not susceptible
		// return without transmission
		if (!source.isInfectious())
			return false;

		// if the victim is not susceptible
		if (vic.getState() != State.SUSCEPTIBLE_STATE)
			return false;

		// if the victim is immune
		if (vic.getType() == Morbus.IMMUNE_TYPE)
			return false;

		// infect victim if succesfull end of this
		if (vic.infect(source, Idunn.getCurrentTime(), infect_at_level))
			return true;

		// else the transmission was not succesfull for some other reason
		// so schedule again a transmission event
		if (vic.getOwner().getLevel().equals(source.getOwner().getLevel()))
			Idunn.getIdunn().scheduleDwellingTransmission(source, vic);

		return false;
	}

	/**
	 * Removes all transmission that would occur within the level of this person
	 * that is removed
	 *
	 * @param person
	 * @param level
	 */
	protected void removedFromLevel(Person person, Level level) {

		for (int i = 0; i < Idunn.LAYERS; i++) {
			// get the Morbus
			Morbus m = person.getMorbus(i);
			// and cancel its events targeted at the level-inhabitants
			m.cancelLevel();

			if (Idunn.twosusmechanisms)
				if (level.getDFactor(i))
					if (!m.isDead)
						m = setTSMPhenotype(m, false);
		}

	}

	/**
	 * Determines the transmission time for this level/these levels and
	 * schedules a transmission event
	 *
	 * @param source
	 * @param vic
	 *
	 */
	protected void scheduleDwellingTransmission(Morbus source, Morbus vic) {

		// obtain random number generator
		Random rand = Herbarium.rand(Herbarium.DIS);

		// get the time of the next transmision for level
		double transtime = source.next(Idunn.getCurrentTime(), Archimedes.askDouble(PN.DWELLINGCONTACT), rand);

		// schedule transmission
		this.scheduleDwellingTransmission(source, vic, transtime);

	}

	/**
	 * Schedules imports of infection events
	 */
	protected void scheduleImport() {
		// do not schedule when all evenst have taken place
		if (this.import_events <= 0) {
			this.clinic.getRegister().setExtinctionMonitor(true);
			return;
		}

		// schedule at time determined by a distribution
		TimeOfImport = Archimedes.askDistribution(PN.TIMING_IMP);

		double time = 0.0;
		if (Idunn.getCurrentTime() < Idunn.time_of_firstImport)
			time = Idunn.time_of_firstImport;
		else {
			// obtain random number generator
			Random rand = Herbarium.rand(Herbarium.DIS);

			time = Idunn.getCurrentTime() + TimeOfImport.drawX(rand.nextDouble());
		}

		// schedule the import
		TransmissionImport ti = new TransmissionImport(this, this, time);

		Idunn.getEventManager().schedule(ti);

		// subtract 1 of number of events
		this.import_events--;
	}

	/**
	 *
	 * @param imps
	 * @param rand
	 */
	private void externalDwelling(int imps, Random rand) {
		// get person from population
		List<Level> l_list = new ArrayList<Level>(Odin.getOdin().getLevelRegister().getLevels(import_hhsize));

		if (l_list.isEmpty())
			return;
		/**/
		for (int i = 0; i < imps; i++) {
			Level level = l_list.get(rand.nextInt(l_list.size()));
			int layer = rand.nextInt(Idunn.LAYERS);

			if (level.getDFactor(layer) || Idunn.twosusmechanisms) {
				Morbus m = ((Person) level.getInhabitant(rand)).getMorbus(layer);

				if (m.getState() == State.SUSCEPTIBLE_STATE) {
					// infect the morbus (infection level is clear)
					m.infect(IMPORT_MARK + this.importcounter, Idunn.getCurrentTime(), Strain.IMPORTARC);
					this.importcounter++;
				}
			} else
				i = i - 1;
		}

	}

	/**
	 * Randomly import infection
	 *
	 * @param imps
	 * @param rand
	 */
	private void externalRandom(int imps, Random rand) {
		for (int i = 0; i < imps; i++) {
			Person p = Odin.getOdin().getPopulationRegister().getPerson(rand);

			// determine a layer (at random)
			int layer = rand.nextInt(Idunn.LAYERS);

			Morbus m = p.getMorbus(layer);

			boolean state = (m.getState() == State.SUSCEPTIBLE_STATE);
			if (state) {
				if (m.getOwner().getLevel().getDFactor(m.getLayer()) || Idunn.twosusmechanisms) {
					// infect the morbus (infection level is clear)
					m.infect(IMPORT_MARK + this.importcounter, Idunn.getCurrentTime(), Strain.IMPORTARC);
					this.importcounter++;
				}
			}

		}

	}

	/**
	 * Infect susceptibles
	 *
	 * @param imps
	 * @param rand
	 */
	private void externalSusceptible(int imps, Random rand) {
		// if there are less susceptibles than imports infect all
		if (Idunn.getIdunn().getClinic().getRegister().getSusceptibles() < imps) {
			Idunn.getIdunn().infectall();
			return;
		}
		// same as random but only with succesfull import count
		for (int i = 0; i < imps;) {
			Person p = Odin.getOdin().getPopulationRegister().getPerson(rand);

			// determine a layer (at random)
			int layer = rand.nextInt(Idunn.LAYERS);

			Morbus m = p.getMorbus(layer);

			if (m.getState() == State.SUSCEPTIBLE_STATE) {
				if (m.getOwner().getLevel().getDFactor(m.getLayer()) || Idunn.twosusmechanisms) {
					// infect the morbus (infection level is clear)

					m.infect(IMPORT_MARK + i, Idunn.getCurrentTime(), Strain.IMPORTARC);
					// add 0one
					i++;
				}
			}

		}

	}

	/**
	 * Import infections in a certain type of leprosy
	 *
	 * @param imps
	 * @param rand
	 */
	private void externalType(int imps, Random rand) {
		// if there are less susceptibles than imports infect all
		if (Idunn.getIdunn().getClinic().getRegister().getType(import_type) < imps) {
			Idunn.getIdunn().infectall();
			return;
		}
		// else but only when having the right type
		Set<Morbus> victims = new LinkedHashSet<Morbus>();
		// select those to import
		int safety_counter = 0;
		while (victims.size() < imps) {

			List<Morbus> type_list = new ArrayList<Morbus>(this.clinic.getRegister().getMorbiOfType(import_type));

			Morbus m = type_list.get(rand.nextInt(type_list.size()));

			if (m.getState() == State.SUSCEPTIBLE_STATE) {
				if (m.getOwner().getLevel().getDFactor(m.getLayer()) || Idunn.twosusmechanisms)
					victims.add(m);

			}
			safety_counter++;

			if (safety_counter == 2 * type_list.size()) {
				SimulationLog
						.log("At time: " + Idunn.getCurrentTime() + NOIMPORTMESSAGE + AFTERROUNDMESSAGE + safety_counter
								+ " tries " + PN.TAB + " in a list of size: " + type_list.size() + PN.LINESEPARATOR);
				break;
			}
		}

		// infect all victims
		for (Morbus m : victims) {
			// infect the morbus (infection level is clear)
			m.infect(IMPORT_MARK + this.importcounter, Idunn.getCurrentTime(), Strain.IMPORTARC);
			this.importcounter++;
		}
	}

	/**
	 * Deal with infeciton extinction
	 *
	 */
	private void extinction() {
		if (this.import_events > 0)
			return;
		else
			// stop simulation
			Ratatosk.notify(Message.formulate(Message.STOP), Message.EXTINCT, this);

	}

	/**
	 * Get the level that is shared by this source and has rank
	 *
	 * @param source
	 * @param rank
	 * @return shared_level
	 */
	private Level getSharedLevel(Morbus source, int rank) {
		Level shared_level = source.getOwner().getLevel();

		for (int x = 0; x < rank; x++)
			shared_level = (Level) shared_level.getSuperlevel();

		return shared_level;

	}

	/**
	 *
	 * @param source
	 * @return
	 */
	@SuppressWarnings("unchecked")
	private Morbus getVictimFromGeneralPopulation(Random rand, Morbus source) {

		/*
		 * This is a selection method that determines the victim using
		 * information on population size, but only drawing one random index
		 */
		int layersize = Odin.getOdin().getPopulationRegister().getSize();

		if (layersize == 0)
			return null;
		// number of layers
		int layers = Idunn.LAYERS;

		int index = rand.nextInt(layersize * layers - 1);

		int sourcelayer = source.getLayer();

		int viclayer = (index - index % layersize) / layersize;

		if (viclayer == sourcelayer) {
			if (index % layersize == layersize - 1)
				viclayer += 1; // add one
			else {
				// select theperson
				return Odin.getOdin().getPopulationRegister().getPerson(index % layersize).getMorbus(viclayer);
			}
		}

		if (viclayer > sourcelayer) {
			index += 1;
		}

		return Odin.getOdin().getPopulationRegister().getPerson(index % layersize).getMorbus(viclayer);
	}

	/**
	 *
	 * @param rand
	 * @param source
	 * @param rank
	 * @return victim
	 */
	private Morbus getVictimFromNeighbourhood(Random rand, Morbus source, int rank) {

		// get shared level
		Level shared_level = this.getSharedLevel(source, rank);

		// if it is the highest use this faster method
		if (shared_level.isHighest())
			return this.getVictimFromGeneralPopulation(rand, source);

		// temporarily remove source from level
		Level source_level = source.getOwner().getLevel();

		source_level.removeInhabitantWithoutNotification(source.getOwner());

		// determine the index of the victim
		int vic_index = rand.nextInt(shared_level.getHabitationSize());

		// iterate the levels until you find the index
		// get the random person from this 'super'level
		Person victim = (Person) shared_level.getInhabitant(vic_index);

		// add source to
		source_level.addInhabitantWithoutNotification(source.getOwner());

		return victim.getMorbus(source.getLayer());
	}

	/**
	 * Try to infect all on the basis of the import method. This means that all
	 * susceptibles are infected for import_method == 1 and all of import_type
	 * for import_method == 2
	 *
	 */
	private void infectall() {

		double time = getCurrentTime();

		for (Object m : this.clinic.getRegister().getRegister()) {
			Morbus morbus = (Morbus) m;

			boolean infect = true;
			if (import_method == 2) {
				infect = (morbus.getType() == Idunn.import_type);
			}
			if (infect) {
				morbus.infect(IMPORT_MARK + this.importcounter, time, Strain.IMPORTARC);
				this.importcounter++;
			}
		}

	}

	/**
	 * Sets the phenotype accordingly to leaving or entering a susceptible
	 * household. Genetic susceptibility overrules any other setting
	 *
	 * @param m
	 * @param enter
	 *            true -> added to household, false -> removed from household
	 * @return
	 */
	private Morbus setTSMPhenotype(Morbus m, boolean enter) {
		// entering a household
		if (enter) {
			if (m.getState() == State.NEVER_SUSCEPTIBLE_STATE)// not genetically
																// susceptible
			{
				// determine the new type
				Distributionable frac = Archimedes.askDistribution(PN.TSMDIST);
				double type = frac.drawX(Herbarium.rand(Herbarium.DIS).nextDouble());

				m.changeType((int) type);
			}
		}
		// removed from the household
		else {
			if (m.getState() == State.SUSCEPTIBLE_STATE) {
				m.changeType(m.getGenome().getPhenotype());
			}
		}

		return m;
	}

	/**
	 * Draw the rank to which the infection will spread
	 *
	 * @param source
	 * @return rank
	 */
	private int spread_rank() {
		// obtain random number generator
		Random rand = Herbarium.rand(Herbarium.DIS);

		int rank = 0;

		if (Archimedes.askInteger(PN.LEVELDEPTH) == Level.DWELLING_RANK)
			rank = 1;
		else {
			// random number
			double rand_neighbourhood = rand.nextDouble();

			// start at a rank lower than the lowest possible ranking
			rank = (int) Archimedes.ask(PN.RANKCONTACT).getY(rand_neighbourhood);
		}
		return rank;
	}

}
