/*
 * This file is part of SIMCOLEP, a simulation model developed by the Department of Public Health,
 * Erasmus MC, University Medical Center Rotterdam, Rotterdam, The Netherlands.
 *
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/
 * or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA
 *
 * You are free to:
 * Share � copy and redistribute the material in any medium or format
 * Under the following terms:
 * Attribution � You must give appropriate credit, provide a link to the license, and indicate if changes were
 * made. You may do so in any reasonable manner, but not in any way that suggests the licensor endorses
 * you or your use.
 * NonCommercial � You may not use the material for commercial purposes.
 * NoDerivatives � If you remix, transform, or build upon the material, you may not distribute the modified material.
 */

/*
 * Created on 04-Nov-2005
 *
 */
package simcolep.asgard;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.Stack;

import nl.mgz.simulation.discreteevent.Schedulable;
import nl.mgz.simulation.discreteevent.Target;
import simcolep.demo.AbstractPopulationRegister;
import simcolep.demo.Female;
import simcolep.demo.Male;
import simcolep.demo.Person;
import simcolep.demo.Sex;
import simcolep.demo.event.Birth;
import simcolep.demo.event.BirthScheduling;
import simcolep.demo.event.SplitLevel;
import simcolep.demo.event.Wedding;
import simcolep.exceptions.NotInRegisterException;
import simcolep.exceptions.SimulationException;
import simcolep.fun2.Archimedes;
import simcolep.fun2.Function;
import simcolep.fun2.Pythagoras;
import simcolep.sim.Gylfi;
import simcolep.sim.Herbarium;
import simcolep.tools.PN;
import simcolep.tools.io.OutputElement;
import simcolep.tools.io.OutputList;
import simcolep.tools.io.SimulationLog;
import simcolep.tools.message.Message;
import simcolep.tools.message.Notifyable;
import simcolep.tools.message.Ratatosk;

/**
 * <i>Freya</i>, the godess of fertility, love (and lust/sex). She is not an Ase
 * (she is a Wane), but lives with the Asen at Asgard. In her halls she keeps
 * half of the man that are killed in battle (the other half is for Odin). She
 * is a warrior-godess. <br>
 * <code>Freya</code> is responsible for marriage and birth. She therefore keeps
 * a list of singles (two males and females) and a list of married
 * women(potential mothers)
 *
 *
 * @author ir E.A.J. Fischer
 * @version 1.0
 *
 */
public final class Freya extends Target implements Ase, Notifyable, Serializable {

	private static final String FREYA_TEXT = "Freya "; //$NON-NLS-1$

	private static final String WEDDING = "wedding"; //$NON-NLS-1$

	private static final String FEMALE = "female"; //$NON-NLS-1$

	private static final String SINGLE = "single"; //$NON-NLS-1$

	private static final String WEDDED = "wed"; //$NON-NLS-1$

	private static final String MALE = "male"; //$NON-NLS-1$

	private static final String MOTHER = "mother"; //$NON-NLS-1$

	private static final String BIRTH = "birth"; //$NON-NLS-1$

	private static final String CAT = "cat"; //$NON-NLS-1$

	/** serial number */
	private static final long serialVersionUID = -7797519120378762342L;

	/** listensTo death, change of age class */
	protected static String[] listensTo = { Message.DEATH, Message.AGE, Message.WEDDING, Message.WOE, Message.BIRTH,
			Message.SPOUSEDEATH, Message.ACTIVATE, Message.CONCEPTIONS };

	/** stepsize */
	protected static double stepsize = 1.0;

	/** population growth function */
	private static Function pop_size = null;

	/** weighing function for attribution of birth to mother */
	protected static Function w_birth = null;

	/** ratio of male/(male+female) default = 0.5 */
	public static double sexratio = 0.5;

	/**
	 * Change function that Freya uses.
	 *
	 *
	 * @param f
	 * @param s
	 *            - These are population growth (pop_size) and age specific
	 *            birth rates (w_birth)
	 */
	public static void changeFunction(Function f, String s) {
		if (s.equalsIgnoreCase(PN.POPSIZE)) {
			pop_size = f;

		} else if (s.equalsIgnoreCase(PN.BIRTH_WEIGTH)) {
			w_birth = f;
		}

	}

	/**
	 * Set's the stepsize for weddings and births
	 *
	 * @param dt
	 */
	public static void setStepsize(double dt) {
		stepsize = dt;
	}

	/** save events */
	private boolean saveevents = false;

	/** number of births */
	private transient List<Double> births = null;

	/** output list birth */
	protected transient List<OutputList> birthout = null;

	/** output list mothers */
	protected transient List<OutputList> motherout = null;

	/** output lists weddings */
	protected transient List<OutputList>[] wedout = null;

	/** output lists singles */
	protected transient List<OutputList>[] singleout = null;

	/** count of weddings */
	private transient List<Double>[] weddingpics = null;

	/** Odin of this population */
	protected transient Odin odin = null;

	/** single males */
	protected List<Set<Person>> singleMen = null;

	/** single females */
	protected List<Set<Person>> singleWomen = null;

	/** mothers */
	protected List<Set<Person>> mothers = null;

	/** mother counter */
	private int[] mothercount = null;

	private int motherTotal = 0;

	/** weddinglist */
	protected transient Stack<Person[]> weddings = null;

	/** interval between the weddings in the Stack */
	private transient double wedding_interval = 1;

	/**
	 *
	 * @param odin
	 */
	@SuppressWarnings(PN.UNCHECKED)
	public Freya(Odin odin) {

		super();

		initialize(odin);
	}

	/**
	 * Add a person to either the singles or mother list
	 *
	 * @param p
	 */
	public void add(Person p) {
		if (!p.ismarried()) {
			// add if this person was not added before
			if (!this.hasSingle(p))
				this.addSingle(p);
		} else if (p.getSex() == Sex.FEMALE) {
			// add if this person was not added before
			if (!this.hasMother(p))
				this.addMother(p);
		}

	}

	/**
	 * Add a female to the list of mothers
	 *
	 * @param female
	 */
	@SuppressWarnings(PN.UNCHECKED)
	public void addMother(Person female) {

		if (female.getSex() != Sex.FEMALE)
			throw new SimulationException("Male is not a mother");

		// add an age class if not present anymore
		while (this.mothers.size() < female.getAgeclass() + 1) {
			this.mothers.add(new LinkedHashSet<Person>());
			this.motherout.add(OutputList.newOutputList(this.motherout.get(0).size(), MOTHER + this.mothers.size()));

		}
		// increase size of the mother counter
		if (female.getAgeclass() + 1 > this.mothercount.length) {
			int[] newCounter = new int[female.getAgeclass() + 1];
			for (int i = 0; i < this.mothercount.length; i++) {
				newCounter[i] = this.mothercount[i];
			}
			this.mothercount = newCounter;
		}

		this.mothercount[female.getAgeclass()]++;
		this.motherTotal++;
		// add to the list of age class
		this.mothers.get(female.getAgeclass()).add(female);

	}

	/**
	 * Adds a person to the right singles list
	 *
	 * @param p
	 */
	public void addSingle(Person p) {

		// don't allow married people to be added
		if (p.ismarried())
			throw new SimulationException("not allowed to add a married person as single!");

		// Add to the correct list
		if (p.getSex() == Sex.MALE) {
			if (!this.singleMen.get(p.getAgeclass()).add(p))
				throw new SimulationException(p + " was not added to singles");

		} else if (p.getSex() == Sex.FEMALE) {
			if (!this.singleWomen.get(p.getAgeclass()).add(p))
				throw new SimulationException(p + " was not added to singles");

		} else
			throw new SimulationException(SimulationException.NOSEX);

		p.add_single = true;
	}

	/**
	 * A birth will take place in level of mother
	 *
	 * @param mother
	 *
	 */
	@SuppressWarnings(PN.UNCHECKED)
	public Person birth(Person baby, Person objmother) {

		// else go on
		Female mother = (Female) objmother;

		// check
		if (Double.isInfinite(baby.getDeathAge()))
			throw new SimulationException(SimulationException.NODEATHSET);

		// set the birth date
		baby.setBdate(Odin.getCurrentTime());

		// schedule the death event (age was determined at conception to allow
		// for corrections)
		odin.getHel().schedule(baby, baby.getDeathTime());

		// set identification number
		baby.setID(Ymer.getID());

		// set disease object
		Idunn.getIdunn().setMorbus(baby, mother);

		// add mother to child
		baby.setParents((Male) mother.getSpouse(), mother);

		// add the child as spouse
		for (Person sib : mother.getChildren()) {
			sib.addSibling(baby);
			baby.addSibling(sib);
		}

		// add as child
		mother.addChild(baby);

		/*
		 * a widow can still get a child that was conceived when the spouse was
		 * still alive (stepsize for birth should be &lt = 9 months)
		 */
		if (mother.getSpouse() != null)
			mother.getSpouse().addChild(baby);

		// schedule movement and migration
		double mage = odin.getLoki().schedule(baby);
		baby.setMoveAge(mage);

		// add to the population
		Odin.getOdin().getPopulationRegister().addPerson(baby);

		// add to the level
		mother.getLevel().addInhabitant(baby);

		// add to singles lists
		this.addSingle(baby);

		// save the birth event
		int[] a = new int[1];
		a[0] = mother.getAgeclass();

		this.saveEvent(Odin.getCurrentTime(), a, Message.BIRTH);

		/// arranging some stuff for the parents now//
		Person survivor = null;
		if (mother.survivor)
			survivor = mother;
		else
			survivor = mother.getSpouse();

		// only add a child etceteras if the survivor will move when the spouse
		// dies
		if (survivor.movesAsWidow()) {
			// if the child doesnot die before the spouse it is in the race for
			// being selected
			boolean longetivityOK = baby.getDeathTime() - survivor.getSpouseDeathTime() > Pythagoras
					.getDefaultPrecision();

			if (longetivityOK) {
				// it has the probability of 1 divided by the number of up to
				// now eligible children.
				if (Herbarium.rand(Herbarium.POP).nextDouble() <= 1 / (survivor.getChildren().size())) {
					// set chosen by parent
					baby.setChosenByParent(true);

					// set the surviving parent
					baby.setsurvivingParent(survivor);

					// if another child was previously chosen
					if (survivor.gettheChosen() != null) {

						// set no longer chosen
						survivor.gettheChosen().setChosenByParent(false);

						// remove the link with the surviving parent
						survivor.gettheChosen().setsurvivingParent(null);

					}
					// set this child to be chosen
					survivor.settheChosen(baby);
				} // else nothing happens
			}
		}
		// return the newly born person
		return baby;
	}

	/**
	 *
	 * @param random
	 * @param sex
	 *            - sex
	 * @param a
	 *            - ageclass
	 * @return random single
	 */
	public Person getSingle(Random random, Sex sex, int a) {
		if (sex == Sex.FEMALE)// female
		{
			return this.select(this.singleWomen.get(a), random);
		} else if (sex == Sex.MALE)// male
		{
			return this.select(this.singleMen.get(a), random);
		}
		return null;
	}

	/**
	 * Single females are grouped by age in a list with linkedhashsets
	 *
	 * @return single females list
	 */
	public List<Set<Person>> getSingleFemales() {
		return this.singleWomen;
	}

	/**
	 * Single males are grouped by age in a list with linkedhashsets
	 *
	 * @return single males list
	 */
	public List<Set<Person>> getSingleMales() {
		return this.singleMen;
	}

	/**
	 * Determine if this female is registered as mother
	 *
	 * @param f
	 * @return true if this female is in the list of mothers
	 */
	public boolean hasMother(Person f) {
		return mothers.get(f.getAgeclass()).contains(f);
	}

	/**
	 *
	 * @param p
	 * @return true if registered as single
	 */
	public boolean hasSingle(Person p) {
		if (p.getSex() == Sex.FEMALE) {
			return this.singleWomen.get(p.getAgeclass()).contains(p);
		} else if (p.getSex() == Sex.MALE) {
			return this.singleMen.get(p.getAgeclass()).contains(p);
		} else
			return false;

	}

	/**
	 * listens to notifications for this population:<br>
	 *
	 *
	 * protected static String[] listensTo = {"d", "cac", "w", "b", "sd", "a"};
	 *
	 * @param message
	 * @param subject
	 * @param source
	 * @throws Exception
	 * @see simcolep.tools.message.Notifyable#notify(simcolep.tools.message.Message,
	 *      java.lang.Object, java.lang.Object)
	 */
	@Override
	@SuppressWarnings(PN.UNCHECKED)
	public void notify(Message message, Object subject, Object source) throws Exception {
		// DEATH
		if (message.equals(Message.DEATH)) {
			// the dead person
			Person deceased = (Person) subject;

			// if single remove from the singles lists
			if (!deceased.ismarried()) {
				this.removeSingle(deceased);
			}
			// if a woman
			if (deceased.getSex() == Sex.FEMALE)
				this.removeMother(deceased);

			// return
			return;

		}

		// change age class
		else if (message.equals(Message.AGE)) {

			this.nextAgeClass((Person) subject);
		}

		// wedding party!
		else if (message.equalsIgnoreCase(Message.WEDDING)) {
			this.wedding();
		}

		// CONCEPTION or scheduling of births
		else if (message.equalsIgnoreCase(Message.CONCEPTIONS)) {
			this.scheduleBirth();
		}

		// START OF A BIRTH
		else if (message.equalsIgnoreCase(Message.WOE)) {

			// birth by Freya
			Person baby = Odin.getOdin().getFreya().birth((Person) subject, (Person) source);
			// execute a birth
			Ratatosk.notify(Message.formulate(Message.BIRTH), baby, this);

		}

		// BIRTH
		else if (message.equalsIgnoreCase(Message.BIRTH)) {
			this.add((Person) subject);
		}

		// spouse death
		else if (message.equalsIgnoreCase(Message.SPOUSEDEATH)) {
			// get the person
			Person w = (Person) subject;

			// if a widow remove as mother
			if (w.getSex() == Sex.FEMALE)
				this.removeMother(w);

			// if not already taken care of add to singles list
			if (!this.hasSingle(w))
				this.addSingle(w);

			// cancel any split level events
			if (w.isTargetOf(SplitLevel.class.getName())) {
				List<Schedulable> temp = new ArrayList<Schedulable>();
				for (Schedulable s : w.getTargetedEvents(SplitLevel.class.getName())) {
					temp.add(s);
				}
				for (Schedulable s : temp) {
					Odin.getEventManager().cancel(s);
				}
			}

		}
		// activated person
		else if (message.equalsIgnoreCase(Message.ACTIVATE))// activate
		{
			// method add will add this person to the proper lists
			Person p = (Person) subject;
			this.add(p);
		} else
			throw new SimulationException("unknown message: " + message);

	}

	/**
	 * Write the outputlists to file
	 *
	 * @throws IOException
	 *
	 */

	@SuppressWarnings(PN.UNCHECKED)
	public void output(boolean onefile) throws IOException {

		if (!saveevents)
			return;

		// separate file for each list
		if (onefile) {
			// print all to the same file
			OutputList.toFile(this.birthout, BIRTH);
			OutputList.toFile(this.motherout, MOTHER);
			OutputList.toFile(this.wedout[Sex.MALE.getValue()], WEDDED + File.separator + MALE);
			OutputList.toFile(this.wedout[Sex.FEMALE.getValue()], WEDDED + File.separator + FEMALE);
			OutputList.toFile(this.singleout[Sex.MALE.getValue()], SINGLE + File.separator + MALE);
			OutputList.toFile(this.singleout[Sex.FEMALE.getValue()], SINGLE + File.separator + FEMALE);

		} else {
			for (int x = 0; x < birthout.size(); x++) {
				this.birthout.get(x).toFile(BIRTH);
				this.motherout.get(x).toFile(MOTHER);
				this.wedout[Sex.MALE.getValue()].get(x).toFile(WEDDED + File.separator + MALE);
				this.wedout[Sex.FEMALE.getValue()].get(x).toFile(WEDDED + File.separator + FEMALE);
				this.singleout[Sex.MALE.getValue()].get(x).toFile(SINGLE + File.separator + MALE);
				this.singleout[Sex.FEMALE.getValue()].get(x).toFile(SINGLE + File.separator + FEMALE);
			}
		}

	}

	/**
	 * Return a specific outputlist
	 *
	 * @param s
	 * @param sex
	 * @return outputlist
	 */
	public List<OutputList> output(String s, int sex) {
		if (s.equalsIgnoreCase(BIRTH)) {
			return this.birthout;
		} else if (s.equalsIgnoreCase(MOTHER)) {
			return this.motherout;
		} else if (s.equalsIgnoreCase(SINGLE)) {
			return this.singleout[sex];
		} else if (s.equalsIgnoreCase(WEDDING)) {
			return this.wedout[sex];
		} else
			throw new SimulationException("no such output: " + s);
	}

	/**
	 *
	 *
	 */
	@Override
	public void ragnarok() {
		// cut aliance with odin
		this.odin = null;

	}

	/**
	 * Remove this Person from any of the lists in Freya's memory
	 *
	 * @param p
	 */
	public void removePerson(Person p) {

		// lists of singles
		if (!p.ismarried())
			this.removeSingleUnchecked(p);

		// women
		if (p.getSex() == Sex.FEMALE)
			if (p.ismarried())
				this.removeMother(p);

	}

	/**
	 * Unchecked removal means that if no removal took place no exception is
	 * thrown
	 *
	 * @param p
	 * @return true if the single was present in the list
	 */
	public boolean removeSingleUnchecked(Person p) {

		// get person
		if (p.getSex() == Sex.MALE) {

			return this.singleMen.get(p.getAgeclass()).remove(p);
		} else if (p.getSex() == Sex.FEMALE) {

			return this.singleWomen.get(p.getAgeclass()).remove(p);

		} else
			throw new SimulationException(SimulationException.NOSEX);
	}

	/**
	 * Write all events that were registered since last time to outputlists
	 *
	 *
	 * @param time
	 */
	public void save(double time) {

		// if saveevent is not set 'on' just return
		if (saveevents == false)
			return;
		// Write the data to outputlists
		// births
		for (int x = 0; x < this.births.size(); x++) {

			Double i = this.births.get(x);
			OutputElement<Double> e = new OutputElement<Double>(time, i);
			birthout.get(x).add(e);
			// set value to zero
			i = new Double(0);
			this.births.set(x, i);

		}

		// weddings
		for (Sex sex : Sex.values()) {
			if (sex != Sex.BOTH) {
				// number of weddings since last output
				for (int x = 0; x < this.weddingpics[sex.getValue()].size(); x++) {

					if (x >= wedout[sex.getValue()].size()) {
						for (int i = this.wedout[sex.getValue()].size(); i <= x; i++) {
							OutputList<Double> nextOlist = new OutputList<Double>(sex + CAT + i);
							for (OutputElement<Double> oe : (OutputList<Double>) this.wedout[sex.getValue()].get(0)) {
								nextOlist.add(new OutputElement<Double>(oe.getTime(), 0.0));
							}

							this.wedout[sex.getValue()].add(nextOlist);
						}
						this.wedout[sex.getValue()].get(x).remove(this.wedout[sex.getValue()].get(x).getLast());
					}
					Double i = this.weddingpics[sex.getValue()].get(x);

					OutputElement<Double> e = new OutputElement<Double>(time, i);

					wedout[sex.getValue()].get(x).add(e);
					// set value to zero
					i = new Double(0);
					this.weddingpics[sex.getValue()].set(x, i);

				}
			}
		}

		// add size
		while (this.motherout.size() < this.mothers.size()) {
			this.motherout
					.add(OutputList.newOutputList(this.motherout.get(0).size(), MOTHER + this.motherout.size() + 1));
		}
		// current number of mothers
		for (int x = 0; x < this.mothers.size(); x++) {

			OutputElement<Double> e = new OutputElement<Double>(time, new Double(this.mothercount[x]));
			this.motherout.get(x).add(e);

		}

		// current number of singles
		for (Sex sex : Sex.values()) {
			if (sex != Sex.BOTH) {
				for (int x = 0; x < this.singleout[sex.getValue()].size(); x++) {

					OutputElement<Double> e = null;
					if (sex == Sex.MALE)
						e = new OutputElement<Double>(time, new Double(this.singleMen.get(x).size()));
					else if (sex == Sex.FEMALE)
						e = new OutputElement<Double>(time, new Double(this.singleWomen.get(x).size()));
					this.singleout[sex.getValue()].get(x).add(e);

				}
			}
		}

	}

	/**
	 * Save the events that come through here
	 *
	 * @param b
	 */
	@Override
	public void setSave(boolean b) {
		// switch on/off saveing
		this.saveevents = b;

	}

	/**
	 *
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return FREYA_TEXT + weddings.size();
	}

	/**
	 * Arranges marriages between available singles
	 *
	 * @throws Exception
	 *
	 */
	@SuppressWarnings(PN.UNCHECKED)
	protected void arrangeMarriages(Random rand) {
		// determine wheter or not to check for family relations
		boolean checkfamily = Gylfi.ask(PN.PREVENTINBREEDING);

		// create a list of male marriage candidates
		// calculate how much males per age class in the next time step
		this.weddings = new Stack<Person[]>();

		// number of age classes
		int classes = Double.valueOf(AbstractPopulationRegister.ageclassmax / AbstractPopulationRegister.ageclasswidth)
				.intValue() - 1;

		// list with male candidates
		Set<Person> mcan = new LinkedHashSet<Person>();

		// create a list of male candidates
		for (int x = 0; x < classes; x++) {

			// determine how many single males are present in this age class
			List<Person> smale = new ArrayList<Person>(singleMen.get(x));

			int Obs = smale.size();

			// determine the number of singles that should be present
			// total number of males in this class is
			int Total = odin.getPopulationRegister().getPersons(x * AbstractPopulationRegister.ageclasswidth,
					(x + 1) * AbstractPopulationRegister.ageclasswidth, Sex.MALE).size();

			// expected fraction of singles is
			double f = 1 - (Archimedes.ask(PN.MARRIED_MALES).getY(0.5 * (x * AbstractPopulationRegister.ageclasswidth
					+ (x + 1) * AbstractPopulationRegister.ageclasswidth)));
			// expected number of singles is
			int Exp = new Double(f * Total).intValue();

			// determine the number of males to marry during the coming period
			int numToMarry = Obs - Exp;
			// add the single males to a list

			// create a list of male candidates
			/// candidates can be picked twice as mcan is a set this will result
			// in less marriage than anticipated
			while (numToMarry > 0 && !smale.isEmpty()) {
				Person can = this.select(smale, rand);

				mcan.add(can);

				numToMarry--;
			}

		}
		// create a list of female marriage candidates
		// list with male candidates
		Set<Person> fcan = new LinkedHashSet<Person>();

		// create a list of male candidates
		for (int x = 0; x < classes; x++) {
			// determine how manu sinfgle males are present in this age class
			List<Person> sfem = new ArrayList<Person>(singleWomen.get(x));

			int Obs = sfem.size();

			// determine the number of singles that should be present
			// total number of males in this class is
			int Total = odin.getPopulationRegister().getPersons(x * AbstractPopulationRegister.ageclasswidth,
					(x + 1) * AbstractPopulationRegister.ageclasswidth, Sex.FEMALE).size();
			// expected fraction of singles is
			double f = 1 - (Archimedes.ask(PN.MARRIED_FEMALES).getY(0.5 * (x * AbstractPopulationRegister.ageclasswidth
					+ (x + 1) * AbstractPopulationRegister.ageclasswidth)));

			// expected number of singles is
			int Exp = new Double(f * Total).intValue();

			// determine the number of males to marry during the coming period
			int numToMarry = Obs - Exp;
			// add the single males to a list

			// create a list of male candidates
			/// candidates can be picked twice as mcan is a set this will result
			// in less marriage than anticipated
			int rescue = 0;
			while (numToMarry > 0 && !sfem.isEmpty() && rescue < sfem.size() * 2) {
				Female can = (Female) this.select(sfem, rand);

				fcan.add(can);

				numToMarry--;
			}
		}

		// couple males and females from both sets
		// Check survival upto the next time step
		// This can be changed in newer versions

		List<Person> fcanlist = new ArrayList<Person>(fcan);

		for (Person groom : mcan) {
			Person[] couple = new Person[2];

			if (fcanlist.size() == 0)
				break;

			if (groom != null) {
				Person wife = this.select(fcanlist, rand);

				if (wife != null) {
					int rescue = 0;

					while (wife.getLevel().equals(groom.getLevel())) {

						// be sure that this won't end up in an endless loop
						if (rescue >= 2 * fcan.size()) {
							wife = null;
							break;
						}

						wife = this.select(fcanlist, rand);

						rescue++;

					}
					// check whether a man and a wife are found
					if (groom != null && wife != null) {
						double deathTimeGroom = groom.getDeathTime();
						double deathTimeBride = wife.getDeathTime();
						double nextStep = Odin.getCurrentTime() + stepsize;
						// check whether bride and groom are not related!
						boolean allow = true;

						if (checkfamily) {
							int relation = groom.getRelationship(wife);
							if (relation != Person.UNRELATED && relation != Person.SPOUSE)
								allow = false;
						}

						if (allow) {
							if ((deathTimeGroom > nextStep && deathTimeBride > nextStep)) {
								// remove female from singles list
								this.removeSingle(wife);
								// remove bride from candidate list
								fcanlist.remove(wife);
								// remove male from singles list
								this.removeSingle(groom);

								couple[0] = groom;
								couple[1] = wife;

								groom.engaged = true;
								wife.engaged = true;
								// put the couples in the list of weddings
								this.weddings.add(couple);
							}
						}
					}
				}
			}
		}

		// calculate the interval between weddings
		this.wedding_interval = stepsize / this.weddings.size();
		// with a maximum of the stepsize
		if (this.wedding_interval > stepsize)
			this.wedding_interval = stepsize;

	}

	/**
	 * Initialize method initializes Freya
	 *
	 * @param odin
	 */
	@SuppressWarnings(PN.UNCHECKED)
	protected void initialize(Odin odin) {

		this.odin = odin;

		// create all these lists
		this.singleMen = new ArrayList<Set<Person>>();
		this.singleWomen = new ArrayList<Set<Person>>();
		this.mothers = new ArrayList<Set<Person>>();
		this.births = new ArrayList<Double>();

		this.weddingpics = new ArrayList[2];
		this.weddingpics[Sex.MALE.getValue()] = new ArrayList<Double>();
		this.weddingpics[Sex.FEMALE.getValue()] = new ArrayList<Double>();

		this.birthout = new ArrayList<OutputList>();

		this.motherout = new ArrayList<OutputList>();

		this.wedout = new ArrayList[2];
		this.wedout[Sex.MALE.getValue()] = new ArrayList<OutputList>();
		this.wedout[Sex.FEMALE.getValue()] = new ArrayList<OutputList>();

		this.singleout = new ArrayList[2];
		this.singleout[Sex.MALE.getValue()] = new ArrayList<OutputList>();
		this.singleout[Sex.FEMALE.getValue()] = new ArrayList<OutputList>();

		// instantiate the list with sets of age classes
		for (long x = 0; x < AbstractPopulationRegister.ageclassmax; x += AbstractPopulationRegister.ageclasswidth) {
			// single males
			this.singleMen.add(new LinkedHashSet<Person>());
			// single women
			this.singleWomen.add(new LinkedHashSet<Person>());
			// mothers
			this.mothers.add(new LinkedHashSet<Person>());
			// births
			this.births.add(new Double(0));
			// wedding lists output
			for (int sex = 0; sex < 2; sex++) {
				this.weddingpics[sex].add(new Double(0));
				this.singleout[sex].add(new OutputList<Double>(sex + CAT + x));
				this.wedout[sex].add(new OutputList<Double>(sex + CAT + x));
			}
			// output
			this.birthout.add(new OutputList<Double>(BIRTH + x));
			this.motherout.add(new OutputList<Double>(MOTHER + x));

		}
		// set mother counter
		this.mothercount = new int[this.mothers.size()];

		// instantiate wedding list
		this.weddings = new Stack<Person[]>();

		// register at Ratatosk
		for (String s : listensTo) {
			Ratatosk.register(Message.formulate(s), this);
		}

		// functions and parameter
		sexratio = Archimedes.askDouble(PN.SEXRATIO);
		w_birth = Archimedes.ask(PN.BIRTH_WEIGTH);
		pop_size = Archimedes.ask(PN.POPSIZE);
	}

	/**
	 * Marry these two people
	 *
	 * @param c
	 * @throws IOException
	 */
	@SuppressWarnings(PN.UNCHECKED)
	protected void marry(Person[] c) {
		// add the female to the mothers list
		this.addMother(c[Sex.FEMALE.getValue()]);

		// add spouse to male
		((Male) c[Sex.MALE.getValue()]).addSpouse(c[Sex.FEMALE.getValue()]);

		// set married
		c[Sex.MALE.getValue()].setMarried(true);

		// add spouse to female
		((Female) c[Sex.FEMALE.getValue()]).addSpouse(c[Sex.MALE.getValue()]);

		// set married
		c[Sex.FEMALE.getValue()].setMarried(true);

		// schedule the death of the spouse as an event to a person
		int d = -1;
		int w = -1;
		if (c[Sex.MALE.getValue()].getDeathTime() > c[Sex.FEMALE.getValue()].getDeathTime()) {
			d = Sex.FEMALE.getValue();
			w = Sex.MALE.getValue();
		} else {
			d = Sex.MALE.getValue();
			w = Sex.FEMALE.getValue();
		}

		// set spouses death time to the widow(er)
		double spouse_death = c[d].getDeathTime();
		c[w].setSpouseDeathTime(spouse_death);

		// let loki handle movement during wedding
		odin.getLoki().movewedding(c);

		// save events
		int[] age = new int[2];
		age[Sex.MALE.getValue()] = c[Sex.MALE.getValue()].getAgeclass();
		age[Sex.FEMALE.getValue()] = c[Sex.FEMALE.getValue()].getAgeclass();
		this.saveEvent(Odin.getCurrentTime(), age, Message.WEDDING);

	}

	/**
	 * Remove a woman from the list of mothers and subtract one from counter
	 *
	 * @param p
	 * @return true if the mother is removed
	 */
	protected boolean removeMother(Person p) {
		if (p.getSex() != Sex.FEMALE)
			throw new SimulationException("Male is not a mother");

		if (p.ismarried()) {
			this.mothercount[p.getAgeclass()]--;
			this.motherTotal--;
		}

		return this.mothers.get(p.getAgeclass()).remove(p);
	}

	/**
	 * removes a person from the singles list
	 *
	 * @param p
	 * @throws NotInRegisterException
	 */
	protected void removeSingle(Person p) throws NotInRegisterException {

		// return if person is married
		if (p.ismarried())
			return;

		// remove singles from appropriate list
		if (p.getSex() == Sex.MALE) {

			if (!this.singleMen.get(p.getAgeclass()).remove(p)) {
				p.ismarried();
				throw new NotInRegisterException(p.toString());
			}
		} else if (p.getSex() == Sex.FEMALE) {
			if (!this.singleWomen.get(p.getAgeclass()).remove(p)) {
				throw new NotInRegisterException(p.toString());
			}
		} else
			throw new SimulationException(SimulationException.NOSEX);

		p.removed_single = true;
	}

	/**
	 * Adds one to the category of ages At the end of a time step the results
	 * are written to file
	 *
	 * @param currentTime
	 *            time of event
	 * @param ageclass
	 *            category to add one
	 * @param type
	 *            What to save
	 * @throws IOException
	 */
	protected void saveEvent(double currentTime, int[] ageclass, String type) {
		if (!saveevents)
			return;

		// other wise gather this piece of data
		if (type.contains(Message.BIRTH)) {
			// if saveevent is not set 'on' just return
			if (saveevents == false)
				return;

			while (ageclass[0] >= this.births.size()) {
				this.births.add(new Double(0.0));
			}
			// put a 1 to the right age class
			Double i = this.births.get(ageclass[0]);
			// add one
			i = new Double(i.intValue() + 1);
			// set
			this.births.set(ageclass[0], i);

		} else if (type.contains(Message.WEDDING)) {
			// if saveevent is not set 'on' just return
			if (saveevents == false)
				return;
			// put a 1 to the right age class
			for (int sex = 0; sex < Sex.BOTH.getValue(); sex++) {
				while (ageclass[sex] >= this.weddingpics[sex].size()) {
					this.weddingpics[Sex.MALE.getValue()].add(new Double(0.0));
					this.weddingpics[Sex.FEMALE.getValue()].add(new Double(0.0));
				}
				// number of weddings since the last output
				Double i = this.weddingpics[sex].get(ageclass[sex]);
				i = new Double(i.intValue() + 1);
				this.weddingpics[sex].set(ageclass[sex], i);

			}

		}
	}

	/**
	 * Schedule birth will schedule the next births (within a time step births
	 * are scheduled at uniform distance). If the timestep has ended a new list
	 * of birth times is created.
	 *
	 *
	 */
	@SuppressWarnings(PN.UNCHECKED)
	protected void scheduleBirth() {
		// set population growth function
		if (pop_size == null)
			pop_size = Archimedes.ask(PN.POPSIZE);

		// set the distribution of births
		if (w_birth == null)
			w_birth = Archimedes.ask(PN.BIRTH_WEIGTH);

		// calculate predicted population size at time + stepsize
		double N = pop_size.getY(Odin.getCurrentTime() + stepsize);

		// determine current size of the population
		double popsize = odin.getPopulationRegister().getSize();

		// determine death during the coming time + stepsize
		double m = odin.getHel().getDeathDuring(Odin.getCurrentTime(), Odin.getCurrentTime() + stepsize);

		// determine the number of births during time + stepsize
		// round such that some times a slightly larger number than needed
		// and sometimes slightly less are born
		// raise the number of births to compensate for this loss
		long b = Math.round(N + m - popsize);

		double precision = Pythagoras.getDefaultDecimals();
		// if births occur
		if (b > 0) {
			// we work with long to avoid problems caused by rounding
			// schedule and target births at levels of women
			long[] ageclassWeight = new long[this.mothercount.length];
			long mothersweight = 0;
			int x = 0;

			// weight are determined at each time step because of possible
			// changes in time
			for (int i : this.mothercount) {
				// get the weight (as a long of at least lengt 9 decimals)
				long weight = (long) (precision * i
						* w_birth.getY((x + 0.5) * AbstractPopulationRegister.ageclasswidth));

				// set the weigth
				ageclassWeight[x] = weight;

				// add to the total weight
				mothersweight += weight;

				x++;
			}

			// schedule b births starting in one/stepsize time
			for (int z = 1; z < b + 1; z++) {
				// if no candidate mothers available break
				if (mothersweight <= 0.0)
					break;

				// determine birht time
				double birthtime = Odin.getCurrentTime() + z * (stepsize / b);

				/* determine age class of the mother */
				// keep selecting women until one will stay alive or the list is
				// empty
				int rescue = 0;
				double motherdeathTime = -1;
				Female mother = null;
				while (motherdeathTime <= birthtime) {
					// check whether we are not looping to long or endlessly
					if (rescue > this.motherTotal * 2) {
						mother = null;
						SimulationLog.log("no mother selected");
						break;
					} else
						rescue++;

					// random number )not using nextLong because we want our own
					// precision
					long rand = (long) (mothersweight * Herbarium.rand(Herbarium.POP).nextDouble());

					long sum = 0;
					int w = -1;
					while (rand > sum) {
						// select a age group
						sum += (ageclassWeight[++w]);

					}
					long thisweight = (long) (precision
							* w_birth.getY(((w - 1) + 0.5) * AbstractPopulationRegister.ageclasswidth));

					// no women available
					if (w < 0) {
						// create a new scheduling event
						BirthScheduling bs = new BirthScheduling(this, this, Odin.getCurrentTime() + stepsize);

						Odin.getEventManager().schedule(bs);

						return;
					}

					// subtract from the ageclass
					ageclassWeight[w] -= thisweight;
					// subtract from the total weight
					mothersweight -= thisweight;
					// determine the number of mothers in this class
					int numOfMothers = this.mothercount[w];

					// select a mother
					if (numOfMothers > 0) {
						mother = (Female) this.select(this.mothers.get(w), Herbarium.rand(Herbarium.POP));
					} else
						throw new SimulationException("no mothers available");

					// set new death time
					if (mother != null)
						motherdeathTime = mother.getDeathTime();

				}
				// if we were able to select a mother proceed
				if (mother != null) {
					// the new person
					Person baby = null;
					// determine sex of baby
					if (Herbarium.rand(Herbarium.POP).nextDouble() < sexratio) {
						baby = Odin.getOdin().getHel().dispense(Sex.FEMALE);
					} else {
						baby = Odin.getOdin().getHel().dispense(Sex.MALE);
					}
					// set birth time
					baby.setBdate(birthtime);

					// get death age
					baby.setDeathAge(Odin.getOdin().getHel().getDeathAge(baby));

					// check whether baby survives time step
					if (birthtime + baby.getDeathAge() < Odin.getCurrentTime() + stepsize)
						// add a birth to compensate
						b++;

					// determine death age so that we know if a new
					Birth birth = new Birth(mother, baby, birthtime);
					Odin.getEventManager().schedule(birth);

				}
			}
		}

		// create a new scheduling event
		BirthScheduling bs = new BirthScheduling(this, this, Odin.getCurrentTime() + stepsize);

		Odin.getEventManager().schedule(bs);
	}

	/**
	 * Make a random selection from the specified collection It adds all
	 * elements of the collection to a new list and than draws a random index
	 *
	 * @param s
	 * @param rand
	 * @return l.get(index)
	 */
	protected Person select(Collection<Person> s, Random rand) {

		// resolve empty collections be returning null
		if (s.isEmpty())
			return null;

		// add all to a list for which it is easy to select a random index
		List<Person> l = new ArrayList<Person>(s);

		// determine a random index between 0 and the size of that list
		int index = s.size() - rand.nextInt(s.size()) - 1;
		// return the person that belong to it
		return l.get(index);
	}

	/**
	 * execute a wedding
	 *
	 * @throws Exception
	 *
	 */
	protected void wedding() {
		// if weddings is not empty
		if (!weddings.isEmpty()) {
			// execute the next wedding
			this.marry(weddings.pop());

		}

		if (weddings.isEmpty()) {

			// arrange new weddings
			this.arrangeMarriages(Herbarium.rand(Herbarium.POP));

		}
		// schedule the first wedding of this stack
		Wedding w = new Wedding(this, this, Odin.getCurrentTime() + this.wedding_interval);

		Odin.getEventManager().schedule(w);

	}

	/**
	 * Puts a person from one to the next age class list
	 *
	 * @param p
	 * @throws NotInRegisterException
	 */
	@SuppressWarnings("unchecked")
	private void nextAgeClass(Person p) throws NotInRegisterException {

		int newIndex = p.getAgeclass() + 1;
		// if the new index is larger than the size of the list with classes
		// increase this list
		int sizeList = this.mothers.size();
		while (sizeList - 1 < newIndex) {
			int size = this.singleout[0].get(0).size();
			// single men
			this.singleMen.add(new LinkedHashSet<Person>());
			// output of single men
			this.singleout[Sex.MALE.getValue()]
					.add(OutputList.newOutputList(size, MALE + SINGLE + this.singleMen.size()));
			// single women
			this.singleWomen.add(new LinkedHashSet<Person>());
			// output of single women
			this.singleout[Sex.FEMALE.getValue()]
					.add(OutputList.newOutputList(size, FEMALE + SINGLE + this.singleMen.size()));
			// mothers
			this.mothers.add(new LinkedHashSet<Person>());
			// output of mothers
			this.motherout.add(OutputList.newOutputList(size, MOTHER + this.mothers.size()));
			// size list update
			sizeList = this.singleMen.size();
		}
		// increase size of the mother counter
		if (sizeList > this.mothercount.length) {
			int[] newCounter = new int[sizeList];
			for (int i = 0; i < this.mothercount.length; i++) {
				newCounter[i] = this.mothercount[i];
			}
			this.mothercount = newCounter;
		}

		// male
		if (p.getSex() == Sex.MALE) {
			// if not married
			if (!p.ismarried()) {
				if (p.engaged)
					return;

				if (!this.singleMen.get(newIndex - 1).remove(p)) {
					if (Odin.inSafeMode())
						this.singleMen.get(this.search(p)).remove(p);
					else
						throw new SimulationException("Person not removed " + "from age class");

				}
				this.singleMen.get(newIndex).add(p);

			} else if (this.hasSingle(p)) {
				throw new SimulationException("single male not recognised as single" + p);
			}
		}
		// female
		else if (p.getSex() == Sex.FEMALE) {
			if (!p.ismarried()) {
				if (!this.singleWomen.get(newIndex - 1).remove(p)) {
					if (Odin.inSafeMode())
						this.singleWomen.get(this.search(p)).remove(p);
					else
						throw new NotInRegisterException(p.toString());

				}
				this.singleWomen.get(newIndex).add(p);
			} else if (p.ismarried()) {
				if (p.engaged)
					return;

				if (!this.mothers.get(newIndex - 1).remove(p)) {
					if (Odin.inSafeMode())
						this.search(p);
					else
						throw new NotInRegisterException(p.toString());
				}
				// get mother
				this.mothers.get(newIndex).add(p);
				// count
				this.mothercount[newIndex - 1]--;
				this.mothercount[newIndex]++;

				// check that this is not a single
				if (Odin.inSafeMode())
					if (this.hasSingle(p))
						throw new SimulationException("single female not recognised as single" + p);

			}
		} else
			throw new SimulationException(SimulationException.NOSEX);
	}

	/**
	 * Searches a person in the lists of singles or mothers.<br>
	 *
	 * @param p
	 * @return the age class in which this person is registered (not necessarily
	 *         the age class that this person has)
	 * @throws NotInRegisterException
	 * @deprecated should not be used, as it is an error when you have to search
	 */
	@Deprecated
	private int search(Person p) throws NotInRegisterException {

		if (p.getSex() == Sex.MALE) {
			if (p.ismarried())
				return -1;

			for (Set<Person> set : this.singleMen) {
				if (set.contains(p)) {

					// return the man
					return this.singleMen.indexOf(set);
				}
			}
			throw new NotInRegisterException(p.toString());

		} else if (p.getSex() == Sex.FEMALE) {
			if (!p.ismarried()) {
				for (Set<Person> set : this.singleWomen) {
					if (set.contains(p)) {
						// return
						return this.singleWomen.indexOf(set);
					}
				}
				throw new NotInRegisterException(p.toString());
			} else if (p.ismarried()) {
				for (Set<Person> set : this.mothers) {
					if (set.contains(p)) {
						return this.mothers.indexOf(set);
					}
				}
			}
		}
		throw new NotInRegisterException(p.toString());

	}

}
