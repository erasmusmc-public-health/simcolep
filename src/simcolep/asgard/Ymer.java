/*
 * This file is part of SIMCOLEP, a simulation model developed by the Department of Public Health,
 * Erasmus MC, University Medical Center Rotterdam, Rotterdam, The Netherlands.
 *
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/
 * or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA
 *
 * You are free to:
 * Share � copy and redistribute the material in any medium or format
 * Under the following terms:
 * Attribution � You must give appropriate credit, provide a link to the license, and indicate if changes were
 * made. You may do so in any reasonable manner, but not in any way that suggests the licensor endorses
 * you or your use.
 * NonCommercial � You may not use the material for commercial purposes.
 * NoDerivatives � If you remix, transform, or build upon the material, you may not distribute the modified material.
 */

/*
 * Created on 17-Nov-2005
 *
 */
package simcolep.asgard;

import org.jdom.DataConversionException;

import simcolep.demo.AbstractPopulationRegister;
import simcolep.demo.Cadastre;
import simcolep.demo.ILevelRegister;
import simcolep.demo.IPopulationRegister;
import simcolep.demo.Level;
import simcolep.demo.Person;
import simcolep.demo.Sex;
import simcolep.exceptions.SimulationException;
import simcolep.fun2.Archimedes;
import simcolep.fun2.Distributionable;
import simcolep.fun2.Function;
import simcolep.lepra.Morbus;
import simcolep.lepra.mendel.Genomable;
import simcolep.sim.Herbarium;
import simcolep.tools.PN;

/**
 *
 * According to Nordic mythology <i>Odin</i> created Midgard (Earth) out of the
 * eyebrows of <i>Ymer</i> after killing him.<br>
 * The class <code> Ymer</code> creates and returns <code>PopRegister</code>s
 * and <code>LevelRegister</code>s to start a simulation from nothing.
 *
 *
 * @author ir E.A.J. Fischer
 * @version 0.1.2 -- Last Modified 17-Nov-2005
 */
public final class Ymer implements Ase {

	/** ID start with */
	private static int ID = 1;

	/**
	 *
	 * @return ID
	 */
	public static int getID() {
		return ID++;
	}

	public static void reset() {
		ID = 0;
	}

	/** population register */
	@SuppressWarnings(PN.UNCHECKED)
	private transient IPopulationRegister populationRegister = null;

	/** level register */
	@SuppressWarnings(PN.UNCHECKED)
	private transient ILevelRegister levelRegister = null;

	/**
	 *
	 * Constructor for Ymer
	 */
	public Ymer() {

	}

	/**
	 * Create <i>Midgard</i>,which is the realm of humans, from the
	 * distributions given in the input.
	 *
	 *
	 * @param popnum
	 * @throws IllegalAccessException
	 * @throws InstantiationException
	 * @throws DataConversionException
	 */
	@SuppressWarnings(PN.UNCHECKED)
	public void createMidgard() throws DataConversionException, InstantiationException, IllegalAccessException {

		// random number generator for population
		java.util.Random pop_rand = Herbarium.rand(Herbarium.POP);

		// random number generator for disease
		java.util.Random dis_rand = Herbarium.rand(Herbarium.DIS);

		// get the distributions from the file
		// distribution
		Distributionable[] agedist = new Distributionable[2];

		// females
		agedist[Sex.FEMALE.getValue()] = Archimedes.askDistribution(PN.INITAGEDIST_FEMALE);
		// females
		agedist[Sex.MALE.getValue()] = Archimedes.askDistribution(PN.INITAGEDIST_MALE);

		// household size distribution
		Distributionable hhsize = Archimedes.askDistribution(PN.INITLEVELSIZEDIST);

		Function bcg = Archimedes.ask(PN.BCGCOVERAGE);

		// size of the start population
		double N = Archimedes.ask(PN.POPSIZE).getY(0);

		// survival functions
		Distributionable[] survival = new Distributionable[2];

		Distributionable sm = Archimedes.askDistribution(PN.MALE_SURVIVAL);
		Distributionable sf = Archimedes.askDistribution(PN.FEMALE_SURVIVAL);

		survival[Sex.MALE.getValue()] = sm;
		survival[Sex.FEMALE.getValue()] = sf;

		// Initialize a population with these distributions and additional the
		// sex ratio
		for (int k = 0; k < N;) {
			// create a level
			Level l = new Level(Level.DWELLING_RANK);

			// set id
			l.setID(Odin.getOdin().getLevelId(l.getRank()));

			// determine the size
			int size = new Double(hhsize.drawX(pop_rand.nextDouble())).intValue();

			for (int x = 0; x < size; x++) {
				// sex
				Sex sex = null;
				if (pop_rand.nextDouble() < Archimedes.askDouble(PN.SEXRATIO)) {
					sex = Sex.FEMALE;
				} else {
					sex = Sex.MALE;
				}

				// age
				double age = agedist[sex.getValue()].drawX(pop_rand.nextDouble());

				// age should be a number
				if (new Double(age).isNaN() || age < 0.0)
					throw new SimulationException("could not draw a suitable age");

				// calculate age class
				int ageclass = (int) ((age - age % AbstractPopulationRegister.ageclasswidth)
						/ AbstractPopulationRegister.ageclasswidth);

				// create morbi
				Morbus[] m = new Morbus[Archimedes.askInteger(PN.L)];

				Genomable genome = null;

				// creates Morbi for this human layer
				for (int index = 0; index < m.length; index++) {

					genome = Idunn.i_Model.getRandomGenotype(dis_rand);

					m[index] = new Morbus(genome);

					// add to the clinic registers
					Idunn.getIdunn().getClinic().getRegister().addMorbus(m[index]);

					m[index].setLayer(index);

					// determine vaccination at birth
					if (dis_rand.nextDouble() < bcg.getY(-age))
						m[index].vaccinate(-age);
				}

				// create the person
				Person p = Odin.getOdin().getHel().dispense(sex);

				// identification
				p.setID(ID++);
				// age
				p.setBdate(0 - age);
				// age class
				p.setAgeclass(ageclass);
				//
				double deathAge = Odin.getOdin().getHel().getDeathAge(p);

				// if settings don't match let every e.g. distribution lets
				// people live above maximum age of survival let
				// people die instantaneously at the beginning of the
				if (deathAge + p.getBdate() < 0) {
					deathAge = -p.getBdate();
				}

				// schedule death of this person
				Odin.getOdin().getHel().schedule(p, deathAge + p.getBdate());

				p.setDeathAge(deathAge);

				l.addInhabitantWithoutNotification(p);

				for (int i = 0; i < m.length; i++) {
					p.setMorbus(m[i], i);
				}

				this.populationRegister.addPerson(p);

				// add 1 to the number of people created
				k++;
			}

			// add the level
			if (size > 0)
				this.levelRegister.addLevel(l);

		}

		// weighting of levels
		Function weight = Archimedes.ask(PN.LEVELWEIGHT);
		((Cadastre) this.levelRegister).setWeights(weight);

	}

	/**
	 *
	 * @return cad
	 */
	@SuppressWarnings(PN.UNCHECKED)
	public ILevelRegister getLevelRegister() {
		return levelRegister;
	}

	/**
	 *
	 * @return pop
	 */
	@SuppressWarnings(PN.UNCHECKED)
	public IPopulationRegister getPopulationRegister() {
		return populationRegister;
	}

	/**
	 * @see simcolep.asgard.Ase#ragnarok()
	 */
	@Override
	public void ragnarok() {
		Ymer.reset();
		this.levelRegister = null;
		this.populationRegister = null;

	}

	/**
	 *
	 * @param pop
	 * @param cad
	 */
	public void setRegisters(IPopulationRegister<?> populationRegister, ILevelRegister<?> levelRegister) {
		this.populationRegister = populationRegister;
		this.levelRegister = levelRegister;
	}

	/**
	 * Nothing to save
	 *
	 * @see simcolep.asgard.Ase#setSave(boolean)
	 */
	@Override
	public void setSave(boolean b) {
		// nothing to save
		return;

	}

}
