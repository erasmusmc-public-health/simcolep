/*
 * This file is part of SIMCOLEP, a simulation model developed by the Department of Public Health,
 * Erasmus MC, University Medical Center Rotterdam, Rotterdam, The Netherlands.
 *
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/
 * or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA
 *
 * You are free to:
 * Share � copy and redistribute the material in any medium or format
 * Under the following terms:
 * Attribution � You must give appropriate credit, provide a link to the license, and indicate if changes were
 * made. You may do so in any reasonable manner, but not in any way that suggests the licensor endorses
 * you or your use.
 * NonCommercial � You may not use the material for commercial purposes.
 * NoDerivatives � If you remix, transform, or build upon the material, you may not distribute the modified material.
 */

/*
 * Created on 04-Nov-2005
 *
 */
package simcolep.asgard;

import java.io.Serializable;
import java.util.Random;
import java.util.SortedMap;
import java.util.Stack;
import java.util.TreeMap;

import simcolep.demo.Female;
import simcolep.demo.Male;
import simcolep.demo.Person;
import simcolep.demo.Sex;
import simcolep.demo.event.Death;
import simcolep.exceptions.SimulationException;
import simcolep.fun2.Archimedes;
import simcolep.fun2.Distributionable;
import simcolep.sim.Herbarium;
import simcolep.tools.PN;

/**
 * <i>Hel</i> is the daughter of Loki and reigns over the underworld which is
 * convenient called Hell as well. Hel has one good side and one black side. The
 * men that die of disease or at old age in bed go to Hel (the ones the die on
 * the battlefield go to Valhalla).
 *
 * In the simulation the Hel class has two functions: -Scheduling death - and
 * keeping and sending out empty Person objects for recycling
 *
 * @author ir. E.A.J. Fischer
 * @version 1.0
 */
@SuppressWarnings("unchecked")
public final class Hel implements Serializable, Ase {

	/** serial version UID */
	private static final long serialVersionUID = 3784036658043902757L;

	/** survival function for males */
	private Distributionable survivalmale = null;

	/** survival function for females */
	private Distributionable survivalfem = null;

	/** list with scheduled death times */
	private transient SortedMap<Double, Integer> deaths = null;

	/** Stacks with dead individuals */
	private transient Stack<Person>[] zombies = new Stack[2];

	/**
	 *
	 * @param popnum
	 */
	public Hel() {
		super();

		this.initialize();
	}

	/**
	 *
	 * @param female
	 * @return an previously used object or a new person
	 */
	public Person dispense(Sex female) {
		if (this.zombies[female.getValue()].isEmpty()) {
			if (female == Sex.MALE) {
				return new Male();
			} else if (female == Sex.FEMALE) {
				return new Female();
			} else {
				throw new SimulationException(SimulationException.NOSEX);
			}
		} else {
			return this.zombies[female.getValue()].pop();
		}
	}

	@SuppressWarnings(PN.UNCHECKED)
	public void enterHel(Person p) {
		// remove its death time from the set
		this.removeDeathTime(Double.valueOf(p.getDeathAge() + p.getBdate()));

		// strip person
		Person person = Person.strip(p);
		// add to the zombies
		this.zombies[p.getSex().getValue()].add(person);

	}

	/**
	 * Draws an age of death and schedules a death event
	 *
	 * @param p
	 * @return deathAge
	 */
	public double getDeathAge(Person p) {
		double deathAge = 0;

		double age = p.getAge(Odin.getCurrentTime());

		Random rand = Herbarium.rand(Herbarium.POP);

		// draw age of death conditional on the current age
		if (p.getSex() == Sex.MALE) {
			if (age <= 0.0)
				deathAge = survivalmale.drawX(rand.nextDouble());
			else
				deathAge = survivalmale
						.drawXconditional(new double[] { rand.nextDouble(), p.getAge(Odin.getCurrentTime()) });
		} else if (p.getSex() == Sex.FEMALE) {
			if (age <= 0.0)
				deathAge = survivalfem.drawX(rand.nextDouble());
			else
				deathAge = survivalfem
						.drawXconditional(new double[] { rand.nextDouble(), p.getAge(Odin.getCurrentTime()) });
		} else {
			throw new SimulationException(SimulationException.NOSEX);
		}

		// return the death age
		return deathAge;
	}

	/**
	 *
	 * @param from
	 * @param to
	 * @return size of the set of death times
	 */
	public int getDeathDuring(double from, double to) {
		SortedMap<Double, Integer> submap = this.deaths.subMap(Double.valueOf(from), Double.valueOf(to));

		if (submap.isEmpty())
			return 0;
		int count = 0;
		for (Integer i : submap.values()) {
			count += i.intValue();
		}
		return count;
	}

	/**
	 * Initialize the godess Hel
	 *
	 */
	public void initialize() {
		// set
		deaths = new TreeMap<Double, Integer>();

		// survival functions
		survivalmale = Archimedes.askDistribution(PN.MALE_SURVIVAL);
		survivalfem = Archimedes.askDistribution(PN.FEMALE_SURVIVAL);

		this.zombies[Sex.MALE.getValue()] = new Stack<Person>();
		this.zombies[Sex.FEMALE.getValue()] = new Stack<Person>();
	}

	/**
	 *
	 * @see simcolep.asgard.Ase#ragnarok()
	 */
	@Override
	public void ragnarok() {
		//
		this.deaths = null;
		//
		this.survivalfem = null;
		//
		this.survivalmale = null;
	}

	/**
	 *
	 * @param p
	 * @param deathTime
	 */
	public void schedule(Person p, double deathTime) {
		// schedule death event
		Death death = new Death(this, p, deathTime);
		Odin.getEventManager().schedule(death);

		// add to the list of death times
		this.addDeathTime(deathTime);

	}

	/**
	 * Nothing to save
	 *
	 * @see simcolep.asgard.Ase#setSave(boolean)
	 */
	@Override
	public void setSave(boolean b) {
		// nothing to save
		return;

	}

	/**
	 * Add one to the count of this death time
	 *
	 * @param d
	 */
	private void addDeathTime(double d) {
		Double key = Double.valueOf(d);

		if (this.deaths.containsKey(key)) {
			// replace

			Integer i = this.deaths.get(key);
			Integer newCount = new Integer(i.intValue() + 1);
			this.deaths.put(Double.valueOf(key), newCount);
		} else // replace new
		{
			this.deaths.put(Double.valueOf(key), new Integer(1));
		}
	}

	/**
	 * remove one count of this death time
	 *
	 * @param d
	 */
	private void removeDeathTime(double d) {
		Double key = Double.valueOf(d);
		Integer i = this.deaths.get(key);

		if (i.intValue() == 1)
			this.deaths.remove(Double.valueOf(key));
		else {
			Integer newCount = new Integer(i.intValue() - 1);
			this.deaths.put(Double.valueOf(key), newCount);
		}
	}

}
