/*
 * This file is part of SIMCOLEP, a simulation model developed by the Department of Public Health,
 * Erasmus MC, University Medical Center Rotterdam, Rotterdam, The Netherlands.
 *
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/
 * or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA
 *
 * You are free to:
 * Share � copy and redistribute the material in any medium or format
 * Under the following terms:
 * Attribution � You must give appropriate credit, provide a link to the license, and indicate if changes were
 * made. You may do so in any reasonable manner, but not in any way that suggests the licensor endorses
 * you or your use.
 * NonCommercial � You may not use the material for commercial purposes.
 * NoDerivatives � If you remix, transform, or build upon the material, you may not distribute the modified material.
 */

/*
 * Created on 04-Nov-2005
 *
 */
package simcolep.asgard;

import java.io.Serializable;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;

import nl.mgz.simulation.discreteevent.Target;
import simcolep.demo.Level;
import simcolep.demo.Person;
import simcolep.demo.Sex;
import simcolep.demo.event.Move;
import simcolep.demo.event.SplitLevel;
import simcolep.exceptions.SimulationException;
import simcolep.fun2.Archimedes;
import simcolep.fun2.Distributionable;
import simcolep.fun2.Function;
import simcolep.sim.Gylfi;
import simcolep.sim.Herbarium;
import simcolep.tools.PN;
import simcolep.tools.io.OutputElement;
import simcolep.tools.io.OutputList;
import simcolep.tools.message.Message;
import simcolep.tools.message.Notifyable;
import simcolep.tools.message.Ratatosk;

/**
 * <b><i>Loki</i></b> is an ambiguous character in Norse mythology. This frost
 * giant became a bloodbrother of Odin in ancient times. Loki is often a
 * intrigant, but in some stories the one that gets the other Aesir out of
 * troubles. The reason this class is called <code>Loki</code> is that Loki
 * should be named when using Norse deities in a naming convention. If anyone
 * has a better name for this class I will agree to change it. <br>
 * Task of Loki: <br>
 * <b> *** Movement ***</b>
 * <li>scheduling random movement
 * <li>scheduling migration (not implemented)
 * <li>execution of all movements
 *
 *
 * @author ir E.A.J. Fischer
 * @version 0.1 -- Last Modified: 03-03-'06
 *
 */
public final class Loki extends Target implements Ase, Notifyable, Serializable {

	/** Widow movement */
	public static final String WIDOW_CODE = "widow"; //$NON-NLS-1$

	/** random movement */
	public static final String RANDOM_CODE = "random";

	/** Overal movement */
	public static final String MOVE_CODE = "move"; //$NON-NLS-1$

	/** Splits and movement */
	public static final String SPLIT_CODE = "split";

	/** wedding movement */
	public static final String WEDDING_CODE = "wedding";

	/** serial versionUID */
	private static final long serialVersionUID = 2851422451380647269L;

	/** Loki listens to the following messages from Ratatosk */
	private static final String[] listensTo = { Message.SPLIT, Message.SPOUSEDEATH };

	// methods and their parameters
	/** move at marriage */
	public static boolean moveatmar = true;// default one of the couple moves at
											// marriage

	/** fraction moving to parents of male */
	public static double movetoman = 0.5;

	/** fraction moving to parents of female */
	public static double movetowoman = 0.5;

	/** random movement after marriage */
	public static boolean RMafterM = false;

	/** * fraction starting single after random movement */
	protected static double fractSingle = 0.0;

	/** move when the size of the current Level is smaller than */
	public static double densityMoveWidow = Double.POSITIVE_INFINITY;

	/**
	 * move fraction of the widow(er)s living in a level of size smaller than
	 * densityMoveWidow
	 */
	public static double[] fractMoveWidow = { 1.0, 1.0 };

	/** * weight of levels to move to randomly */
	protected static Function levelweight = null;

	/**
	 * Probability of movement of a widow(er)
	 *
	 * @param p
	 * @return true of moveing
	 */
	public static boolean getMovesasWidow(Person p) {
		return Herbarium.rand(Herbarium.POP).nextDouble() < fractMoveWidow[p.getSex().getValue()];
	}

	/**
	 * The output codes of Loki can be used in method output(String s)
	 *
	 * @return codes
	 */
	public static String[] outputCodes() {

		String[] codes = { MOVE_CODE, RANDOM_CODE, WEDDING_CODE, SPLIT_CODE, WIDOW_CODE };

		return codes;
	}

	/** * save the events occuring when true */
	private boolean savedata = false;

	/** random movement ages distribution */
	private Distributionable[] move = null;

	/** time until this split */
	public Distributionable splitdist = null;
	/* counters */
	/** overall */
	private int[] overallCounter = null;
	/** widowers */
	private int[] widowCounter = null;
	/** splits */
	private int[] splitCounter = null;
	/** random movement */
	private int[] randomCounter = null;

	/** wedding movement */
	private int[] weddingCounter = null;

	/* Output storage */
	/** output list for moveing of people */
	private transient OutputList<Integer>[] overallmove = null;

	/** output count of movement by widow(er)s by size of level */
	private transient OutputList<Integer>[] widmove = null;

	/** Output list for number of people moving during level splits */
	private transient OutputList<Integer>[] splitmove = null;

	/** Output list for number of people moving during level splits */
	private transient OutputList<Integer>[] randommove = null;
	/** Output list for number of people moving at marriage */
	private transient OutputList<Integer>[] weddingmove = null;

	/**
	 *
	 */
	public Loki() {
		super();

		this.init();
	}

	/**
	 * Initialize Loki
	 */
	@SuppressWarnings({ PN.UNCHECKED })
	public void init() {
		Loki.RMafterM = Gylfi.ask(PN.MOVEAFTERMARRIAGE);
		// initialize the movement functions
		this.move = new Distributionable[2];

		this.move[Sex.FEMALE.getValue()] = Archimedes.askDistribution(PN.MOVEFEMALE);
		this.move[Sex.MALE.getValue()] = Archimedes.askDistribution(PN.MOVEMALE);

		this.splitdist = Archimedes.askDistribution(PN.SPLIT);

		/* function for random movement */
		levelweight = Archimedes.ask(PN.LEVELWEIGHT);
		/* values for widow movement */
		densityMoveWidow = Archimedes.askDouble(PN.DENSMOVEWID);
		fractMoveWidow[Sex.MALE.getValue()] = Archimedes.askDouble(PN.MOVEWIDOWER);
		fractMoveWidow[Sex.FEMALE.getValue()] = Archimedes.askDouble(PN.MOVEWIDOW);
		/* values for wedding movement */
		movetoman = Archimedes.askDouble(PN.MOVETOMALE);
		movetowoman = Archimedes.askDouble(PN.MOVETOFEMALE);
		fractSingle = Archimedes.askDouble(PN.MOVESINGLE);

		/* set counters */
		this.overallCounter = new int[2];
		this.splitCounter = new int[2];
		this.widowCounter = new int[2];
		this.randomCounter = new int[2];
		this.weddingCounter = new int[2];
		/* set output lists */
		this.overallmove = new OutputList[2];
		this.overallmove[Sex.MALE.getValue()] = new OutputList<Integer>();
		this.overallmove[Sex.FEMALE.getValue()] = new OutputList<Integer>();
		this.splitmove = new OutputList[2];
		this.splitmove[Sex.MALE.getValue()] = new OutputList<Integer>();
		this.splitmove[Sex.FEMALE.getValue()] = new OutputList<Integer>();
		this.randommove = new OutputList[2];
		this.randommove[Sex.MALE.getValue()] = new OutputList<Integer>();
		this.randommove[Sex.FEMALE.getValue()] = new OutputList<Integer>();
		this.widmove = new OutputList[2];
		this.widmove[Sex.MALE.getValue()] = new OutputList<Integer>();
		this.widmove[Sex.FEMALE.getValue()] = new OutputList<Integer>();
		this.weddingmove = new OutputList[2];
		this.weddingmove[Sex.MALE.getValue()] = new OutputList<Integer>();
		this.weddingmove[Sex.FEMALE.getValue()] = new OutputList<Integer>();

		/* register at Ratatosk */
		for (String s : listensTo) {
			Ratatosk.register(Message.formulate(s), this);
		}
	}

	/**
	 * move person from current level to level l
	 *
	 * @param p
	 * @param l
	 */
	@SuppressWarnings(PN.UNCHECKED)
	public void move(Person p, Level l) {
		Level old = p.getLevel();

		old.removeInhabitant(p);

		l.addInhabitant(p);

		if (l.equals(old))
			return;

		if (this.savedata)
			this.overallCounter[p.getSex().getValue()] += 1;

	}

	/**
	 * move person p to a random level
	 *
	 * @param p
	 * @param rand
	 *
	 */
	public void moveRandom(Person p) {
		/** if random movement after marriage is not allow */
		if (!Loki.RMafterM)
			/** and the target is ever been married */
			if (p.isevermarried())
				/** return */
				return;

		// get random number generator
		Random rand = Herbarium.rand(Herbarium.POP);
		Level l = null;

		if (rand.nextDouble() < fractSingle) {
			// create a new level of dwelling rank
			l = this.newLevel();

			// add to the level register
			Odin.getOdin().getLevelRegister().addLevel(l);

		} else {

			l = Odin.getOdin().getLevelRegister().getLevel(rand, levelweight);

		}

		// check if the person is in the population
		if (Odin.inSafeMode())
			if (!Odin.getOdin().getPopulationRegister().contains(p))
				throw new SimulationException("cannot move non registered people");

		// if no level was returned e.g. l == null stay in your level
		if (l == null)
			return;

		// if the same level is chosen to move to stay in your level
		if (l.equals(p.getLevel()))
			return;

		// count the move
		if (this.savedata)
			this.randomCounter[p.getSex().getValue()] += 1;

		// execute the move
		this.move(p, l);
	}

	/**
	 * Move couple c according to the movement of wedding rules
	 *
	 * @param c
	 */
	@SuppressWarnings(PN.UNCHECKED)
	public void movewedding(Person[] c) {

		Random randgen = Herbarium.rand(Herbarium.POP);
		// no movement because of marriage
		if (!moveatmar)
			return;

		// draw random number
		double rand = randgen.nextDouble();

		// to the male-household
		if (rand < movetoman) {
			// count move
			if (this.savedata)
				this.weddingCounter[Sex.FEMALE.getValue()] += 1;
			// move
			this.move(c[Sex.FEMALE.getValue()], c[Sex.MALE.getValue()].getLevel());

			// time to split is
			double split_time = Odin.getCurrentTime() + splitdist.drawX(randgen.nextDouble());

			if (Double.isInfinite(split_time))
				return;
			// schedule
			SplitLevel s = new SplitLevel(c[Sex.MALE.getValue()], c[Sex.MALE.getValue()].getLevel(), split_time);
			Odin.getEventManager().schedule(s);

		} // female household
		else if (rand < movetoman + movetowoman) {
			// count move
			if (this.savedata)
				this.weddingCounter[Sex.MALE.getValue()] += 1;

			this.move(c[Sex.MALE.getValue()], c[Sex.FEMALE.getValue()].getLevel());
			// time to split is
			double split_time = Odin.getCurrentTime() + splitdist.drawX(randgen.nextDouble());
			if (Double.isInfinite(split_time))
				return;

			SplitLevel s = new SplitLevel(c[Sex.MALE.getValue()], c[Sex.FEMALE.getValue()].getLevel(), split_time);
			Odin.getEventManager().schedule(s);

		} else // a new household
		{
			// create new level
			Level l = this.newLevel();

			// add level to the levelregister
			Odin.getOdin().getLevelRegister().addLevel(l);

			// count move
			if (this.savedata) {
				this.weddingCounter[Sex.FEMALE.getValue()] += 1;
				this.weddingCounter[Sex.MALE.getValue()] += 1;
			}
			// add both to this level
			this.move(c[Sex.FEMALE.getValue()], l);
			this.move(c[Sex.MALE.getValue()], l);

		}
	}

	/**
	 * Moves a widow to the child that was chosen before if the density of the
	 * current level is lower than or equal parameter: densityMoveWidow a random
	 * generated number is smaller or equal to parameter: fractMoveWidow
	 *
	 * @param w
	 */
	@SuppressWarnings(PN.UNCHECKED)
	public void movewidow(Person w) {
		// no child chosen return
		if (w.gettheChosen() == null)
			return;

		// moving
		if (w.movesAsWidow()) {

			// move the widow
			if (w.getLevel().getSize() <= densityMoveWidow) {

				// get the selected child and move
				Level l = null;

				// get the level of the chosen child
				l = w.gettheChosen().getLevel();

				if (!l.equals(w.getLevel()))// widow stays with her child that
											// lives in the same level
				{
					// save widow movement
					if (this.savedata)
						this.widowCounter[w.getSex().getValue()] += 1;

					// move the widow
					this.move(w, l);
				}

			}
		}
		// remove links between parent and child

		// set the chosen status of the child to false
		w.gettheChosen().setChosenByParent(false);

		// remove the surviving parent
		w.gettheChosen().setsurvivingParent(null);

		// remove the chosen child
		w.settheChosen(null);

		// set move at widowing to false
		w.movesAsWidow(false);
	}

	/**
	 * @see simcolep.tools.message.Notifyable#notify(simcolep.tools.message.Message,
	 *      java.lang.Object, java.lang.Object)
	 */
	@Override
	public void notify(Message message, Object subject, Object source) {
		// if splitting a level
		if (message.equalsIgnoreCase(Message.SPLIT)) {
			splitLevel((Person) source);
		} else if (message.equalsIgnoreCase(Message.SPOUSEDEATH)) {
			movewidow((Person) subject);
		} else
			throw new SimulationException("unknown message: " + message);

	}

	/**
	 * get the appropiate outputlist
	 *
	 * @param s
	 *
	 */
	@SuppressWarnings(PN.UNCHECKED)
	public OutputList<Integer>[] output(String s) {
		if (s.equalsIgnoreCase(MOVE_CODE)) {
			return this.overallmove;

		} else if (s.equalsIgnoreCase(RANDOM_CODE)) {
			return this.randommove;
		} else if (s.equalsIgnoreCase(WIDOW_CODE)) {
			return this.widmove;
		} else if (s.equalsIgnoreCase(SPLIT_CODE)) {
			return this.splitmove;

		} else if (s.equalsIgnoreCase(WEDDING_CODE)) {
			return this.weddingmove;
		} else
			throw new SimulationException(SimulationException.UNKNOWNFUNCTION_MESSAGE + PN.TAB + s);
	}

	/**
	 *
	 * @see simcolep.asgard.Ase#ragnarok()
	 */
	@Override
	public void ragnarok() {
		this.overallCounter = null;
		this.splitCounter = null;
		this.widowCounter = null;
		this.randomCounter = null;
		return;
	}

	/**
	 * Save the temporary stored data in an outputlist
	 *
	 * @param time
	 */
	public void save(double time) {
		// save random movement
		for (Sex sex : Sex.values()) {
			if (sex != Sex.BOTH) {
				// save overall movement
				this.overallmove[sex.getValue()]
						.add(new OutputElement<Integer>(time, this.overallCounter[sex.getValue()]));
				// save wedding movement
				this.weddingmove[sex.getValue()]
						.add(new OutputElement<Integer>(time, this.weddingCounter[sex.getValue()]));
				// widow movement
				this.widmove[sex.getValue()].add(new OutputElement<Integer>(time, this.widowCounter[sex.getValue()]));
				// random movement
				this.randommove[sex.getValue()]
						.add(new OutputElement<Integer>(time, this.randomCounter[sex.getValue()]));

				// save movement by splitting levels
				this.splitmove[sex.getValue()].add(new OutputElement<Integer>(time, this.splitCounter[sex.getValue()]));
			}
		}
		// set all to 0
		this.overallCounter = new int[2];
		this.splitCounter = new int[2];
		this.widowCounter = new int[2];
		this.randomCounter = new int[2];
		this.weddingCounter = new int[2];

		return;
	}

	/**
	 * Schedule random movement
	 *
	 * @param p
	 * @return time of random movement
	 */
	@SuppressWarnings(PN.UNCHECKED)
	public double schedule(Person p) {
		Random rand = Herbarium.rand(Herbarium.POP);
		// draw movement time
		double moveage = this.move[p.getSex().getValue()]
				.drawXconditional(new double[] { rand.nextDouble(), p.getAge(Odin.getCurrentTime()) });

		if (moveage < p.getDeathAge() && moveage > p.getAge(Odin.getCurrentTime())) {
			Move emove = new Move(this, p, moveage + p.getBdate());
			Odin.getEventManager().schedule(emove);
		}

		// return these calculated ages
		return moveage;

	}

	/**
	 * Set saving on/off
	 *
	 */
	@Override
	public void setSave(boolean start) {
		// switch on saving of data
		this.savedata = start;

	}

	/**
	 * Split an existing level into two separate levels of which person, its
	 * spouse and children will make up the new level
	 *
	 * @param person
	 */
	protected void splitLevel(Person person) {
		// new level people are added without notification
		// later at end of this method this level is added to the cadastre
		Level l = this.newLevel();

		// retrieve the old level
		Level oldLevel = person.getLevel();

		// create a set of those people that will move
		Set<Person> movers = new LinkedHashSet<Person>();

		// move children of these people
		List<Person> children = person.getChildren();

		// iterate the children
		for (Person p : children) {
			// if the child is still living in the same level
			if (oldLevel.equals(p.getLevel())) {
				movers.add(p);
			}
		}

		// move spouse
		if (person.getSpouse() != null)
			movers.add(person.getSpouse());

		movers.add(person);

		for (Person p : movers) {
			// remove from old level
			p.getLevel().removeInhabitant(p);
			// add to the new level
			l.addInhabitantWithoutNotification(p);
			Idunn.getIdunn().addedToLevel(p);
			// count
			if (this.savedata) {
				this.splitCounter[p.getSex().getValue()] += 1;
				this.overallCounter[p.getSex().getValue()] += 1;
			}
			// history
			p.addtoHistory(Odin.getCurrentTime(), "subject of splitting event", p);
		}

		// add the level to the register
		Odin.getOdin().getLevelRegister().addLevel(l);

	}

	/**
	 * Create and return a new level
	 *
	 * @return l
	 */
	private Level newLevel() {
		// new level
		Level l = new Level(Level.DWELLING_RANK);

		l.setID(Odin.getOdin().getLevelId(Level.DWELLING_RANK));

		return l;
	}

}
