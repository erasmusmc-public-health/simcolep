/*
 * This file is part of SIMCOLEP, a simulation model developed by the Department of Public Health,
 * Erasmus MC, University Medical Center Rotterdam, Rotterdam, The Netherlands.
 *
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/
 * or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA
 *
 * You are free to:
 * Share � copy and redistribute the material in any medium or format
 * Under the following terms:
 * Attribution � You must give appropriate credit, provide a link to the license, and indicate if changes were
 * made. You may do so in any reasonable manner, but not in any way that suggests the licensor endorses
 * you or your use.
 * NonCommercial � You may not use the material for commercial purposes.
 * NoDerivatives � If you remix, transform, or build upon the material, you may not distribute the modified material.
 */

/*
 * Created on 04-Nov-2005
 *
 */
package simcolep.asgard;

import java.io.Serializable;

import org.jdom.DataConversionException;

import nl.mgz.simulation.discreteevent.EventManager;
import nl.mgz.simulation.discreteevent.Schedulable;
import simcolep.demo.AbstractPopulationRegister;
import simcolep.demo.Cadastre;
import simcolep.demo.IPopulationRegister;
import simcolep.demo.Level;
import simcolep.demo.ILevelRegister;
import simcolep.demo.Person;
import simcolep.demo.PopulationRegister;
import simcolep.demo.event.Age;
import simcolep.exceptions.SimulationException;
import simcolep.fun2.Archimedes;
import simcolep.sim.DiseaseRun;
import simcolep.sim.Gylfi;
import simcolep.tools.PN;

/**
 * <b>Odin</b> (Old Norse <b>��inn</b>) is considered the chief god in Norse
 * mythology. His name is related to ��r, meaning "excitation," "fury" or
 * "poetry," and his role, like many of the Norse pantheon, is complex: He is
 * god of wisdom, war, battle and death. He is also attested as being a god of
 * magic, poetry, prophecy, victory, and the hunt. <br>
 * <br>
 * <code>Odin</code> rules the demographic processes in the simulation software
 *
 * @author ir. E.A.J. Fischer
 * @version 1.0.x
 */
public final class Odin implements Serializable, Ase {

	/**
	 * serial number
	 */
	private static final long serialVersionUID = 126427195779647907L;

	/** instance of Odin for each population */
	private static Odin instance = null;

	/** the event manager */
	private static EventManager em = null;

	private static boolean dataPopRegister = false;

	private static boolean dataLevelRegister = false;

	/**
	 * Call collect method for the data of all registers (incl. Freya and Loki)
	 *
	 * @param time
	 */
	public static void collectData(double time) {

		if (instance == null)
			return;
		// PopRegister
		instance.populationRegister.collectData(time);
		// Household register
		instance.levelRegister.collectData(time);
		// Freya
		instance.freya.save(time);
		// Loki
		instance.loki.save(time);

	}

	/**
	 * @return current simulation time
	 */
	public static double getCurrentTime() {
		if (em == null)
			return Double.NEGATIVE_INFINITY;

		return em.getTime();
	}

	/**
	 * Get the event manager
	 *
	 * @return em
	 */
	public static EventManager getEventManager() {
		return em;

	}

	/**
	 *
	 * @param popnum
	 * @return instance of Odin with population number popnum
	 */
	public static Odin getOdin() {
		// return instance
		return instance;
	}

	/**
	 * Initialise Odin. It will create NoOfPop Odin's and populations
	 *
	 * @throws IllegalAccessException
	 * @throws InstantiationException
	 * @throws DataConversionException
	 */
	public static void init() throws DataConversionException, InstantiationException, IllegalAccessException {
		// save personal history
		Person.saveHistory = Gylfi.ask(PN.SAVEHISTORY);

		// set age class width
		AbstractPopulationRegister.ageclasswidth = Archimedes.askDouble(PN.AGECLASSWIDTH);
		AbstractPopulationRegister.ageclassmax = Archimedes.askDouble(PN.AGECLASSMAX);

		// instances
		instance = new Odin();
		// create
		instance.create();
	}

	/**
	 *
	 * @return DiseaseRun.inSafeMode()
	 */
	public static boolean inSafeMode() {
		return DiseaseRun.inSafeMode();
	}

	/**
	 * Schedule the schedulable in the event manager
	 *
	 * @param s
	 */
	public static void schedule(Schedulable s) {
		em.schedule(s);

	}

	/**
	 * Set whether or not to save household data when all data is saved
	 *
	 * @param b
	 */
	public static void setDataLevelRegister(boolean b) {
		Odin.dataLevelRegister = b;
	}

	/**
	 * Set whether or not to save population data when all data is saved
	 *
	 * @param b
	 */
	public static void setDataPopRegister(boolean b) {
		Odin.dataPopRegister = b;

	}

	/**
	 * Set the eventmanager
	 *
	 * @param sem
	 */
	public static void setEventManager(EventManager sem) {
		Odin.em = sem;
	}

	/** freya regulates birth and marriages */
	protected Freya freya = null;

	/** hel regulates death */
	protected transient Hel hel = null;

	/** loki regulates movement and migration */
	protected Loki loki = null;

	/** population register */
	protected IPopulationRegister<Person> populationRegister = null;

	/** register of the levels */
	protected ILevelRegister<Level> levelRegister = null;
	/** level Id's */
	protected int[] levelIds = null;

	/**
	 *
	 * @param popnum
	 * @throws Exception
	 */
	protected Odin() {
		super();
		levelIds = new int[25];
	}

	/**
	 *
	 * @param time
	 *            - time of error occurence
	 * @param errorcode
	 */
	public void collectError(double time, double errorcode) {

		this.getPopulationRegister().collectDataAtTimeAs(time, errorcode);
		this.levelRegister.collectDataAtTimeAs(time, errorcode);
	}

	/**
	 *
	 * @return freya
	 */
	public Freya getFreya() {
		return this.freya;
	}

	/**
	 *
	 * @return hel
	 */
	public Hel getHel() {
		return this.hel;
	}

	/**
	 * Get an unique id for a level
	 *
	 * @param rank
	 * @return
	 */
	public int getLevelId(int rank) {

		return levelIds[rank]++;
	}

	/**
	 * The register of levels
	 *
	 * @return this.levelregister
	 */
	public ILevelRegister<Level> getLevelRegister() {
		return this.levelRegister;
	}

	/**
	 *
	 * @return loki
	 */
	public Loki getLoki() {
		return this.loki;
	}

	/**
	 * Return the size of modelled population at the current time. Odin decides
	 * whether this number is the actual simulated population or some other
	 * value calculated. This depends on what type of population is used.
	 *
	 * @return this.popregister.getSize()
	 */
	public double getPopSize() {
		return this.populationRegister.getSize();
	}

	/**
	 *
	 * @return population register
	 */
	public IPopulationRegister<Person> getPopulationRegister() {
		return this.populationRegister;
	}

	/**
	 * Open databases for recording. This is different than saveing data. Data
	 * bases report each individual seperately and detailed. No databases
	 * implemented
	 */
	public void openDatabase() {

	}

	/**
	 *
	 * @see simcolep.asgard.Ase#ragnarok()
	 */
	@Override
	public void ragnarok() {
		this.getFreya().ragnarok();
		this.getHel().ragnarok();
		this.getLoki().ragnarok();

		// reset Ymer
		Ymer.reset();
	}

	/**
	 * Switch on and off saving of data
	 *
	 * @param b
	 */
	@Override
	public void setSave(boolean b) {
		this.freya.setSave(b);

		this.loki.setSave(b);

		this.populationRegister.storeData(Odin.dataPopRegister);

		this.levelRegister.storeData(Odin.dataLevelRegister);
	}

	/**
	 * Creates a Odin as specified with the type of run (either quick or full)
	 *
	 * @throws IllegalAccessException
	 * @throws InstantiationException
	 * @throws DataConversionException
	 */
	@SuppressWarnings(PN.UNCHECKED)
	protected void create() throws DataConversionException, InstantiationException, IllegalAccessException {
		// Freya
		this.freya = new Freya(this);
		// Gymir
		this.hel = new Hel();
		// Loki
		this.loki = new Loki();

		// Population and Level register
		// Ymer can only create midgard if all functions are set into
		// Archimedes!
		Ymer ymer = new Ymer();

		ymer.setRegisters(new PopulationRegister(), new Cadastre(Archimedes.askInteger(PN.LEVELDEPTH) + 1));

		ymer.createMidgard();

		// register to Odin
		this.populationRegister = ymer.getPopulationRegister();
		this.levelRegister = ymer.getLevelRegister();

		// activate population
		for (Object p : this.populationRegister.getRegister())
			((Person) p).activate();

		// check ageing
		if (Odin.inSafeMode()) {
			for (Person p : this.populationRegister.getRegister()) {
				if (!p.isTargetOf(Age.class.getName())) {
					// if the current age class and the ageclass at death differ
					int curAgeClass = p.getAgeclass();
					int deathAgeClass = (int) ((p.getDeathAge()
							- p.getDeathAge() % AbstractPopulationRegister.ageclasswidth)
							/ AbstractPopulationRegister.ageclasswidth);
					if (deathAgeClass > curAgeClass)
						throw new SimulationException(SimulationException.NOTAGEING_MESSAGE + p);
				}
			}
		}

		// start scheduling births
		this.freya.wedding();
		// schedule first births
		this.freya.scheduleBirth();

	}

}
