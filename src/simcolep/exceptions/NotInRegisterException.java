/*
 * This file is part of SIMCOLEP, a simulation model developed by the Department of Public Health,
 * Erasmus MC, University Medical Center Rotterdam, Rotterdam, The Netherlands.
 *
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/
 * or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA
 *
 * You are free to:
 * Share � copy and redistribute the material in any medium or format
 * Under the following terms:
 * Attribution � You must give appropriate credit, provide a link to the license, and indicate if changes were
 * made. You may do so in any reasonable manner, but not in any way that suggests the licensor endorses
 * you or your use.
 * NonCommercial � You may not use the material for commercial purposes.
 * NoDerivatives � If you remix, transform, or build upon the material, you may not distribute the modified material.
 */

/**
 * created: 05-Jul-2006
 */
package simcolep.exceptions;

import simcolep.asgard.Odin;
import simcolep.demo.Level;
import simcolep.demo.Person;
import simcolep.demo.Sex;
import simcolep.lepra.Morbus;
import simcolep.tools.PN;

/**
 * This exception is used when a Person, Level or Morbus is not found in a
 * register where it was expected. The message of the exception contains a check
 * for those register where the object could have been expected
 *
 *
 * @author ir. E.A.J. Fischer
 * @version 1.0
 */
public final class NotInRegisterException extends SimulationException {

	/**
	 *
	 */
	private static final long serialVersionUID = 5235053853631287693L;

	/**
	 *
	 * @param l
	 * @return
	 */
	private static String levelMessage(Level l) {
		String exc = "\r\n" + l.toString() + "\r\n";
		exc += "Cadastre:" + Odin.getOdin().getLevelRegister().contains(l);
		return exc;
	}

	/**
	 *
	 * @param p
	 * @return
	 */
	@SuppressWarnings("unchecked")
	private static String personMessage(Person p) {
		StringBuffer exc = new StringBuffer();
		exc.append(PN.LINESEPARATOR + p.toString() + PN.LINESEPARATOR);
		Odin odin = Odin.getOdin();
		exc.append("Population register:" + PN.TAB + odin.getPopulationRegister().contains(p) + PN.LINESEPARATOR);
		if (!p.ismarried())
			exc.append("Singles register:" + PN.TAB + odin.getFreya().hasSingle(p) + PN.LINESEPARATOR);
		else if (p.getSex() == Sex.FEMALE)
			exc.append("Mothers register:" + PN.TAB + odin.getPopulationRegister().contains(p) + PN.LINESEPARATOR);

		return exc.toString();
	}

	/**
	 *
	 * @param l
	 */
	public NotInRegisterException(Level l) {
		super(levelMessage(l));
	}

	/**
	 *
	 * @param m
	 */
	public NotInRegisterException(Morbus m) {
		super(m.toString());
	}

	/**
	 *
	 * @param p
	 */
	public NotInRegisterException(Person p) {
		super(personMessage(p));

	}

	/**
	 * @param arg0
	 */
	public NotInRegisterException(String arg0) {
		super(arg0);

	}

}
