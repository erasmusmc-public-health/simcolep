/*
 * This file is part of SIMCOLEP, a simulation model developed by the Department of Public Health, 
 * Erasmus MC, University Medical Center Rotterdam, Rotterdam, The Netherlands.
 *
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/ 
 * or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA
 *
 * You are free to:
 * Share � copy and redistribute the material in any medium or format
 * Under the following terms:
 * Attribution � You must give appropriate credit, provide a link to the license, and indicate if changes were 
 * made. You may do so in any reasonable manner, but not in any way that suggests the licensor endorses
 * you or your use.
 * NonCommercial � You may not use the material for commercial purposes.
 * NoDerivatives � If you remix, transform, or build upon the material, you may not distribute the modified material.
 */

/**
 * created: 14-Aug-2006
 */
package simcolep.exceptions;

import simcolep.tools.PN;


/**
 * This Exception is used for things in the simulation that are not allowed
 * As opposed to the normal IllegalArgumentException can be used for putting wrong arguments in method or so this can be used for things that are not allowed in the simulation (already dead people etc).  
 * 
 * @author ir. E.A.J. Fischer
 * @version 1.0
 */
public class SimulationException 
		extends IllegalArgumentException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5957756719667134454L;
	
	/**simulation input file not found message*/
	public static final String SIMIN_FILE_NOT_FOUND_MESSAGE = "SIMCOLEP cannot start up. "+System.getProperty("line.separator") +
		        			"Obligatory input file could not be found" + 
		        			" or is corrupted." + System.getProperty("line.separator")+
		        			"Create inputfile: " +System.getProperty("line.separator");
		        			
	
	/**
	 * 
	 */
	public static final String NOTIMPLEMENTED = 
		"This method is not yet implemented ";
	/***
	 * 
	 */
	public static final String SHOULDBEDEAD = 
		"This person or morbus should be dead at this time! ";

	public static final String NOSEX = "no sex defined ";

	public static final String UNKNOWNFUNCTION_MESSAGE = "Unknown function: ";

	public static final String UNKNOWNPARAMETER_MESSAGE = "Unknown parameter: ";
	
	public static final String NOTADDED = "not added to register ";

	public static final String NOTREMOVED = "not removed from register ";

	public static final String NOTAGEING_MESSAGE = "Person is not ageing. Is his/her name McCleod? ";

	public static final String RANDOMVARIATEINTERVAL = "random variates should be an element of [0,1]: ";

	public static final String MORBUSNOTPRESENT = "No Morbus found";

	public static final String INTHEPAST = "Event in the past";

	public static final String NODEATHSET = "Person  has no death. Is his name Duncan McCleod?";

	public static final String TYPESDONOTMATCH = "Types do not match ";

	public static final String NOTSAMEDWELLING = "Not in the same dwelling or contact level" ;

	public static final String NOTINITIALIZED = "Not initialized";

	public static final String UNKNOWNOUTPUT_MESSAGE = "This output is unknown for this class";

	

	/**
	 * 
	 */
	public SimulationException() {
		super();
		
	}

	/**
	 * @param s
	 */
	public SimulationException(String s) {
		super(s+PN.LINESEPARATOR+PN.LINESEPARATOR);
		
	}

	/**
	 * @param message
	 * @param cause
	 */
	public SimulationException(String message, Throwable cause) {
		super(message+PN.LINESEPARATOR+PN.LINESEPARATOR, cause);
		
	}

	/**
	 * @param cause
	 */
	public SimulationException(Throwable cause) {
		super(cause);
		
	}

}
