/*
 * This file is part of SIMCOLEP, a simulation model developed by the Department of Public Health, 
 * Erasmus MC, University Medical Center Rotterdam, Rotterdam, The Netherlands.
 *
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/ 
 * or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA
 *
 * You are free to:
 * Share � copy and redistribute the material in any medium or format
 * Under the following terms:
 * Attribution � You must give appropriate credit, provide a link to the license, and indicate if changes were 
 * made. You may do so in any reasonable manner, but not in any way that suggests the licensor endorses
 * you or your use.
 * NonCommercial � You may not use the material for commercial purposes.
 * NoDerivatives � If you remix, transform, or build upon the material, you may not distribute the modified material.
 */
/**
 * created: 23-Aug-2006
 */
package simcolep.sim;

import java.util.Map;
import java.util.LinkedHashMap;

import simcolep.tools.PN;

/**
 * A mythical king of Sweden, <b> Gylfi</b> was in contact with the king of the Asen, Odin<br>
 * Here <code> Gylfi</code> knows all settings of methods 
 * 
 * @author ir. E.A.J. Fischer
 * @version 1.0
 */
public final class Gylfi {

	private static final String GYLFISBRAIN_HEADERTEXT = "******Gylfi's deeper thoughts********"; //$NON-NLS-1$
	
	private static final String NOBRAINTEXT = "Gylfi has no brain"; //$NON-NLS-1$

	private static final String ENDTAG = "***************************************";
	
	/**Brain containing settings*/
	private static Map<String, Boolean> Brain = null;
		
	
	/**
	 * 
	 *
	 */
	private static void makeBrain() {
		Brain = new LinkedHashMap<String,Boolean>();
	}
	/**
	 * 
	 *
	 */
	public static void reset()
	{
		Brain = null;
	}
	/**
	 * Set the brain in one move
	 * @param m
	 */
	public static void set(Map<String,Boolean> m)
	{
		if(Brain==null)
			Brain = m;
		
		else 
			throw new IllegalArgumentException(
					"Brain is not empty");
	}
	
	/**
	 * 
	 * @param s
	 * @param b
	 */
	private static void set(String s, Boolean b)
	{
		if(Brain == null)
			makeBrain();
		
		Brain.put(s,b);
	}
	/**
	 * Add a method to the brain
	 * @param s
	 * @param b
	 */
	public static void tell(String s, Boolean b)
	{
		//if neccesary create brain
		if(Brain==null)
			Gylfi.set(s,b);
		
		//check if this method was not already in the brain
		else if(Brain.containsKey(s))
			throw new IllegalArgumentException(
					"Gylfi's brain already contains " +
					"this method: "+s+"\r\n" +
					"consider using convinceGylfi");
		else
		//put
		Gylfi.set(s,b);
		
	}
	/**
	 * Add a method to the brain
	 * @param s
	 * @param b
	 */
	public static void tell(String s, boolean b)
	{
		//add a Boolean
		tell(s, new Boolean(b));
	}
	
	/**
	 * This method always gets the boolean into the brain
	 * @param s
	 * @param b
	 */
	public static void convince(String s, boolean b)
	{
		//don't ask questions just put it into the brain.
		Gylfi.set(s,b);
	}
	
	/**
	 * ask a certain method
	 * If method is not present return false
	 * @param s
	 * @return false if not in the brain otherwise value
	 */
	public static boolean ask(String s)
	{
		if(Brain == null)
			throw new IllegalArgumentException
					(NOBRAINTEXT);
		//if the brain does not contain the method 
		if(!Brain.containsKey(s))
			return false;
		
		//get the method
		return Brain.get(s).booleanValue();
		
	}
	
	/**
	 * Return the brain of Gylfi as a string
	 * @return
	 */
	public static String BraintoString() {
		StringBuffer brain = new StringBuffer(GYLFISBRAIN_HEADERTEXT+PN.LINESEPARATOR);
		
		for(String s : Brain.keySet())
			brain.append(PN.LINESEPARATOR + s +PN.TAB+Brain.get(s).toString());
				
		brain.append(PN.LINESEPARATOR+ENDTAG);
		return brain.toString();
	}
	
	

	
	
}
