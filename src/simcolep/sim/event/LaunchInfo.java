/*
 * This file is part of SIMCOLEP, a simulation model developed by the Department of Public Health, 
 * Erasmus MC, University Medical Center Rotterdam, Rotterdam, The Netherlands.
 *
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/ 
 * or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA
 *
 * You are free to:
 * Share � copy and redistribute the material in any medium or format
 * Under the following terms:
 * Attribution � You must give appropriate credit, provide a link to the license, and indicate if changes were 
 * made. You may do so in any reasonable manner, but not in any way that suggests the licensor endorses
 * you or your use.
 * NonCommercial � You may not use the material for commercial purposes.
 * NoDerivatives � If you remix, transform, or build upon the material, you may not distribute the modified material.
 */
package simcolep.sim.event;


import java.text.DateFormat;
import java.util.Locale;

import simcolep.tools.PN;
import simcolep.tools.io.SimulationLog;
import simcolep.tools.message.Message;
import simcolep.tools.message.Ratatosk;

import nl.mgz.simulation.discreteevent.Event;
import nl.mgz.simulation.discreteevent.Targetable;

/**
 * This event sends out a message with the setting of this run. That is it.
 * Time of this message is therefore always 0.
 * 
 * @author ir.E.A.J.Fischer
 * @version 1.0
 * 
 *
 */
public final class LaunchInfo extends Event {
	private static final String TYPE_TEXT1 = "type{unknown}"; //$NON-NLS-1$
	private static final String DATE_TEXT = "date:{"; //$NON-NLS-1$
	private static final String TYPE_TEXT = "type:{"; //$NON-NLS-1$
	private static final String LENGTH_TEXT = "length:{"; //$NON-NLS-1$
	private static final String CLOSE_BRACE = "}"; //$NON-NLS-1$
	private static final String NAME_TEXT = "LaunchInfo:"; //$NON-NLS-1$
	/**
	 * 
	 */
	private static final long serialVersionUID = 8584052681189409023L;
	/**
	 * The higher the priority the more important and sooner an event is executed as compared to events at exactly the same moment.<br>
     * To have a consistancy in prioritizing the following rules are used;
     * <li> Priority from high to low: Simulation events > Disease events > Demographic
     * <li> Priority of simulation events > 500
     * <li> Priority of disease events > 200
     * <li> Priority of demographic events > 100
     * <li> further ordering is logical in biological or epidemiological sense
     *  
     * @see nl.mgz.simulation.discreteevent.Event.priority
     * */
	private static final int PRIORITY = 599;
	
	/**default is unspecified input*/
	private static String MAINFILENAME = "UNSPECIFIED";
	
	/**launch information*/
	private String info = null;
	
	/**
	 * @param source
	 * @param target
	 * @param time
	 */
	public LaunchInfo(Object source, Targetable target, double time) {
		
		super(source, target,0.0);


		if(time>0.0) 
			throw new IllegalArgumentException(
					"LaunchInfo is executed at the beginning of the run");
	}

	/**
	 * 
	 */
	public LaunchInfo() {
		super();
		
	}

	/**
	 * launch information has the following format:
	 * LaunchInfo:\r\n
	 * length:{length of the run}\r\n
	 * type:{'type of run'}\r\n
	 * 
	 * @see nl.mgz.simulation.discreteevent.Event#execute()
	 */
	@Override
	public void execute() {
		//pass the launch information on
		//launch information has the following format:
		//LaunchInfo:\r\n
		//length:{length of the run}\r\n
		//type:{'type of run'}\r\n
		//date:{'start date of run'}\r\n
		Ratatosk.notify(Message.formulate(info),
				this,
				this);
		
		//set this information to the simulation log sothat this can be used
		SimulationLog.setHeaderText(info);

	}
	
	
	/**
	 * Launch information has the following format:
	 * LaunchInfo:\r\n
	 * length:{length of the run}\r\n
	 * type:{'type of run'}\r\n
	 * date:{'start date of run'}\r\n
	 * @param length
	 * @param type
	 * @param date
	 */
	public void setLaunchInfo(double length, 
			String type, 
			java.util.Date date)
	{
		this.info = NAME_TEXT+PN.TAB+MAINFILENAME+PN.LINESEPARATOR+
		LENGTH_TEXT+length+CLOSE_BRACE+PN.LINESEPARATOR+
		TYPE_TEXT+type+CLOSE_BRACE+PN.LINESEPARATOR+
		DATE_TEXT+DateFormat.getDateTimeInstance(DateFormat.SHORT,
                DateFormat.SHORT,
                Locale.getDefault()).format(date)+CLOSE_BRACE+PN.LINESEPARATOR;
	}
	/**
	 * Launch information has the following format:
	 * LaunchInfo:\r\n
	 * length:{length of the run}\r\n
	 * type:{'type of run'}\r\n
	 * date: is set to Now!
	 * @param length
	 */
	public void setLaunchInfoLength(double length) {
		this.info = NAME_TEXT+PN.TAB+MAINFILENAME+PN.LINESEPARATOR
		+LENGTH_TEXT+length+CLOSE_BRACE
		+PN.LINESEPARATOR+TYPE_TEXT1
		+PN.LINESEPARATOR;
		
	}

	/**
	 * 
	 * @param time
	 * @param type
	 */
	public void setLaunchInfo(double time, String type) {
		this.setLaunchInfo(time, 
				type, 
				new java.util.Date(System.currentTimeMillis()));
		
	}

	 /**
	    * @see nl.mgz.simulation.discreteevent.Event#getPriority()
	    */
	    @Override
	    public int getPriority()
	    {
	    	return PRIORITY;
	    }

	    /**
	     * 
	     * @param name
	     */
	    public static void setMainfile(String name) {
			//
	    	MAINFILENAME = name;
		
	    }
}
