/*
 * This file is part of SIMCOLEP, a simulation model developed by the Department of Public Health, 
 * Erasmus MC, University Medical Center Rotterdam, Rotterdam, The Netherlands.
 *
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/ 
 * or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA
 *
 * You are free to:
 * Share � copy and redistribute the material in any medium or format
 * Under the following terms:
 * Attribution � You must give appropriate credit, provide a link to the license, and indicate if changes were 
 * made. You may do so in any reasonable manner, but not in any way that suggests the licensor endorses
 * you or your use.
 * NonCommercial � You may not use the material for commercial purposes.
 * NoDerivatives � If you remix, transform, or build upon the material, you may not distribute the modified material.
 */
/*
 * Created on 01-Dec-2005
 *
 */
package simcolep.sim.event;

import simcolep.asgard.Idunn;
import simcolep.asgard.Odin;
import simcolep.exceptions.SimulationException;


import nl.mgz.simulation.discreteevent.Event;
import nl.mgz.simulation.discreteevent.Targetable;

/**
 *Press Record or start recording data of this simulation
 *
 * 
 * 
 * @author ir E.A.J. Fischer
 * @version 0.1.2 -- Last Modified 01-Dec-2005
 */
public final class PressRecord extends Event {
	/**
	 * 
	 */
	private static final long serialVersionUID = -3824753853416391528L;
	/**
	 * The higher the priority the more important and sooner an event is executed as compared to events at exactly the same moment.<br>
     * To have a consistancy in prioritizing the following rules are used;
     * <li> Priority from high to low: Simulation events > Disease events > Demographic
     * <li> Priority of simulation events > 500
     * <li> Priority of disease events > 200
     * <li> Priority of demographic events > 100
     * <li> further ordering is logical in biological or epidemiological sense
     *  
     * @see nl.mgz.simulation.discreteevent.Event.priority
     * */
	private static final int PRIORITY = 502;
	/**value returned when there are no times left*/
	public static final double NOTIME = -999;
 
    private static double[] recordtimes = null;
    
    private static int recordtime_index =	-1;
  
    /**
     * @param source
     * @param target
     * @param time
     * 
     */
    public PressRecord(Object source, Targetable target, double time) {
        super(source, target, time);
      
       }

    
    /**
     * Set saving for Freya and Loki to start and than schedules the first record event
     * 
     * 
     * @see nl.mgz.simulation.discreteevent.Event#execute()
     */
    @SuppressWarnings("unchecked")
    @Override
    public void execute() {
       
        /*start saving 
         /*Odin*/
        Odin.getOdin().setSave(true);
        
        /*Idunn*/
        Idunn.getIdunn().setSave(true);
        
        /*create first record event*/
       Record record 
       		= new Record(this,
       				this.getTarget(),
       				PressRecord.getNextRecordTime());
      
       /*schedule*/
       Odin.getEventManager().schedule(record);
    }
    
    /**
     * @see nl.mgz.simulation.discreteevent.Event#getPriority()
     */
     @Override
     public int getPriority()
     {
     	return PRIORITY;
     }

     /**
      * 
      * @return
      */
	public static double getNextRecordTime() {
		//increase index
		recordtime_index++;
		//no more
		if(recordtime_index>=recordtimes.length)
			return NOTIME;
		
		//return time
		return recordtimes[recordtime_index];
	}


	public static void setRecordTimes(double start,
			double interval, double stop) {
		if(stop < start)
			throw new SimulationException("Run ends before start of recording!");
		
		//set index to 0
		recordtime_index = -1;
		//determine number of intervals
		int intervals = 1+(int)((stop - start)/interval);
		//create array
		recordtimes = new double[intervals];
		//set times
		for(int i =0; i<recordtimes.length;i++)
		{
			recordtimes[i] = start + i*interval;
		}
		
		return;
	}

	/**
	 * 
	 * @return 
	 */
	public static double getCurrentRecordTime() {
		//if index was not yet activated return first
		if(recordtime_index-1<0)
			return recordtimes[1];
		//no more
		if(recordtime_index>=recordtimes.length)
			return NOTIME;
		
		//return time
		return recordtimes[recordtime_index];
	}

}
