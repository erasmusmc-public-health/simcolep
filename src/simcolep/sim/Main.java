/*
 * This file is part of SIMCOLEP, a simulation model developed by the Department of Public Health,
 * Erasmus MC, University Medical Center Rotterdam, Rotterdam, The Netherlands.
 *
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/
 * or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA
 *
 * You are free to:
 * Share � copy and redistribute the material in any medium or format
 * Under the following terms:
 * Attribution � You must give appropriate credit, provide a link to the license, and indicate if changes were
 * made. You may do so in any reasonable manner, but not in any way that suggests the licensor endorses
 * you or your use.
 * NonCommercial � You may not use the material for commercial purposes.
 * NoDerivatives � If you remix, transform, or build upon the material, you may not distribute the modified material.
 */
/*
 * Created on 16-Nov-2005
 *
 */
package simcolep.sim;

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.jdom.DataConversionException;
import org.jdom.JDOMException;

import nl.mgz.simulation.discreteevent.CalendarQueueEventManager;
import nl.mgz.simulation.discreteevent.EventManager;
import nl.mgz.simulation.discreteevent.Targetable;
import simcolep.asgard.Idunn;
import simcolep.asgard.Ragnarok;
import simcolep.exceptions.SimulationEndException;
import simcolep.exceptions.SimulationException;
import simcolep.fun2.Archimedes;
import simcolep.fun2.Constant;
import simcolep.fun2.Function;
import simcolep.lepra.State;
import simcolep.sim.event.LaunchInfo;
import simcolep.sim.event.Stop;
import simcolep.tools.PN;
import simcolep.tools.TimeMeasurer;
import simcolep.tools.io.FileRegister;
import simcolep.tools.io.SimulationLog;
import simcolep.tools.message.Ratatosk;
import simcolep.tools.xmlparser.XMLInput;

/**
 *
 * The simulation always starts in this class. It is the main class
 *
 *
 * @author ir E.A.J. Fischer
 * @version 0.1.2 -- Last Modified 16-Nov-2005
 */
public final class Main {

	private static final String XML_EXTENSION = ".xml"; //$NON-NLS-1$

	/** file with this name should be present if not determined differently */
	public static final String DEFAULTINPUTFILE = "simin.xml";

	/** Error code in output */
	public static final double ERRORCODE = -999;

	/** Code for manually stopped runs */
	private static final double STOPCODE = -666;

	/** event manager */
	private static EventManager em = null;

	/** instance responsible for a disease run */
	public static DiseaseRun dr = null;

	/** Map containing outputs for disease run */
	public static Map<String, List<Double>> output = new LinkedHashMap<String, List<Double>>();

	/**
	 * this boolean is true when a disease run is running This is used in method
	 * collectData to know which classes to ask for output
	 */
	public static boolean diseaserun = false;

	/**
	 * Collect the data at that moment by calling the right objects to do it
	 *
	 * @param time
	 * @throws Exception
	 * @throws IOException
	 * @throws IOException
	 */
	public static void collectData(double time) {
		try {
			// disease run collects
			dr.collect(time);
		} catch (IOException ioe) {
			handleException(ioe);
		}
	}

	/**
	 * Reset / delete / destroy / kill everything of the simulation!
	 *
	 */
	public static void crushMain() {

		// RAGNAROK will set all instances of gods to null!
		Ragnarok.start();

		// Eventmanager
		em = null;

		// Ratatosk
		Ratatosk.kill();

		//
		diseaserun = false;

		// disase run
		dr = null;

		// collect the garbage
		System.gc();

	}

	/**
	 * 
	 * @return
	 */
	public static int currentrun() {
		return dr.getRepeat();
	}

	/**
	 * 
	 * @param e
	 * 
	 */
	public static void handleException(Exception e) {

		if (e.getClass().equals(SimulationException.class)) {
			try {
				// simulation exception can be handled by stopping current
				// repeat and continue with next
				Main.handleSimulationException(e);
			} catch (IOException e1) {

				// create an error log and throw it anyways
				// create error log
				SimulationLog.speciallog(SimulationLog.ERRORLOG, errorMessage(e1), false, true, true);
				// throw an exception with which we can live
				throw new SimulationEndException(e1);
			}
			return;
		} else { // create an error log and throw it anyways
					// create error log
			SimulationLog.speciallog(SimulationLog.ERRORLOG, errorMessage(e), false, true, true);
			// throw an exception with which we can live
			throw new SimulationEndException(e);
		}

	}

	/**
	 * Initialize the program
	 * 
	 * == reading all input and setting the eventmanagers settings
	 * 
	 * @param start
	 *            start xml file
	 * @param args
	 *            name and path to the start file
	 * @throws JDOMException
	 * @throws IOException
	 * @throws IllegalAccessException
	 * @throws InstantiationException
	 * 
	 * @throws Exception
	 *
	 */
	public static void initialize(XMLInput start, String[] args)
			throws InstantiationException, IllegalAccessException, IOException, JDOMException {

		// read all input
		Main.readInput(start, args);

		// set eventmanager settings
		CalendarQueueEventManager.managerSettings(Archimedes.askInteger(PN.BUCKETSIZE),
				Archimedes.askLong(PN.BUCKETWIDTH));

		// initialize the Herbarium
		Herbarium.initHerbarium(Archimedes.askInteger(PN.REPEATS), Gylfi.ask(PN.SEED));

	}

	/**
	 * The arguments that can be given to the program are: args[0] - start input
	 * filename <b> default: simin.xml</b><br>
	 * args[1] - start input path <b>default:
	 * System.<i>getProperty</i>("user.dir")<br>
	 * 
	 * @param args
	 * @throws IllegalAccessException
	 * @throws InstantiationException
	 * @throws JDOMException
	 * @throws IOException
	 * 
	 * 
	 */

	public static void main(String[] args)
			throws IOException, JDOMException, InstantiationException, IllegalAccessException {

		// determine the start of simulation input file
		XMLInput start = null;
		if (args.length == 0)
			start = new XMLInput(DEFAULTINPUTFILE);
		if (args.length == 1)
			start = new XMLInput(args[0]);
		if (args.length >= 2)
			start = new XMLInput(args[0], args[1]);

		// initialise settings
		// which is reading all input files
		initialize(start, args);

		// start the time measurer
		TimeMeasurer time = TimeMeasurer.startNew(start.getTitle());

		// get a fresh event manager
		em = CalendarQueueEventManager.getInstance();

		em.initialize();

		DiseaseRun.setSafeMode(Gylfi.ask(PN.SAFEMODE));

		// run
		try {
			Main.diseaseRun();
		} catch (Exception e) {
			handleException(e);

		}

		// End and return full dialog if gui
		time.endWithReport(Gylfi.ask(PN.OVERWRITE_ALL));
		// simulation logs
		SimulationLog.close();
		SimulationLog.closeSpecial(true);

		// crush everything that is still running anywhere
		crushMain();

	}

	/**
	 * Method starts with reseting Archimedes, Gylfi and Herbarium sothat new
	 * input can be set without problem
	 * 
	 * Starting from start it will read all files specified there and read the
	 * input from it.
	 * 
	 * @param start
	 * @param args
	 *            name and path of the start file
	 * @throws IllegalAccessException
	 * @throws InstantiationException
	 * @throws JDOMException
	 * @throws IOException
	 * 
	 */
	public static void readInput(XMLInput start, String[] args)
			throws InstantiationException, IllegalAccessException, IOException, JDOMException

	{
		// reset Archimedes
		Archimedes.reset();

		// reset Gylfi
		Gylfi.reset();

		// reset Herbarium
		Herbarium.resetHerbarium();

		// set the file name as launch information
		LaunchInfo.setMainfile(start.getInternalFile().getName());

		// start reading the start file
		// should contain necessary information to obtain all other input
		readFile(start);

		// set new default in and output paths
		String in_path = FileRegister.getPath(PN.INPUT_DIR, FileRegister.IN);
		String out_path = FileRegister.getPath(PN.OUTPUT_DIR, FileRegister.OUT);

		// set
		XMLInput.setPaths(in_path, out_path);

		// get all the input files
		Map<String, String> files = FileRegister.getFiles(FileRegister.IN);

		// get the values using a set so duplicates are eliminated
		Set<String> hs = new LinkedHashSet<String>();

		if (files != null)
			hs.addAll(files.values());

		// remove (potentially) the name of the start file. This one was already
		// read
		if (args.length == 0)
			hs.remove(DEFAULTINPUTFILE);
		else
			hs.remove(args[args.length - 1]);
		// set functions into Archimedes, methods
		// into Gylfi and plant seed into the herbarium
		for (String s : hs) {
			// if the extension is .xml use it otherwise don't
			if (s.contains(XML_EXTENSION)) {

				// create input-file-parser
				XMLInput p = new XMLInput(s);

				// read out the input
				readFile(p);

			}

		}
	}

	/**
	 * Schedules a stop event and a LaunchInfo event
	 * 
	 * @param source
	 * @param time
	 */
	public static void scheduleStop(Object source, Object info, double time) {
		// schedule stop event
		Stop stop = new Stop(source, em, time);
		em.schedule(stop);

		// if no info is given use the source
		if (info == null)
			info = source;
		// schedule LaunchInfo
		LaunchInfo li = new LaunchInfo(source, (Targetable) em, 0);
		li.setLaunchInfo(time, info.toString());
		em.schedule(li);

	}

	/**
	 * Sets the eventmanager
	 * 
	 * @param newEm
	 */
	public static void setEventManager(EventManager newEm) {
		em = newEm;
	}

	/**
	 * Stop the simulation
	 * 
	 * @throws Exception
	 *
	 */
	public static void stop() {

		if (dr != null)
			try {
				dr.stopAll(STOPCODE);
			} catch (Exception e) {
				handleException(e);
			}

	}

	/**
	 * Run the simulation without 'non-neccesary' people.
	 * 
	 * @throws Exception
	 * @throws ClassNotFoundException
	 * @throws IOException
	 * @throws IllegalAccessException
	 * @throws InstantiationException
	 * @throws DataConversionException
	 * 
	 */
	@SuppressWarnings(PN.UNCHECKED)
	private static void diseaseRun() throws ClassNotFoundException, DataConversionException, InstantiationException,

			IllegalAccessException, IOException {
		double maxfu = (Archimedes.askDouble(PN.FUEND) / Archimedes.askDouble(PN.FUFREQ));
		Idunn.setMaxFU(maxfu);
		// detection probability per state
		Function[][] det_prob = new Function[State.MAX_STATE][(int) maxfu + 1];

		for (int x = 0; x < State.MAX_STATE; x++) {
			for (int y = 0; y < maxfu + 1; y++) {// each follow up separately
				if (Archimedes.contains(PN.ACTIVEDETECTIONPROBABILITY + x + "_" + y, Archimedes.FUNCTION))
					det_prob[x][y] = Archimedes.ask(PN.ACTIVEDETECTIONPROBABILITY + x + "_" + y);
				// or all the same
				else if (Archimedes.contains(PN.ACTIVEDETECTIONPROBABILITY + x, Archimedes.FUNCTION))
					det_prob[x][y] = Archimedes.ask(PN.ACTIVEDETECTIONPROBABILITY + x);
				else
					det_prob[x][y] = new Constant();
			}
		}
		State.set(det_prob);
		// Set multiple runs
		dr = new DiseaseRun();

		// set Eventmanager
		dr.setEventmanager(em);

		// set default output paths
		FileRegister.setMainPath(FileRegister.getPath(PN.OUTPUT_DIR, FileRegister.OUT));
		FileRegister.setMainPath(FileRegister.getPath(PN.OUTPUT_DIR, FileRegister.OUT), FileRegister.OUT);

		// set the input for the run(s)
		dr.setInput();

		// set disease run if true
		diseaserun = true;

		// set progress
		if (!Gylfi.ask(PN.GUI))
			new Progress();

		// run
		dr.go();

		// set false
		diseaserun = false;

	}

	/**
	 * 
	 * @param e
	 * @return message
	 */
	private static String errorMessage(Exception e) {
		// create dialog message
		String message = e.toString() + PN.LINESEPARATOR;

		// Get the trace
		StackTraceElement[] ste = e.getStackTrace();
		// keep size some what within bounds
		int x = 0;

		for (StackTraceElement el : ste) {
			x++;
			message += PN.LINESEPARATOR + el.toString();
			if (x > 15) {
				message += PN.LINESEPARATOR + "......" + PN.LINESEPARATOR;
				message += ste[ste.length - 2] + PN.LINESEPARATOR;
				message += ste[ste.length - 1];
				break;
			}

		}
		return message;
	}

	/**
	 * In the case of a simulation exception, The current repeat/run is stopped
	 * and for all resulting time an error code -999 is filled in<br>
	 * The error is written to a log-file: RUNERROR.log.txt
	 * 
	 * @param e
	 * @throws ClassNotFoundException
	 * @throws IOException
	 */
	private static void handleSimulationException(Exception e) throws IOException {

		// stop current repeat and add error codes to this run
		try {
			if (dr != null)
				dr.stopRepeat(ERRORCODE);
		} catch (Exception new_e) {
			// create error log
			SimulationLog.speciallog(SimulationLog.ERRORLOG, errorMessage(new_e), true, true, true);
		}

		// create error log
		SimulationLog.speciallog(SimulationLog.ERRORLOG, errorMessage(e), true, true, true);

		// return
		return;

	}

	/**
	 * Read all information from this xml-input object and adds them to the
	 * appropriate places
	 *
	 * @param xml
	 * @throws IllegalAccessException
	 * @throws InstantiationException
	 * @throws DataConversionException
	 */
	@SuppressWarnings("unchecked")
	private static void readFile(XMLInput xml)
			throws DataConversionException, InstantiationException, IllegalAccessException {

		// FUNCTIONS
		Map<String, Function> funcs = xml.getFunctions();
		for (String s : funcs.keySet()) {
			Archimedes.tell(s, funcs.get(s));
		}
		// PARAMETERS
		Map<String, Double> params = xml.getParameters();
		for (String s : params.keySet()) {
			Archimedes.tell(s, params.get(s).doubleValue());
		}

		// METHODS or BOOLEANS
		Map<String, Boolean> booleans = xml.getBooleans();
		for (String s : booleans.keySet()) {
			Gylfi.tell(s, booleans.get(s));
		}

		// SEEDS
		Map<String, Long[]> seeds = xml.getSeeds();

		for (String s : seeds.keySet()) {

			Herbarium.addSeeds(s, seeds.get(s));

		}

		// FILES
		Map[] allfiles = xml.getFileNames();
		int io = 0;
		for (Map<String, String> m : allfiles) {
			for (String s : m.keySet()) {
				FileRegister.putFile(s, m.get(s), io);
			}
			io++;
		}

		// PATHS
		Map[] allpaths = xml.getPathNames();
		io = 0;
		for (Map<String, String> m : allpaths) {
			for (String s : m.keySet()) {
				FileRegister.putPath(s, m.get(s), io);
			}
			io++;
		}

		// OUTPUTS which values are temporarily stored in this class
		Main.output.putAll(xml.getOutputs());

	}

}
