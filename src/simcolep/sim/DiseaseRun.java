/*
 * This file is part of SIMCOLEP, a simulation model developed by the Department of Public Health,
 * Erasmus MC, University Medical Center Rotterdam, Rotterdam, The Netherlands.
 *
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/
 * or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA
 *
 * You are free to:
 * Share � copy and redistribute the material in any medium or format
 * Under the following terms:
 * Attribution � You must give appropriate credit, provide a link to the license, and indicate if changes were
 * made. You may do so in any reasonable manner, but not in any way that suggests the licensor endorses
 * you or your use.
 * NonCommercial � You may not use the material for commercial purposes.
 * NoDerivatives � If you remix, transform, or build upon the material, you may not distribute the modified material.
 */
/*
 * created 14-01-06
 */
package simcolep.sim;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.jdom.DataConversionException;

import nl.mgz.simulation.discreteevent.EventManager;
import nl.mgz.simulation.discreteevent.Target;
import simcolep.asgard.Freya;
import simcolep.asgard.Idunn;
import simcolep.asgard.Loki;
import simcolep.asgard.Odin;
import simcolep.asgard.Ymer;
import simcolep.demo.AbstractPopulationRegister;
import simcolep.demo.Cadastre;
import simcolep.demo.ILevelRegister;
import simcolep.demo.IPopulationRegister;
import simcolep.demo.Inhabitant;
import simcolep.demo.Level;
import simcolep.demo.Person;
import simcolep.demo.Sex;
import simcolep.fun2.Archimedes;
import simcolep.fun2.Pythagoras;
import simcolep.lepra.Clinic;
import simcolep.lepra.ClinicRegister;
import simcolep.lepra.Morbus;
import simcolep.lepra.State;
import simcolep.lepra.Strain;
import simcolep.sim.event.ChangeSeed;
import simcolep.sim.event.OpenDatabases;
import simcolep.sim.event.PressRecord;
import simcolep.tools.ClusterMeasurer;
import simcolep.tools.PN;
import simcolep.tools.TimeMeasurer;
import simcolep.tools.io.DataFormat;
import simcolep.tools.io.FileRegister;
import simcolep.tools.io.OutputList;
import simcolep.tools.io.OutputListFormat;
import simcolep.tools.io.OutputManager;
import simcolep.tools.io.SimulationLog;
import simcolep.tools.io.StringFormat;
import simcolep.tools.io.Table;
import simcolep.tools.io.TableFormat;
import simcolep.tools.message.Message;
import simcolep.tools.message.Notifyable;
import simcolep.tools.message.Ratatosk;

/**
 * This class does runs with disease. This can either be repeating the run x
 * times or for a number of parameter combination repeating it x times<br>
 *
 * This class is responsible for two things:<br>
 * 1. setting the right parameter value(s) for a certain run<br>
 * 2. saving and accumulating data of each set of repeats<br>
 *
 * Additional this class returns a false statement if all runs are completed
 * (method go())
 *
 * @author ir E.A.J. Fischer
 * @version 0.1.2 -- Last Modified 14-01-06
 *
 */
public final class DiseaseRun extends Target implements Notifyable {

	/** serializable */
	private static final long serialVersionUID = 1265738154028711544L;

	private static String[] listensTo = { Message.STOP };

	private static final String FRACINF_TEXT = "fractioninfected"; //$NON-NLS-1$

	/** text when saving */
	private static final String SAVINGTEXT = "Saving: "; //$NON-NLS-1$
	/** text for repeat */
	private static final String REPEATTEXT = "Repeat: "; //$NON-NLS-1$
	/** Initialization text */
	private static final String INIT_TEXT = "Initializing: "; //$NON-NLS-1$

	/** value for 'normal' stopping of output */
	private static final double STOP_CODE = 0;

	/** the event manager */
	private static EventManager em = null;

	/**
	 * safe run is taking more time but checks vital functions for debugging use
	 * true
	 */
	private static boolean safe = false;

	/* output codes */
	/* Main codes */
	/** Strain output */
	public static final String STRAIN_CODE = "S"; //$NON-NLS-1$

	/** Clinic demography */
	public static final String CLINIC_CODE = "C"; //$NON-NLS-1$

	/** Overall demography */
	public static final String POPULATION_CODE = "D"; //$NON-NLS-1$

	/** Leprosy statistics */
	public static final String LEPROSY_CODE = "L"; //$NON-NLS-1$

	/** Measure cluster statistics also age dependent */
	public static final String CLUSTER_CODE = "Q";

	/** Age distributions */
	public static final String AGE_CODE = "A";

	/* specifications */
	public static final String TURNOVER_CODE = "t"; //$NON-NLS-1$

	public static final String HOUSEHOLD_CODE = "h"; //$NON-NLS-1$

	public static final String DEMOGRAPHICS_CODE = "g"; //$NON-NLS-1$
	public static final String MOVEMENT_CODE = "m";
	public static final String PREVALENCE_CODE = "p";
	public static final String INCIDENCE_CODE = "i"; //$NON-NLS-1$

	// names of outputs
	/** Get (and thus set) these names of output for prevalence */
	public static String[] PREVALENCE_OUTPUT = { ClinicRegister.EXTINCTION, ClinicRegister.STATEPREV,
			ClinicRegister.GENEPREV, ClinicRegister.DIAGPREV, ClinicRegister.POPCOUNT, ClinicRegister.TYPEPREV };

	/** Get (and thus set) these names of output for incidence */
	public static String[] INCIDENCE_OUTPUT = { ClinicRegister.EXTINCTION, ClinicRegister.STATEINC,
			ClinicRegister.DIAGTOTALINC, ClinicRegister.PASTOTALINC, ClinicRegister.CONTACTTOTALINC,
			ClinicRegister.CONTACTTOTALEX, ClinicRegister.FUTOTALINC, ClinicRegister.FUTOTALEX,
			ClinicRegister.SURVEYTOTALINC, ClinicRegister.SURVEYTOTALEX, ClinicRegister.PRIMCASEHH,
			ClinicRegister.CHEMOTOTAL, ClinicRegister.POPCOUNT, ClinicRegister.PASDETINC, ClinicRegister.DIAGINC,
			ClinicRegister.CONTACTDETINC, ClinicRegister.CONTACTDETEX, ClinicRegister.RELATION_INC_MALE,
			ClinicRegister.RELATION_EX_MALE, ClinicRegister.RELATION_INC_FEMALE, ClinicRegister.RELATION_EX_FEMALE,
			ClinicRegister.FUDETINC, ClinicRegister.FUEX, ClinicRegister.SURVEYDETINC, ClinicRegister.SURVEYDETEX,
			ClinicRegister.CHEMO, ClinicRegister.TRANSINC, ClinicRegister.RELINC };
	private static final String SUM_CODE = "s";
	public static final String EXCEL_CODE = "x";
	public static final String AVERAGE_CODE = "a";

	public static final String STATE_CODE = "state";
	public static final String DET_CODE = "det";

	public static final String DIAG_CODE = "diag";

	public static final String TRANS_CODE = "trans";
	public static final String EXAME_CODE = "ex";
	public static final String TOTAL_CODE = "total";
	public static final String RELATION_CODE = "relation";
	public static final String PAJEKNETWORKOUTPUT = "n";
	private static final String MAX_CODE = "m";
	private static final String TIME_CODE = "time";
	private static final String SUSCEPTIBLE_CODE = "susceptible";
	private static final String INFECTION_CODE = "infection";
	private static final String TYPE_CODE = "type";
	private static final String GENE_CODE = "genes";

	private static final String CONTACTGROUP_CODE = "contactgroup";

	private static final String DATABASE = "GB";

	/**
	 * Safe mode for persons. If this returns true it will check after some
	 * methods
	 *
	 * @return safe
	 */
	public static boolean inSafeMode() {
		return safe;
	}

	/**
	 * Turns safe mode on and off
	 *
	 * @param b
	 */
	public static void setSafeMode(boolean b) {
		safe = b;
	}

	/** simulations are stopped */
	private boolean stopped = false;

	/** number of repeats to average over */
	private int repeat = 1;

	/** counter of the number of repeats */
	private int counter = 0;

	/** time to stop */
	private double stop = 0;

	/** which data to gather */
	private Map<Double, Set<String>> data = null;

	/** collect incidence (detection rate) data */
	private boolean collect_time_serie_data = false;

	/** started true if started */
	private boolean started = false;

	/**
	 * empty constructor
	 *
	 */
	public DiseaseRun() {
		super();

		for (String s : listensTo)
			Ratatosk.register(Message.formulate(s), this);

	}

	/**
	 * Record for the settings that are set with this time
	 *
	 *
	 * @throws IOException
	 *
	 */
	public void collect(double time) throws IOException {
		// Odin collects demographic stuff
		Odin.collectData(time);

		// Idunn collects disease stuff
		Idunn.collectData(time);

	}

	/**
	 * Return the current repeat
	 *
	 * @return repeat
	 */
	public int getRepeat() {

		return this.counter + 1;
	}

	/**
	 * Hey ho let's GO! <br>
	 * <i><quote> The Ramones </quote></i> <br>
	 * <br>
	 * Run the model now! <br>
	 * If stopped manually or by an Exception the stopped field is true and no
	 * data collection actions are taken by this method
	 *
	 * @throws IOException
	 * @throws FileNotFoundException
	 * @throws ClassNotFoundException
	 * @throws IllegalAccessException
	 * @throws InstantiationException
	 * @throws DataConversionException
	 */
	public void go() throws FileNotFoundException, IOException, ClassNotFoundException, DataConversionException,
			InstantiationException, IllegalAccessException {

		TimeMeasurer.lap(INIT_TEXT);

		for (; this.counter < this.repeat; this.counter++) {
			// reset the random number generators
			Herbarium.setRandom(Herbarium.getSeed(PN.POPSEED, this.counter), Herbarium.POP);

			Herbarium.setRandom(Herbarium.getSeed(PN.DISSEED, this.counter), Herbarium.DIS);

			Herbarium.setRandom(Herbarium.getSeed(PN.INTROSEED, this.counter), Herbarium.INTRO);

			// run
			this.simulate(REPEATTEXT + (this.counter + 1));

			if (!this.stopped)
				// collectdata
				this.collectData_EndOfRun();

			// record time of this 'lap'
			TimeMeasurer.lap(REPEATTEXT + (this.counter + 1));
		}

		if (!this.stopped) {// finalize output
			OutputManager.getManager().finishAll(Gylfi.ask(PN.OUTPUT_TO_ONE_FILE));
			// record time to save output
			TimeMeasurer.lap(SAVINGTEXT);
		}
	}

	/**
	 * @see simcolep.tools.message.Notifyable#notify(simcolep.tools.message.Message,
	 *      java.lang.Object, java.lang.Object)
	 */
	@Override
	public void notify(Message message, Object subject, Object source) throws Exception {
		if (message.equals(Message.STOP))
			this.stopRepeat(STOP_CODE);

	}

	/**
	 * Set the event manager of this simulation
	 *
	 * @param em2
	 */
	public void setEventmanager(EventManager em2) {
		em = em2;
	}

	/**
	 * Sets all neccesary input for a disease run It reads out the advanced
	 * cycling from the xml-element and sets if neccesary the hypercube
	 *
	 * @param advanced
	 * @param hc
	 * @throws DataConversionException
	 * @throws InstantiationException
	 * @throws IllegalAccessException
	 * @throws IOException
	 */

	public void setInput() throws DataConversionException, InstantiationException, IllegalAccessException, IOException {

		// stepsizes
		Freya.setStepsize(Archimedes.askDouble(PN.STEP));

		// set parameters
		// for run
		// the end of the run is slightly extended in case of output exactly at
		// the end of a run
		this.stop = Archimedes.askDouble(PN.RUNTIME) + Pythagoras.getDefaultPrecision();
		// number of repeats
		this.repeat = Archimedes.askInteger(PN.REPEATS);

		// set output
		this.setData(Main.output, this.repeat);
	}

	/**
	 * Stops the multiple runs and collects and writes data until the stopping
	 * point
	 *
	 * @param fill
	 *            values for output of last repeat from this point onwards
	 */
	public void stopAll(double fill) throws IOException, ClassNotFoundException {
		// stop current repeat
		this.stopRepeat(fill);

		// stop definitely
		this.stopped = true;

		// set counter
		this.counter = this.repeat + 1;

		// collect data
		this.collectData_EndOfRun();

		// finish the output manager
		OutputManager.getManager().finishAll(Gylfi.ask(PN.OUTPUT_TO_ONE_FILE));

		// return
		return;

	}

	/**
	 * Stops current repeat and fills the data collection with fill_codes. Than
	 * returns.
	 *
	 * @param fill
	 */
	public void stopRepeat(double fill) {
		if (!this.started)
			return;
		// already stopped
		if (stopped)
			return;

		this.stopped = true;

		// stop the event manager
		em.stop();

		/*
		 * start "saving" /*Odin
		 */
		Odin.getOdin().setSave(true);

		/* Idunn */
		Idunn.getIdunn().setSave(true);
		// get the record time that was not yet executed
		double recordtime = PressRecord.getCurrentRecordTime();
		while (recordtime != PressRecord.NOTIME) {

			// set all remaining to errors
			Idunn.getIdunn().collectError(recordtime, fill);
			Odin.getOdin().collectError(recordtime, fill);

			recordtime = PressRecord.getNextRecordTime();
		}
		// mute ratatosk to avoid 'dangling' messages
		Ratatosk.silence();

		// pretend that the simulation was 'normally' stopped
		this.stopped = false;
		// return
		return;
	}

	/**
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return this.getClass().getSimpleName() + PN.TAB + this.repeat;
	}

	/**
	 * Collect the data at the end of a repeat
	 *
	 *
	 * @throws IOException
	 *
	 */

	@SuppressWarnings("unchecked")
	private void collectData_EndOfRun() throws IOException {

		// collect incidence
		if (!this.collect_time_serie_data)
			return;

		// iterate the data to be collected
		for (String code : this.data.get(Double.POSITIVE_INFINITY)) {

			if (code.contains(DiseaseRun.STRAIN_CODE)) {

				/*
				 * String strain_output = Strain.outputForPajek();
				 */
				// write it to a file
				// create path
				File strain_output = new File(OutputManager.getManager().getHandler(code).getTempDir().getPath()
						+ File.separator + code + this.counter);
				// its a temporary file of course
				strain_output.deleteOnExit();

				FileWriter fw = new FileWriter(strain_output, true);

				Strain.outputForPajek(fw);
				fw.flush();
				fw.close();
				// use manager
				OutputManager.getManager().getHandler(code).collect(strain_output);

				// separate files
				if (code.contains(PAJEKNETWORKOUTPUT)) {
					SimulationLog.speciallog(code + (this.counter + 1), OutputManager.PAJEKNETWORKEXTENSION,
							strain_output, true, false, false);
					SimulationLog.closeSpecial(code + (this.counter + 1));
				}

			}

			// disease output
			if (code.contains(LEPROSY_CODE)) {
				// determine which output
				List<String> output_names = new ArrayList<String>();

				// incidence
				if (code.contains(INCIDENCE_CODE)) {
					for (String s : INCIDENCE_OUTPUT) {
						output_names.add(s);
					}

				}

				// prevalence
				if (code.contains(PREVALENCE_CODE)) {
					for (String s : PREVALENCE_OUTPUT) {
						output_names.add(s);
					}
				}

				// get clinic
				simcolep.lepra.Clinic clinic = Idunn.getIdunn().getClinic();

				// get register
				ClinicRegister register = clinic.getRegister();

				// gather information
				for (String output : output_names) {
					// the output lists from the clinic register
					OutputList[] out_lists = register.toOutputLists(output);
					// put them to the output handlers
					for (int i = 0; i < out_lists.length; i++) {

						OutputManager.getManager().getHandler(code + output + i).collect(out_lists[i]);

					}
				}
				if (code.contains(MAX_CODE)) {
					OutputManager.getManager().getHandler(code + MAX_CODE).collect(register.maxString());

					OutputManager.getManager().getHandler(code + MAX_CODE + TIME_CODE)
							.collect(register.maxtimeString());
				}
			}

			// population output
			else if (code.contains(POPULATION_CODE)) {

				// output population or cadastre to a temporary file
				// get Odin
				Odin odin = Odin.getOdin();

				if (code.contains(DEMOGRAPHICS_CODE)) {

					// get simulated population
					IPopulationRegister pop = odin.getPopulationRegister();

					// save the female data
					OutputManager.getManager().getHandler(code + Sex.FEMALE).collect(pop.toTable(Sex.FEMALE));

					// save the male data
					OutputManager.getManager().getHandler(code + Sex.MALE).collect(pop.toTable(Sex.MALE));

				}
				if (code.contains(HOUSEHOLD_CODE)) {
					// register
					ILevelRegister cad = odin.getLevelRegister();

					// save data
					OutputManager.getManager().getHandler(code + HOUSEHOLD_CODE).collect(cad.toTable());

				}
				if (code.contains(TURNOVER_CODE)) {
					// get simulated population
					IPopulationRegister pop = odin.getPopulationRegister();

					// add the table to the output manager
					OutputManager.getManager().getHandler(code + TURNOVER_CODE).collect(pop.movementTable());
				}
				if (code.contains(MOVEMENT_CODE)) {
					Loki loki = Odin.getOdin().getLoki();
					String[] outs = Loki.outputCodes();

					for (String out : outs) {
						OutputList<Integer>[] ol = loki.output(out);
						for (Sex sex : Sex.values()) {
							if (sex != Sex.BOTH) {
								OutputManager.getManager().getHandler(code + out + sex.toString())
										.collect(ol[sex.getValue()]);
							}
						}
					}

				}
				if (code.contains(DATABASE)) {
					Odin.getOdin().getPopulationRegister().toDatabase(Main.currentrun(),
							OutputManager.getManager().getDatabaseWriter(POPULATION_CODE));
				}

			}
			// demographic output of subjects in the clinic
			else if (code.contains(CLINIC_CODE)) {
				// get the subjects
				Collection<Morbus> subjects = Idunn.getIdunn().getClinic().getRegister().getInfected();

				// create output tables
				// household sizes
				Table hh_sizes = new Table(1, Cadastre.catsize);

				// a set with those household numbers that were already added
				Set[] hhs = new Set[Archimedes.askInteger(PN.L)];

				for (int i = 0; i < hhs.length; i++)
					hhs[i] = new LinkedHashSet<Level>();

				// agedistribution
				Table age_dist = new Table(3,
						(int) (AbstractPopulationRegister.ageclassmax / AbstractPopulationRegister.ageclasswidth));
				// set row names
				age_dist.setRowName(Sex.FEMALE.getValue(), Sex.FEMALE.toString());
				age_dist.setRowName(Sex.MALE.getValue(), Sex.MALE.toString());
				age_dist.setRowName(Sex.BOTH.getValue(), Sex.BOTH.toString());

				// columnnames
				String[] ageclassnames = AbstractPopulationRegister.getCategoryNames();
				age_dist.setColumnNames(ageclassnames);

				// turn over
				Table turnover = new Table(15,
						(int) (AbstractPopulationRegister.ageclassmax / AbstractPopulationRegister.ageclasswidth));
				turnover.setColumnNames(ageclassnames);

				// process data to
				for (Morbus m : subjects) {
					// owner
					Person p = m.getOwner();

					// only add one if the level was not counted yet
					if (!hhs[m.getLayer()].contains(p.getLevel())) {
						hhs[m.getLayer()].add(p.getLevel());
						// distribution of household sizes
						if (p.getLevel().getHabitationSize() >= hh_sizes.getSize()[1]) {
							hh_sizes.setColumnName(Cadastre.catsize - 1, ">" + (Cadastre.catsize - 1));
							hh_sizes.addtoCell(0, Cadastre.catsize - 1, 1.0);
						} else {
							hh_sizes.addtoCell(0, p.getLevel().getHabitationSize(), 1.0);
						}
					}
					// distribution of ages
					age_dist.addtoCell(p.getSex().getValue(), p.getAgeclass(), 1.0);
					age_dist.addtoCell(Sex.BOTH.getValue(), p.getAgeclass(), 1.0);
					// distribution of turn over
					if (p.life_time_level > turnover.getSize()[0]) {
						turnover.setRowName(14, ">14");
						turnover.addtoCell(14, p.getAgeclass(), 1.0);
					} else {
						turnover.addtoCell(p.life_time_level, p.getAgeclass(), 1.0);
					}
				}

				// process data for each unique household
				Table fracinf = new Table(Cadastre.catsize, Cadastre.catsize);

				// iterate the households
				for (int layer = 0; layer < hhs.length; layer++) {
					// iterate levels in these sets
					for (Level l : (Set<Level>) hhs[layer]) {
						int l_size = l.getHabitationSize();
						int inf = 0;
						for (Inhabitant p : l.getInhabitants()) {

							if (((Person) p).getMorbus(layer).getState() != State.SUSCEPTIBLE_STATE
									&& ((Person) p).getMorbus(layer).getState() < State.RECOVERED_STATE)
								inf++;
						}
						if (l_size >= fracinf.getSize()[0]) {
							l_size = fracinf.getSize()[0] - 1;
							inf = Math.min(l_size, inf);
							fracinf.setColumnName(l_size, " >" + l_size);
							fracinf.setRowName(l_size, " >" + l_size);
						}
						fracinf.addtoCell(l_size, inf, 1.0);
					}
				}

				// add to the outputmanager
				OutputManager.getManager().getHandler(code + HOUSEHOLD_CODE).collect(hh_sizes);
				OutputManager.getManager().getHandler(code + DEMOGRAPHICS_CODE).collect(age_dist);
				OutputManager.getManager().getHandler(code + TURNOVER_CODE).collect(turnover);
				OutputManager.getManager().getHandler(code + FRACINF_TEXT).collect(fracinf);
			} else if (code.contains(CLUSTER_CODE)) {
				OutputManager.getManager().getHandler(code + CONTACTGROUP_CODE)
						.collect(Idunn.getIdunn().getClinic().getRegister().outputContractTracing());

				ClusterMeasurer cm = new ClusterMeasurer(Odin.getOdin().getLevelRegister());

				OutputManager.getManager().getHandler(code + SUSCEPTIBLE_CODE).collect(cm.susceptibility());

				OutputManager.getManager().getHandler(code + INFECTION_CODE).collect(cm.infection());
				OutputManager.getManager().getHandler(code + TYPE_CODE).collect(cm.type());

				for (int i = 0; i < Idunn.i_Model.genes(); i++) {
					OutputManager.getManager().getHandler(code + GENE_CODE + "_" + i)
							.collect(Idunn.i_Model.inheritancetable(i));
				}
			} else if (code.contains(AGE_CODE)) {
				// age distributions

				// the output lists from the clinic register
				ClinicRegister register = Idunn.getIdunn().getClinic().getRegister();
				OutputList[] out_lists_m = register.toOutputLists(ClinicRegister.AGEINCMALETOTAL);

				// put them to the output handlers
				for (int i = 0; i < out_lists_m.length; i++) {
					OutputList ol = out_lists_m[i];

					OutputManager.getManager().getHandler(code + ClinicRegister.AGEINCMALETOTAL + i).collect(ol);

				}

				OutputList[] out_lists_f = register.toOutputLists(ClinicRegister.AGEINCFEMALETOTAL);
				// put them to the output handlers
				for (int i = 0; i < out_lists_f.length; i++) {
					OutputList ol = out_lists_f[i];

					OutputManager.getManager().getHandler(code + ClinicRegister.AGEINCFEMALETOTAL + i).collect(ol);

				}

				out_lists_m = register.toOutputLists(ClinicRegister.AGEINCMALECON);

				// put them to the output handlers
				for (int i = 0; i < out_lists_m.length; i++) {
					OutputList ol = out_lists_m[i];

					OutputManager.getManager().getHandler(code + ClinicRegister.AGEINCMALECON + i).collect(ol);

				}

				out_lists_f = register.toOutputLists(ClinicRegister.AGEINCFEMALECON);
				// put them to the output handlers
				for (int i = 0; i < out_lists_f.length; i++) {
					OutputList ol = out_lists_f[i];

					OutputManager.getManager().getHandler(code + ClinicRegister.AGEINCFEMALECON + i).collect(ol);

				}
				out_lists_m = register.toOutputLists(ClinicRegister.AGEINCMALEEX);

				// put them to the output handlers
				for (int i = 0; i < out_lists_m.length; i++) {
					OutputList ol = out_lists_m[i];

					OutputManager.getManager().getHandler(code + ClinicRegister.AGEINCMALEEX + i).collect(ol);

				}

				out_lists_f = register.toOutputLists(ClinicRegister.AGEINCFEMALEEX);
				// put them to the output handlers
				for (int i = 0; i < out_lists_f.length; i++) {
					OutputList ol = out_lists_f[i];

					OutputManager.getManager().getHandler(code + ClinicRegister.AGEINCFEMALEEX + i).collect(ol);

				}
			}
		}

	}

	/**
	 * Set the types of data that should be saved and optional the times during
	 * simulation when these should be obtained. <br>
	 * <b>Output coding system</b>:<br>
	 * <br>
	 * Type of outputs
	 * <UL>
	 * <li>L: disease output
	 * <ul>
	 * <li>i: incidence data
	 * <li>p: prevalence data
	 * <li>r: requested prevalence at specific times (should be given)
	 * </ul>
	 * <li>D: demographic output
	 * <ul>
	 * <li>h: household data
	 * <li>g: demographic data
	 * <li>t: turnover data
	 * </ul>
	 * <li>M: demgographic output of those that are registered in the clinic (no
	 * sub types will always return those given above)
	 *
	 * Format of output
	 * <ul>
	 * <li>a: average
	 * <li>x: keep number of columns limited (better compatability with eXcel)
	 * </ul>
	 *
	 * @param data_codes
	 *            Contains codes for output as key for a list of times at which
	 *            these should be collected for those outputs where the option
	 *            is available
	 * @param repeats
	 *            number of repeats
	 * @throws IOException
	 */
	private void setData(Map<String, List<Double>> data_codes, int repeats) throws IOException {

		// map containing data
		this.data = new LinkedHashMap<Double, Set<String>>();

		// determine the number of states, disease types and diagnosis types
		int states = State.MAX_STATE;

		int distypes = Archimedes.askInteger(PN.DISEASETYPES);

		int diagnoses = Clinic.DIAGNOSIS;

		// check whether the times are not too long otherwise warn
		for (String code : data_codes.keySet()) {
			// average coded by an 'a'
			int outmode = DataFormat.COMBINE;
			if (code.contains(AVERAGE_CODE))
				outmode = DataFormat.AVERAGE;
			else if (code.contains(SUM_CODE))
				outmode = DataFormat.SUM;

			// excell compatability coded by an 'X'
			boolean excell = code.contains(EXCEL_CODE);

			// table format
			TableFormat tableFormat = new TableFormat();
			if (excell)
				tableFormat.cAlignment = TableFormat.Format.VERTICAL;
			else
				tableFormat.cAlignment = TableFormat.Format.HORIZONTAL;

			if (!this.data.containsKey(Double.POSITIVE_INFINITY))
				this.data.put(Double.POSITIVE_INFINITY, new LinkedHashSet<String>());
			// add code to the hashset
			this.data.get(Double.POSITIVE_INFINITY).add(code);

			// leprosy disease coded by a 'L'
			if (code.contains(LEPROSY_CODE)) {
				if (code.contains(DATABASE)) {
					OutputManager.getManager().addDatabasewriter(LEPROSY_CODE);
				}
				// incidence
				if (code.contains(INCIDENCE_CODE)) {
					String[] names = INCIDENCE_OUTPUT;

					// set true
					this.collect_time_serie_data = true;

					// set
					for (int y = 0; y < names.length; y++) {
						// number of outputs
						int num = 0;
						if (names[y].contains(TOTAL_CODE) || names[y].contains(ClinicRegister.EXTINCTION)) {
							num = 1;
						} else if (names[y].contains(STATE_CODE) || names[y].contains(DET_CODE)
								|| names[y].contains(EXAME_CODE))
							// four types of diagnosis
							num = states;
						else if (names[y].contains(DIAG_CODE))
							// as many as the possible states
							num = diagnoses;
						else if (names[y].contains(TRANS_CODE))
							num = Math.max(Archimedes.askInteger(PN.LEVELDEPTH), 2);
						else if (names[y].equals(ClinicRegister.POPCOUNT))
							num = 1;
						else if (names[y].contains(RELATION_CODE))
							num = Person.RELATIONS;
						else if (names[y].contains(ClinicRegister.PRIMCASEHH)) {
							num = 25;
						} else
							num = distypes;
						// set OutputHandler(s)
						for (int x = 0; x < num; x++) {
							String path = FileRegister.getPath(FileRegister.OUT) + File.separator;
							String file = code + names[y] + x + OutputManager.DEFAULT_EXTENSION;

							// Outputmanager creates new handler
							OutputManager.getManager().newHandler(code + names[y] + x, path, file, outmode,
									new OutputListFormat(excell), repeats);

						}
					}

				}
				// prevalence coded by a 'p'
				if (code.contains(PREVALENCE_CODE)) {

					String[] names = PREVALENCE_OUTPUT;

					// collect information for time series
					this.collect_time_serie_data = true;

					// set
					for (int y = 0; y < names.length; y++) {
						// number of categories
						int num = 5;
						if (names[y].contains(DIAG_CODE))
							num = diagnoses;
						else if (names[y].contains(STATE_CODE))
							num = states;
						else if (names[y].equals(ClinicRegister.POPCOUNT)
								|| names[y].contains(ClinicRegister.EXTINCTION))
							num = 1;
						else if (names[y].equals(ClinicRegister.GENEPREV)) {
							num = 0;
							int genes = Archimedes.askInteger(PN.NUMOFGENES);
							for (int i = 0; i < genes; i++)
								num += Archimedes.askInteger(PN.NUMOFALLELES + i);
						} else if (names[y].equals(ClinicRegister.TYPEPREV)) {
							num = Morbus.DISEASE_TYPES;
						}

						// set OutputHandler(s)
						for (int x = 0; x < num; x++) {

							String path = FileRegister.getPath(PN.OUTPUT_DIR, FileRegister.OUT);

							String file = File.separator + code + names[y] + x + OutputManager.DEFAULT_EXTENSION;

							// Outputmanager creates new
							OutputManager.getManager().newHandler(code + names[y] + x, path, file, outmode,
									new OutputListFormat(excell), repeats);

						}
					}

				}
				if (code.contains(MAX_CODE)) {
					String path = FileRegister.getPath(PN.OUTPUT_DIR, FileRegister.OUT);

					String file = File.separator + code + MAX_CODE + OutputManager.DEFAULT_EXTENSION;

					// Outputmanager creates new
					OutputManager.getManager().newHandler(code + MAX_CODE, path, file, DataFormat.COMBINE,
							new StringFormat(true), repeats);

					String timefile = File.separator + code + MAX_CODE + TIME_CODE + OutputManager.DEFAULT_EXTENSION;

					// Outputmanager creates new
					OutputManager.getManager().newHandler(code + MAX_CODE + TIME_CODE, path, timefile,
							DataFormat.COMBINE, new StringFormat(true), repeats);
				}
				// POPULATION DATA coded by a D(emographic)
			} else if (code.contains(POPULATION_CODE)) {
				if (code.contains(DATABASE)) {
					OutputManager.getManager().addDatabasewriter(POPULATION_CODE);
				}
				// set true to get the data later here
				this.collect_time_serie_data = true;

				// demographics (g of demoGraphics)
				if (code.contains(DEMOGRAPHICS_CODE)) {
					Odin.setDataPopRegister(true);

					String path = FileRegister.getPath(PN.OUTPUT_DIR, FileRegister.OUT);

					// create a file for males
					String male_file =

							File.separator + code + Sex.MALE.toString() + OutputManager.DEFAULT_EXTENSION;

					// Outputmanager for males
					OutputManager.getManager().newHandler(code + Sex.MALE.toString(), path, male_file, outmode,
							tableFormat, repeats);

					// create a file for females
					String female_file = File.separator + code + Sex.FEMALE.toString()
							+ OutputManager.DEFAULT_EXTENSION;

					// Outputmanager for males
					OutputManager.getManager().newHandler(code + Sex.FEMALE.toString(), path, female_file, outmode,
							tableFormat, repeats);

				}
				// household size information
				if (code.contains(HOUSEHOLD_CODE)) {
					Odin.setDataLevelRegister(true);

					String path = FileRegister.getPath(PN.OUTPUT_DIR, FileRegister.OUT);

					// create a file for household sizes
					String hh_file = File.separator + code + HOUSEHOLD_CODE + OutputManager.DEFAULT_EXTENSION;

					// Outputmanager for males
					OutputManager.getManager().newHandler(code + HOUSEHOLD_CODE, path, hh_file, outmode, tableFormat,
							repeats);

				}
				// turnover data = number of household a person lived in
				if (code.contains(TURNOVER_CODE)) {

					String path = FileRegister.getPath(PN.OUTPUT_DIR, FileRegister.OUT);

					// create a file for males
					String turnover_file = File.separator + code + TURNOVER_CODE + OutputManager.DEFAULT_EXTENSION;

					// Outputmanager for males
					OutputManager.getManager().newHandler(code + TURNOVER_CODE, path, turnover_file, outmode,
							tableFormat, repeats);
				}
				// movement output
				if (code.contains(MOVEMENT_CODE)) {
					String[] outs = Loki.outputCodes();

					// set OutputHandler(s)
					for (String out : outs) {
						for (Sex sex : Sex.values()) {

							String path = FileRegister.getPath(PN.OUTPUT_DIR, FileRegister.OUT);

							String file =

									File.separator + code + out + sex.toString() + OutputManager.DEFAULT_EXTENSION;

							// Outputmanager creates new
							OutputManager.getManager().newHandler(code + out + sex.toString(), path, file, outmode,
									new OutputListFormat(excell), repeats);

						}
					}

				}

			} // Demographics of infected coded by M
			else if (code.contains(CLINIC_CODE)) {

				String path = FileRegister.getPath(PN.OUTPUT_DIR, FileRegister.OUT);

				// add to the outputmanager
				// create household handler
				String hh_file = File.separator + code + HOUSEHOLD_CODE + OutputManager.DEFAULT_EXTENSION;

				OutputManager.getManager().newHandler(code + HOUSEHOLD_CODE, path, hh_file, outmode, tableFormat,
						repeats);

				// create demographic handler
				String age_file =

						File.separator + code + DEMOGRAPHICS_CODE + OutputManager.DEFAULT_EXTENSION;
				OutputManager.getManager().newHandler(code + DEMOGRAPHICS_CODE, path, age_file, outmode, tableFormat,
						repeats);
				// turn over handler
				String turnover_file = File.separator + code + TURNOVER_CODE + OutputManager.DEFAULT_EXTENSION;

				OutputManager.getManager().newHandler(code + TURNOVER_CODE, path, turnover_file, outmode, tableFormat,
						repeats);

				// fraction infected per home
				String fracinf_file =

						File.separator + code + FRACINF_TEXT + OutputManager.DEFAULT_EXTENSION;
				OutputManager.getManager().newHandler(code + FRACINF_TEXT, path, fracinf_file, outmode, tableFormat,
						repeats);

			} else if (code.contains(DiseaseRun.STRAIN_CODE)) {
				// turn linking of Strain-elements on for output
				Strain.keepLink(true);
				// get path
				String path = FileRegister.getPath(PN.OUTPUT_DIR, FileRegister.OUT);

				// for strains
				String strain_file =

						File.separator + code + OutputManager.DEFAULT_EXTENSION;
				OutputManager.getManager().newHandler(code, path, strain_file, DataFormat.COMBINE,
						new StringFormat(false), repeats);

			} else if (code.contains(CLUSTER_CODE)) {

				String path = FileRegister.getPath(PN.OUTPUT_DIR, FileRegister.OUT);

				// add to the outputmanager
				// createcontact group handler
				String cg_file = File.separator + code + CONTACTGROUP_CODE + OutputManager.DEFAULT_EXTENSION;

				OutputManager.getManager().newHandler(code + CONTACTGROUP_CODE, path, cg_file, outmode, tableFormat,
						repeats);

				// create susceptible handler
				String sus_file = (File.separator + code + SUSCEPTIBLE_CODE + OutputManager.DEFAULT_EXTENSION);

				OutputManager.getManager().newHandler(code + SUSCEPTIBLE_CODE, path, sus_file, outmode, tableFormat,
						repeats);
				// add to the outputmanager
				// create infection handler
				String inf_file = (File.separator + code + INFECTION_CODE + OutputManager.DEFAULT_EXTENSION);

				OutputManager.getManager().newHandler(code + INFECTION_CODE, path, inf_file, outmode, tableFormat,
						repeats);
				// create infection handler
				String type_file = (File.separator + code + TYPE_CODE + OutputManager.DEFAULT_EXTENSION);

				OutputManager.getManager().newHandler(code + TYPE_CODE, path, type_file, outmode, tableFormat, repeats);
				// genes
				for (int i = 0; i < Archimedes.askDouble(PN.NUMOFGENES); i++) {
					String gene_file = (File.separator + code + GENE_CODE + "_" + i + OutputManager.DEFAULT_EXTENSION);

					OutputManager.getManager().newHandler(code + GENE_CODE + "_" + i, path, gene_file, outmode,
							tableFormat, repeats);
				}
			} else if (code.contains(AGE_CODE)) {
				String path = FileRegister.getPath(PN.OUTPUT_DIR, FileRegister.OUT);
				// age
				OutputListFormat lf = new OutputListFormat(excell);
				int ageclasses = (int) Math
						.round(Archimedes.askDouble(PN.AGECLASSMAX) / Archimedes.askDouble(PN.AGECLASSWIDTH));

				for (int i = 0; i < ageclasses; i++) {
					String age_file_m_tot = (File.separator + code + ClinicRegister.AGEINCMALETOTAL + i
							+ OutputManager.DEFAULT_EXTENSION);

					OutputManager.getManager().newHandler(code + ClinicRegister.AGEINCMALETOTAL + i, path,
							age_file_m_tot, outmode, lf, repeats);
					String age_file_f_tot = (File.separator + code + ClinicRegister.AGEINCFEMALETOTAL + i
							+ OutputManager.DEFAULT_EXTENSION);

					OutputManager.getManager().newHandler(code + ClinicRegister.AGEINCFEMALETOTAL + i, path,
							age_file_f_tot, outmode, lf, repeats);
					String age_file_m_con = (File.separator + code + ClinicRegister.AGEINCMALECON + i
							+ OutputManager.DEFAULT_EXTENSION);

					OutputManager.getManager().newHandler(code + ClinicRegister.AGEINCMALECON + i, path, age_file_m_con,
							outmode, lf, repeats);
					String age_file_f_con = (File.separator + code + ClinicRegister.AGEINCFEMALECON + i
							+ OutputManager.DEFAULT_EXTENSION);

					OutputManager.getManager().newHandler(code + ClinicRegister.AGEINCFEMALECON + i, path,
							age_file_f_con, outmode, lf, repeats);
					String age_file_m_ex = (File.separator + code + ClinicRegister.AGEINCMALEEX + i
							+ OutputManager.DEFAULT_EXTENSION);

					OutputManager.getManager().newHandler(code + ClinicRegister.AGEINCMALEEX + i, path, age_file_m_ex,
							outmode, lf, repeats);
					String age_file_f_ex = (File.separator + code + ClinicRegister.AGEINCFEMALEEX + i
							+ OutputManager.DEFAULT_EXTENSION);

					OutputManager.getManager().newHandler(code + ClinicRegister.AGEINCFEMALEEX + i, path, age_file_f_ex,
							outmode, lf, repeats);

				}
			}
		}

	}

	/**
	 * Runs one simulation
	 *
	 * @param cr
	 * @param information
	 *            for the launching of the run
	 * @throws IllegalAccessException
	 * @throws InstantiationException
	 * @throws DataConversionException
	 *
	 * @throws Exception
	 *
	 */

	private void simulate(Object launchInfo)
			throws DataConversionException, InstantiationException, IllegalAccessException

	{
		// no more simulating when stopped
		if (stopped)
			return;

		// reset Ratatosk
		Ratatosk.reset();
		// register
		Ratatosk.register(Message.formulate(Message.STOP), this);

		// reset Ymer (i.e. the id of people)
		Ymer.reset();

		// (re)initialize the event manager
		em.initialize();

		// set record interval
		PressRecord.setRecordTimes(Archimedes.askDouble(PN.RECORDSTART), Archimedes.askDouble(PN.RECORDINTERVAL),
				this.stop);
		// press record
		PressRecord pr = new PressRecord(this, this, Math.max(0.0, PressRecord.getNextRecordTime()));

		// schedule
		em.schedule(pr);

		// opening of databases
		OpenDatabases od = new OpenDatabases(this, this, Archimedes.askDouble(PN.OPENDATABASE));

		em.schedule(od);

		// initialise Idunn
		Idunn.setEventManager(em);
		Idunn.init();
		Idunn.initImport();
		// initialise Odin
		Odin.setEventManager(em);
		Odin.init();

		// schedule a stop event
		Main.scheduleStop(this, launchInfo, this.stop);

		// schedule seed change
		if (Gylfi.ask(PN.SEEDCHANGE)) {
			ChangeSeed cs = new ChangeSeed(this, this, Archimedes.askDouble(PN.SEEDCHANGETIME));
			cs.newSeeds(Herbarium.getSeedLong(PN.POPSEED2, repeat - 1), Herbarium.getSeedLong(PN.DISSEED2, repeat - 1));

			em.schedule(cs);

		}

		// start the event manager
		try {

			this.started = true;

			em.start();

			this.started = false;
		} catch (Exception e) {
			Main.handleException(e);
		}

		// return
		return;

	}

}
