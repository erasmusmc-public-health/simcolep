/*
 * This file is part of SIMCOLEP, a simulation model developed by the Department of Public Health, 
 * Erasmus MC, University Medical Center Rotterdam, Rotterdam, The Netherlands.
 *
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/ 
 * or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA
 *
 * You are free to:
 * Share � copy and redistribute the material in any medium or format
 * Under the following terms:
 * Attribution � You must give appropriate credit, provide a link to the license, and indicate if changes were 
 * made. You may do so in any reasonable manner, but not in any way that suggests the licensor endorses
 * you or your use.
 * NonCommercial � You may not use the material for commercial purposes.
 * NoDerivatives � If you remix, transform, or build upon the material, you may not distribute the modified material.
 */
package simcolep.sim;

import simcolep.tools.message.Eavesdropper;
import simcolep.tools.message.Message;
import simcolep.tools.message.Ratatosk;

/**
 * @author 37384efi
 *
 */
public class Progress implements Eavesdropper {

	private static final int STEP = 1000;
	private static final int BREAK = 80000;
	private static final String BARSIGN = ".";
	private int counter = 0;

	/**
	 * 
	 */
	public Progress()
	{
		//register
		Ratatosk.register(this);
		
	}
	/**
	 * @see simcolep.tools.message.Eavesdropper#listenIn(simcolep.tools.message.Message)
	 */
	public void listenIn(Message message) {
		//SET THE PROGRESS BAR TO THE RIGHT LENGTH OF THE RUN
		if(message.getMessage().contains(Message.LAUNCHINFO))
		{							
			//first statement between { and } is the length of the run
		
			System.out.println();
			System.out.println(message.getMessage());
			
			return;
		}
		
		
		//count the number of events passed by
		counter++;
		
		if(counter%STEP==0) 
			{
				System.out.print(BARSIGN);
			}
		if(counter%BREAK ==0)
		{
			System.out.println();
		}
		
	}
}
