/*
 * This file is part of SIMCOLEP, a simulation model developed by the Department of Public Health,
 * Erasmus MC, University Medical Center Rotterdam, Rotterdam, The Netherlands.
 *
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/
 * or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA
 *
 * You are free to:
 * Share � copy and redistribute the material in any medium or format
 * Under the following terms:
 * Attribution � You must give appropriate credit, provide a link to the license, and indicate if changes were
 * made. You may do so in any reasonable manner, but not in any way that suggests the licensor endorses
 * you or your use.
 * NonCommercial � You may not use the material for commercial purposes.
 * NoDerivatives � If you remix, transform, or build upon the material, you may not distribute the modified material.
 */
/**
 * created: 23-Aug-2006
 */
package simcolep.sim;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Random;
import java.util.Set;

import simcolep.exceptions.SimulationException;
import simcolep.tools.PN;
import simcolep.tools.io.SimulationLog;

/**
 * Class contains both the random number generators as the seeds of these random
 * number generators
 * 
 * @author ir. E.A.J. Fischer
 * @version 1.0
 */
public final class Herbarium {

	/** population code */
	public static final int POP = 0;
	/** disease code */
	public static final int DIS = 1;
	/** disease introduction code */
	public static final int INTRO = 2;

	/** number of possible random number generators */
	private static final int NUM_OF_RAND_GEN = 4;

	/***/
	private static final String HEADERTEXT = "********HERBARIUM*********";
	private static final String REPLACESEEDMESSAGE = "Herbarium already has " + "this seed in the collection "
			+ PN.LINESEPARATOR + "consider using replaceSeed";

	/** random number generator of demography excluding drawing from file */
	private static Random[] rand = new Random[NUM_OF_RAND_GEN];

	/** SeedBank containing settings */
	private static Map<String, Long[]> SeedBank = null;

	/**
	 * Herbarium is initialized for use. Initialization needs number of repeats
	 */
	private static boolean initialized = false;

	/**
	 * Add a method to the brain
	 * 
	 * @param s
	 * @param l
	 */
	public static void addSeeds(String s, Long[] l) {
		// if neccesary create brain
		if (SeedBank == null)
			Herbarium.set(s, l);

		// check if this method was not already in the brain
		else if (SeedBank.containsKey(s))
			throw new SimulationException(REPLACESEEDMESSAGE);
		else// put
			Herbarium.set(s, l);

	}

	/**
	 * Set new random number generators with new seeds for this repeat
	 *
	 * @param population
	 * @param disease
	 */
	public static void changeSeeds(Long population, Long disease) {
		if (population != null)
			Herbarium.setRandom(population.longValue(), POP);
		if (disease != null)
			Herbarium.setRandom(disease.longValue(), DIS);
	}

	/**
	 * ask a certain seed coded
	 * 
	 * @param s
	 * @param repeat
	 *            - for this run
	 */
	public static long getSeed(String s, int repeat) {

		// or get it from the seedbank
		// if the seed bank is null
		if (SeedBank == null)
			throw new SimulationException("Herbarium has no seedbank");
		// if the brain does not contain the method
		if (!SeedBank.containsKey(s))
			throw new SimulationException("no such seed: " + s);

		// first initialize the Herbarium
		if (initialized)
			// get the method
			return SeedBank.get(s)[repeat].longValue();
		else
			throw new SimulationException(SimulationException.NOTINITIALIZED);

	}

	/**
	 * Get a seeds as a Long or null if not present
	 * 
	 * @param s
	 * @param repeat
	 * @return new Long (getSeed(s,repeat))
	 */
	public static Long getSeedLong(String s, int repeat) {
		if (!initialized)
			throw new SimulationException(SimulationException.NOTINITIALIZED);

		if (!SeedBank.containsKey(s))
			return null;

		return new Long(getSeed(s, repeat));

	}

	/**
	 * Initialize Herbarium
	 *
	 * @param n
	 */
	public static void initHerbarium(int n, boolean timeasseed) {
		if (SeedBank == null)
			throw new SimulationException("add at least names of seeds");

		Set<String> keySet = SeedBank.keySet();

		if (timeasseed) { // use current time and seed
			for (String s : keySet) {
				Herbarium.set(s, createSeeds(null, n));
			}
		} else {
			// determine if we have enough seeds
			for (String s : keySet) {
				// if smaller create some more
				// using the first entry as a seed for the sawyer
				if (SeedBank.get(s).length < n) {
					Long[] seeds = SeedBank.get(s);

					if (seeds.length < 1)
						throw new SimulationException("at least specify one seed! or the clock");

					Long[] additionalseeds = createSeeds(seeds[0], n - seeds.length);

					Long[] newSeeds = new Long[n];
					// combine
					for (int i = 0; i < n; i++) {
						if (i < seeds.length)
							newSeeds[i] = seeds[i];
						else
							newSeeds[i] = additionalseeds[i - seeds.length];
					}
					// replace the seed
					set(s, newSeeds);

					// if the seeds had a length of more than one create a log
					if (seeds.length > 1)
						SimulationLog.log("For " + s + " additional seeds were created using " + seeds[0]
								+ " as seed to determine extra seeds." + PN.LINESEPARATOR
								+ "new seeds were created for runs " + seeds.length + " until " + n);
				}
				// else do nothing as there are enough seeds anyways
			}
		}
		// set initialized is true
		Herbarium.initialized = true;

	}

	/**
	 * Return a random number generator coded by i -integer
	 *
	 * @param i
	 * @return rand
	 */
	public static Random rand(int i) {
		return rand[i];
	}

	/**
	 * This method always gets the seed into the brain
	 * 
	 * @param s
	 * @param l
	 */
	public static void replaceSeed(String s, long l, int repeat) {
		// don't ask questions just put it into the brain.
		Herbarium.set(s, l, repeat);
	}

	/**
	 * Reset all seeds
	 *
	 */
	public static void resetHerbarium() {
		SeedBank = null;
		initialized = false;
	}

	/**
	 * Create a string containing the seeds
	 */
	public static String SeedBanktoString() {
		String seeds = HEADERTEXT + PN.LINESEPARATOR;

		for (String s : SeedBank.keySet()) {
			seeds += s + PN.TAB;

			for (Long l : SeedBank.get(s))
				seeds += l.toString() + PN.TAB;

			seeds += PN.LINESEPARATOR;
		}
		return seeds;
	}

	/**
	 * Creates a random number generators
	 * 
	 * @param i
	 *            random number generator for this purpose
	 * @param seed
	 */
	public static void setRandom(long seed, int i) {
		rand[i] = new Random(seed);

	}

	/**
	 * Create seeds based upon a seed (l) for a type of proces (s) for certain
	 * (n) times When the seed (l) is null, the current time is used as the
	 * first seed.
	 *
	 *
	 * @param l
	 * @param n
	 *
	 * @return seed
	 */
	private static Long[] createSeeds(Long l, int n) {

		// the basic seed
		long seed;

		if (l == null)
			seed = System.currentTimeMillis();
		else
			seed = l.longValue();

		// the array for the repeats
		Long[] seeds = new Long[n];

		// random seeds
		Random sawyer = new Random(seed);

		for (int x = 0; x < n; x++) {

			// next seed is random number with intial seed as seed
			// seeds[x] = new Long(sawyer.nextLong());

			// next seed is previous seed + 1
			seeds[x] = seed + 1 + x;
		}

		// set to the seedbank
		return seeds;

	}

	/**
	 * create a seed bank for random number generators
	 *
	 */
	private static void makeBank() {
		SeedBank = new LinkedHashMap<String, Long[]>();
	}

	/**
	 * Set this long as a seed for this s-run and for this repeat
	 * 
	 * @param s
	 * @param l
	 * @param repeat
	 */
	private static void set(String s, Long l, int repeat) {
		if (SeedBank == null)
			makeBank();

		Long[] seed = new Long[repeat + 1];

		if (SeedBank.get(s) != null) {
			seed = SeedBank.get(s);

			if (seed.length < repeat) {
				Long[] rep_seed = new Long[repeat + 1];

				for (int x = 0; x < rep_seed.length; x++)
					rep_seed[x] = seed[x];

				seed = rep_seed;
			}

			seed[repeat] = l;

		}
		SeedBank.put(s, seed);
	}

	/**
	 * Set this array for this type
	 *
	 * @param s
	 * @param seeds
	 */
	private static void set(String s, Long[] seeds) {
		if (SeedBank == null)
			makeBank();

		SeedBank.put(s, seeds);

	}

}
